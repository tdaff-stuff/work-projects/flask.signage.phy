# Digital Signage

**`signage.phy`** is a content delivery system for Digital Signage screens.

**`flask.signage.phy`** is the server component delivering fullscreen HTML
content to be displayed by a browser, or browser based client, on digital
signage screens. Each slide can be configured with a number of different
layouts:

* Image or slide background (from uploaded image or `.pdf`)
* Text overlay
* Calendar of events (from `.ics`/talks.cam feed)
* Lecture times (fed through meeting room booking system)

The signage running in the Cavendish Laboratory can be previewed at
[signage.phy.cam.ac.uk](https://signage.phy.cam.ac.uk/). Refreshing the page
will move to the next slide in the default rotation currently being displayed
on screens around the Department of Physics.

# Signage Clients

Any browser can be used as a client device, just point it at the URL and
the configured set of slides will be displayed in turn. For a much smoother
experience, we have developed some kiosk software for Raspberry Pi devices,
[kiosk.phy](https://codeshare.phy.cam.ac.uk/itservices.phy/kiosk.phy), which
will boot into a self maintaining client with smooth transitions.

# Editing

Navigating to `/edit` will give the interface for creating and managing
slides.

# Installation

The signage server is a Python/Flask/SQLAlchemy application that uses a
simple database and some on-disk storage of assets which will be mostly
self contained under the default configuration.

We deploy servers with an Ansible playbook, which ensures all the required
packages are installed and that the system is configured correctly.

Alternatively the package can be installed manually.

## Ansible deployment

The Ansible playbook to deploy to a host is located in `deploy/ansible/`.
It is designed to install the software in an Apache `virtualhost` with all
the data contained within a directory on the server. Since the playbook
will manage some of the system configuration, it is recommended to install
on a server that has a fairly vanilla configuration.

Configuration of the hosts is achieved by creating an inventory file with
a few variables to get the server initialised.

```yaml
default:
  hosts:
    host.name.cam.ac.uk:
  vars:
    host_cnames:
      - another.url.cam.ac.uk
    secret_key: <any random string>
    admins:
      - abc12
      - xyz99
    enable_ssl: certbot
    certbot_registration_email: it.support@department.cam.ac.uk
```

Required `vars`:
* `host` - The primary address used to access the server for this instance.
  If you have multiple vhosts on a web-server ensure that this is set to
  the hostname that you wish to use to access the signage, not the FQDN of the
  hosting server.
* `secret_key` - Any random string, which is used to maintain cookies that
  keep track of which slide to send to each client.
* `admins` - CRSid of users to initialise as admins. Further admins can be
  added through the interface, but at least one must be set in the system
  initially.
* `enable_ssl`
  - `false` disables ssl;
  - `selfsigned` generates a self-signed certificate for testing;
  - `files` will install `{{ ansible_fqdn }}.key` and `{{ ansible_fqdn }}.pem`
    into the Apache configuration (they must be in the working directory);
  - `certbot` will obtain a Let's Encrypt certificate and install the
    automatic renewal via certbot.
* `certbot_registration_email` must be provided if using certbot.

1. Tested with Debian Buster and Ubuntu 18.04. Only Python and an SSH server
   should be required.
2. Install with `ansible-playbook server.yml -i inventory` where inventory
   is the name of the inventory file (or full path).
3. By default the latest signage code will be installed from codeshare.phy. A
   different source for the code can be given in `signage_src` which should
   be a valid pip identifier.
4. To allow display of Booker listings, add credentials for an unprivileged
   user that can view the required rooms as `booker_username`
   and `booker_password`.

## Manual installation

### `signage.cfg`

Some configuration of the signage is required on the server. A file called
`signage.cfg` in the current working directory will be read during
initialisation or the file can be explicitly specified by setting the
`SIGNAGE_SETTINGS` environment variable.

Required variables:
* `SECRET_KEY` - Used to generate cookies to determine which slide to
  send to the client next. Can be any random string, and does not protect any
  sensitive information.
* `HOSTNAME` - Used so the server can identify itself so must be the preferred
  hostname used to access the signage.

Recommended variables:
* `DATA_PATH` - Directory for storing uploaded content, which the webserver
  process (e.g. `www-data`) must be able to write to. Defaults to the code
  root directory.

Other options:
* `SLIDE_DB` - The system will use an sqlite database by default, but it can
  also be pointed at any SQLAlchemy compatible URI.
* `CACHE_DIR` - Directory where processed images are stored.
* `BOOKER_USERNAME`, `BOOKER_PASSWORD` - Credentials for an unprivileged user
  to use for extracting room listings from Booker.

```python
# signage.cfg
HOSTANME = "server.url.cam.ac.uk"
SECRET_KEY = "<put some random text here>"
DATA_PATH = "/var/www/signage/data"
```

### Apache config

For manual installation you will need to set up Apache to point to the `wsgi`
script. The very minimal configuration below will get the server running,
but it is strongly recommended to add SSL to the connection and tune
the `threads`, `processes` and other wsgi parameters for your server.

```apache
<VirtualHost *>
#    SetEnv SIGNAGE_SETTINGS "/path/to/signage.cfg"

    WSGIProcessGroup signage
    WSGIApplicationGroup %{GLOBAL}
    WSGIDaemonProcess signage user=www-data group=www-data \
        python-home=/var/www/signage/env home=/var/www/signage/data
    WSGIScriptAlias / /var/www/signage/env/bin/signage.wsgi

    AACookieKey "<replace with a random string>"
    AAAlwaysDecode on
    AACacheControl off
    RequestHeader unset "X-AAPrincipal"

    <Location /edit/login>
        AuthType Ucam-WebAuth
        Require valid-user
    </Location>
</VirtualHost>

```

### Installation steps

For the most up to date installation procedure, follow the steps described
by the Ansible tasks in the playbook,
[main.yml](https://codeshare.phy.cam.ac.uk/itservices.phy/signage.phy/flask.signage.phy/blob/master/deploy/ansible/roles/signage-server/tasks/main.yml).

Generally:
1. Install required OS packages
2. Install the code in a virtualenv
3. Create directories
4. Initialise/migrate database,
   `FLASK_APP=signage.http.app flask database migrate`
5. Set up Apache and Raven
6. Set up cron jobs to clear cache and distribute slides

## Room booking integration

### MRBS

To integrate with an [MRBS](https://mrbs.sourceforge.io/) installation, the
signage server needs unauthenticated access to the `report.php` endpoint.
If using Apache, an exception to the auth mechanism can be made for the
signage server:

```apache
    <Files "report.php">
        AuthMerging or
        Require ip 123.4.5.67
    </Files>
```

### Booker

Have a user created in the Booker system that is able to view the rooms that
will be displayed on the signage.

Add the user's credentials to the configuration on the server:
`BOOKER_USERNAME`, `BOOKER_PASSWORD`. Rooms can then be accessed using their
id number, which can be extracted from the URL when viewing the room
on the Booker website.

# Migrating slides from previous versions

The data format for slides changed significantly in version 2 and slides
from previous versions are not automatically imported. Running the
command `FLASK_APP=signage.http.app flask signage import slides.json`
will import all the slides from the json file into new slide groups.
Backgrounds will be preserved if the data directory is preserved or copied.


# Development and contribution

All contributions are encouraged through `codeshare.phy`.

A development instance can be run locally with a standard `flask run` command.
Put a `signage.cfg` file in the root of the source and start Flask in
development mode:

```
FLASK_APP=signage.http.app FLASK_ENV=development flask run
```

To use the editor on a developer machine you will need a browser extension
that can add a CRSid as a `X-AAPrincipal` header to emulate a Raven login.

## Code style

* Python 3.6+ compatible
* [PEP484](https://www.python.org/dev/peps/pep-0484/) type annotations
* `numpydoc` style docstrings
* [`black`](https://black.readthedocs.io/en/stable/) Python formatting
* [prettier](https://prettier.io/) Javascript formatting

## License

The software is licensed under the BSD license.

# Authors

* @ara32 - Initial server software and RPi client firmware
* @tdd20 - Slide management interface and expanded server capabilities
