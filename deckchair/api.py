"""
Slide Management API

Methods for frontend to interact with the database of slides.

"""

from functools import wraps
from socket import getaddrinfo
from typing import Any, Callable, Type, Union, Tuple

from flask import Blueprint, request, redirect, url_for, abort
from flask import g, current_app, jsonify
from sqlalchemy.exc import IntegrityError
from werkzeug import Response

from deckchair.slides import SlideManager
from deckchair.util import whoami, is_local_addr

api = Blueprint("api", __name__, url_prefix="/api")


def abort_on_error(
    code: int, error: Union[Type[BaseException], Tuple[Type[BaseException]]]
):
    """
    Decorator to raise the appropriate http error when encountering
    the given exception.
    """

    def decorate(func: Callable) -> Callable:
        @wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            try:
                return func(*args, **kwargs)
            except error:
                return abort(code)

        return wrapper

    return decorate


def loopback_only(func: Callable) -> Callable:
    """If function is not called from loopback IP, deny the request."""

    @wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        if is_local_addr(request.remote_addr):
            return func(*args, **kwargs)
        else:
            return abort(403)

    return wrapper


@api.before_request
def global_setup() -> None:
    """Set up the database for all API requests."""
    g.current_user = whoami()
    g.slide_db = SlideManager(current_app.slide_db, user=g.current_user)


@api.route("/status", methods=["GET"])
def get_status() -> Response:
    """
    Information required to build the frontend application.

    The status can be stored as a global state object that can be retained
    as the view changes.
    """
    current_user = g.slide_db.current_user
    if current_user:
        user_info = current_user.to_dict(max_depth=1)
        user_info["all_slide_group_permissions"] = [
            group.to_dict(max_depth=1)
            for group in current_user.all_slide_group_permissions
        ]
        user_info["all_display_group_permissions"] = [
            group.to_dict(max_depth=1)
            for group in current_user.all_display_group_permissions
        ]
        user_info["all_display_permissions"] = [
            display.to_dict(max_depth=0)
            for display in current_user.all_display_permissions
        ]
        user_info["all_rule_permissions"] = [
            rule.to_dict(max_depth=0) for rule in current_user.all_rule_permissions
        ]
    else:
        user_info = None

    page_data = {"user": user_info}
    return jsonify({"data": page_data})


@api.route("/slides", methods=["GET"])
def slides_get() -> Response:
    """
    List of all slides.

    Return json with the `data` attribute containing a
    list of json representations of each slide.

    This list contains **every** slide, and using slide groups
    will be a better choice in general.
    """
    return jsonify({"data": g.slide_db.slides()})


# POST for slide must be done in a slide_group as a slide
# cannot exist outside of a slide_group


@api.route("/slides/<int:slide_id>")
@abort_on_error(404, LookupError)
def slides_slide_id_get(slide_id: int) -> Response:
    """
    Get a single slide.

    Retrieve the data for a single slide and return the
    json representation as the `data` attribute of the
    response.

    Parameters
    ----------
    slide_id
        Slide ID as stored in the database
    """

    return jsonify({"data": g.slide_db.slide(slide_id)})


@api.route("/slides/<int:slide_id>", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def slides_slide_id_patch(slide_id: int) -> Response:
    """
    Update a single slide with partial data.

    Read the json from the request data and merge that
    with the data for the given slide. Once the slide is
    successfully updated redirect the request to a GET
    for that slide (returns the slide).

    Parameters
    ----------
    slide_id
        Slide ID as stored in the database
    """
    data = request.get_json()

    updated_slide = g.slide_db.slide_update(slide_id, data)

    return jsonify({"data": updated_slide})


@api.route("/slides/<int:slide_id>", methods=["DELETE"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def slides_slide_id_delete(slide_id: int) -> Response:
    """
    Delete a slide.

    Completely remove a slide from the database. No archive
    functionality is implemented so a deleted slide is gone
    forever. Better to remove from all groups first.

    Response `data` attribute contains the contents of the deleted
    slide.

    Parameters
    ----------
    slide_id
        Slide ID as stored in the database
    """
    deleted_slide = g.slide_db.slide_delete(slide_id)

    # TODO: Default to garbage collecting unreferenced assets
    # if request.args.get("clean", "true") != "false":
    #    AssetStore().delete_orphans_background()

    return jsonify({"data": deleted_slide})


@api.route("/slide_groups", methods=["GET"])
def slide_groups_get() -> Response:
    """
    List of all slide groups.

    Returns json with the `data` attribute containing a
    list of all slide groups and some shallow data attributes.
    """

    return jsonify({"data": g.slide_db.slide_groups()})


@api.route("/slide_groups", methods=["POST"])
@abort_on_error(400, ValueError)
@abort_on_error(403, PermissionError)
def slide_groups_post() -> Response:
    """
    Create a new slide group.

    Read json from the request data and make a new slide group
    using that data. Once a new group has been created, redirect
    the request to it.
    """
    data = request.get_json()

    new_slide_group = g.slide_db.slide_group_create(data)

    return redirect(
        url_for("api.slide_groups_group_id_get", group_id=new_slide_group["id"]), 303,
    )


@api.route("/slide_groups/<int:group_id>", methods=["GET"])
@abort_on_error(404, LookupError)
@abort_on_error(403, PermissionError)
def slide_groups_group_id_get(group_id: int) -> Response:
    """
    Get the contents of a single slide group.

    Retrieve the metadata and slides that are contained
    within a single slide group and return as the `data`
    attribute of the json response.

    Parameters
    ----------
    group_id
        Group ID as stored in the database
    """
    return jsonify({"data": g.slide_db.slide_group(group_id)})


@api.route("/slide_groups/<int:group_id>", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def slide_groups_group_id_patch(group_id: int) -> Response:
    """
    Update a slide group with new data.

    Read the json from the request and update the slide group
    metadata with it. For managing slides in the group,

    Parameters
    ----------
    group_id
        Group ID as stored in the database
    """
    data = request.get_json()

    updated_slide_group = g.slide_db.slide_group_update(group_id, data)

    return jsonify({"data": updated_slide_group})


@api.route("/slide_groups/<int:group_id>", methods=["DELETE"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def slide_groups_group_id_delete(group_id: int) -> Response:
    """
    Delete slide group.

    Completely remove a slide group and and all the
    metadata associated with it. Return the data for the
    deleted group as the `data` attribute of the response.

    Parameters
    ----------
    group_id
        Group ID as stored in the database
    """
    deleted_group = g.slide_db.slide_group_delete(group_id)

    return jsonify({"data": deleted_group})


@api.route("/slide_groups/<int:group_id>/slides", methods=["POST"])
@abort_on_error(400, ValueError)
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def slide_groups_group_id_slides_post(group_id: int) -> Response:
    """
    Insert a new slide into a slide group.

    Read the json from the request data and create a new
    slide with that data. Once the slide is successfully
    created redirect the request to the url for that
    slide. A slide must be created as part of a slide group.
    """
    data = request.get_json()

    new_slide = g.slide_db.slide_group_slides_create(group_id=group_id, data=data)

    return redirect(url_for("api.slides_slide_id_get", slide_id=new_slide["id"]), 303)


@api.route("/slide_groups/<int:group_id>/order", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def slide_groups_group_id_order_patch(group_id: int) -> Response:
    """
    Modify the order of the slides in the slide group.

    Read the json from the request data that describes an
    operation on the order of the slides. Return the order
    once the operation has been applied.
    """
    data = request.get_json()

    new_order = g.slide_db.slide_group_order_change(group_id=group_id, data=data)

    return jsonify({"data": new_order})


@api.route("/displays", methods=["GET"])
def displays_get() -> Response:
    """
    List of all displays.

    Return json with the `data` attribute containing a
    list of json representations of each display.
    """
    return jsonify({"data": g.slide_db.displays()})


@api.route("/displays", methods=["POST"])
@abort_on_error(403, PermissionError)
@abort_on_error(400, IntegrityError)
def displays_post() -> Response:
    """
    Create a new display.

    Read json from the request data and make a new display
    using that data. Once a new display has been created, redirect
    the request to it.
    """
    data = request.get_json()

    new_display = g.slide_db.display_create(data)

    return redirect(
        url_for("api.displays_display_id_get", display_id=new_display["id"]), 303
    )


@api.route("/displays/<int:display_id>", methods=["GET"])
@abort_on_error(404, LookupError)
def displays_display_id_get(display_id: int) -> Response:
    """
    Get a single display.

    Retrieve the data associated with a single display and
    return as the `data` attribute of the json response.

    Parameters
    ----------
    display_id
        Display ID as stored in the database
    """
    return jsonify({"data": g.slide_db.display(display_id)})


@api.route("/displays/<int:display_id>", methods=["PATCH"])
@abort_on_error(400, ValueError)
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def displays_display_id_patch(display_id: int) -> Response:
    """
    Update a display with new data.

    Read the json from the request and update the display
    metadata.

    Parameters
    ----------
    display_id
        Display ID as stored in the database
    """
    data = request.get_json()

    updated_display = g.slide_db.display_update(display_id, data)

    return jsonify({"data": updated_display})


@api.route("/displays/<int:display_id>", methods=["DELETE"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def displays_display_id_delete(display_id: int) -> Response:
    """
    Delete the display.

    Completely remove a display from the database.
    Return the data for the removed display.

    Parameters
    ----------
    display_id
        Display ID as stored in the database
    """
    deleted_display = g.slide_db.display_delete(display_id)

    return jsonify({"data": deleted_display})


@api.route("/displays/<int:display_id>/display_groups", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def display_display_id_display_groups_patch(display_id: int) -> Response:
    """
    Modify the list of display groups of a display.

    Read json from the request data and update the display group list
    according to the operations sent.

    Parameters
    ----------
    display_id
        Display ID as stored in the database
    """
    data = request.get_json()

    display_groups = g.slide_db.display_display_groups_patch(
        display_id=display_id, data=data
    )

    return jsonify({"data": display_groups})


@api.route("/display_groups", methods=["GET"])
def display_groups_get() -> Response:
    """
    List of all display groups.

    Return json with the `data` attribute containing a
    list of json representations of each display group.
    """
    return jsonify({"data": g.slide_db.display_groups()})


@api.route("/display_groups", methods=["POST"])
@abort_on_error(403, PermissionError)
def display_groups_post() -> Response:
    """
    Create a new display group.

    Read json from the request data and make a new display
    group using that data. Once a new display group has been
    created, redirect the request to it.
    """
    data = request.get_json()

    new_display_group = g.slide_db.display_group_create(data)

    return redirect(
        url_for("api.display_groups_group_id_get", group_id=new_display_group["id"],),
        303,
    )


@api.route("/display_groups/<int:group_id>", methods=["GET"])
@abort_on_error(404, LookupError)
def display_groups_group_id_get(group_id: int) -> Response:
    """
    Get a single display group.

    Retrieve the data associated with a single display group
    and return as the `data` attribute of the json response

    Parameters
    ----------
    group_id
        Display group ID as stored in the database
    """
    return jsonify({"data": g.slide_db.display_group(group_id)})


@api.route("/display_groups/<int:group_id>", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def display_groups_group_id_patch(group_id: int) -> Response:
    """
    Update a display group with new data.

    Read the json from the request and update the display
    group metadata.

    Parameters
    ----------
    group_id
        Display group ID as stored in the database
    """
    data = request.get_json()

    updated_display_group = g.slide_db.display_group_update(group_id, data)

    return jsonify({"data": updated_display_group})


@api.route("/display_groups/<int:group_id>", methods=["DELETE"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def display_groups_group_id_delete(group_id: int) -> Response:
    """
    Delete the display group.

    Completely remove a display group from the database.
    Return the data for the removed display group.

    Parameters
    ----------
    group_id
        Display group ID as stored in the database
    """
    deleted_display_group = g.slide_db.display_group_delete(group_id)

    return jsonify({"data": deleted_display_group})


@api.route("/display_groups/<int:group_id>/displays", methods=["POST"])
@abort_on_error(400, ValueError)
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def display_groups_group_id_displays_post(group_id: int) -> Response:
    """
    Create a new display inside a display group.

    Read json from the request data and make a new display
    using that data and include it in the current display group.
    Once a new display has been created, redirect the request
    to it.

    Parameters
    ----------
    group_id
        Display group ID as stored in the database
    """
    data = request.get_json()

    # Create in a group
    data["groups"] = [group_id]

    new_display = g.slide_db.display_create(data)

    return redirect(
        url_for("api.displays_display_id_get", display_id=new_display["id"]), 303
    )


@api.route("/display_groups/<int:group_id>/displays", methods=["PATCH"])
@abort_on_error(400, ValueError)
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def display_groups_group_id_displays_patch(group_id: int) -> Response:
    """
    Modify the list of displays in a display group.

    Read json from the request data and update the display list according
    to the operations sent.

    Parameters
    ----------
    group_id
        Display group ID as stored in the database
    """
    data = request.get_json()

    displays = g.slide_db.display_group_displays_patch(group_id=group_id, data=data)

    return jsonify({"data": displays})


@api.route("/rules", methods=["GET"])
def rules_get() -> Response:
    """
    List of all display rules.

    Return json with the `data` attribute containing a
    list of json representations of each rule.
    """
    return jsonify({"data": g.slide_db.rules()})


@api.route("/rules", methods=["POST"])
@abort_on_error(400, IntegrityError)
@abort_on_error(400, ValueError)
@abort_on_error(403, PermissionError)
def rules_post() -> Response:
    """
    Create a new display rule.

    Read json from the request data and make a new display rule
    using that data. Once a new display rule has been created,
    redirect the request to it.
    """
    data = request.get_json()

    new_rule = g.slide_db.rule_create(data)

    return redirect(url_for("api.rules_rule_id_get", rule_id=new_rule["id"]), 303)


@api.route("/rules/<int:rule_id>", methods=["GET"])
@abort_on_error(404, LookupError)
def rules_rule_id_get(rule_id: int) -> Response:
    """
    Get a single display rule.

    Retrieve the data associated with a single display rule and
    return as the `data` attribute of the json response.

    Parameters
    ----------
    rule_id
        Rule ID as stored in the database
    """
    return jsonify({"data": g.slide_db.rule(rule_id)})


@api.route("/rules/<int:rule_id>", methods=["PATCH"])
@abort_on_error(400, ValueError)
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def rules_rule_id_patch(rule_id: int) -> Response:
    """
    Update a display rule with new data.

    Read the json from the request and update the display
    rule metadata.

    Parameters
    ----------
    rule_id
        Rule ID as stored in the database
    """
    data = request.get_json()

    updated_rule = g.slide_db.rule_update(rule_id, data)

    return jsonify({"data": updated_rule})


@api.route("/rules/<int:rule_id>", methods=["DELETE"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def rules_rule_id_delete(rule_id: int) -> Response:
    """
    Delete the display rule.

    Completely remove a display rule from the database.
    Return the data for the removed display rule.

    Parameters
    ----------
    rule_id
        Rule ID as stored in the database
    """
    deleted_rule = g.slide_db.rule_delete(rule_id)

    return jsonify({"data": deleted_rule})


@api.route("/rules/<int:rule_id>/members", methods=["GET"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def rules_rule_id_members_get(rule_id: int) -> Response:
    """
    Get the ordered list of members of the rule.

    Parameters
    ----------
    rule_id
        Rule ID as stored in the database
    """

    return jsonify({"data": g.slide_db.rule_members(rule_id)})


@api.route("/rules/<int:rule_id>/members", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def rules_rule_id_members_patch(rule_id: int) -> Response:
    """
    Modify the list of members.

    Read json from the request data and carry out the required
    operation on the member list, for example inserting a
    new slide group or slide into the list.

    Parameters
    ----------
    rule_id
        Rule ID as stored in the database
    """
    data = request.get_json()

    members = g.slide_db.rule_members_patch(rule_id=rule_id, data=data)

    return jsonify({"data": members})


@api.route("/rules/<int:rule_id>/displays", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def rules_rule_id_displays_patch(rule_id: int) -> Response:
    """
    Modify the list of displays.

    Read json from the request data and carry out the required
    operation on the display list, for example inserting a
    new display into the list.

    Parameters
    ----------
    rule_id
        Rule ID as stored in the database
    """
    data = request.get_json()

    displays = g.slide_db.rule_displays_patch(rule_id=rule_id, data=data)

    return jsonify({"data": displays})


@api.route("/rules/<int:rule_id>/display_groups", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def rules_rule_id_display_groups_patch(rule_id: int) -> Response:
    """
    Modify the list of display groups.

    Read json from the request data and carry out the required
    operation on the display group list, for example inserting a
    new display group into the list.

    Parameters
    ----------
    rule_id
        Rule ID as stored in the database
    """
    data = request.get_json()

    displays = g.slide_db.rule_display_groups_patch(rule_id=rule_id, data=data)

    return jsonify({"data": displays})


@api.route("/users", methods=["GET"])
def users_get() -> Response:
    """
    List of all users.

    Return json with the `data` attribute containing a
    list of json representations of each user.
    """
    return jsonify({"data": g.slide_db.users()})


@api.route("/users", methods=["POST"])
@abort_on_error(403, PermissionError)
@abort_on_error(400, IntegrityError)
def users_post() -> Response:
    """
    Create a new user.

    Read json from the request data and make a new user
    using that data. Once a new user has been created, redirect
    the request to it.
    """
    data = request.get_json()

    new_user = g.slide_db.user_create(data)

    return redirect(url_for("api.users_user_id_get", user_id=new_user["id"]), 303)


@api.route("/users/<string:user_id>", methods=["GET"])
@abort_on_error(404, LookupError)
def users_user_id_get(user_id: str) -> Response:
    """
    Get a single user's information.

    Retrieve the data associated with a single display and
    return as the `data` attribute of the json response.

    Parameters
    ----------
    user_id
        The id of the user in the database (probably CRSid)
    """
    return jsonify({"data": g.slide_db.user(user_id)})


@api.route("/users/<string:user_id>", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def users_user_id_patch(user_id: str) -> Response:
    """
    Update a user with new data.

    Read the json from the request and update the user
    metadata.

    Parameters
    ----------
    user_id
        The id of the user in the database (probably CRSid)
    """
    data = request.get_json()

    updated_user = g.slide_db.user_update(user_id, data)

    return jsonify({"data": updated_user})


@api.route("/users/<string:user_id>", methods=["DELETE"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def users_user_id_delete(user_id: str) -> Response:
    """
    Delete the user.

    Completely remove a user from the database.
    Return the data for the removed user.

    Parameters
    ----------
    user_id
        The id of the user in the database (probably CRSid)
    """
    deleted_user = g.slide_db.user_delete(user_id)

    return jsonify({"data": deleted_user})


@api.route("/users/<string:user_id>/groups", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def user_user_id_user_groups_patch(user_id: str) -> Response:
    """
    Modify the list of users in a user group.

    Read json from the request data and update the user list according
    to the operations sent.

    Parameters
    ----------
    user_id
        The id of the user in the database (probably CRSid)
    """
    data = request.get_json()

    user_groups = g.slide_db.user_user_groups_patch(user_id=user_id, data=data)

    return jsonify({"data": user_groups})


@api.route("/users/<string:user_id>/permissions", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def users_user_id_permissions_patch(user_id: str) -> Response:
    """
    Modify the list of permissions for a user.

    Unified endpoint that works with permissions for all different
    item types types. Reads json from the request data and update the
    user list according to the operations sent. JSON should have a
    `type` key to identify the permission being modified.

    Parameters
    ----------
    user_id
        The id of the user in the database (probably CRSid)
    """
    data = request.get_json()

    permissions = g.slide_db.user_permissions_patch(user_id=user_id, data=data)

    return jsonify({"data": permissions})


@api.route("/user_groups", methods=["GET"])
def user_groups_get() -> Response:
    """
    List of all user groups.

    Return json with the `data` attribute containing a
    list of json representations of each user group.
    """
    return jsonify({"data": g.slide_db.user_groups()})


@api.route("/user_groups", methods=["POST"])
@abort_on_error(403, PermissionError)
def user_groups_post() -> Response:
    """
    Create a new user group.

    Read json from the request data and make a new user
    group using that data. Once a new user group has been
    created, redirect the request to it.
    """
    data = request.get_json()

    new_user_group = g.slide_db.user_group_create(data)

    return redirect(
        url_for("api.user_groups_group_id_get", group_id=new_user_group["id"]), 303
    )


@api.route("/user_groups/<string:group_id>", methods=["GET"])
@abort_on_error(404, LookupError)
def user_groups_group_id_get(group_id: str) -> Response:
    """
    Get a single user group.

    Retrieve the data associated with a single user group
    and return as the `data` attribute of the json response

    Parameters
    ----------
    group_id
        User group ID as stored in the database
    """
    return jsonify({"data": g.slide_db.user_group(group_id)})


@api.route("/user_groups/<string:group_id>", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def user_groups_group_id_patch(group_id: str) -> Response:
    """
    Update a user group with new data.

    Read the json from the request and update the user
    group metadata.

    Parameters
    ----------
    group_id
        User group ID as stored in the database
    """
    data = request.get_json()

    updated_user_group = g.slide_db.user_group_update(group_id, data)

    return jsonify({"data": updated_user_group})


@api.route("/user_groups/<string:group_id>", methods=["DELETE"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def user_groups_group_id_delete(group_id: str) -> Response:
    """
    Delete the user group.

    Completely remove a user group from the database.
    Return the data for the removed user group.

    Parameters
    ----------
    group_id
        User group ID as stored in the database
    """
    deleted_user_group = g.slide_db.user_group_delete(group_id)

    return jsonify({"data": deleted_user_group})


@api.route("/user_groups/<string:group_id>/users", methods=["POST"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def user_groups_group_id_users_post(group_id: int) -> Response:
    """
    Create a new user inside a user group.

    Read json from the request data and make a new user
    using that data and include it in the current user group.
    Once a new user has been created, redirect the request
    to it.

    Parameters
    ----------
    group_id
        User group ID as stored in the database
    """
    data = request.get_json()

    # Create in a group
    data["groups"] = [group_id]

    new_user = g.slide_db.user_create(data)

    return redirect(url_for("api.users_user_id_get", user_id=new_user["id"]), 303)


@api.route("/user_groups/<string:group_id>/permissions", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def user_groups_group_id_permissions_patch(group_id: str) -> Response:
    """
    Modify the list of permissions for a user group.

    Unified endpoint that works with permissions for all different
    item types types. Reads json from the request data and update the
    user list according to the operations sent. JSON should have a
    `type` key to identify the permission being modified.

    Parameters
    ----------
    group_id
        User group ID as stored in the database
    """
    data = request.get_json()

    permissions = g.slide_db.user_group_permissions_patch(group_id=group_id, data=data)

    return jsonify({"data": permissions})


@api.route("/admins/<string:user_id>", methods=["PUT"])
@loopback_only
def admins_put(user_id: str) -> Response:
    """
    Create a new admin user.

    Note: Prefer flask cli to add users: flask signage create-admin

    Method to directly create an admin user. Useful when no users exist
    in the system that have admin privileges so they can be created
    locally.

    Can only be called on a loopback interface, i.e. curl on the server,
    but use the correct hostname if there are several vhosts.

    curl https://server.hostname/api/admins/<crsid> -X PUT \
        -d '{"name": "<User Name>"}' -H "Content-Type: application/json"

    Note that there isn't a simple way to get curl to follow a PUT that
    redirects to a GET.
    """
    data = request.get_json()

    new_user = g.slide_db.admin_create(user_id, data=data)

    return redirect(url_for("api.users_user_id_get", user_id=new_user["id"]), 303)


@api.route("/client/@me/slide_rotation", methods=["GET"])
@api.route("/client/<string:ident>/slide_rotation", methods=["GET"])
def client_ident_slide_rotation_get(ident: str = None) -> Response:
    """
    Get the list of currently showing slides for a given client.

    Use the @me construct to get the slide list for the accessing
    client, otherwise use the IP address.

    Parameters
    ----------
    ident
        IP address of the accessing client.
    """
    if ident is None:
        ident = request.remote_addr

    slides = [slide.to_dict(max_depth=0) for slide in g.slide_db.slide_rotation(ident)]

    # Helper for kiosks to link directly to this slide
    for slide in slides:
        slide["url"] = url_for("view.view_slide", oid=slide["oid"], _external=True)

    return jsonify({"data": slides})


##
# Network of known signage instances
##
@api.route("/handshake", methods=["POST"])
@abort_on_error(403, PermissionError)
@abort_on_error(400, OSError)
def handshake():
    """
    Route for a server to announce itself as a remote signage instance
    and to offer a list of slides that should be advertised on a remote
    signage server. Reply with the information about this instance,
    including the list of shared slides.

    The data must contain the hostname that it claims to be and that
    will be used to confirm the identity of the server.
    """
    data = request.get_json()

    # identity checks
    # Hostname should match what it says it is. A bad (or empty) hostname
    # is caught as an OSError. Port is stripped for IP address resolution.
    hostname = data.get("hostname", "").strip().split(":")[0]
    # Compare the IP address of the originating request to the ones that
    # are associated with that hostname.
    addresses = [info[4][0] for info in getaddrinfo(hostname, None)]
    if not any(addr == request.remote_addr for addr in addresses):
        raise PermissionError

    # Process the data! Depending on the status of the host, can store
    # data, register the host as seen, or reject the call completely.
    g.slide_db.handshake(data=data)

    # Reply with a equivalent view of the local server
    local = g.slide_db.local_info()

    return jsonify({"data": local})


@api.route("/neighbours", methods=["GET"])
@abort_on_error(403, PermissionError)
def neighbours_get() -> Response:
    """
    List all the neighbours of the server.

    Returns json with the `data` attribute containing a list of
    all the neighbouring signage servers.
    """
    return jsonify({"data": g.slide_db.neighbours()})


@api.route("/neighbours", methods=["POST"])
@abort_on_error(400, ValueError)
@abort_on_error(403, PermissionError)
def neighbours_post() -> Response:
    """
    Add a new neighbouring signage server.

    Read json from the request data and register a new signage instance
    that will be polled by the federating process.
    """
    data = request.get_json()

    new_neighbour = g.slide_db.neighbour_create(data)

    return redirect(
        url_for("api.neighbours_hostname_get", hostname=new_neighbour["hostname"]), 303,
    )


@api.route("/neighbours/<string:hostname>", methods=["GET"])
@abort_on_error(404, LookupError)
@abort_on_error(403, PermissionError)
def neighbours_hostname_get(hostname: str) -> Response:
    """
    Get the information for a single neighbouring server.

    Information about the remote server includes identification,
    properties set by admins, shared signs and any neighbours
    that it knows about. Data is returned as the `data` attribute
    of the json response.

    Parameters
    ----------
    hostname
        The identifying hostname of the remote system.
    """
    return jsonify({"data": g.slide_db.neighbour(hostname)})


@api.route("/neighbours/<string:hostname>", methods=["PATCH"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def neighbours_hostname_patch(hostname: str) -> Response:
    """
    Update information about a neighbouring server.

    Read the json from the request and update properties of the server
    that are not self identification, such as how much trust to give
    the server.

    Parameters
    ----------
    hostname
        The identifying hostname of the remote system.
    """
    data = request.get_json()

    updated_neighbour = g.slide_db.neighbour_update(hostname, data)

    return jsonify({"data": updated_neighbour})


@api.route("/neighbours/<string:hostname>", methods=["DELETE"])
@abort_on_error(403, PermissionError)
@abort_on_error(404, LookupError)
def neighbours_hostname_delete(hostname: str) -> Response:
    """
    Forget a remote server.

    Remove a remote server and any properties that have been set for
    it from the database. Server may re-appear through the
    federation procedure if delegating trust.

    Parameters
    ----------
    hostname
        The identifying hostname of the remote system.
    """
    deleted_neighbour = g.slide_db.neighbour_delete(hostname)

    return jsonify({"data": deleted_neighbour})


@api.route("/remote_slides", methods=["GET"])
@abort_on_error(400, ValueError)
def remote_slides_get() -> Response:
    """
    Get the list of all remote slides that are advertised on the
    server.

    These are public so should always return something. A forced refresh
    of the cache can be achieved using the request header
    "Cache-Control: max-age=0" or some other age that should be less than
    the most recent refresh.
    """
    # Should be an integer or None, otherwise it generates a ValueError
    max_age = request.cache_control.max_age

    return jsonify({"data": g.slide_db.neighbour_slides(max_age=max_age)})


@api.route("/remote_slides/<string:hostname>", methods=["GET"])
@abort_on_error(400, ValueError)
@abort_on_error(404, LookupError)
def remote_slides_hostname_get(hostname) -> Response:
    """
    Get the list of advertised slides for a remote server.

    A forced refresh of the cache can be achieved by setting the header
    "Cache-Control: max-age=0" or some other age that should be less
    than the most recent refresh.
    """
    # Should be an integer or None, otherwise it generates a ValueError
    max_age = request.cache_control.max_age

    slides = g.slide_db.neighbour_slides(hostnames=[hostname], max_age=max_age)

    if len(slides) != 1:
        raise KeyError

    return jsonify({"data": slides[0]})


@api.after_request
def shutdown_request_databases(response: Response) -> Response:
    g.slide_db.close()
    return response
