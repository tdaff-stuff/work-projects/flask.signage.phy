"""

Persistent file store and file type conversion.

Store an original file with an image and thumbnail version.

"""

import multiprocessing
import os
import re
import subprocess
from hashlib import sha1
from io import BytesIO
from mimetypes import guess_extension
from pathlib import Path
from shutil import which, rmtree
from tempfile import TemporaryDirectory
from typing import Optional, Iterable, Union, Dict, List, Any

import magic
import requests
from flask import safe_join, current_app
from PIL import Image
from tinydb import TinyDB, Query, where
from tinydb.database import Document

from deckchair.slides import SlideManager
from deckchair.util import whoami


# matches the sha1 hashing of files
asset_re = re.compile("(?:/*assets/)?(?P<hash>[0-9a-f]{40})/(?P<filename>.*)")
hash_re = re.compile("(?P<a>[0-9a-f])(?P<b>[0-9a-f])(?P<c>[0-9a-f]{38})")


def str_to_list(
    value: Union[str, list], separators: Iterable[str] = (",", ";")
) -> List[str]:
    """
    Convert a list of items stored as a string into a list of strings.
    Will separate on whitespace and the given characters (which are stripped)

    Parameters
    ----------
    value
        The string to be converted to a list
    separators
        A series of characters to strip as separators from the string.
        Will be combined and removed.

    Returns
    -------
    items
        List of the separated strings
    """
    # no-op if already is a list
    if isinstance(value, list):
        return value

    # Convert all separators to whitespace so they will get combined on split
    for separator in separators:
        value = value.replace(separator, " ")

    # Split on any whitespace
    return value.split()


class FileConverter:
    """
    File format converter. Takes binary source content and provides
    methods to get image versions of them.
    """

    def __init__(
        self,
        content: bytes,
        pdftoppm: str = "pdftoppm",
        libreoffice: str = "libreoffice",
    ):
        """Absorb file contents into something that can be made into images."""
        # Contents of the file
        self.content = content
        # File type conversion
        self.pdftoppm = which(pdftoppm)
        self.libreoffice = which(libreoffice)
        self.mime_type = magic.from_buffer(content, mime=True)
        # Best guess, not perfect (e.g. `.jpe` vs `.jpg`)
        self.suffix = guess_extension(self.mime_type) or ""
        # Collisions quite unlikely
        self.hash = sha1(content).hexdigest()
        # Cache converted versions
        self._image = None
        self._thumbnail = None

    def image(self) -> Image:
        """The contents of the file as a PIL Image"""
        # Use cached image if it exists
        if self._image is not None:
            return self._image

        # Directly convert if it's a known pdf
        if self.mime_type == "application/pdf":
            self._image = self.pdf_to_image()
        else:
            try:
                self._image = self.image_to_image()
            except OSError:
                self._image = self.office_to_image()

        # Return whatever is there
        return self._image

    def thumbnail(self, size=(384, 216)) -> Image:
        """The contents of the file as a thumbnailed PIL Image"""
        # Use cached image if it exists
        if self._thumbnail is not None:
            return self._thumbnail

        # Image.thumbnail modifies in place so need to copy
        image = self.image().copy()
        image.thumbnail(size)
        self._thumbnail = image
        return self._thumbnail

    def pdf_to_image(self) -> Image:
        """Use poppler to convert to a 4K png."""
        # scale-to maintains aspect ratio, or use -1 in either dimension
        # Can pipe through stdin and stdout
        stdout, _stdin = subprocess.Popen(
            [self.pdftoppm, "-png", "-scale-to", "3840", "-singlefile"],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
        ).communicate(input=self.content)

        # PIL reads in png
        return Image.open(BytesIO(stdout))

    def office_to_image(self) -> Image:
        """
        Use libreoffice and poppler to convert to a 4K image. Works for
        any format that libreoffice can read, from MSOffice to plain
        text files, but recommended user to convert to pdf before
        uploading so it looks correct, as libreoffice will not look
        the same as MSOffice.
        """
        # Have to convert with temporary files as no stdin/stdout
        # options with libreoffice
        with TemporaryDirectory() as workdir:
            src_doc = Path(workdir) / "conv"
            src_doc.write_bytes(self.content)
            tmp_pdf = Path(workdir) / "conv.pdf"
            # doc to pdf, in working directory
            # libreoffice gets upset if it can't write to HOME as java
            # tries to create some config, so override it for this command
            subprocess.call(
                [
                    self.libreoffice,
                    "--convert-to",
                    "pdf",
                    "--outdir",
                    str(workdir),
                    str(src_doc),
                ],
                env=dict(os.environ, HOME=workdir),
            )
            # convert file to pft and read through stdout
            stdout = subprocess.check_output(
                [
                    self.pdftoppm,
                    "-png",
                    "-scale-to",
                    "3840",
                    "-singlefile",
                    str(tmp_pdf),
                ]
            )

        # Read with PIL
        return Image.open(BytesIO(stdout))

    def image_to_image(self) -> Image:
        """Read directly as an image, let PIL guess the format."""
        return Image.open(BytesIO(self.content))


class AssetStore:
    """Store converted versions of a single file."""

    def __init__(self, location: Optional[Union[str, Path]] = None):
        """
        Define a directory where files can be stored.

        Parameters
        ----------
        location
            Path to directory where the assets will be stored in a
            hash based tree of directories.
        """
        # Define absolute path relative to project
        if location is None:
            data = current_app.config.get("DATA_PATH", current_app.root_path)
            self.location = Path(data, "assets")
        else:
            self.location = Path(location)

        self.location.mkdir(parents=True, exist_ok=True)
        # The index is required to keep track of assets in the library
        self.index = TinyDB(str(self.location / "index.json"))
        self.user = None

    def __iter__(self) -> Iterable:
        """Enumerate all the assets on the filesystem."""
        # assets are h/a/sh/
        for path in self.location.glob("*/*/*/"):
            yield self.path_to_hash(path)

    @property
    def has_permissions(self) -> bool:
        """
        Check if the current user can modify assets
        """
        if not self.user:
            self.user = SlideManager(current_app.slide_db, user=whoami()).current_user

        # User has write access to some slide groups somewhere.
        # Allowed to generate assets.
        if self.user.admin or self.user.all_slide_group_permissions:
            return True

        # No permissions, read-only
        return False

    @staticmethod
    def hash_to_path(asset_hash: str) -> Path:
        """
        Generate the directory Path for a hash.

        Ensures that path is clean with only alphanumeric hash
        components so is safe to append to location when using as
        a file path.

        Parameters
        ----------
        asset_hash
            A string that can be identified as an asset hash.

        Returns
        -------
        path
            The relative Path for the hash.
        """
        hash_parts = hash_re.match(asset_hash)
        if hash_parts is None:
            raise ValueError("Invalid hash: {}".format(asset_hash))
        return Path(hash_parts.group("a"), hash_parts.group("b"), hash_parts.group("c"))

    @staticmethod
    def path_to_hash(asset_path: Path) -> str:
        """
        Generate the hash equivalent to a file location.

        The hash is validated before being returned.

        Parameters
        ----------
        asset_path
            The location to inspect.

        Returns
        -------
        hash
            The constructed hash from the path given.
        """
        asset_hash = "".join(asset_path.parts[-3:])
        if hash_re.match(asset_hash) is None:
            raise ValueError("Invalid asset directory: {}".format(asset_path))
        return asset_hash

    def store(self, content, filename: Optional[str] = None, save: bool = False) -> str:
        """
        Store the file in the directory structure and return the
        hash based identifier for it.

        Parameters
        ----------
        content
            Raw binary contents of the file to be stored.
        filename
            Original filename if it should be stored with the file.
        save
            Add the asset to the persistent library

        Returns
        -------
        asset_hash
            The SHA1 hash of the file contents, used to identify it.

        """
        # check permissions
        if not self.has_permissions:
            raise PermissionError

        original = FileConverter(content)
        file_location = self.location / self.hash_to_path(original.hash)
        file_location.mkdir(parents=True, exist_ok=True)

        if filename is None:
            filename = "original" + original.suffix
        else:
            filename = "original_" + filename

        # Write the raw file to the store
        (file_location / filename).write_bytes(content)

        # generate an image version
        original.image().save(file_location / "image.png")
        # generate a thumbnail
        original.thumbnail().save(file_location / "thumbnail.png")

        if save:
            self.save(
                original.hash,
                image="assets/" + original.hash + "/image.png",
                thumbnail="assets/" + original.hash + "thumbnail.png",
            )

        return original.hash

    def save(self, asset_hash: str, **metadata: Any) -> Document:
        """
        Register the asset in the library and attach the given metadata.
        If asset is already registered then merge the given metadata.

        Parameters
        ----------
        asset_hash
            Unique hash identifying the asset
        metadata
            Information to include in the record for the asset. Can
            be any sets of key-value pairs which are stored verbatim
            except for tags which are converted to a list.

        Returns
        -------
        asset
            Complete metadata record for the asset
        """
        # check permissions
        if not self.has_permissions:
            raise PermissionError

        # Ensure that the asset exists in the system
        if not self.dir(asset_hash):
            raise KeyError("Unknown asset")

        # Convert tags to a list, using whitespace, comma or semicolon
        # as a separator
        metadata["tags"] = str_to_list(metadata.get("tags", ""))
        # Required for search, normalised to strings
        metadata["description"] = str(metadata.get("description", ""))
        metadata["license"] = str(metadata.get("license", ""))
        metadata["source"] = str(metadata.get("source", ""))

        doc_id = self.index.upsert(
            dict(**metadata, asset=asset_hash), where("asset") == asset_hash
        )

        # Complete record expected
        return self.index.get(doc_id=doc_id[0])

    def forget(self, asset_hash: str, clean: bool = True) -> None:
        """
        Remove the asset from the library of saved assets.

        Parameters
        ----------
        asset_hash
            Unique hash identifying the asset
        clean
            Call the procedure to garbage_collect any unreferenced files
        """
        # check permissions
        if not self.has_permissions:
            raise PermissionError

        self.index.remove(where("asset") == asset_hash)

        if clean:
            self.delete_orphans_background()

    def library(self, asset: Optional[str] = None, **narrow: str) -> List[Document]:
        """
        A view on the saved assets. Return all assets by default.

        If an asset hash is give, the asset matching that value is returned
        as the single element of a list (or an empty list if none is found)

        Otherwise, query parameters can be added as keyword arguments and
        only assets matching the given parameters will be returned. The
        implemented parameters are:

          *  description: substring search
          *  license: exact match
          *  source: substring search
          *  tags: has all tags

        """
        # ORM-style for combined query
        slide = Query()

        # Exact match on a single asset
        if asset is not None:
            return list(self.index.search(slide.asset == asset))

        # Search parameters default to allowing all values
        description = narrow.get("description", "")  # substring
        lic = narrow.get("license", ".*")  # exact match
        source = narrow.get("source", "")  # substring
        tags = str_to_list(narrow.get("tags", ""))  # all required

        slides_query = self.index.search(
            slide.description.search(description, flags=re.IGNORECASE)
            & slide.license.matches(lic)
            & slide.source.search(source, flags=re.IGNORECASE)
            & slide.tags.all(tags)
        )

        return list(slides_query)

    def dir(self, asset_hash: str) -> Dict[str, str]:
        """Contents of an asset directory"""
        # Hash validity checked in the conversion method
        directory = self.location / self.hash_to_path(asset_hash)

        # The asset does not exist; return something Falsey
        if not directory.exists():
            return {}

        # inject current asset directory
        fileinfo = {".": asset_hash}

        for item in directory.iterdir():
            if item.name.startswith("original"):
                fileinfo["original"] = item.name
            elif item.name == "image.png":
                fileinfo["image"] = "image.png"
            elif item.name == "thumbnail.png":
                fileinfo["thumbnail"] = "thumbnail.png"

        return fileinfo

    def asset_location(
        self, asset_hash: str, filename: str, origin: Optional[str] = None
    ) -> Union[Path, str]:
        """Convert an asset hash and filename to a real file location."""
        location = safe_join(self.location / self.hash_to_path(asset_hash), filename)

        # Remote data must be copied locally if it's not already there
        if origin is not None and not os.path.exists(location):
            self.remote_get(asset_hash, origin)

        return location

    def file_location(
        self, url_str: str, origin: Optional[str] = None
    ) -> Union[Path, str]:
        """Convert an asset url type path to a real file location."""

        asset_match = asset_re.match(url_str)

        if asset_match is None:
            raise FileNotFoundError

        location = safe_join(
            self.location / self.hash_to_path(asset_match.group("hash")),
            asset_match.group("filename"),
        )

        # Remote data must be copied locally if it's not already there
        if origin is not None and not os.path.exists(location):
            self.remote_get(asset_match.group("hash"), origin)

        return location

    def remote_get(self, asset_hash: str, origin: str) -> None:
        """Retrieve a remote asset and store it locally."""
        remote_asset = "http://{}/assets/{}/".format(origin, asset_hash)
        # Query OPTIONS to see if there is a redirect -> https
        options = requests.options(remote_asset, allow_redirects=True, timeout=3.05)

        try:
            asset_data = requests.get(options.url, timeout=3.05).json()["data"]
        except (OSError, ValueError, KeyError):
            raise FileNotFoundError

        # Sanity check
        if asset_data["."] != asset_hash:
            raise FileNotFoundError

        # Local file storage
        local_directory = self.location / self.hash_to_path(asset_hash)
        local_directory.mkdir(parents=True, exist_ok=True)

        # Copy each item if it exists in the remote location
        for item in ["original", "image", "thumbnail"]:
            try:
                filename = asset_data[item]
                local_file = local_directory / filename
                remote_path = options.url + filename
                src = requests.get(remote_path, timeout=3.05)
                src.raise_for_status()
                local_file.write_bytes(src.content)
            except (OSError, KeyError):
                pass

    def delete_orphans(self) -> None:
        """
        Delete any assets from the filesystem that are not referenced
        anywhere. Check slides and the library for references
        and delete anything that's not found there.
        """
        # List all currently referenced assets from slides
        slide_database = SlideManager(current_app.slide_db)
        referenced_assets = []
        for slide in slide_database.slides():
            asset_match = asset_re.match(slide["content"].get("asset", ""))
            if asset_match is not None:
                referenced_assets.append(asset_match.group("hash"))

        # Append assets stored in the library
        for library_asset in self.index:
            referenced_assets.append(library_asset["asset"])

        # Delete any unreferenced assets. Iterate over all the assets
        # currently stored on the filesystem
        for asset in self:
            if asset not in referenced_assets:
                asset_path = self.location / self.hash_to_path(asset)
                rmtree(asset_path)

    def delete_orphans_background(self) -> multiprocessing.Process:
        """
        Garbage collect assets in a thread.

        """
        # Run the garbage collection in a thread so that it doesn't
        # block returning the request.
        # Since threads are not joined explicitly, they will end up as
        # zombie processes when they are done. Cleanup could be added as
        # some wsgi middleware, but multiprocessing docs state that either
        # a call to active_children() or starting a new process will
        # automatically join any finished processes. Do both to be sure.
        # TODO: Could be implemented in a task queue
        multiprocessing.active_children()
        mp_delete = multiprocessing.Process(target=self.delete_orphans)
        mp_delete.start()

        return mp_delete
