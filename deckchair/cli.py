"""
Deckchair CLI

Run commands to manage aspects of the slide database fom the
command line. Requires the environment to be set up correctly
so that it can pull in any extra configuration information.

FLASK_APP=signage.http.app
SIGNAGE_SETTINGS=/path/to/config, or signage.cfg in cwd
"""
import json
import logging
from pprint import pprint
from typing import Optional

import click
from alembic import command, migration
from alembic.config import Config
from flask import current_app
from flask.cli import AppGroup

from deckchair.slides import SlideDatabase, SlideManager

FILE_TEMPLATE = "%%(year)d-%%(month)02d-%%(day)02d_%%(rev)s-%%(slug)s"

db_cli = AppGroup("database", help="Commands to manage databases")
signage_cli = AppGroup("signage", help="Commands to manage signage data")


@db_cli.command("migrate")
@click.option("--verbose", "-v", count=True)
def migrate(verbose: int = 0) -> None:
    """
    Carry out any migrations, including initialisation.

    \b
    Parameters
    ----------
    verbose
        Turn on all logging levels.
    """
    # Output is minimal without logging
    if verbose > 0:
        logging.basicConfig(level=logging.DEBUG)

    # Configured database is attached to the current app object.
    database = current_app.slide_db  # type: SlideDatabase
    # Alembic setup using current configuration
    config = Config()
    config.set_main_option("script_location", "deckchair:migrations")
    config.set_main_option("sqlalchemy.url", database.url)

    # Check whether the database is initialised by alembic
    context = migration.MigrationContext.configure(database.engine.connect())
    current_rev = context.get_current_revision()

    # Database does not exist, or not yet migrated
    if current_rev is None:
        # Will create a database or any missing tables
        database.base.metadata.create_all(database.engine)
        # Stamp the current alembic revision
        command.stamp(config, "head")

    command.upgrade(config, "head")
    # Show the final rev if it's changed
    final_rev = context.get_current_revision()
    if verbose > 0 or final_rev != current_rev:
        print("{} -> {}".format(current_rev, final_rev))


@db_cli.command("makemigrations")
@click.argument("message")
def revision(message: str) -> None:
    """
    Use autogenerate to create migrations for the current database.

    \b
    Parameters
    ----------
    message
        Description of changes, also appears as a slug in the filename.
    """
    # Configured database is attached to the current app object.
    database = current_app.slide_db  # type: SlideDatabase
    # Alembic setup using current configuration
    config = Config()
    config.set_main_option("script_location", "deckchair:migrations")
    config.set_main_option("sqlalchemy.url", database.url)

    # Mostly sequential migration files
    config.set_main_option("file_template", FILE_TEMPLATE)

    # Run the command
    command.revision(config, message=message, autogenerate=True)


@db_cli.command("downgrade", context_settings={"ignore_unknown_options": True})
@click.argument("rev")
@click.option("--verbose", "-v", count=True)
def downgrade(rev: str, verbose: int = 0) -> None:
    """
    Downgrade the database to an earlier revision. Most common
    is probably `-1`, but an exact revision can also be specified.

    \b
    Parameters
    ----------
    rev
        Revision slug or relative change, e.g. -1.
    verbose
        Turn on all logging levels.
    """
    # `ignore_unknown_options` required to stop negative numbers getting
    # eaten by the parser

    # Output is minimal without logging
    if verbose > 0:
        logging.basicConfig(level=logging.DEBUG)

    # Configured database is attached to the current app object.
    database = current_app.slide_db  # type: SlideDatabase
    # Alembic setup using current configuration
    config = Config()
    config.set_main_option("script_location", "deckchair:migrations")
    config.set_main_option("sqlalchemy.url", database.url)

    # Run the command
    command.downgrade(config, rev)


@signage_cli.command("create-admin")
@click.argument("user_id")
@click.argument("name", required=False)
@click.option("--verbose", "-v", count=True)
def create_admin(user_id: str, name: Optional[str] = None, verbose: int = 0) -> None:
    """
    Create an admin user, or promote a regular user.

    \b
    Parameters
    ----------
    user_id
        The CRSid of the user to create or promote.
    name
        If supplied, a new user will be created with the given name.
        Existing users are not modified.
    verbose
        If greater than 0, print the user information after the
        successful completion.
    """
    if name:
        data = {"name": name}
    else:
        data = {}

    database = SlideManager(current_app.slide_db)
    admin = database.admin_create(user_id, data=data)

    if verbose > 0:
        pprint(admin)

    database.close()


@signage_cli.command("import")
@click.argument("filename")
def import_legacy(filename: str) -> None:
    """
    Import slides from a version 1 instance.

    Read the TinyDB json from a legacy installation of signage and
    add them to the current database.

    Slides go into "Imported (published)" and "Imported (draft)"
    slide groups. The server will need a copy of the "assets" directory
    otherwise backgrounds will not get imported.

    \b
    Parameters
    ----------
    filename
        Path to a json file that has slides from an old version.
    """
    with open(filename) as src:
        legacy = json.load(src)

    database = SlideManager(current_app.slide_db)

    # meta - slide order, ignored
    # users - print all users
    for user in legacy["users"].values():
        print("{:10s} {}".format(user["user_id"], ", ".join(user["permissions"])))

    # slides -> import
    database.import_legacy_slides(legacy["slides"].values())
