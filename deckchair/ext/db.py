import base64
import json
from typing import Any, Optional, List, Dict

from sqlalchemy import inspect
from sqlalchemy.ext.hybrid import HYBRID_PROPERTY
from sqlalchemy.types import Text, LargeBinary
from sqlalchemy.types import TypeDecorator
from sqlalchemy.ext.mutable import MutableDict, MutableList


class JSONEncoded(TypeDecorator):
    """An immutable structure represented as a json-encoded string."""

    impl = Text

    def process_bind_param(self, value, dialect) -> str:
        if value is not None:
            value = json.dumps(value)
        return value

    def process_result_value(self, value, dialect) -> Any:
        if value is not None:
            value = json.loads(value)
        return value

    @property
    def python_type(self):
        raise NotImplementedError

    def coerce_compared_value(self, op, value):
        raise NotImplementedError

    def process_literal_param(self, value, dialect):
        raise NotImplementedError


class B64Binary(TypeDecorator):
    """
    A binary blob that is represented as a base64 encoded string
    in the application.
    """

    impl = LargeBinary

    def process_bind_param(self, value, dialect) -> str:
        if value is not None:
            value = base64.b64decode(value)
        return value

    def process_result_value(self, value, dialect) -> str:
        if value is not None:
            value = base64.b64encode(value).decode("ascii")
        return value

    @property
    def python_type(self):
        return str

    def coerce_compared_value(self, op, value):
        raise NotImplementedError

    def process_literal_param(self, value, dialect):
        raise NotImplementedError


# Column types for SQLAlchemy
DictType = MutableDict.as_mutable(JSONEncoded)
ListType = MutableList.as_mutable(JSONEncoded)


class ToDictMixin:
    def to_dict(
        self, max_depth: int = 1, exclude: Optional[List[str]] = None
    ) -> Dict[str, Any]:
        """
        Return a dictionary view of the item.

        Parameters
        ----------
        max_depth
            Descend into recursive relationship properties up to
            a maximum number of levels to prevent circular relationships.
        exclude
            A list of key names properties to ignore.

        Returns
        -------
        as_dict
            The object as a dictionary of Python types.
        """
        as_dict = {}
        exclude = exclude or []

        mapper = inspect(self).mapper

        # individual values
        for prop in mapper.column_attrs + mapper.composites:
            if prop.key in exclude:
                continue
            as_dict[prop.key] = getattr(self, prop.key)

        # descend into relationships
        if max_depth > 0:
            depth = max_depth - 1
            for prop in mapper.relationships:
                value = getattr(self, prop.key)
                if prop.uselist:  # many value relationship
                    as_dict[prop.key] = [
                        item.to_dict(max_depth=depth, exclude=exclude) for item in value
                    ]
                else:
                    # Short circuit and for the NULL case
                    as_dict[prop.key] = value and value.to_dict(
                        max_depth=depth, exclude=exclude
                    )

        # hybrid properties
        for attr, val in mapper.all_orm_descriptors.items():
            # skipped items, or the mapper itself
            if attr in exclude or attr == "__mapper__":
                continue
            if val.extension_type == HYBRID_PROPERTY:
                as_dict[attr] = getattr(self, attr)

        # aliased values
        for prop in mapper.synonyms:
            if prop.key in exclude:
                continue
            as_dict[prop.key] = as_dict.get(prop.name, None)

        return as_dict
