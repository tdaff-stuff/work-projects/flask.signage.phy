"""
Database of slides.
"""
import enum
import re
from datetime import datetime, timedelta
from ipaddress import ip_address
from itertools import chain
from random import randint
from typing import Optional, Union, List, Dict, Any
from uuid import uuid4

import dateutil.parser
import requests
from cachelib import SimpleCache, BaseCache
from flask import Flask, current_app, json
from sqlalchemy import Table, Column, ForeignKey, Text
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship, synonym
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.types import Integer, String, Float, Boolean, DateTime, Time, Enum

from deckchair.ext.db import DictType, ListType, B64Binary, ToDictMixin
from signage.display.common import CLOCK_PANEL

Base = declarative_base()

re_oid = re.compile(r"(?P<class>\w+):(?P<id>\d+)(?:$|@(?P<origin>.*))")


class Visibility(enum.Enum):
    """Levels of privacy or sharing for slide groups."""

    private = 0
    local = 1
    public = 2
    share = 3


def parse_date(date_string: Optional[str]) -> Optional[datetime]:
    """Date parsing and None -> None"""
    if date_string is None:
        return None
    return dateutil.parser.parse(date_string).replace(tzinfo=None)


def parse_time(time_string: Optional[str]) -> Optional[datetime.time]:
    """Time parsing and None -> None"""
    if time_string is None:
        return None
    return dateutil.parser.parse(time_string).time()


def strip(text: Optional[str]) -> Optional[str]:
    """Strip a string, or pass None unchanged"""
    if text is None:
        return None
    return text.strip()


def merge(*args: List) -> List:
    """Merge multiple lists into a list of unique items"""
    return list(set(chain(*args)))


# Relationships between slides, displays, their groupings and associated
# permissions
display_groupings = Table(
    "display_groupings",
    Base.metadata,
    Column("display_id", Integer, ForeignKey("displays.id")),
    Column("display_group_id", Integer, ForeignKey("display_groups.id")),
)


user_groupings = Table(
    "user_groupings",
    Base.metadata,
    Column("user_id", String, ForeignKey("users.id")),
    Column("user_group_id", String, ForeignKey("user_groups.id")),
)


# General permissions can be used to link any pair of objects, rather
# than a separate table for each pairing.
permissions = Table(
    "permissions",
    Base.metadata,
    Column("slide_group_id", Integer, ForeignKey("slide_groups.id")),
    Column("user_id", String, ForeignKey("users.id")),
    Column("user_group_id", String, ForeignKey("user_groups.id")),
    Column("display_id", Integer, ForeignKey("displays.id")),
    Column("display_group_id", Integer, ForeignKey("display_groups.id")),
)

# Association table for linking a rule for showing content with the
# content to be displayed or the screens to display on
display_rules = Table(
    "display_rules",
    Base.metadata,
    Column("rule_id", Integer, ForeignKey("rules.id")),
    Column("slide_id", Integer, ForeignKey("slides.id")),
    Column("slide_group_id", Integer, ForeignKey("slide_groups.id")),
    Column("display_id", Integer, ForeignKey("displays.id")),
    Column("display_group_id", Integer, ForeignKey("display_groups.id")),
)


class Slide(Base, ToDictMixin):
    """
    A single slide with metadata about displaying and management.
    What to display is contained in a free-form `content` attribute.
    """

    __tablename__ = "slides"
    origin = None

    id = Column(Integer, primary_key=True)
    # Simple scalar fields
    name = Column(String)
    description = Column(Text)
    content = Column(DictType)
    display_time = Column(Float)
    valid_from = Column(DateTime)
    valid_to = Column(DateTime)
    show_clock = Column(Boolean, default=True, nullable=False)
    thumbnail = Column(B64Binary)
    # Relationship and complex properties
    updated_by_id = Column(String, ForeignKey("users.id"))
    updated_by = relationship("User", foreign_keys=updated_by_id)
    updated = Column(DateTime)
    created_by_id = Column(String, ForeignKey("users.id"))
    created_by = relationship("User", foreign_keys=created_by_id)
    created = Column(DateTime)
    group_id = Column(Integer, ForeignKey("slide_groups.id"))
    group = relationship("SlideGroup", foreign_keys=group_id, back_populates="slides")
    rules = relationship("Rule", secondary=display_rules, back_populates="slides")

    @hybrid_property
    def oid(self) -> str:
        """
        Object identifier, includes the object type as well as the
        database assigned id value for use where lists of mixed values
        are used for sorting.
        """
        if self.origin is not None:
            return "SlideExternal:{}@{}".format(self.id, self.origin)
        else:
            return "Slide:{}".format(self.id)

    def update_scalar(self, **kwargs: Any) -> None:
        """
        Update using all the keys in kwargs that are valid scalar
        properties of a Slide.

        None and blank values where keys are explicitly given
        will result in NULL and empty fields.
        """
        if "name" in kwargs:
            self.name = strip(kwargs["name"])
        if "description" in kwargs:
            self.description = strip(kwargs["description"])
        if "content" in kwargs:
            self.content = kwargs["content"]
        if "display_time" in kwargs:
            self.display_time = kwargs["display_time"]
        if "valid_from" in kwargs:
            self.valid_from = parse_date(kwargs["valid_from"])
        if "valid_to" in kwargs:
            self.valid_to = parse_date(kwargs["valid_to"])
        if "show_clock" in kwargs:
            self.show_clock = kwargs["show_clock"]
        if "thumbnail" in kwargs:
            self.thumbnail = kwargs["thumbnail"]

    def is_active(self, date: Optional[datetime] = None) -> bool:
        """
        Check if a slide is still within the desired date range.

        Parameters
        ----------
        date
            Check if slide is active at a specific moment, otherwise
            use the current time.

        Returns
        -------
        active
            True if the slide is within any specified date range.
        """
        if date is None:
            date = datetime.now()

        # Too early
        if self.valid_from and date < self.valid_from:
            return False
        # Too late
        if self.valid_to and date > self.valid_to:
            return False

        return True

    def to_view(self) -> Dict:
        """
        Export parts of the slide required for view functions.

        The ``widget` is pulled from the content to determine the
        correct `display` endpoint.
        The `display_time` is read if it has been set.
        The clock is added if it is enabled on the slide.

        Returns
        -------
        view
            A dictionary with `components` and, if set, a `display_time` key.
        """

        view = {"components": []}

        # Clock is at the top of a page, if required.
        # Blank "aside" is required for positioning some slides (e.g. events)
        if self.show_clock is not False:
            view["components"].append(CLOCK_PANEL)
        else:
            view["components"].append(("aside", []))

        widget = self.content.get("widget", "text")
        if widget == "calendar":
            display = "display.upcoming.full"
        elif widget == "web_page":
            display = "display.web_page.full"
        elif widget == "bookings":
            display = "display.bookings.full"
        else:
            display = "display.deckchair.full"

        # Pass the `id` as the only parameter of the slide, full contents
        # will be pulled from the database upon rendering. Components get
        # passed to the display templates.
        components = ("section", [(display, {"slide": self.id, "origin": self.origin})])
        view["components"].append(components)

        # Assume numerical value from the database
        if self.display_time:
            view["display_time"] = self.display_time

        return view

    def to_dict_shared(self):
        """
        Create a reduced representation of the slide with only info
        required for federation.

        Does not include nested fields like permissions, only enough
        to identify and render the slide. Adds the origin so that it can
        be referenced from external sources.

        Returns
        -------
        slide
            A restricted dictionary view of the slide, with origin.
        """
        return {
            "id": self.id,
            "name": self.name,
            "origin": current_app.config["HOSTNAME"],
            "description": self.description,
            "content": self.content,
            "display_time": self.display_time,
            "valid_from": self.valid_from,
            "valid_to": self.valid_to,
            "show_clock": bool(self.show_clock),
        }


class SlideGroup(Base, ToDictMixin):
    """
    A set of slides that are treated as a unit. Order in which they will
    be displayed is a property of the group.
    """

    __tablename__ = "slide_groups"
    origin = None

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(Text)
    visibility = Column(Enum(Visibility), default="public", nullable=False)
    slides = relationship(
        "Slide", back_populates="group", cascade="all, delete"
    )  # type: List[Slide]
    order = Column(ListType, default=[], nullable=False)
    user_permissions = relationship(
        "User", secondary=permissions, back_populates="slide_group_permissions"
    )
    user_group_permissions = relationship(
        "UserGroup", secondary=permissions, back_populates="slide_group_permissions"
    )
    rules = relationship("Rule", secondary=display_rules, back_populates="slide_groups")

    @hybrid_property
    def oid(self) -> str:
        """
        Object identifier, includes the object type as well as the
        database assigned id value for use where lists of mixed values
        are used for sorting.
        """
        if self.origin is not None:
            return "SlideGroupExternal:{}@{}".format(self.id, self.origin)
        else:
            return "SlideGroup:{}".format(self.id)

    def update_scalar(self, **kwargs: Any) -> None:
        """
        Update using all the keys in kwargs that are valid scalar
        properties of a SlideGroup.

        None and blank values where keys are explicitly given
        will result in NULL and empty fields.
        """
        if "name" in kwargs:
            self.name = strip(kwargs["name"])
        if "description" in kwargs:
            self.description = strip(kwargs["description"])
        if "visibility" in kwargs:
            self.visibility = strip(kwargs["visibility"])

    def add_slide(self, slide: Slide) -> None:
        """
        Add a slide to the group and add it to the end of the
        order list.

        Parameters
        ----------
        slide
            The slide to add to this group.
        """
        self.slides.append(slide)
        self.order.append(slide.id)

    def remove_slide(self, slide: Slide) -> None:
        """
        Remove the slide from the slide list.

        Parameters
        ----------
        slide
            The Slide to remove from this group.
        """
        self.slides.remove(slide)
        while slide.id in self.order:
            self.order.remove(slide.id)

    def slides_ordered(self) -> List[Slide]:
        """Slides contained in the group in their set ordering."""
        # Sort by the list of indexes
        try:
            return sorted(self.slides, key=lambda slide: self.order.index(slide.id))
        except ValueError:  # Fallback if order is corrupt
            return self.slides

    def to_dict_shared(self):
        """
        Create a reduced representation with the minimum info for federation.

        Does not include any nested items, such as permissions, only
        enough to identify and render the group.

        Returns
        -------
        slide_group
            A restricted dictionary view of the group, including the slide
            list and origin.
        """
        return {
            "id": self.id,
            "origin": current_app.config["HOSTNAME"],
            "name": self.name,
            "description": self.description,
            "order": self.order,
            "slides": [slide.to_dict_shared() for slide in self.slides],
        }

    @property
    def is_valid(self) -> bool:
        """
        Check the integrity of the object before committing it to the
        database.

        Returns
        -------
        valid
            True if the object has all the required non-empty
            fields, otherwise False.
        """
        if not self.name:
            return False
        # nothing wrong
        return True


class Display(Base, ToDictMixin):
    """
    A single display. Identified by the ident field, which is
    most likely the IP address that it takes.
    """

    __tablename__ = "displays"

    id = Column(Integer, primary_key=True)
    ident = Column(String, unique=True, index=True)
    name = Column(String)
    description = Column(Text)
    groups = relationship(
        "DisplayGroup", secondary=display_groupings, back_populates="displays"
    )
    user_permissions = relationship(
        "User", secondary=permissions, back_populates="display_permissions"
    )
    user_group_permissions = relationship(
        "UserGroup", secondary=permissions, back_populates="display_permissions"
    )
    rules = relationship("Rule", secondary=display_rules, back_populates="displays")

    def update_scalar(self, **kwargs: Any) -> None:
        """
        Update using all the keys in kwargs that are valid scalar
        properties of a Display.

        None and blank values where keys are explicitly given
        will result in NULL and empty fields.
        """
        if "ident" in kwargs:
            self.ident = strip(kwargs["ident"])
        if "name" in kwargs:
            self.name = strip(kwargs["name"])
        if "description" in kwargs:
            self.description = strip(kwargs["description"])

    @property
    def is_valid(self) -> bool:
        """
        Check the integrity of the object before committing it to the
        database.

        Returns
        -------
        valid
            True if the object has all the required non-empty
            fields, otherwise False.
        """
        if not self.name or not self.name.strip():
            return False
        try:
            ip_address(self.ident)
        except ValueError:
            return False
        # nothing wrong
        return True

    def slide_rotation(self, date: Optional[datetime] = None) -> List[Slide]:
        """
        Determine the list of active slides for the screen based
        on the display rules managing the display and any
        groups that it is a member of.

        Parameters
        ----------
        date
            Get the rules for a specific moment. If unset, will default
            to `now`.

        Returns
        -------
        slides
            The list of active slides in the current rotation
        """
        # Generate a list of all the unique rules for the display
        all_rules = []
        all_rules.extend(self.rules)
        for group in self.groups:
            for rule in group.rules:
                if rule not in all_rules:
                    all_rules.append(rule)

        # Get the active slide lists and concatenate them
        all_slides = []
        # If any active rules are marked exclusive, then only show them
        exclusive_slides = []
        # Ensure rules are always the same order
        all_rules.sort(key=lambda x: x.id)
        # Rules -> slides
        for rule in all_rules:
            # prune any inactive slides
            slides = [
                slide for slide in rule.get_slides(date=date) if slide.is_active(date)
            ]
            all_slides.extend(slides)
            if rule.exclusive:
                exclusive_slides.extend(slides)

        # Exclusive list takes priority
        return exclusive_slides or all_slides


class DisplayGroup(Base, ToDictMixin):
    """
    A collection of displays that should be considered together.
    Displays are not ordered in any way and can exist outside of
    a group or in multiple groups.
    """

    __tablename__ = "display_groups"

    id = Column(Integer, primary_key=True)  # internal identifier
    name = Column(String)
    description = Column(Text)
    displays = relationship(
        "Display", secondary=display_groupings, back_populates="groups"
    )
    user_permissions = relationship(
        "User", secondary=permissions, back_populates="display_group_permissions"
    )
    user_group_permissions = relationship(
        "UserGroup", secondary=permissions, back_populates="display_group_permissions"
    )
    rules = relationship(
        "Rule", secondary=display_rules, back_populates="display_groups"
    )

    def update_scalar(self, **kwargs: Any) -> None:
        """
        Update using all the keys in kwargs that are valid scalar
        properties of a DisplayGroup.

        None and blank values where keys are explicitly given
        will result in NULL and empty fields.
        """
        if "name" in kwargs:
            self.name = strip(kwargs["name"])
        if "description" in kwargs:
            self.description = strip(kwargs["description"])

    @property
    def is_valid(self) -> bool:
        """
        Check the integrity of the object before committing it to the
        database.

        Returns
        -------
        valid
            True if the object has all the required non-empty
            fields, otherwise False.
        """
        if not self.name or not self.name.strip():
            return False
        # nothing wrong
        return True


class User(Base, ToDictMixin):
    """
    A user is identified by their CRSid. Exist in the database to grant
    permissions and link to slides.
    """

    __tablename__ = "users"

    id = Column(String, primary_key=True, nullable=False)
    crsid = synonym("id")
    name = Column(String)
    admin = Column(Boolean, default=False, nullable=False)
    groups = relationship("UserGroup", secondary=user_groupings, back_populates="users")
    slide_group_permissions = relationship(
        "SlideGroup", secondary=permissions, back_populates="user_permissions"
    )
    display_group_permissions = relationship(
        "DisplayGroup", secondary=permissions, back_populates="user_permissions"
    )
    display_permissions = relationship(
        "Display", secondary=permissions, back_populates="user_permissions"
    )

    def update_scalar(self, **kwargs: Any) -> None:
        """
        Update using all the keys in kwargs that are valid scalar
        properties of a User.

        None and blank values where keys are explicitly given
        will result in NULL and empty fields.
        """
        if "name" in kwargs:
            self.name = strip(kwargs["name"])
        if "admin" in kwargs:
            self.admin = kwargs["admin"]

    @property
    def all_slide_group_permissions(self) -> List[SlideGroup]:
        """Slide group permissions, including inherited from user groups."""
        return merge(
            self.slide_group_permissions,
            *(group.slide_group_permissions for group in self.groups)
        )

    @property
    def all_display_group_permissions(self) -> List[DisplayGroup]:
        """Display group permissions, including inherited from user groups."""
        return merge(
            self.display_group_permissions,
            *(group.display_group_permissions for group in self.groups)
        )

    @property
    def all_display_permissions(self) -> List[Display]:
        """Display permissions, including inherited from user groups."""
        return merge(
            self.display_permissions,
            *(group.display_permissions for group in self.groups)
        )

    @property
    def all_rule_permissions(self) -> List["Rule"]:
        """Rule permissions, linked to display permissions"""
        return merge(
            *(display.rules for display in self.all_display_permissions),
            *(group.rules for group in self.all_display_group_permissions),
            *(
                display.rules
                for group in self.all_display_group_permissions
                for display in group.displays
            )
        )

    def promote(self) -> None:
        """Make user an admin"""
        self.admin = True

    def demote(self) -> None:
        """Make user a regular user"""
        self.admin = False

    def has_permissions(
        self, obj: Union[Slide, SlideGroup, DisplayGroup, Display, "Rule"]
    ) -> bool:
        """
        Check whether the user is authorised to modify the given object.

        Admin users are allowed to modify anything. Anyone else must have
        permissions listed either locally to the user or in a group they
        are a member of.

        Parameters
        ----------
        obj
            Any database object
        """
        if self.admin:
            return True
        elif isinstance(obj, Slide):
            if obj.group in self.slide_group_permissions:
                return True
            for group in self.groups:
                if obj.group in group.slide_group_permissions:
                    return True
            return False
        elif isinstance(obj, SlideGroup):
            if obj in self.slide_group_permissions:
                return True
            for group in self.groups:
                if obj in group.slide_group_permissions:
                    return True
            return False
        elif isinstance(obj, Display):
            if obj in self.display_permissions:
                return True
            for group in self.groups:
                if obj in group.display_permissions:
                    return True
            for display_group in obj.groups:
                if display_group in self.display_group_permissions:
                    return True
                for group in self.groups:
                    if display_group in group.display_group_permissions:
                        return True
            return False
        elif isinstance(obj, DisplayGroup):
            if obj in self.display_group_permissions:
                return True
            for group in self.groups:
                if obj in group.display_group_permissions:
                    return True
            return False
        elif isinstance(obj, Rule):
            if any(self.has_permissions(display) for display in obj.displays) or any(
                self.has_permissions(display_group)
                for display_group in obj.display_groups
            ):
                return True
            return False

        else:
            return False

    def has_read(self, obj: Union[SlideGroup]) -> bool:
        """
        Check whether the user is authorised to read the given object.

        Read permissions are more relaxed than write permissions, but some
        objects like SlideGroups can be restricted to only be shown to
        editors.

        Parameters
        ----------
        obj
            Any database object
        """
        if self.admin:
            return True
        elif isinstance(obj, SlideGroup):
            if obj.visibility in [Visibility.share, Visibility.public]:
                return True
            elif obj.visibility in [Visibility.local]:
                if self.crsid:
                    return True
                else:
                    return False
            elif obj in self.slide_group_permissions:
                return True
            else:
                for group in self.groups:
                    if obj in group.slide_group_permissions:
                        return True
                return False
        else:
            return True


class UserGroup(Base, ToDictMixin):
    """
    User groups can be used to assign permissions to a subset of people.
    """

    __tablename__ = "user_groups"

    id = Column(String, default=lambda: uuid4().hex[:8], primary_key=True)
    name = Column(String)
    description = Column(Text)
    users = relationship("User", secondary=user_groupings, back_populates="groups")
    slide_group_permissions = relationship(
        "SlideGroup", secondary=permissions, back_populates="user_group_permissions"
    )
    display_group_permissions = relationship(
        "DisplayGroup", secondary=permissions, back_populates="user_group_permissions"
    )
    display_permissions = relationship(
        "Display", secondary=permissions, back_populates="user_group_permissions"
    )

    def update_scalar(self, **kwargs: Any) -> None:
        """
        Update using all the keys in kwargs that are valid scalar
        properties of a UserGroup.

        None and blank values where keys are explicitly given
        will result in NULL and empty fields.
        """
        if "name" in kwargs:
            self.name = strip(kwargs["name"])
        if "description" in kwargs:
            self.description = strip(kwargs["description"])

    @property
    def is_valid(self) -> bool:
        """
        Check the integrity of the object before committing it to the
        database.

        Returns
        -------
        valid
            True if the object has all the required non-empty
            fields, otherwise False.
        """
        if not self.name or not self.name.strip():
            return False
        # nothing wrong
        return True


class Rule(Base, ToDictMixin):
    """
    Relationship between slides, groups of slides, displays and groups
    of displays.
    """

    __tablename__ = "rules"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(Text)
    start_date = Column(DateTime)
    end_date = Column(DateTime)
    active = Column(Boolean, default=True, nullable=False)
    # Days use iso numbering, Mon=1 ... Sun=7, default to all
    active_days = Column(ListType, default=[1, 2, 3, 4, 5, 6, 7], nullable=False)
    active_time_start = Column(Time)
    active_time_end = Column(Time)
    # Can this rule override all others
    exclusive = Column(Boolean, default=False, nullable=True)
    order = Column(ListType, default=[], nullable=False)
    allow_duplicates = Column(Boolean, default=True, nullable=False)
    displays = relationship("Display", secondary=display_rules, back_populates="rules")
    display_groups = relationship(
        "DisplayGroup", secondary=display_rules, back_populates="rules"
    )
    slides = relationship("Slide", secondary=display_rules, back_populates="rules")
    slide_groups = relationship(
        "SlideGroup", secondary=display_rules, back_populates="rules"
    )

    def update_scalar(self, **kwargs: Any) -> None:
        """
        Update using all the keys in kwargs that are valid scalar
        properties of a Rule.

        None and blank values where keys are explicitly given
        will result in NULL and empty fields.
        """
        if "name" in kwargs:
            self.name = strip(kwargs["name"])
        if "description" in kwargs:
            self.description = strip(kwargs["description"])
        if "start_date" in kwargs:
            self.start_date = parse_date(kwargs["start_date"])
        if "end_date" in kwargs:
            self.end_date = parse_date(kwargs["end_date"])
        if "active" in kwargs:
            self.active = kwargs["active"]
        if "active_days" in kwargs:
            self.active_days = [int(x) for x in kwargs["active_days"]]
        if "active_time_start" in kwargs:
            self.active_time_start = parse_time(kwargs["active_time_start"])
        if "active_time_end" in kwargs:
            self.active_time_end = parse_time(kwargs["active_time_end"])
        if "exclusive" in kwargs:
            self.exclusive = kwargs["exclusive"]

    def add_slide_or_group(self, member: Union[Slide, SlideGroup]) -> None:
        """
        Add a slide or a slide group to the list of members.
        And update the order to match.

        Parameters
        ----------
        member
            Either a Slide or a SlideGroup to add to the rotation.
        """
        # External member are only added to the order
        if member.origin:
            self.order.append(member.oid)
        elif isinstance(member, Slide):
            self.slides.append(member)
            self.order.append(member.oid)
        elif isinstance(member, SlideGroup):
            self.slide_groups.append(member)
            self.order.append(member.oid)
        else:
            raise ValueError

    def remove_slide_or_group(self, member: Union[Slide, SlideGroup]) -> None:
        """
        Remove a slide or slide group from the appropriate list
        of members and purge it from the order list.

        Parameters
        ----------
        member
            Either a Slide or SlideGroup to disassociate from this rule.
        """
        if isinstance(member, Slide) and member in self.slides:
            self.slides.remove(member)
        elif isinstance(member, SlideGroup) and member in self.slide_groups:
            self.slide_groups.remove(member)
        elif member is None or member.origin is not None:
            # External slide or no longer accessible
            return
        else:
            raise ValueError

        # Ensure the member is no longer referenced by the rule
        while member.oid in self.order:
            self.order.remove(member.oid)

    def is_active(self, date: Optional[datetime] = None) -> bool:
        """Determine if anything is stopping the rule being active."""
        # Rule can be fully deactivated
        if not self.active:
            return False
        # Use current time if not looking at a specific timestamp
        if date is None:
            date = datetime.utcnow()
        # Too early
        if self.start_date and date < self.start_date:
            return False
        # Too late
        if self.end_date and date > self.end_date:
            return False
        # Only specific days
        if self.active_days and date.isoweekday() not in self.active_days:
            return False
        # If active hours are set, deactivate out of those hours
        if (
            self.active_time_start
            and self.active_time_end
            and self.active_time_end < self.active_time_start
        ):
            # End is before start, so inactive between them
            if self.active_time_end <= date.time() < self.active_time_start:
                return False
        elif self.active_time_start and date.time() < self.active_time_start:
            return False
        elif self.active_time_end and date.time() >= self.active_time_end:
            return False
        # Nothing to say it's not active, so we're active!
        return True

    def get_members(self) -> List[Union[Slide, SlideGroup]]:
        """
        Retrieve an ordered list of all members of the rule.
        """

        return slide_sorter(
            slides=self.slides,
            slide_groups=self.slide_groups,
            order=self.order,
            allow_duplicates=True,
            expand_groups=False,
        )

    def get_slides(self, date: Optional[datetime] = None) -> List[Slide]:
        """
        Retrieve the list of active slides.

        Will either be the full list of all slides or an empty list.
        Ordered is set by the order parameter.
        """
        # if the rule is not active then we get an empty list
        if not self.is_active(date):
            return []

        # Slide list is ordered internally and returned as such
        return slide_sorter(
            slides=self.slides,
            slide_groups=self.slide_groups,
            order=self.order,
            allow_duplicates=self.allow_duplicates,
            expand_groups=True,
        )

    def can_add_display(self, user: User) -> bool:
        """
        Determine if the user is allowed to add to the list of displays.

        To add a display, the user must have permissions for all the
        current displays, compared to editing which only requires
        permissions for one item.

        Parameters
        ----------
        user
            The user to check permissions for.

        Returns
        -------
        has_permission
            True if the user is allowed to add a display or group.
        """
        return all(
            user.has_permissions(group) for group in self.display_groups
        ) and all(user.has_permissions(display) for display in self.displays)

    @property
    def is_valid(self) -> bool:
        """
        Check the integrity of the object before committing it to the
        database.

        Returns
        -------
        valid
            True if the object has all the required non-empty
            fields, otherwise False.
        """
        if not self.name or not self.name.strip():
            return False
        # nothing wrong
        return True

    @property
    def has_display(self) -> bool:
        """
        Check if the rule has a display associated with it. A rule
        should not exist without one!

        Returns
        -------
        has_display
            True if there is one or more display-lik targets
            for the rule, otherwise False.
        """
        if self.display_groups or self.displays:
            return True
        else:
            return False


def slide_sorter(
    slides: List[Slide],
    slide_groups: List[SlideGroup],
    order: List[str],
    allow_duplicates: bool = True,
    expand_groups: bool = False,
) -> List[Union[Slide, SlideGroup]]:
    """
    Sort lists of slides and slide groups by the ordering key. Flatten the
    result into a list of slides, if desired.

    Since slides and groups can have the same id, the order is a list
    of `oid` values that identify the object type.

    Parameters
    ----------
    slides
        Individual slides.
    slide_groups
        Groups of slides with their own ordering. Order is preserved.
    order
        Required order of items as oid values.
    expand_groups
        Whether to produce a flat list of slides or just ordered items.
    allow_duplicates
        Whether to remove duplicates from the final list.

    Returns
    -------
    ordered_members
        Ordered list of either slides or slides and groups.
    """

    # Use a lookup table as we can have duplicates in order
    member_map = {item.oid: item for item in chain(slides, slide_groups)}
    external_slides = ExternalSlideManager()

    ordered_members = []
    for oid in order:
        # External members should be cached
        if re_oid.match(oid).group("origin") is not None:
            member = external_slides.get(oid)
        else:
            member = member_map.get(oid)

        # slides_ordered should work even on external members which
        # have no ID but are pre-sorted
        if expand_groups and hasattr(member, "slides_ordered"):
            ordered_members.extend(member.slides_ordered())
        elif member is not None:
            ordered_members.append(member)

    # Remove duplicates if required
    if allow_duplicates:
        return ordered_members
    else:
        pruned_members = []
        for member in ordered_members:
            if member not in pruned_members:
                pruned_members.append(member)
        return pruned_members


class SignageServer(Base, ToDictMixin):
    """
    A signage server with federated content.

    Keep track of the ones that are PATCH"known so that they can
    be regularly polled for content.
    """

    __tablename__ = "signage_servers"

    hostname = Column(String, primary_key=True)
    name = Column(String)
    description = Column(Text)
    blacklisted = Column(Boolean, default=False, nullable=False)
    trusted = Column(Boolean, default=False, nullable=False)
    trust_neighbours = Column(Boolean, default=False, nullable=False)
    source_neighbour = Column(String)
    neighbours = Column(ListType, default=[])
    slide_groups = Column(ListType, default=[])
    updated = Column(DateTime, default=datetime.utcnow)
    online = Column(Boolean, default=False, nullable=False)

    def update_scalar(self, **kwargs: Any) -> None:
        """
        Update using all the keys in kwargs that are valid scalar
        properties of a RemoteSignage.

        None and blank values where keys are explicitly given
        will result in NULL and empty fields.
        """
        if "name" in kwargs:
            self.name = strip(kwargs["name"])
        if "description" in kwargs:
            self.description = strip(kwargs["description"])

    def to_public(self) -> Dict:
        """
        Cut down representation containing only identity info and
        the dictionary of slide groups.
        """
        return {
            "hostname": self.hostname,
            "name": self.name,
            "description": self.description,
            "slide_groups": self.slide_groups,
        }


class ExternalSlideManager:
    """
    A manager for slides hosted on an external instance.

    oid will be of the form

    SlideExternal:4@server.url
    """

    def __init__(self, cache: BaseCache = SimpleCache()):
        """
        Create a new manager for external slides.

        The cache is shared within the process unless another is given.
        Reduces external requests but does not need persistence.
        """
        self.cache = cache

    def get(self, oid: str) -> Optional[Union[Slide, SlideGroup]]:
        """Retrieve a single object, either a slide or slide group"""
        # Check if the response has been cached first
        if self.cache.has(oid):
            return self.cache.get(oid)
        else:
            return self.pull(oid)

    def pull(self, oid: str) -> Optional[Union[Slide, SlideGroup]]:
        """
        Get an object from an external signage instance

        Parameters
        ----------
        oid
            Will be of the form type:id@origin, all fields required

        Returns
        -------
        item
            Either a Slide instance or a populated SlideGroup
        """
        api = "http://{server}/api/{type}/{id}"
        item_class, item_id, item_origin = re_oid.match(oid).groups()

        # The actual object generated
        item = None

        # if there are any errors we just say there is no matching slide
        try:

            if item_class == "SlideExternal":
                url = api.format(server=item_origin, type="slides", id=item_id)
                resp = requests.get(url=url, allow_redirects=True, timeout=3.05)
                data = resp.json()["data"]
                item = Slide()
                item.update_scalar(**data)
                item.origin = item_origin
                item.id = item_id
                item.updated = parse_date(data["updated"])

            elif item_class == "SlideGroupExternal":
                url = api.format(server=item_origin, type="slide_groups", id=item_id)
                resp = requests.get(url=url, allow_redirects=True, timeout=3.05)
                data = resp.json()["data"]
                item = SlideGroup()
                item.update_scalar(**data)
                item.order = data["order"]
                item.id = item_id
                item.origin = item_origin
                for slide in data["slides"]:
                    new_slide = Slide()
                    new_slide.update_scalar(**slide)
                    new_slide.id = slide["id"]
                    new_slide.origin = item_origin
                    new_slide.updated = parse_date(slide["updated"])
                    item.slides.append(new_slide)

        except (OSError, ValueError, KeyError):
            # Covers RequestException and any malformed data in the response
            item = None

        # Cache the result, even if it is a failure so that if remote server
        # is down it will not need to timeout every request, only every
        # few minutes. timeout is randomly generated to stagger requests
        self.cache.set(oid, item, timeout=randint(120, 1800))

        return item

    def slide(self, slide_id, origin) -> Optional[Slide]:
        """Get a single slide from the individual id and origin"""
        oid = "SlideExternal:{}@{}".format(slide_id, origin)
        return self.get(oid)

    def slide_group(self, group_id, origin) -> Optional[SlideGroup]:
        """Get a single slide group from the individual id and origin"""
        oid = "SlideGroupExternal:{}@{}".format(group_id, origin)
        return self.get(oid)


class SlideDatabase:
    """Abstraction for the slide database."""

    def __init__(self, app: Optional[Flask] = None, db_url: Optional[str] = None):
        if app is not None:
            db_url = app.config.get("SLIDE_DB", "sqlite:///slides.sqlite3")
        elif db_url is None:
            db_url = "sqlite:///slides.sqlite3"

        # stop thread errors
        if db_url.startswith("sqlite://"):
            connect_args = {"check_same_thread": False}
        else:
            connect_args = {}

        # Properties accessible as attributes
        self.url = db_url
        self.base = Base
        # Engine used to connect the the database
        self.engine = create_engine(db_url, connect_args=connect_args)
        # Bind a session instance to the instance -- easier to query itself
        self.Session = scoped_session(sessionmaker(bind=self.engine, autoflush=True))

        if app is not None:
            app.slide_db = self

    def __del__(self):
        self.Session.remove()


class SlideManager:
    """
    Higher level manipulation of the slide database.

    Exposes the operations required to interact with the database.
    Implemented functions should check permissions and will
    generally return items serialised to dicts.
    """

    def __init__(self, database: SlideDatabase, user: Optional[str] = None):
        """Connect (a user) to the database."""
        self.db = database
        self.session = database.Session()
        # Ensure a User is attached to the manager, even if they're not real
        current_user = user and self.session.query(User).get(user)
        if not current_user:
            current_user = User()
            current_user.crsid = user
        self.current_user = current_user
        # Connection to the wider internet
        self.external = ExternalSlideManager()

    def slide_or_group_by_oid(self, oid: str) -> Union[Slide, SlideGroup]:
        """
        Get either a Slide or a Slide group from the oid.

        Parameters
        ----------
        oid
            Unique identifier, e.g. "Slide:7"

        Returns
        -------
        item
            Either a Slide or a SlideGroup that matches the unique
            oid value.

        Raises
        ------
        ValueError
            The oid is not valid.
        KeyError
            The referenced item does not exist.
        """

        # ValueError if malformed
        item_class, item_id, _ = re_oid.match(oid).groups()

        if item_class == "Slide":
            item = self.session.query(Slide).get(item_id)
        elif item_class == "SlideGroup":
            item = self.session.query(SlideGroup).get(item_id)
        elif item_class == "SlideExternal":
            item = self.external.get(oid)
        elif item_class == "SlideGroupExternal":
            item = self.external.get(oid)
        else:
            raise ValueError("Invalid oid: {}".format(oid))

        if item is None:
            raise KeyError

        return item

    def slides(self) -> List[Dict]:
        """
        A list of slides as dictionaries.

        Returns
        -------
        slides
            List of Slides converted to dict.
        """
        # Just get all slides
        slides = self.session.query(Slide).all()

        return [slide.to_dict() for slide in slides]

    def slide(self, slide_id: Union[int, str], origin: Optional[str] = None) -> Dict:
        """
        Dictionary representation of a single slide.

        Parameters
        ----------
        slide_id
            The slide ID as assigned in the database.
        origin
            The server that hosts the slide if not local

        Returns
        -------
        slide
            The Slide converted to a dict.

        Raises
        ------
        KeyError
            The slide is not found in the database.
        """
        if origin is not None:
            slide = self.external.slide(slide_id, origin)
        else:
            slide = self.session.query(Slide).get(slide_id)

        if slide is None:
            raise KeyError
        else:
            return slide.to_dict()

    def slide_update(self, slide_id: int, data: Dict[str, Any]) -> Dict:
        """
        Modify the slide data of the given slide.

        Parameters
        ----------
        slide_id
            The ID of the slide to be updated.
        data
            The dictionary of values to be updated.

        Returns
        -------
        slide
            The Slide with updated values.

        Raises
        ------
        KeyError
            The slide is not found in the database.
        """
        # Slide must already exist in the database
        slide = self.session.query(Slide).get(slide_id)
        if slide is None:
            raise KeyError

        if not self.current_user.has_permissions(slide):
            raise PermissionError

        # Pass all the values and the scalar values will be picked out
        slide.update_scalar(**data)

        # The slide group is the only editable relationship
        if "group" in data and data["group"] != slide.group.id:
            old_group = slide.group
            new_group = self.session.query(SlideGroup).get(data["group"])
            if new_group is None:
                raise ValueError
            if not self.current_user.has_permissions(new_group):
                raise PermissionError
            old_group.remove_slide(slide)
            new_group.add_slide(slide)

        # Record which user made changes
        slide.updated_by = self.current_user
        slide.updated = datetime.utcnow()

        self.session.add(slide)
        self.session.commit()

        return slide.to_dict()

    def slide_delete(self, slide_id: int) -> Dict:
        """
        Completely remove the slide from the database.

        Parameters
        ----------
        slide_id
            The ID of the slide to be deleted.

        Returns
        -------
        slide
            The deleted slide.

        Raises
        ------
        KeyError
            The slide is not found in the database.
        """
        slide = self.session.query(Slide).get(slide_id)
        if slide is None:
            raise KeyError

        # permission check
        if not self.current_user.has_permissions(slide):
            raise PermissionError

        # Remove from group to keep order consistent
        slide.group.remove_slide(slide)

        # Ensure slide is not included in any rule orders
        for rule in slide.rules:
            rule.remove_slide_or_group(slide)

        self.session.delete(slide)
        self.session.commit()

        # No relationships as object is in the bin
        return slide.to_dict(max_depth=0)

    def slide_groups(self) -> List[Dict]:
        """
        Get a list of all accessible slide groups.

        Returns
        -------
        slide_groups
            List of SlideGroups converted to dicts.
        """

        if self.current_user.admin:
            # Admin sees all slide groups
            groups = self.session.query(SlideGroup)
        elif not self.current_user.crsid:
            # User not logged in, public only
            groups = self.session.query(SlideGroup).filter(
                SlideGroup.visibility.in_([Visibility.share, Visibility.public])
            )
        else:
            # User can see their own groups, and anything else not private
            groups = self.current_user.all_slide_group_permissions
            query = self.session.query(SlideGroup).filter(
                SlideGroup.visibility != Visibility.private
            )
            # Don't duplicate groups
            for slide_group in query:
                if slide_group not in groups:
                    groups.append(slide_group)

        # Always return in the same order
        groups = sorted(groups, key=lambda group: group.name)

        return [slide_group.to_dict() for slide_group in groups]

    def slide_groups_shared(self) -> List[Dict]:
        """
        Get the list of groups that should be advertised across the wider
        install base of signage.

        Returns
        -------
        slide_groups
            List of SlideGroups with shared visibility converted to dict.
        """

        # No permissions check as these are shared globally
        groups = self.session.query(SlideGroup).filter(
            SlideGroup.visibility == Visibility.share
        )

        return [slide_group.to_dict_shared() for slide_group in groups]

    def slide_group_create(self, data: Dict[str, Any]) -> Dict:
        """
        Create a new slide group using the given parameters.

        Parameters
        ----------
        data
            The dictionary of values to use to create the slide
            group.

        Returns
        -------
        slide_group
            The dictionary representation of the new slide group.
        """

        if not self.current_user.admin:
            raise PermissionError

        new_slide_group = SlideGroup()
        new_slide_group.update_scalar(**data)

        if not new_slide_group.is_valid:
            raise ValueError

        self.session.add(new_slide_group)
        self.session.commit()

        return new_slide_group.to_dict()

    def slide_group(self, group_id: int) -> Dict:
        """
        Dictionary representation of a single slide group.

        Parameters
        ----------
        group_id
            The slide group ID as assigned in the database.

        Returns
        -------
        slide_group
            The SlideGroup converted to a dict.

        Raises
        ------
        KeyError
            The slide group is not found in the database.
        """
        slide_group = self.session.query(SlideGroup).get(group_id)
        if slide_group is None:
            raise KeyError
        elif self.current_user.has_read(slide_group):
            return slide_group.to_dict()
        else:
            raise PermissionError

    def slide_group_update(self, group_id: int, data: Dict[str, Any]) -> Dict:
        """
        Modify the data of a slide group.

        Parameters
        ----------
        group_id
            The ID of the slide group to be updated.
        data
            The dictionary of values to be updated.

        Returns
        -------
        slide_group
            The SlideGroup with updated values.

        Raises
        ------
        KeyError
            The slide group doesn't exist.
        """
        if not self.current_user.admin:
            raise PermissionError

        slide_group = self.session.query(SlideGroup).get(group_id)
        if slide_group is None:
            raise KeyError

        slide_group.update_scalar(**data)
        if not slide_group.is_valid:
            raise ValueError

        self.session.add(slide_group)
        self.session.commit()

        return slide_group.to_dict()

    def slide_group_delete(self, group_id):
        """
        Completely remove the slide group and all its slides
        from the database.

        Parameters
        ----------
        group_id
            The ID of the slide group to be deleted.

        Returns
        -------
        slide_group
            The deleted slide group.

        Raises
        ------
        KeyError
            The slide group does not exist.
        """
        if not self.current_user.admin:
            raise PermissionError

        slide_group = self.session.query(SlideGroup).get(group_id)
        if slide_group is None:
            raise KeyError

        # Remove any of the contained slides from any rules
        for slide in slide_group.slides:
            for rule in slide.rules:
                rule.remove_slide_or_group(slide)

        # Remove the slide group from any rules
        for rule in slide_group.rules:
            rule.remove_slide_or_group(slide_group)

        self.session.delete(slide_group)
        self.session.commit()

        return slide_group.to_dict(max_depth=0)

    def slide_group_slides_create(self, group_id: int, data: Dict[str, Any]) -> Dict:
        """
        Create a new slide within a slide group.

        All slides must be associated with a slide group and managed in
        that group to keep their order maintained. The slide will be
        added to the end of the slide order.

        Parameters
        ----------
        group_id
            The ID of the slide group within which to create the new
            slide.
        data
            The properties of the slide to be created.

        Returns
        -------
        slide
            The newly created slide.

        Raises
        ------
        KeyError
            The slide group does not exist.
        """
        slide_group = self.session.query(SlideGroup).get(group_id)
        if slide_group is None:
            raise KeyError

        if not self.current_user.has_permissions(slide_group):
            raise PermissionError

        new_slide = Slide()
        # Insert the supplied data
        new_slide.update_scalar(**data)
        # creation info
        db_user = self.current_user
        new_slide.created_by = db_user
        new_slide.created = datetime.utcnow()
        # update info
        new_slide.updated_by = db_user
        new_slide.updated = datetime.utcnow()

        # The slide must be flushed in the session before
        # adding to the group as it needs an id to be added
        # to the `order`
        self.session.add(new_slide)
        self.session.flush()
        # Can be added now it has an id
        slide_group.add_slide(new_slide)
        # Finalise creation
        self.session.commit()
        return new_slide.to_dict()

    def slide_group_order_change(self, group_id: int, data: Dict) -> List[int]:
        """
        Modify the order of slides in the group.

        Valid operations: "move", "insert", "replace". Move and insert
        change the location of a single slide and replace modifies the
        complete list.

        Keys that may be required in data:

        - `op` - the operation name
        - `value` - the payload for the operation
        - `id` - the id of the slide to move (if required)

        Parameters
        ----------
        group_id
            The ID of the slide group to re-order.
        data
            The description of the operation and the values to use
            when applying the operation. Must have an "op" key.

        Returns
        -------
        order
            The updated slide order.

        Raises
        ------
        KeyError
            The slide group does not exist.
        ValueError
            Invalid operation is specified.
        """
        slide_group = self.session.query(SlideGroup).get(group_id)
        if slide_group is None:
            raise KeyError

        if not self.current_user.has_permissions(slide_group):
            raise PermissionError

        order = slide_group.order[:]
        slide = data.get("id", None)
        value = data.get("value", None)

        # Slide must be in the list if given
        if slide is not None and slide not in order:
            order.append(slide)

        if data["op"] == "move":
            # Don't go below 0 as that wraps round, just put it
            # at the beginning. Values larger than the length of
            # the list just go to the end.
            position = order.index(slide)
            order.remove(slide)
            order.insert(max(position + value, 0), slide)
        elif data["op"] == "insert":
            # insert exactly like Python list
            order.remove(slide)
            order.insert(value, slide)
        elif data["op"] == "replace":
            # Completely replace the list
            order = value

        # Check order make sense
        valid_slides = [slide.id for slide in slide_group.slides]
        # Ensure all members of order exist
        order = [idx for idx in order if idx in valid_slides]
        # Ensure all slides included
        for idx in valid_slides:
            if idx not in order:
                order.append(idx)

        # Save the new order
        slide_group.order = order
        self.session.add(slide_group)
        self.session.commit()

        return order

    def displays(self) -> List[Dict]:
        """
        Get a list of all registered displays.

        Returns
        -------
        displays
            List of Displays converted to dicts.
        """
        displays = self.session.query(Display).order_by(Display.ident).all()

        return [display.to_dict() for display in displays]

    def display_create(self, data: Dict[str, Any]) -> Dict:
        """
        Create a new display using the given parameters.

        Parameters
        ----------
        data
            A dictionary of values to use to create the display.

        Returns
        -------
        display
            The dictionary representation of the newly created
            display.
        """
        if not self.current_user.admin:
            raise PermissionError

        new_display = Display()
        new_display.update_scalar(**data)

        if not new_display.is_valid:
            raise ValueError

        for group_id in data.get("groups") or []:
            group = self.session.query(DisplayGroup).get(group_id)
            if group is not None:
                new_display.groups.append(group)

        try:
            self.session.add(new_display)
            self.session.commit()
        except IntegrityError:
            self.session.rollback()
            raise ValueError("Duplicate display IP")

        return new_display.to_dict()

    def display(self, display_id: int) -> Dict:
        """
        Dictionary representation of a single display.

        Parameters
        ----------
        display_id
            The display ID as assigned in the database.

        Returns
        -------
        display
            The display converted to a dict.

        Raises
        ------
        KeyError
            The display is not found in the database.
        """

        display = self.session.query(Display).get(display_id)
        if display is None:
            raise KeyError
        else:
            return display.to_dict()

    def display_update(self, display_id: int, data: Dict[str, Any]) -> Dict:
        """
        Modify the data for a display.

        Parameters
        ----------
        display_id
            The ID of the display to be updated.
        data
            The dictionary of values to be updated.

        Returns
        -------
        display
            The Display with updated values.

        Raises
        ------
        KeyError
            The display does not exist.
        """
        if not self.current_user.admin:
            raise PermissionError

        display = self.session.query(Display).get(display_id)
        if display is None:
            raise KeyError

        display.update_scalar(**data)

        if not display.is_valid:
            raise ValueError

        try:
            self.session.add(display)
            self.session.commit()
        except IntegrityError:
            self.session.rollback()
            raise ValueError("Duplicate display IP")

        return display.to_dict()

    def display_delete(self, display_id: int) -> Dict:
        """
        Completely remove the display from the database.

        Parameters
        ----------
        display_id
            The ID of the display to be deleted.

        Returns
        -------
        display
            The deleted display.

        Raises
        ------
        KeyError
            The display does not exist.
        """
        if not self.current_user.admin:
            raise PermissionError

        display = self.session.query(Display).get(display_id)
        if display is None:
            raise KeyError

        self.session.delete(display)
        self.session.commit()

        return display.to_dict(max_depth=0)

    def display_display_groups_patch(self, display_id: int, data: Dict) -> List[Dict]:
        """
        Update the list of display groups for a display according to
        the given operations.

        Parameters
        ----------
        display_id
            The ID of the display to be modified.
        data
            The description of the operation and the values to use
            when applying the operation. Must contain an "op" key.

        Returns
        -------
        groups
            The updated list of display groups
        """
        display = self.session.query(Display).get(display_id)
        if display is None:
            raise KeyError

        if data["op"] == "add":
            group = self.session.query(DisplayGroup).get(data["id"])
            if group is None:
                raise KeyError
            if not self.current_user.has_permissions(group):
                raise PermissionError
            if group in display.groups:
                raise ValueError
            display.groups.append(group)
            self.session.add(display)
            self.session.commit()
        elif data["op"] == "remove":
            group = self.session.query(DisplayGroup).get(data["id"])
            if group in display.groups:
                if not self.current_user.has_permissions(group):
                    raise PermissionError
                display.groups.remove(group)
                self.session.add(display)
                self.session.commit()

        return [group.to_dict() for group in display.groups]

    def display_groups(self) -> List[Dict]:
        """
        Get a list of all display groups.

        Returns
        -------
        display_groups
            List of DisplayGroups converted to dicts.
        """
        # Just get all the groups
        display_groups = self.session.query(DisplayGroup).all()

        return [display_group.to_dict() for display_group in display_groups]

    def display_group_create(self, data: Dict[str, Any]) -> Dict:
        """
        Create a new display group using the given parameters.

        Parameters
        ----------
        data
            The dictionary of values to use to create the display
            group.

        Returns
        -------
        display_group
            The dictionary representation of the new display group.
        """
        if not self.current_user.admin:
            raise PermissionError

        new_display_group = DisplayGroup()
        new_display_group.update_scalar(**data)

        if not new_display_group.is_valid:
            raise ValueError

        self.session.add(new_display_group)
        self.session.commit()

        return new_display_group.to_dict()

    def display_group(self, group_id: int) -> Dict:
        """
        Dictionary representation of a single display group.

        Parameters
        ----------
        group_id
            The display group ID as assigned in the database.

        Returns
        -------
        display_group
            The DisplayGroup converted to a dict.

        Raises
        ------
        KeyError
            The display group is not found in the database.
        """
        display_group = self.session.query(DisplayGroup).get(group_id)
        if display_group is None:
            raise KeyError
        else:
            return display_group.to_dict(max_depth=2)

    def display_group_update(self, group_id: int, data: Dict) -> Dict:
        """
        Modify the data of a display group.

        Parameters
        ----------
        group_id
            The ID of the display group to be updated.
        data
            The dictionary of values to be updated.

        Returns
        -------
        display_group
            The DisplayGroup with updated values.

        Raises
        ------
        KeyError
            The display group doesn't exist.
        """
        if not self.current_user.admin:
            raise PermissionError

        display_group = self.session.query(DisplayGroup).get(group_id)
        if display_group is None:
            raise KeyError

        display_group.update_scalar(**data)

        if not display_group.is_valid:
            raise ValueError

        self.session.add(display_group)
        self.session.commit()

        return display_group.to_dict()

    def display_group_delete(self, group_id: int) -> Dict:
        """
        Completely remove the slide group from the database.

        Parameters
        ----------
        group_id
            The ID of the display group to be deleted.

        Returns
        -------
        display_group
            The deleted display group.

        Raises
        ------
        KeyError
            The display group does not exist.
        """
        if not self.current_user.admin:
            raise PermissionError

        display_group = self.session.query(DisplayGroup).get(group_id)
        if display_group is None:
            raise KeyError

        self.session.delete(display_group)
        self.session.commit()

        return display_group.to_dict(max_depth=0)

    def display_group_displays_patch(self, group_id: int, data: Dict) -> List[Dict]:
        """
        Update the list of displays for a display group according to
        the given operations.

        Parameters
        ----------
        group_id
            The ID of the display group to be modified.
        data
            The description of the operation and the values to use
            when applying the operation. Must contain an "op" key.

        Returns
        -------
        displays
            The updated list of displays
        """
        if not self.current_user.admin:
            raise PermissionError

        display_group = self.session.query(DisplayGroup).get(group_id)
        if display_group is None:
            raise KeyError

        if data["op"] == "add":
            display = self.session.query(Display).get(data["id"])
            if display is None:
                raise KeyError
            if display in display_group.displays:
                raise ValueError
            display_group.displays.append(display)
            self.session.add(display_group)
            self.session.commit()
        elif data["op"] == "remove":
            display = self.session.query(Display).get(data["id"])
            if display in display_group.displays:
                display_group.displays.remove(display)
                self.session.add(display_group)
                self.session.commit()

        return [display.to_dict() for display in display_group.displays]

    def rules(self) -> List[Dict]:
        """
        Get a list of all rules in the database.

        Returns
        -------
        rules
            List of Rules converted to dicts.
        """
        rules = self.session.query(Rule).all()

        return [rule.to_dict(max_depth=2) for rule in rules]

    def rule_create(self, data: Dict[str, Any]) -> Dict:
        """
        Create a new rule with the given parameters.

        Parameters
        ----------
        data
            A dictionary of values to use to create the rule.

        Returns
        -------
        rule
            The dictionary representation of the newly created
            rule
        """
        new_rule = Rule()
        new_rule.update_scalar(**data)

        if not new_rule.is_valid:
            raise ValueError

        for display_group_id in data.get("display_groups") or []:
            display_group = self.session.query(DisplayGroup).get(display_group_id)
            if display_group is not None:
                new_rule.display_groups.append(display_group)

        for display_id in data.get("displays") or []:
            display = self.session.query(Display).get(display_id)
            if display is not None:
                new_rule.displays.append(display)

        if not new_rule.has_display:
            raise ValueError

        for display in new_rule.displays:
            if not self.current_user.has_permissions(display):
                raise PermissionError

        for display_group in new_rule.display_groups:
            if not self.current_user.has_permissions(display_group):
                raise PermissionError

        self.session.add(new_rule)
        self.session.commit()

        return new_rule.to_dict()

    def rule(self, rule_id: int) -> Dict:
        """
        Dictionary representation of a single display rule.

        Parameters
        ----------
        rule_id
            The ID of the rule in the database.

        Returns
        -------
        rule
            The rule converted to a dict.

        Raises
        ------
        KeyError
            The rule is not found in the database.
        """

        rule = self.session.query(Rule).get(rule_id)
        if rule is None:
            raise KeyError
        else:
            return rule.to_dict(max_depth=2)

    def rule_update(self, rule_id: int, data: Dict[str, Any]) -> Dict:
        """
        Modify the data for a rule.

        Parameters
        ----------
        rule_id
            The ID in the database of the rule to modify.
        data
            A dictionary of values to be updated

        Returns
        -------
        rule
            The rule with updated values.

        Raises
        ------
        KeyError
            The rule does not exist.
        """
        rule = self.session.query(Rule).get(rule_id)
        if rule is None:
            raise KeyError

        if not self.current_user.has_permissions(rule):
            raise PermissionError

        rule.update_scalar(**data)

        if not rule.is_valid:
            raise ValueError

        self.session.add(rule)
        self.session.commit()

        return rule.to_dict(max_depth=2)

    def rule_members(self, rule_id: int) -> List[Dict]:
        """
        The list of all members belonging to the rule, both
        slides and slide groups, in the set order.

        Parameters
        ----------
        rule_id
            The ID in the database of the rule.

        Returns
        -------
        members
            Ordered list of Slides anf SlideGroups as dictionaries.
        """
        rule = self.session.query(Rule).get(rule_id)
        if rule is None:
            raise KeyError

        return [member.to_dict() for member in rule.get_members()]

    def rule_members_patch(self, rule_id: int, data: Dict[str, Any]) -> List[Dict]:
        """
        Modify the members of the rule.

        Valid operations are "add".

        Keys are required for most operations:

        - `oid` - the the unique identifier of a slide or group
        - `idx` - a specific index for an item in the list

        Parameters
        ----------
        rule_id
            The ID in the database of the rule to modify.
        data
            The description of the operation and the values to use
            when applying the operation. Must contain an "op" key.

        Returns
        -------
        members
            The updated member list.
        """
        rule = self.session.query(Rule).get(rule_id)
        if rule is None:
            raise KeyError

        if not self.current_user.has_permissions(rule):
            raise PermissionError

        if data["op"] == "add":
            oid = data["oid"]
            member = self.slide_or_group_by_oid(oid)
            rule.add_slide_or_group(member)
            self.session.add(rule)
            self.session.commit()
        elif data["op"] == "remove":
            idx = data["idx"]
            oid = rule.order.pop(idx)
            if oid not in rule.order:
                try:
                    # No longer a member
                    member = self.slide_or_group_by_oid(oid)
                    rule.remove_slide_or_group(member)
                except KeyError:
                    # Slide doesn't exist, but still remove
                    pass
            self.session.add(rule)
            self.session.commit()
        elif data["op"] == "move":
            idx = data["idx"]
            oid = rule.order.pop(idx)
            rule.order.insert(max(idx + data["value"], 0), oid)
            self.session.add(rule)
            self.session.commit()

        return self.rule_members(rule_id)

    def rule_displays_patch(self, rule_id: int, data: Dict[str, Any]) -> List[Dict]:
        """
        Modify the display list of the rule.

        Valid operations are "add", "remove".

        Keys are required for most operations:

        - `id` - the the unique identifier of a display

        To add a display, the user must have permissions for all current
        displays and display groups. To remove a display the user only
        needs permissions for that display.

        Parameters
        ----------
        rule_id
            The ID in the database of the rule to modify.
        data
            The description of the operation and the values to use
            when applying the operation. Must contain an "op" key.

        Returns
        -------
        displays
            The updated display list.
        """
        rule = self.session.query(Rule).get(rule_id)
        if rule is None:
            raise KeyError

        if data["op"] == "add":
            display = self.session.query(Display).get(data["id"])
            if display is None:
                raise KeyError
            if display in rule.displays:
                raise ValueError
            if not self.current_user.has_permissions(display):
                raise PermissionError
            if not rule.can_add_display(self.current_user):
                raise PermissionError
            rule.displays.append(display)
            self.session.add(rule)
            self.session.commit()
        elif data["op"] == "remove":
            display = self.session.query(Display).get(data["id"])
            if display in rule.displays:
                if not self.current_user.has_permissions(display):
                    raise PermissionError
                rule.displays.remove(display)
                self.session.add(rule)
                self.session.commit()

        return [display.to_dict() for display in rule.displays]

    def rule_display_groups_patch(
        self, rule_id: int, data: Dict[str, Any]
    ) -> List[Dict]:
        """
        Modify the display group list of the rule.

        Valid operations are "add", "remove".

        Keys are required for most operations:

        - `id` - the the unique identifier of a display group

        To add a display group, the user must have permissions for all
        current displays and display groups. To remove a display group
        the user only needs permissions for that display group.

        Parameters
        ----------
        rule_id
            The ID in the database of the rule to modify.
        data
            The description of the operation and the values to use
            when applying the operation. Must contain an "op" key.

        Returns
        -------
        display_groups
            The updated display list.
        """
        rule = self.session.query(Rule).get(rule_id)
        if rule is None:
            raise KeyError

        if data["op"] == "add":
            group = self.session.query(DisplayGroup).get(data["id"])
            if group is None:
                raise KeyError
            if group in rule.display_groups:
                raise ValueError
            if not self.current_user.has_permissions(group):
                raise PermissionError
            if not rule.can_add_display(self.current_user):
                raise PermissionError
            rule.display_groups.append(group)
            self.session.add(rule)
            self.session.commit()
        if data["op"] == "remove":
            group = self.session.query(DisplayGroup).get(data["id"])
            if group in rule.display_groups:
                if not self.current_user.has_permissions(group):
                    raise PermissionError
                rule.display_groups.remove(group)
                self.session.add(rule)
                self.session.commit()

        return [group.to_dict() for group in rule.display_groups]

    def rule_delete(self, rule_id: int) -> Dict:
        """
        Remove the rule from the database.

        Only a user that has permissions on all display members may delete
        a rule since. They can only detach their screens from rules otherwise.

        Parameters
        ----------
        rule_id
            The ID in the database of the rule to be deleted.

        Returns
        -------
        rule
            The contents of the deleted rule.

        Raises
        ------
        KeyError
            The rule does not exist.
        """
        rule = self.session.query(Rule).get(rule_id)
        if rule is None:
            raise KeyError

        if not rule.can_add_display(self.current_user):
            raise PermissionError

        self.session.delete(rule)
        self.session.commit()

        return rule.to_dict(max_depth=0)

    def users(self) -> List[Dict]:
        """
        Get a list of all users in the database.

        Returns
        -------
        users
            List of Users converted to dicts.
        """
        users = self.session.query(User).all()

        return [user.to_dict() for user in users]

    def user_create(self, data: Dict[str, Any]) -> Dict:
        """
        Create a new user using the given parameters.

        Parameters
        ----------
        data
            A dictionary of values to use to create the user.

        Returns
        -------
        user
            The dictionary representation of the newly created
            user.
        """
        if not self.current_user.admin:
            raise PermissionError

        crsid = data.get("crsid") or data.get("id")
        if not crsid:
            raise ValueError

        new_user = User()
        new_user.crsid = crsid
        new_user.update_scalar(**data)

        for group_id in data.get("groups") or []:
            group = self.session.query(UserGroup).get(group_id)
            if group is not None:
                new_user.groups.append(group)

        self.session.add(new_user)
        self.session.commit()

        return new_user.to_dict()

    def user(self, user_id: str) -> Dict:
        """
        Dictionary representation of a single user.

        Parameters
        ----------
        user_id
            The user's ID (probably CRSid).

        Returns
        -------
        user
            The user converted to a dict.

        Raises
        ------
        KeyError
            The user is not found in the database.
        """

        user = self.session.query(User).get(user_id)
        if user is None:
            raise KeyError
        else:
            return user.to_dict()

    def user_update(self, user_id: str, data: Dict[str, Any]) -> Dict:
        """
        Modify the data for a user.

        Parameters
        ----------
        user_id
            The ID (probably CRSid) of the user to be updated.
        data
            The dictionary of values to be updated.

        Returns
        -------
        user
            The User with updated values.

        Raises
        ------
        KeyError
            The user does not exist.
        """
        if not self.current_user.admin:
            raise PermissionError

        user = self.session.query(User).get(user_id)
        if user is None:
            raise KeyError

        user.update_scalar(**data)

        self.session.add(user)
        self.session.commit()

        return user.to_dict()

    def user_delete(self, user_id: str) -> Dict:
        """
        Remove the user from the database.

        Cannot be fully deleted if they are still associated
        with any slides.

        Parameters
        ----------
        user_id
            The ID (probably CRSid) of the user to be deleted.

        Returns
        -------
        user
            The deleted user.

        Raises
        ------
        KeyError
            The user does not exist.
        """
        if not self.current_user.admin:
            raise PermissionError

        user = self.session.query(User).get(user_id)
        if user is None:
            raise KeyError

        self.session.delete(user)
        self.session.commit()

        return user.to_dict(max_depth=0)

    def user_user_groups_patch(self, user_id: str, data: Dict) -> List[Dict]:
        """
        Update the list of user groups for a user according to
        the given operations.

        Parameters
        ----------
        user_id
            The ID of the user to be modified (probably CRSid).
        data
            The description of the operation and the values to use
            when applying the operation. Must contain an "op" key.

        Returns
        -------
        groups
            The updated list of user groups
        """
        if not self.current_user.admin:
            raise PermissionError

        user = self.session.query(User).get(user_id)
        if user is None:
            raise KeyError

        if data["op"] == "add":
            group = self.session.query(UserGroup).get(data["id"])
            if group is None:
                raise KeyError
            if group in user.groups:
                raise ValueError
            user.groups.append(group)
            self.session.add(user)
            self.session.commit()
        elif data["op"] == "remove":
            group = self.session.query(UserGroup).get(data["id"])
            if group in user.groups:
                user.groups.remove(group)
            self.session.add(user)
            self.session.commit()

        return [group.to_dict() for group in user.groups]

    def user_permissions_patch(self, user_id: str, data: Dict) -> List[Dict]:
        """
        Modify the permissions of a user according to the
        given operations. Permission type should be described in the
        data.

        Parameters
        ----------
        user_id
            The ID of the user to be modified (probably CRSid).
        data
            The description of the operation and the values to use
            then applying the operation. Must contain an "op" key
            and a "type" key.

        Returns
        -------
        permissions
            The modified permissions list
        """
        if not self.current_user.admin:
            raise PermissionError

        user = self.session.query(User).get(user_id)
        if user is None:
            raise KeyError

        if data["type"] == "slide_group":
            item = self.session.query(SlideGroup).get(data["id"])
            item_list = user.slide_group_permissions
        elif data["type"] == "display_group":
            item = self.session.query(DisplayGroup).get(data["id"])
            item_list = user.display_group_permissions
        elif data["type"] == "display":
            item = self.session.query(Display).get(data["id"])
            item_list = user.display_permissions
        else:
            item = None
            item_list = []

        if data["op"] == "add":
            if item is None:
                raise KeyError
            if item in item_list:
                raise ValueError
            item_list.append(item)
        elif data["op"] == "remove":
            if item in item_list:
                item_list.remove(item)

        self.session.add(user)
        self.session.commit()

        return [this_item.to_dict() for this_item in item_list]

    def user_groups(self) -> List[Dict]:
        """
        Get a list of all user groups.

        Returns
        -------
        user_groups
            List of UserGroups converted to dicts.
        """
        # Just get all the groups
        user_groups = self.session.query(UserGroup).all()

        return [user_group.to_dict() for user_group in user_groups]

    def user_group_create(self, data: Dict[str, Any]) -> Dict:
        """
        Create a new user group using the given parameters.

        Parameters
        ----------
        data
            The dictionary of values to use to create the user
            group.

        Returns
        -------
        user_group
            The dictionary representation of the new user group.
        """
        if not self.current_user.admin:
            raise PermissionError

        new_user_group = UserGroup()
        new_user_group.update_scalar(**data)
        if not new_user_group.is_valid:
            raise ValueError

        self.session.add(new_user_group)
        self.session.commit()

        return new_user_group.to_dict()

    def user_group(self, group_id: str) -> Dict:
        """
        Dictionary representation of a single user group.

        Parameters
        ----------
        group_id
            The user group ID as assigned in the database.

        Returns
        -------
        user_group
            The UserGroup converted to a dict.

        Raises
        ------
        KeyError
            The user group is not found in the database.
        """
        user_group = self.session.query(UserGroup).get(group_id)
        if user_group is None:
            raise KeyError
        else:
            return user_group.to_dict(max_depth=2)

    def user_group_update(self, group_id: str, data: Dict[str, Any]) -> Dict:
        """
        Modify the data of a user group.

        Parameters
        ----------
        group_id
            The ID of the user group to be updated.
        data
            The dictionary of values to be updated.

        Returns
        -------
        user_group
            The UserGroup with updated values.

        Raises
        ------
        KeyError
            The user group doesn't exist.
        """
        if not self.current_user.admin:
            raise PermissionError

        user_group = self.session.query(UserGroup).get(group_id)
        if user_group is None:
            raise KeyError

        user_group.update_scalar(**data)
        if not user_group.is_valid:
            raise ValueError

        self.session.add(user_group)
        self.session.commit()

        return user_group.to_dict()

    def user_group_delete(self, group_id: str) -> Dict:
        """
        Completely remove the user group from the database.

        Parameters
        ----------
        group_id
            The ID of the user group to be deleted.

        Returns
        -------
        user_group
            The deleted user group.

        Raises
        ------
        KeyError
            The user group does not exist.
        """
        if not self.current_user.admin:
            raise PermissionError

        user_group = self.session.query(UserGroup).get(group_id)
        if user_group is None:
            raise KeyError

        self.session.delete(user_group)
        self.session.commit()

        return user_group.to_dict(max_depth=0)

    def user_group_permissions_patch(self, group_id: str, data: Dict) -> List[Dict]:
        """
        Modify the permissions of a user group according to the
        given operations. Permission type should be described in the
        data.

        Parameters
        ----------
        group_id
            The ID of the user group to be modified.
        data
            The description of the operation and the values to use
            then applying the operation. Must contain an "op" key
            and a "type" key.

        Returns
        -------
        permissions
            The modified permissions list
        """
        if not self.current_user.admin:
            raise PermissionError

        user_group = self.session.query(UserGroup).get(group_id)
        if user_group is None:
            raise KeyError

        if data["type"] == "slide_group":
            item = self.session.query(SlideGroup).get(data["id"])
            item_list = user_group.slide_group_permissions
        elif data["type"] == "display_group":
            item = self.session.query(DisplayGroup).get(data["id"])
            item_list = user_group.display_group_permissions
        elif data["type"] == "display":
            item = self.session.query(Display).get(data["id"])
            item_list = user_group.display_permissions
        else:
            item = None
            item_list = []

        if data["op"] == "add":
            if item is None:
                raise KeyError
            if item in item_list:
                raise ValueError
            item_list.append(item)
        elif data["op"] == "remove":
            if item in item_list:
                item_list.remove(item)

        self.session.add(user_group)
        self.session.commit()

        return [this_item.to_dict() for this_item in item_list]

    def admin_create(self, user_id: str, data: Optional[Dict] = None) -> Dict:
        """
        Create a new admin, or promote an existing user.

        Parameters
        ----------
        user_id
            The CRSid of the user to make an admin.
        data
            Any extra parameters to be used if a new user is created.

        Returns
        -------
        admin
            The newly minted admin user.
        """
        if not user_id or not user_id.strip():
            raise ValueError

        admin = self.session.query(User).get(user_id)

        if admin is None:
            admin = User()
            admin.crsid = user_id
            admin.update_scalar(**(data or {}))

        admin.admin = True

        self.session.add(admin)
        self.session.commit()

        return admin.to_dict()

    def admin_delete(self, user_id: str) -> Optional[Dict]:
        """
        Remove admin privileges from a user.

        Parameters
        ----------
        user_id
            The CRSid of the user to demote.

        Returns
        -------
        user
            The demoted user, if they exist or None.
        """
        user = self.session.query(User).get(user_id)

        if user is None:
            return None

        user.admin = False

        self.session.add(user)
        self.session.commit()

        return user.to_dict()

    def slide_rotation(self, addr: str, date: Optional[datetime] = None) -> List[Slide]:
        """
        Determine the current slide rotation for the given display.

        Parameters
        ----------
        addr
            IP address of the given display
        date
            Moment for which to calculate the active slides

        Returns
        -------
        slides
            The list of currently active slides for the given display
        """
        # Get the current display, if it doesn't exist fall back to one
        # with address 0.0.0.0, or an empty list of slides
        display = (
            self.session.query(Display).filter(Display.ident == addr).first()
            or self.session.query(Display).filter(Display.ident == "0.0.0.0").first()
            or Display()
        )

        return display.slide_rotation(date)

    def handshake(self, data: Dict) -> Dict:
        """
        Receive a remote signage's information and either store it,
        note that it exists, or reject it.

        Parameters
        ----------
        data
            Structured information about the external server.

        Returns
        -------
        remote
            The locally stored data for the server.
        """
        hostname = data.get("hostname", "").strip()

        if not hostname:
            raise PermissionError

        # does this host already exist in the system?
        remote = self.session.query(SignageServer).get(hostname)

        # New remote instance if it doesn't already exist
        if not remote:
            remote = SignageServer()
            remote.hostname = hostname

        # Blacklisted hosts are ignored. Stops them showing data too.
        if remote.blacklisted:
            raise PermissionError

        # Initialise fields (just identity)
        remote.update_scalar(**data)

        # Accept data if it's a trusted server, otherwise blank it.
        # Attributes can only be edited through the admin interface.
        if remote.trusted:
            remote.slide_groups = data.get("slide_groups", [])
            remote.neighbours = data.get("neighbours", [])
            remote.updated = datetime.utcnow()
            remote.online = True
        else:
            remote.slide_groups = []
            remote.neighbours = []

        # Done editing remote data, update this server.
        self.session.add(remote)
        self.session.commit()

        # Add neighbours if they are trusted.
        if remote.trust_neighbours:
            for neighbour in remote.neighbours:
                # Skip own server
                if neighbour == current_app.config["HOSTNAME"]:
                    continue
                # Ensure an entry exists for the neighbour
                trusted_neighbour = self.session.query(SignageServer).get(neighbour)
                if trusted_neighbour is None:
                    trusted_neighbour = SignageServer()
                    trusted_neighbour.hostname = neighbour
                    trusted_neighbour.source_neighbour = hostname
                # Set flags, will get data when it's next polled
                if not trusted_neighbour.trusted:
                    trusted_neighbour.trusted = True

                self.session.add(trusted_neighbour)
                self.session.commit()

        return remote.to_dict()

    def shake_hands(self, hostnames: Optional[List[str]] = None) -> Dict[str, bool]:
        """
        Sending side of a handshake.

        For each of the listed hostnames, or all trusted hosts if none are
        given, send the server's federation information in a POST request
        and process the response from the remote server to update the
        stored information.
        """
        if hostnames is None:
            hostnames = [
                host.hostname
                for host in self.session.query(SignageServer)
                .filter_by(trusted=True)
                .filter_by(blacklisted=False)
                .filter(SignageServer.hostname != current_app.config["HOSTNAME"])
            ]

        # Track which ones worked
        success = {}

        # Skip gathering the local info if there's nothing to do
        if not hostnames:
            return success

        # Data to send is the same for everyone, so generate it once
        # Uses the flask encoder to deal with Time and Enum
        local_info = json.dumps(self.local_info())

        for hostname in hostnames:
            updated = False
            try:
                # Try with http first as developments server are often
                # not running https. A probe with options will redirect
                # to the correct url, then carry out the POST
                remote_url = "http://{}/api/handshake".format(hostname)
                req = requests.options(remote_url, allow_redirects=True, timeout=3.05)
                # URL is updated to the correct Location
                req = requests.post(
                    req.url,
                    data=local_info,
                    headers={"Content-Type": "application/json"},
                    timeout=3.02,
                )
                remote_data = req.json()["data"]
                # hostname must match what's reported as it's used in handshake
                if remote_data.get("hostname", "").strip() == hostname:
                    self.handshake(remote_data)
                else:
                    raise ValueError
            except (IOError, ValueError, KeyError):
                # Mark as offline if there's an error
                server = self.session.query(SignageServer).get(hostname)
                if server is not None:
                    server.slide_groups = []
                    server.neighbours = []
                    server.online = False
                    self.session.commit()
            finally:
                success[hostname] = updated

        return success

    def local_server(self) -> SignageServer:
        """
        Get a the SignageServer that corresponds to the local
        instance. Will be created if it does not exist yet so will
        always return a non-empty response.
        """
        hostname = current_app.config["HOSTNAME"]
        server = self.session.query(SignageServer).get(hostname)

        # local server must always exist so create a placeholder if
        # it's not created yet.
        if server is None:
            server = SignageServer()
            server.hostname = hostname
            server.name = "Signage Server"
            self.session.add(server)
            self.session.commit()

        return server

    def local_info(self) -> Dict[str, Any]:
        """
        Get the information for the current server.

        Merge the current list of advertised signage and trusted neighbours
        into the info about the server and return that.
        """
        server = self.local_server()
        info = server.to_dict()
        info["slide_groups"] = self.slide_groups_shared()
        trusted = (
            self.session.query(SignageServer)
            .filter_by(trusted=True)
            .filter_by(blacklisted=False)
            .filter(SignageServer.hostname != current_app.config["HOSTNAME"])
        )
        info["neighbours"] = [neighbour.hostname for neighbour in trusted]

        return info

    def neighbours(self, show_blacklisted: bool = True) -> List[Dict]:
        """
        Get a list of federated neighbour signage servers.

        Parameters
        ----------
        show_blacklisted
            If enabled will include servers that have been blacklisted.

        Returns
        -------
        neighbours
            List of SignageServer converted to dicts.
        """
        if not self.current_user.admin:
            raise PermissionError

        # get the list, excluding the local server
        servers = self.session.query(SignageServer).filter(
            SignageServer.hostname != current_app.config["HOSTNAME"]
        )

        if not show_blacklisted:
            servers = servers.filter_by(blacklisted=False)

        return [server.to_dict() for server in servers]

    def neighbour_create(self, data: Dict[str, Any]) -> Dict:
        """
        Create a new federated neighbour server using the given parameters.

        Parameters
        ----------
        data
            The dictionary of values to use to create the neighbour,
            which can include properties, but not slide data.

        Returns
        -------
        neighbour
            The dictionary representation of the new neighbour.
        """

        if not self.current_user.admin:
            raise PermissionError

        hostname = data.get("hostname", "").strip()

        if not hostname:
            raise ValueError

        new_neighbour = SignageServer()
        new_neighbour.hostname = hostname
        new_neighbour.update_scalar(**data)

        # Manually created defaults to being trusted
        new_neighbour.blacklisted = data.get("blacklisted", False)
        new_neighbour.trusted = data.get("trusted", True)
        new_neighbour.trust_neighbours = data.get("trust_neighbours", False)

        self.session.add(new_neighbour)
        self.session.commit()

        return new_neighbour.to_dict()

    def neighbour(self, hostname: str) -> Dict:
        """
        Dictionary representation of a single neighbour server.

        Parameters
        ----------
        hostname
            The hostname identifying the server.

        Returns
        -------
        neighbour
            The neighbour converted to a dict.

        Raises
        ------
        KeyError
            The hostname is not known to the system.
        """

        if not self.current_user.admin:
            raise PermissionError

        if hostname in ["@server", current_app.config["HOSTNAME"]]:
            neighbour = self.local_server()
        else:
            neighbour = self.session.query(SignageServer).get(hostname)

        if neighbour is None:
            raise KeyError

        return neighbour.to_dict()

    def neighbour_update(self, hostname: str, data: Dict[str, Any]) -> Dict:
        """
        Modify the properties of a federated server.

        Parameters
        ----------
        hostname
            The hostname identifying the server.
        data
            A dictionary of values to be updated, polling properties
            will be the most relevant.

        Returns
        -------
        neighbour
            The server with updated values converted to a dict.

        Raises
        ------
        KeyError
            The hostname is not known to the system.
        """
        if not self.current_user.admin:
            raise PermissionError

        if hostname in ["@server", current_app.config["HOSTNAME"]]:
            neighbour = self.local_server()
        else:
            neighbour = self.session.query(SignageServer).get(hostname)

        if neighbour is None:
            raise KeyError

        # These will be overwritten by the self identification of a remote
        # server later, but is relevant for the local instance.
        neighbour.update_scalar(**data)

        # Admin modifiable properties
        if "blacklisted" in data:
            neighbour.blacklisted = data["blacklisted"]
        if "trusted" in data:
            neighbour.trusted = data["trusted"]
        if "trust_neighbours" in data:
            neighbour.trust_neighbours = data["trust_neighbours"]

        self.session.add(neighbour)
        self.session.commit()

        return neighbour.to_dict()

    def neighbour_delete(self, hostname: str) -> Dict:
        """
        Remove the record of a neighbouring signage server.

        Parameters
        ----------
        hostname
            The hostname identifying the server.

        Returns
        -------
        neighbour
            The contents of the deleted server record.

        Raises
        ------
        KeyError
            The hostname is not know to the system.
        """
        if not self.current_user.admin:
            raise PermissionError

        neighbour = self.session.query(SignageServer).get(hostname)
        if neighbour is None:
            raise KeyError

        self.session.delete(neighbour)
        self.session.commit()

        return neighbour.to_dict(max_depth=0)

    def neighbour_slides(
        self, hostnames: Optional[List[str]] = None, max_age: Optional[int] = None
    ) -> List[Dict]:
        """
        Get the public info from all neighbouring servers including
        their advertised slides.

        Parameters
        ----------
        hostnames
            Restrict to only the given hostnames. Will still only pull
            info for `trusted` hosts.
        max_age
            Age of cached data to consider stale and refresh everything.

        Returns
        -------
        servers
            A list of dictionary objects with server info and lists
            of slides.
        """

        # Neighbours must be both trusted and not blacklisted
        # Filter the local server as the entry does not have any slide data
        neighbours = (
            self.session.query(SignageServer)
            .filter_by(trusted=True)
            .filter_by(blacklisted=False)
            .filter(SignageServer.hostname != current_app.config["HOSTNAME"])
        )

        if hostnames is not None:
            neighbours = neighbours.filter(SignageServer.hostname.in_(hostnames))

        if max_age is not None:
            # Try to refresh all hosts if any are out of date
            stale_date = datetime.utcnow() - timedelta(seconds=max_age)
            if any([server.updated < stale_date for server in neighbours]):
                self.shake_hands()

        return [server.to_public() for server in neighbours]

    def import_legacy_slides(
        self,
        slides: List[Dict],
        active_group: str = "Imported (published)",
        inactive_group: str = "Imported (draft)",
    ) -> None:
        """
        Import slides from a legacy slide representation as new Slides
        in a new SlideGroups.

        Method does not check permissions as is should only be executed
        from a shell and not exposed to the API.

        Parameters
        ----------
        slides
            A list of slides as dictionaries parsed from an old-style
            TinyDB slide list.
        active_group
            Name of group to import "active" slides into.
        inactive_group
            Name of group to import "inactive slides into.
        """
        active_slides = []
        inactive_slides = []

        # Slide groups to add to
        active_slide_group = SlideGroup()
        inactive_slide_group = SlideGroup()
        active_slide_group.name = active_group
        inactive_slide_group.name = inactive_group

        # Check these are valid before starting to import things
        if not active_slide_group.is_valid or not inactive_slide_group.is_valid:
            raise ValueError

        # Flush to the database to populate "order" (and other attributes)
        self.session.add(active_slide_group)
        self.session.add(inactive_slide_group)
        self.session.flush()

        for slide in slides:
            # "title" has become "name"
            db_slide = Slide()
            db_slide.update_scalar(**slide, name=slide.get("title", ""))
            # Dates are converted from timestamps to datetime
            if slide.get("not_before"):
                db_slide.valid_from = datetime.fromtimestamp(slide["not_before"])
            if slide.get("not_after"):
                db_slide.valid_to = datetime.fromtimestamp(slide["not_after"])
            # Timestamp creation
            db_slide.created = datetime.utcnow()
            db_slide.updated = datetime.utcnow()

            # Put into separate groups
            if slide.get("active"):
                active_slides.append(db_slide)
            else:
                inactive_slides.append(db_slide)

            self.session.add(db_slide)

        # Flush to the db to assign id values to slides
        self.session.flush()

        # The add_slide method ensures the slide is added to "order"
        for slide in active_slides:
            active_slide_group.add_slide(slide)
        for slide in inactive_slides:
            inactive_slide_group.add_slide(slide)

        # Done
        self.session.commit()

    def close(self):
        self.db.Session.remove()
