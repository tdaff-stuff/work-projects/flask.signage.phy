"use strict";

/**
 * Global info and constants
 */
let basePrefix = "/api";

/** RegExpr for any kind of newline */
const newlines = /\r\n|\r|\n/g;
const asset_re = /(?:\/*assets\/)?([0-9a-f]{40})(\/.*)?/;
// For 00:00 to 23:59
const time_re = /^([0-1][0-9]|2[0-3]):([0-5][0-9])$/;
// parsing external oid values
const oid_re = /(\w+):(\d+)@(.*)/;

const colours = [
  { code: "r", name: "red", hex: "#e62b28" },
  { code: "o", name: "orange", hex: "#e7b200" },
  { code: "y", name: "yellow", hex: "#e4cd0e" },
  { code: "g", name: "green", hex: "#30ab5d" },
  { code: "b", name: "blue", hex: "#28b1e7" },
  { code: "i", name: "indigo", hex: "#1572b9" },
  { code: "v", name: "violet", hex: "#6b3b8f" },
  { code: "w", name: "white", hex: "#ffffff" },
  { code: "k", name: "black", hex: "#000000" },
];

const accents = {
  1: "#35ffbf",
  2: "#e85629",
  3: "#7484ed",
};

const monthShort = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

const weekdays = [
  [1, "Monday"],
  [2, "Tuesday"],
  [3, "Wednesday"],
  [4, "Thursday"],
  [5, "Friday"],
  [6, "Saturday"],
  [7, "Sunday"],
];

const visibilityIcons = {
  private: "visibility_off",
  local: "visibility",
  public: "public",
  share: "share",
};

const visibilityText = {
  private: "Only visible to authorised users",
  local: "Visible to logged-in users",
  public: "Visible to everyone",
  share: "Advertised publicly",
};

/**
 * Assign some standard icons to specific objects.
 */
const iconMap = {
  slide: "panorama",
  slide_group: "collections",
  display: "tv",
  display_group: "account_tree",
  external: "link",
  external_off: "link_off",
  delete: "delete_forever",
  save: "save",
  up: "arrow_upward",
  down: "arrow_downward",
  add: "add_box",
  create: "add_circle_outline",
  play: "play_circle_outline",
  group: "group",
};

const icon = function (name) {
  return $("<i>", {
    class: "material-icons md-inline mr-1",
    text: iconMap[name] || name,
  });
};

const iconButton = function (name, title) {
  return $("<button>", {
    type: "button",
    class: "material-icons md-inline mr-1",
    text: iconMap[name] || name,
    title: title,
  });
};

/**
 * Construct the "oid" for the given object, for example
 * Slide:4 for a local slide or
 * SlideGroupExternal:7@signage.elsewhere.ac.uk for  an external one.
 *
 * @param {String} obj - The type of object e.g. "slide_group"
 * @param {Number|String} id - Database id of the object
 * @param {String} [origin] - Source of a remote slide
 * @returns {string|undefined}
 */
const oid = function (obj, id, origin) {
  let ext = "";
  let suffix = "";
  if (origin) {
    ext = "External";
    suffix = "@" + origin;
  }
  if (obj === "slide_group") {
    return "SlideGroup" + ext + ":" + id + suffix;
  } else if (obj === "slide") {
    return "Slide" + ext + ":" + id + suffix;
  }
  return undefined;
};

/* keep some items cached for page refreshes */
let userData = {
  crsid: null,
  id: null,
  name: null,
  admin: false,
  groups: [],
  all_slide_group_permissions: [],
  slide_group_ids: [],
  all_display_group_permissions: [],
  display_group_ids: [],
  all_display_permissions: [],
  display_ids: [],
  all_rule_permissions: [],
  rule_ids: [],
  refresh: function () {
    let user = this;
    let mapId = function (obj) {
      return obj.map(function (item) {
        return item.id;
      });
    };
    return $.getJSON(basePrefix + "/status").done(function (response) {
      Object.assign(user, response.data.user);
      user.slide_group_ids = mapId(user.all_slide_group_permissions);
      user.display_group_ids = mapId(user.all_display_group_permissions);
      user.display_ids = mapId(user.all_display_permissions);
      user.rule_ids = mapId(user.all_rule_permissions);
    });
  },
  getView: function (item) {
    if (item === "user_groups") {
      return $.getJSON(basePrefix + "/user_groups");
    } else if (item === "slide_groups") {
      return $.getJSON(basePrefix + "/slide_groups");
    } else if (item === "display_groups") {
      return $.getJSON(basePrefix + "/display_groups");
    } else if (item === "displays") {
      return $.getJSON(basePrefix + "/displays");
    } else if (item === "rules") {
      return $.getJSON(basePrefix + "/rules");
    }
  },
  getEdit: function (item) {
    if (item === "user_groups") {
      if (this.admin) {
        return $.getJSON(basePrefix + "/user_groups");
      } else {
        return $.Deferred().resolve({ data: [] }).promise();
      }
    } else if (item === "slide_groups") {
      if (this.admin) {
        return $.getJSON(basePrefix + "/slide_groups");
      } else {
        return $.Deferred()
          .resolve({ data: this.all_slide_group_permissions })
          .promise();
      }
    } else if (item === "display_groups") {
      if (this.admin) {
        return $.getJSON(basePrefix + "/display_groups");
      } else {
        return $.Deferred()
          .resolve({ data: this.all_display_group_permissions })
          .promise();
      }
    } else if (item === "displays") {
      if (this.admin) {
        return $.getJSON(basePrefix + "/displays");
      } else {
        return $.Deferred()
          .resolve({ data: this.all_display_permissions })
          .promise();
      }
    } else if (item === "rules") {
      if (this.admin) {
        return $.getJSON(basePrefix + "/rules");
      } else {
        return $.Deferred()
          .resolve({ data: this.all_rule_permissions })
          .promise();
      }
    }
  },
  /**
   *
   * @param {Slide|Display|Object} item
   * @param {String} itemType
   * @returns {boolean}
   */
  hasPermission: function (item, itemType) {
    if (this.admin) {
      // Admin get full access to everything
      // No further checks for admin only items ("user", "user_group")
      return true;
    } else if (itemType === "slide_group") {
      return this.slide_group_ids.indexOf(item.id) > -1;
    } else if (itemType === "display_group") {
      return this.display_group_ids.indexOf(item.id) > -1;
    } else if (itemType === "display") {
      if (this.display_ids.indexOf(item.id) > -1) {
        return true;
      } else {
        return item.groups.some(function (group) {
          return userData.hasPermission(group, "display_group");
        });
      }
    } else if (itemType === "rule") {
      return this.rule_ids.indexOf(item.id) > -1;
    }
    return false;
  },
  hasRulePermission: function () {
    return !!(
      this.admin ||
      this.display_group_ids.length ||
      this.display_ids.length
    );
  },
  ifPermission: function (item, itemType, obj, otherwise) {
    if (this.hasPermission(item, itemType)) {
      return obj;
    } else {
      return otherwise;
    }
  },
  ifAdmin: function (obj, otherwise) {
    if (this.admin) {
      return obj;
    } else {
      return otherwise;
    }
  },
};

/**
 * Sorting function that sorts alphabetically by a particular
 * attribute of an object.
 *
 * @param {string} attr - Attribute name used in the comparison
 * @returns {Function}
 */
const attrSort = function (attr) {
  return function (a, b) {
    let attrA = a[attr].toUpperCase();
    let attrB = b[attr].toUpperCase();
    if (attrA < attrB) {
      return -1;
    }
    if (attrA > attrB) {
      return 1;
    }
    return 0;
  };
};

/**
 * Generate a function to get a specific "data" attribute from an
 * object. Useful for .map() to get an array of values.
 *
 * Works for map on a jQuery object collection or a plain array
 * of items.
 *
 * @param {string} attr - Data item name
 * @returns {Function}
 */
const getData = function (attr) {
  return function (item) {
    // For jQuery item is the index and this is the item
    // For an Array this is undefined and item is passed
    return $(this || item).data(attr);
  };
};

/**
 * Get the item from the current page hash. Optionally check that the
 * hash has the correct prefix.
 *
 * @param {number} [index] Component of the hash to return, defaults to 1
 * @param {string} [prefix] If defined, check that the hash has the given prefix
 * @returns {string|undefined}
 */
const getUrlHashItem = function (index, prefix) {
  let hash = window.location.hash.substring(1);
  // Usually type/id
  if (index === undefined) index = 1;
  // Check prefix if it's supplied
  if (prefix !== undefined && !hash.startsWith(prefix)) {
    return undefined;
  } else {
    return hash.split("/")[index];
  }
};

/**
 * If value requires a plural, then add s to the text
 * and return it.
 *
 * @param {string} text A text string that can be pluralised, or empty
 * @param {number} value The number to check if it needs a plural
 * @returns {string} Pluralised text
 */
const plural = function (text, value) {
  if (value !== 1) {
    return text + "s";
  } else {
    return text;
  }
};

/**
 * Convert a date to the format required for <input type="date">,
 * i.e. "Y-m-d".
 *
 * @param {Date|null} date The date to convert
 * @returns {string|null} - Equivalent of "Y-m-d"
 */
const dateToInputDate = function (date) {
  // pass empty values
  if (!date) return null;
  // <input> require %Y-%m-%d formatted value fields
  let Y = date.getFullYear();
  let m = ("0" + (date.getMonth() + 1)).slice(-2);
  let d = ("0" + date.getDate()).slice(-2);
  return Y + "-" + m + "-" + d;
};

/**
 * Convert a time component of a date to the format required for
 * <input type="time">, i.e. "H:M".
 *
 * @param {Date|null} date The date to convert
 * @returns {string|null} - Equivalent of "H:M"
 */
const dateToInputTime = function (date) {
  // pass empty values
  if (!date) return null;
  // <input> require %H:%M formatted value fields
  let H = ("0" + date.getHours()).slice(-2);
  let M = ("0" + date.getMinutes()).slice(-2);
  return H + ":" + M;
};

/**
 * Calculate the approximate duration since the given
 * date. String will only return the largest period of
 * time with no decimal places.
 *
 * @param {string|number} date Something that can be parsed as a Date
 * @returns {string} "XX period[s]"
 *
 */
const sinceDate = function (date) {
  // Start with the span in seconds
  let span = Math.abs(new Date() - new Date(date)) / 1000;
  // Check if any time period is greater than one, progressively
  // getting smaller.
  let years = Math.floor(span / (60 * 60 * 24 * 365));
  if (years) {
    return years + plural(" year", years);
  }
  let months = Math.floor(span / (60 * 60 * 24 * 30));
  if (months) {
    return months + plural(" month", months);
  }
  let days = Math.floor(span / (60 * 60 * 24));
  if (days) {
    return days + plural(" day", days);
  }
  let hours = Math.floor(span / (60 * 60));
  if (hours) {
    return hours + plural(" hour", hours);
  }
  let minutes = Math.floor(span / 60);
  return minutes + plural(" minute", minutes);
};

/**
 * Parse a date-like value as a Javascript Date object. Return null
 * if no valid Date can be constructed, or the argument is null.
 *
 * @param {number|string|null} dateVal Either a date string or unix time (milliseconds)
 * @returns {null|Date}
 */
const maybeDate = function (dateVal) {
  let date = new Date(dateVal);
  if (!dateVal || isNaN(date.getTime())) {
    return null;
  } else {
    return date;
  }
};

/**
 * Check if a value is a valid time string in the format HH:MM and
 * return it if valid. Return null if it's not valid.
 *
 * @param {string|null} timeVal
 * @returns {string|null}
 */
const maybeTime = function (timeVal) {
  if (timeVal && time_re.test(timeVal)) {
    return timeVal;
  } else {
    return null;
  }
};

/**
 * Combine a date string and a time string to a Date object, using the
 * default time if the time value is empty. One of timeVal or defaultTime
 * must be valid.
 *
 * Returns null if a valid Date can't be constructed
 *
 * @param {string} dateVal A date in YYYY-MM-DD format
 * @param {string|null} timeVal A time in HH:MM format
 * @param {string} defaultTime A time to use if timeVal is empty
 * @returns {null|Date}
 */
const maybeDateTime = function (dateVal, timeVal, defaultTime) {
  if (dateVal && timeVal) {
    return maybeDate(dateVal + "T" + timeVal);
  } else if (dateVal && defaultTime) {
    return maybeDate(dateVal + "T" + defaultTime);
  } else {
    return null;
  }
};

/**
 * Format a read-only date range with an icon.
 *
 * @param {jQuery|HTMLElement} elem - Render into this element
 * @param {Date} [start] - Start date, shown on left
 * @param {Date} [end] - End date, shown on right
 * @returns {*|jQuery} - elem with the inserted output
 */
const dateRange = function (elem, start, end) {
  return $(elem)
    .append($("<i>", { class: "material-icons md-inline", text: "date_range" }))
    .append(
      $("<span>", { text: start ? start.toLocaleString() : "", title: start })
    )
    .append($("<span>", { text: " - " }))
    .append(
      $("<span>", { text: end ? end.toLocaleString() : "", title: start })
    );
};

/**
 * Format a list of days, given as an Array of day indices
 *
 * @param {jQuery|HTMLElement} elem - Render into this element
 * @param {Array} days - A list of day numbers, 1=Mon, 7=Sun
 * @returns {*|jQuery} - elem with the rendered output included
 */
const activeDays = function (elem, days) {
  return $(elem)
    .append(
      $("<i>", {
        class: "material-icons md-inline",
        text: "calendar_today",
      })
    )
    .append(
      weekdays.map(function (weekday) {
        return $("<span>", {
          class: "active-day",
          text: weekday[1].substring(0, 2),
          title: weekday[1],
        }).toggleClass("active", days.indexOf(weekday[0]) > -1);
      })
    );
};

/**
 * Format a read-only time range with an icon.
 *
 * @param {jQuery|HTMLElement} elem - Render into this element
 * @param {String} [start] - Start time, shown on left
 * @param {String} [end] - End time, shown on right
 * @returns {*|jQuery} - elem with the inserted output
 */
const timeRange = function (elem, start, end) {
  return $(elem)
    .append($("<i>", { class: "material-icons md-inline", text: "timelapse" }))
    .append($("<span>", { text: start || "" }))
    .append($("<span>", { text: " - " }))
    .append($("<span>", { text: end || "" }));
};

/**
 * Generate a random string of length `len`
 *
 * @param {number} len length of string to construct
 * @param {string} [valid] character set of valid values
 * @returns {string}
 */
const randomString = function (len, valid) {
  let text = [];
  if (!valid) valid = "abcdefghijklmnopqrstuvwxyz";
  for (let i = 0; i < len; i++) {
    text.push(valid.charAt(Math.floor(Math.random() * valid.length)));
  }
  return text.join("");
};

/**
 * Generate the url for a pattern image from signage generator
 *
 * @param {number} pattern The numerical index of the pattern to use
 * @param {string} colours The colourstring of the pattern as "roy..."
 * @param {string} [size] Size of the image as WxH
 * @returns {string} the url for the required image
 */
const patternImg = function (pattern, colours, size) {
  if (!size) size = "1920x1080";
  return "/pattern/" + size + "/" + pattern + "/" + colours + ".png";
};

/**
 * Generate the "view" URL for a slide preview.
 *
 * @param {string|number} id - Actual id of the slide
 * @param {string} [origin] - Remote location if slide is not local
 * @returns {string} - the "/view/..." URL.
 */
const previewUrl = function (id, origin) {
  if (origin) {
    return "/view/SlideExternal:" + id + "@" + origin;
  } else {
    return "/view/Slide:" + id;
  }
};

/**
 * Convert multi-line string into <br> joined html with
 * html entities encoded to avoid script injection
 *
 * @param {string} inStr String to split and join
 */
const brJoin = function (inStr) {
  // Don't trip on undefined and empty strings
  if (!inStr) return "";
  // Use jQuery to convert anything dangerous to html entities
  return inStr
    .split(newlines)
    .map(function (item) {
      return $("<div>").text(item).html();
    })
    .join("<br>");
};

/**
 * Create a colour string in rgba() format from the
 * hex value and an opacity.
 *
 * @param {string} hex The colour in #RRGGBB format
 * @param {number} [opacity] Opacity from 0.0 to 1.0
 * @returns {string} "rbga(r,g,b,a)"
 */
const hexToRGBA = function (hex, opacity) {
  if (opacity === undefined) opacity = 1.0;
  let r = parseInt(hex.substr(1, 2), 16);
  let g = parseInt(hex.substr(3, 2), 16);
  let b = parseInt(hex.substr(5, 2), 16);
  return "rgba(" + r + "," + g + "," + b + "," + opacity + ")";
};

/**
 * Create a colour string in rgba() format that is either
 * black or white with the opposing lightness to the colour
 * in hex
 *
 * @param {string} hex The colour in #RRGGBB format
 * @param {number} [opacity] Opacity from 0.0 to 1.0
 * @returns {string} "rgba(w,w,w,a)"
 */
const hexToShadow = function (hex, opacity) {
  if (opacity === undefined) opacity = 1.0;
  let r = parseInt(hex.substr(1, 2), 16);
  let g = parseInt(hex.substr(3, 2), 16);
  let b = parseInt(hex.substr(5, 2), 16);
  let linear = (0.2126 * r + 0.7152 * g + 0.0722 * b) / 255;
  let srgb =
    linear <= 0.0031308
      ? 12.92 * linear
      : 1.055 * Math.pow(linear, 1 / 2.4) - 0.055;
  let w = 255 * Math.round(1 - srgb);
  return "rgba(" + w + "," + w + "," + w + "," + opacity + ")";
};

/**
 * Process a drag and drop or file selector event for adding
 * an image to a slide.
 *
 * @param event Either a drop or a file select
 */
const uploadSlideAction = function (event) {
  let sourceSlide = $(event.target).closest(".slide-panel");
  let sourceForm = $(event.target).closest("form");
  let slidePreview = sourceSlide.find(".slide-preview-image");
  // get file either from drag and drop or the form element
  // Just upload as data blob
  let fileBlobData = ((event.originalEvent.dataTransfer &&
    event.originalEvent.dataTransfer.files) ||
    event.target.files)[0];
  // Convert to an image for the slide content
  slidePreview.addClass("loading");
  $.ajax("/assets", {
    type: "POST",
    data: fileBlobData,
    processData: false,
    contentType: false,
    success: function (result) {
      // redirects to asset listing, stored in data
      let asset_url = "assets/" + result.data["."] + "/";
      // assign to slide content in the form attribute
      let formContent = sourceForm.data("content");
      formContent.asset = asset_url;
      formContent.image = asset_url + result.data["image"];
      formContent.thumbnail = asset_url + result.data["thumbnail"];
      // Write back to the page
      sourceForm.addClass("changed");
      sourceSlide.find(".image-properties").removeClass("d-none");
      sourceSlide.find(".background-pattern").addClass("d-none");
      sourceSlide.find(".slide-preview").trigger("redraw");
    },
    error: function (evt, status, text) {
      alert("Error:" + text);
      console.log(evt);
      console.log(status);
      slidePreview.removeClass("loading");
    },
  });
};

/**
 * Create a fullscreen preview overlay of the "src" link.
 * Can click outside to dismiss the preview.
 *
 * @param src String link to preview in the fullscreen preview
 */
const fullscreenPreview = function (src) {
  let aspect = 16.0 / 9.0;
  let maxSize = 0.8; // 80% of screen
  let previewWidth = 1920;
  let previewHeight = previewWidth / aspect;
  let scale = Math.min(
    (window.innerWidth * maxSize) / previewWidth,
    (window.innerHeight * maxSize) / previewHeight
  );
  // Preview is a div covering the screen that dismisses the
  // preview when clicked. the <iframe> is given the desired
  // screen size, put in the middle of the screen, and scaled
  // (about its centre) using transform. Position should be
  // relative to unscaled size!
  $("body")
    .addClass("no-scroll")
    .append(
      $("<div>", { class: "modal-dismiss" })
        .append(
          $("<iframe>", {
            src: src,
          }).css({
            position: "fixed",
            left: (window.innerWidth - previewWidth) / 2,
            top: (window.innerHeight - previewHeight) / 2,
            width: previewWidth,
            height: previewHeight,
            transform: "scale(" + scale + ")",
            background: "white",
            border: 4 / scale + "px solid black",
            "box-sizing": "content-box",
          })
        )
        .on("click", function () {
          $("body").removeClass("no-scroll");
          $(this).remove();
        })
    );
};

/**
 * Pop up a an element in a modal that removes focus from the
 * rest of the page until it is submitted or dismissed.
 *
 * Can be dismissed by clicking outside the element, or by
 * bubbling a "dismiss" event up. Propagation of click events
 * is prevented from the embedded elements.
 *
 * @param {string|HTMLElement} element Either an element or a selector
 */
const modalElement = function (element) {
  // Dismiss any existing modal
  $(".modal-dismiss").trigger("click");
  // Overlay mode with a click to exit
  $("body")
    .addClass("no-scroll")
    .append(
      $("<div>", { class: "modal-dismiss" })
        .append(
          $("<div>", { class: "overlay-content" })
            .append(
              $("<button>", {
                class: "close btn",
                text: "close",
              })
                .prepend(icon("close"))
                .on("click", function (event) {
                  $(".modal-dismiss").trigger("click");
                  event.preventDefault();
                })
            )
            .append(
              $(element).on("click", function (event) {
                // Prevent interactions with the modal closing the popup
                event.stopPropagation();
              })
            )
        )
        .on("click dismiss", function () {
          $("body").removeClass("no-scroll");
          $(this).remove();
        })
    );
};

/**
 * Create the image library browsing applet. Brings up a list of
 * saved images and allows one to be selected for the slide that
 * launched the applet.
 *
 * @param {HTMLElement} [ref] - The button used to call the function
 * @param {Object} [narrow] - A mapping of search terms for the backgrounds
 */
const launchImageLibrary = function (ref, narrow) {
  // Dismiss any existing modal
  $(".modal-dismiss").trigger("click");
  // Library is just a table of the selected images with their info
  // If user has approve or higher permission, they can edit things too
  let assetLibrary = $("<tbody>");
  // Overlay mode with a click to exit
  $("body")
    .addClass("no-scroll")
    .append(
      $("<div>", { class: "modal-dismiss" })
        .append(
          $("<div>", { class: "overlay-content" })
            .append(
              $("<button>", {
                class: "close btn",
                text: "close [x]",
              }).on("click", function (event) {
                $(".modal-dismiss").trigger("click");
                event.preventDefault();
              })
            )
            .append($("<h2>", { class: "text-center", text: "Image library" }))
            .append(function () {
              // Some info on the narrowing parameters for the image library
              if (narrow === undefined) {
                return null;
              } else if (narrow.asset) {
                return $("<h3>", {
                  class: "text-center",
                  text: "Showing single image",
                });
              } else {
                let searched = $("<h3>", { class: "text-center" });
                if (narrow.description) {
                  searched.append(
                    $("<span>", { text: "description: " + narrow.description })
                  );
                }
                if (narrow.license) {
                  searched.append(
                    $("<span>", { text: "license: " + narrow.license })
                  );
                }
                if (narrow.source) {
                  searched.append(
                    $("<span>", { text: "source: " + narrow.source })
                  );
                }
                if (narrow.tags) {
                  searched.append(
                    $("<span>", { text: "tags: " + narrow.tags })
                  );
                }
                // Reset button just re-launches with blank parameters
                searched.append(
                  $("<sup>", { class: "dismiss", text: "🗙" }).on(
                    "click",
                    function () {
                      launchImageLibrary(ref);
                    }
                  )
                );
                return searched;
              }
            })
            .append(
              $("<table>", { class: "asset-library" })
                .append(
                  $("<thead>").append(
                    $("<tr>")
                      .append($("<td>", { text: "Preview" }))
                      .append($("<td>", { text: "Info" }))
                  )
                )
                .append(assetLibrary)
            )
            .on("click", function (event) {
              event.stopPropagation();
            })
        )
        .on("click", function () {
          $("body").removeClass("no-scroll");
          $(this).remove();
        })
    );

  /**
   * Format metadata of an item in the image library as a table
   * cell.
   *
   * The edit state of the item is taken from the global scope.
   * The inclusion of the option to send image to a slide is
   * determined from the presence of a dest element.
   *
   * Editable state will be referenced from the global scope.
   *
   * @param {Object} item - mapping of the metadata for the element
   * @param {HTMLElement} [dest] - where to send a selected image
   */
  let formatItemInfo = function (item, dest) {
    // We are a table cell
    let formattedItem = $("<td>", { class: "item-info" });
    let assetHash = item.asset;
    /**
     * Tags get formatted as a list of clickable links that brings
     * up the list of images with that tag. Returned as a table
     * cell.
     *
     * @param {Array} tags - list of tag names
     * @returns {jQuery} - the cell containing the tags
     */
    let formatTags = function (tags) {
      // Standard cell in the image info table
      let tagsCell = $("<td>", {
        "data-key": "tags",
        class: "attribute-value",
      });
      // Shouldn't happen since server ensures this exists on save,
      // but just in case
      if (tags === undefined) {
        return tagsCell;
      }
      for (let i = 0; i < tags.length; i++) {
        // tag name needs to be wrapped in a second closure as
        // it gets mutated otherwise
        (function (e) {
          let tag = tags[e];
          tagsCell.append(
            $("<span>", { class: "tag-link", text: tag }).on(
              "click",
              function () {
                launchImageLibrary(ref, { tags: tag });
              }
            )
          );
          // Commas except for the last one
          if (i + 1 < tags.length) {
            tagsCell.append(", ");
          }
        })(i);
      }
      return tagsCell;
    };
    // Option to send to calling slide if there is one
    if (dest) {
      formattedItem.append(
        $("<div>").append(
          $("<button>", { text: "Add to slide" }).on("click", function () {
            // Standard updating content followed by a redraw
            let destSlide = $(dest).closest("form");
            let asset_url = "assets/" + item.asset + "/";
            // assign to slide content in the form attribute
            let formContent = destSlide.data("content");
            formContent.asset = asset_url;
            formContent.image = asset_url + "image.png";
            formContent.thumbnail = asset_url + "thumbnail.png";
            // Ensure that slide is in image mode and update
            destSlide.addClass("changed");
            destSlide.find(".image-properties").removeClass("d-none");
            destSlide.find(".background-pattern").addClass("d-none");
            destSlide.find(".slide-preview").trigger("redraw");
            // Go to that slide
            $(".modal-dismiss").trigger("click");
            scrollToMiddle(destSlide);
          })
        )
      );
    }
    // Metadata is a table that can be edited with the correct
    // permissions
    formattedItem.append(
      $("<table>", { class: "image-attributes" })
        .append(
          $("<tr>")
            .append($("<td>", { text: "Description:" }))
            .append(
              $("<td>", {
                "data-key": "description",
                class: "attribute-value",
                text: item.description,
              })
            )
        )
        .append(
          $("<tr>")
            .append($("<td>", { text: "License:" }))
            .append(
              $("<td>", {
                "data-key": "license",
                class: "attribute-value",
              }).append(
                $("<span>", { class: "tag-link", text: item.license }).on(
                  "click",
                  function () {
                    launchImageLibrary(ref, { license: item.license });
                  }
                )
              )
            )
        )
        .append(
          $("<tr>")
            .append($("<td>", { text: "Source:" }))
            .append(
              $("<td>", {
                "data-key": "source",
                class: "attribute-value",
                text: item.source,
              })
            )
        )
        .append(
          $("<tr>")
            .append($("<td>", { text: "Tags:" }))
            .append(formatTags(item.tags))
        )
    );

    // Enable editing of values in the table if permissions allow
    if (userData.admin) {
      formattedItem
        .append(
          $("<div>").append(
            $("<button>", { text: "Edit info" }).on("click", function () {
              // Button uses flip-flop data attribute
              let btn = $(this);
              if (btn.data("mode") === "edit") {
                // done editing, collect data and send to backend;
                // clear editable property on the fields.
                let updatedData = {};
                btn
                  .closest("td")
                  .find(".attribute-value")
                  .each(function () {
                    let elem = $(this);
                    updatedData[elem.data("key")] = elem.text();
                    elem.attr("contenteditable", null);
                  });
                // Patch will update the existing entry
                $.ajax("/assets/library/" + assetHash, {
                  type: "PATCH",
                  data: JSON.stringify(updatedData),
                  contentType: "application/json",
                  success: function (new_info) {
                    btn
                      .closest(".item-info")
                      .replaceWith(formatItemInfo(new_info, dest));
                  },
                });
              } else {
                // start editing, make fields editable and
                // button becomes "save"
                btn
                  .text("Save info")
                  .data("mode", "edit")
                  .closest("td")
                  .find(".attribute-value")
                  .attr("contenteditable", true)
                  .find(".tag-link")
                  .removeClass("tag-link")
                  .off();
              }
            })
          )
        )
        .append(
          $("<div>").append(
            $("<button>", { text: "Remove from library" }).on(
              "click",
              function () {
                let btn = $(this);
                $.ajax("/assets/library/" + item.asset, {
                  type: "DELETE",
                  success: function () {
                    if (narrow && narrow.asset) {
                      btn.closest(".modal-dismiss").trigger("click");
                    } else {
                      btn.closest("tr").remove();
                    }
                  },
                });
              }
            )
          )
        );
    }
    // Cell is complete
    return formattedItem;
  };

  // Add a table row for each item to the library element
  $.ajax("/assets/library", {
    data: narrow,
    contentType: "application/json",
    success: function (response) {
      assetLibrary.append(
        response["data"].map(function (item) {
          return $("<tr>")
            .append(
              $("<td>").append(
                $("<div>", { class: "slide-preview" }).append(
                  $("<div>", { class: "slide-preview-image" }).css({
                    "background-image":
                      "url('/assets/" + item.asset + "/thumbnail.png')",
                    "background-repeat": "no-repeat",
                    "background-size": "contain",
                  })
                )
              )
            )
            .append(formatItemInfo(item, ref));
        })
      );
    },
  });
};

/**
 * Move the slide divs into the order given. Inserts them
 * into the start of the #slides div in reverse order. Slides
 * that are not referenced get pushed to the end, and invalid
 * slides do nothing.
 *
 * @param {Array} order The id values of the slides in the required order
 * @param {HTMLElement|jQuery} target Container for the slides
 */
const reOrderSlides = function (order, target) {
  let slideList = $(target);
  // prependTo should just move as long as the selectors
  // only match a single item.
  order
    .slice()
    .reverse()
    .map(function (slide_id) {
      $("#slide-" + slide_id).prependTo(slideList);
    });
};

/**
 * Scroll the view vertically so that the centre of the element
 * is in the middle of the screen
 *
 * @param {jQuery|HTMLElement|String} elem The element, or selector to centre
 * @param {number} [timeout] Animation time in ms for the scroll, defaults to 750
 */
const scrollToMiddle = function (elem, timeout) {
  if (timeout === undefined) timeout = 750;
  let element = $(elem);
  if (!element.length) return;
  $("html, body").animate(
    {
      scrollTop:
        element.offset().top - $(window).height() / 2 + element.height() / 2,
    },
    timeout
  );
};

/**
 * Data corresponding to a single slide
 *
 * @constructor
 * @param {Object} [raw] - The raw JSON for a slide object
 */
const Slide = function (raw) {
  // Create a blank slide if nothing is given
  if (!raw) raw = {};
  // This is the numeric id from the database (if it has one)
  this.id = raw.id;
  // New slides have no id defined (assigned later by database)
  // uid is used to give form elements unique id values
  this.uid = this.id || randomString(6);
  // Remote slides have their origin, otherwise not defined
  this.origin = raw.origin;
  this.name = raw.name || "";
  this.description = raw.description || "";
  this.content = raw.content || {}; // Blob of content
  this.display_time = raw.display_time || 30;
  // Process date fields if they are not empty
  // A new slide has no dates set by default
  this.valid_from = maybeDate(raw.valid_from);
  this.valid_to = maybeDate(raw.valid_to);
  // Default to clock on
  this.show_clock = raw.show_clock !== false;
  this.created_by_id = raw.created_by_id;
  this.created = raw.created;
  this.updated_by_id = raw.updated_by_id;
  this.updated = raw.updated;
};

Slide.serializeForm = function (formElement) {
  let form = $(formElement);
  let data = {};
  // Extract all named fields
  form.find("[name]").each(function () {
    if (this.type === "range") {
      // Range returns a string by default
      data[this.name] = this.valueAsNumber;
    } else if (this.type === "checkbox") {
      // Checkboxes are compiled into individual items
      data[this.name] = this.checked;
    } else if (this.type === "select") {
      data[this.name] = this.value;
    } else {
      data[this.name] = this.value;
    }
  });

  return {
    id: form.data("id"),
    name: data.name,
    description: data.description,
    content: form.data("content"),
    valid_from: maybeDateTime(
      data["valid-from-date"],
      data["valid-from-time"],
      "00:00"
    ),
    valid_to: maybeDateTime(
      data["valid-to-date"],
      data["valid-to-time"],
      "23:59"
    ),
    display_time: data["display-time"],
    show_clock: data["show-clock"],
  };
};

/**
 * Generate the preview image version of the slide
 * with either background image or pattern,
 * and the overlay of the selected content type
 */
Slide.prototype.toSlidePreview = function () {
  let self = this;
  // General parameters for images.
  let slideCSS = {
    "background-size": this.content["image-fit"] || "cover",
    "background-repeat": "no-repeat",
  };
  // URL and some other parameters for the background depend on the
  // contents of the slide
  if (this.content["widget"] === "web_page") {
    // Web page is purely an image
    slideCSS["background-image"] =
      "url('/image/snapshot/480x270/?params=" +
      JSON.stringify({ url: this.content.src, zoom: 0.25, max_age: 60 }) +
      "')";
  } else {
    // Other images have a blur/transform property
    slideCSS["filter"] = "blur(" + (this.content["blur"] || 0) + "px)";
    slideCSS["transform"] =
      "scale(" + (1.0 + (this.content["blur"] || 0) / 100) + ")";

    if (this.content.image) {
      // Image just points to the image url
      let thumbUrl = this.content["thumbnail"];
      if (this.origin) {
        thumbUrl += "?origin=" + this.origin;
      }
      slideCSS["background-image"] = "url('/" + thumbUrl + "')";
    } else {
      // Default to a pattern (randomly chosen)
      if (this.content["pattern"] === undefined) {
        this.content["pattern"] = Math.floor(14 * Math.random());
      }
      if (!this.content["colours"]) {
        this.content["colours"] = randomString(
          Math.floor(8 * Math.random() + 1),
          "roygbivkw"
        );
      }
      slideCSS["background-image"] =
        "url('" +
        patternImg(this.content["pattern"], this.content["colours"]) +
        "')";
    }
  }
  // Add background css - Background is a child div of the slide so
  // that it can have blur without blurring the contents
  let slidePreview = $("<div>", { class: "slide-preview" }).append(
    $("<div>", {
      class: "slide-preview-image",
      id: "slide-preview-image-" + this.uid,
    }).css(slideCSS)
  );
  // Now add the specific widget content (default to text)
  if (this.content["widget"] === "calendar") {
    // Header box
    slidePreview.append(
      $("<div>", {
        class: "content-header calendar-header",
        text: this.content["header"],
      })
    );
    // Draw calendar inside a function that can be triggered
    // whenever the source of the data changes
    slidePreview
      .on("refresh-data", function () {
        $.ajax("/display/upcoming/events", {
          data: {
            src: self.content["src"],
            "accent-1": self.content["accent-1"],
            "accent-2": self.content["accent-2"],
            "accent-3": self.content["accent-3"],
          },
          success: function (data) {
            // Remove any calendar contents first (if it exists)
            slidePreview.find(".slide-calendar-container").remove();
            slidePreview
              .find(".content-url")
              .before(
                $("<div>", { class: "slide-calendar-container" }).append(
                  data.map(function (item) {
                    let start = new Date(item.start);
                    let startString =
                      start.getDate() + "<br>" + monthShort[start.getMonth()];
                    if (item["today"]) {
                      startString =
                        start.getHours() +
                        ":" +
                        ("0" + start.getMinutes()).slice(-2);
                    }
                    // Truncate the summary as extra lines become visible
                    // on Chrome
                    let summaryText = item["summary"];
                    if (summaryText.length > 60) {
                      summaryText = summaryText.substr(0, 87) + "...";
                    }

                    return $("<div>", { class: "calendar-item" })
                      .append(
                        $("<div>", {
                          class: "calendar-time",
                          html: startString,
                          style: "background:" + item["colour"],
                        })
                      )
                      .append(
                        $("<div>", {
                          class: "calendar-summary",
                          text: summaryText,
                        })
                      );
                  })
                )
              )
              .trigger("refresh"); // Set box/text colours after a re-draw
          },
        });
      })
      .on("refresh", function () {
        // Background image fit to view
        let backgroundSize = self.content["image-fit"] || "cover";
        let backgroundFill = self.content["background-fill"] || "#ffffff";
        $(this)
          .css("background", backgroundFill)
          .find(".slide-preview-image")
          .css("background-size", backgroundSize);

        // update box/text colours on slide, doesn't require a new
        // data fetch, just css changes
        let opacity = 1.0;
        if (self.content["box-opacity"] !== undefined) {
          opacity = self.content["box-opacity"];
        }
        let textColour = self.content["text-colour"] || "#FFFFFF";
        let boxColour = self.content["box-colour"] || "#808080";
        let textShadow = hexToShadow(textColour);
        let boxShadow = hexToShadow(boxColour, opacity);
        $(this)
          .find(".calendar-header")
          .css({
            color: self.content["text-colour"] || "#FFFFFF",
            textShadow: "0 1px 1px " + textShadow,
          });
        $(this)
          .find(".calendar-item")
          .css({
            color: self.content["text-colour"] || "#FFFFFF",
            backgroundColor: hexToRGBA(boxColour, opacity),
            textShadow: "0 1px 1px " + textShadow,
            boxShadow: "0 2px 2px " + boxShadow,
          });
      })
      .trigger("refresh-data");
  } else if (this.content["widget"] === "bookings") {
    // Draw bookings view inside a function that can be triggered
    // whenever the source of the data changes
    slidePreview
      .on("refresh-data", function () {
        $.ajax("/display/bookings/events", {
          data: {
            "booking-type": self.content["booking-type"],
            src: self.content["src"],
            room: self.content["room"],
            area: self.content["area"],
            "accent-1": self.content["accent-1"],
            "accent-2": self.content["accent-2"],
            "accent-3": self.content["accent-3"],
          },
        }).done(function (data) {
          let current = data[0] || { summary: "Current Event" };
          // Remove any calendar contents first (if it exists)
          slidePreview.find(".slide-bookings-container").remove();
          let bookingsPreview = $("<div>", {
            class: "slide-bookings-container",
          });
          if (String(self.content["layout"]).startsWith("cols-", 0)) {
            bookingsPreview
              .append(
                $("<div>", {
                  class: "content-header bookings-title",
                  text: self.content["header"],
                })
              )

              .append(
                $("<div>", { class: "slide-bookings-column mt-3" })
                  .append(
                    $("<div>", {
                      class: "bookings-header",
                    })
                  )
                  .append(
                    data.slice(0, 6).map(function (item) {
                      let start = new Date(item.start);
                      let startString =
                        start.getDate() + "<br>" + monthShort[start.getMonth()];
                      if (item["today"]) {
                        startString =
                          start.getHours() +
                          ":" +
                          ("0" + start.getMinutes()).slice(-2);
                      }
                      // Truncate the summary as extra lines become visible
                      // on Chrome
                      let summaryText = item["summary"];
                      if (summaryText.length > 60) {
                        summaryText = summaryText.substr(0, 87) + "...";
                      }
                      return $("<div>", { class: "bookings-item" })
                        .append(
                          $("<div>", {
                            class: "calendar-time",
                            html: startString,
                            style: "background:" + item["colour"],
                          })
                        )
                        .append(
                          $("<div>", {
                            class: "calendar-summary",
                            text: summaryText,
                          })
                        );
                    })
                  )
              )
              .append(
                $("<div>", { class: "slide-bookings-column mt-3" })
                  .append(
                    $("<div>", {
                      class: "bookings-header",
                    })
                  )
                  .append(
                    data.slice(6, 12).map(function (item) {
                      let start = new Date(item.start);
                      let startString =
                        start.getDate() + "<br>" + monthShort[start.getMonth()];
                      if (item["today"]) {
                        startString =
                          start.getHours() +
                          ":" +
                          ("0" + start.getMinutes()).slice(-2);
                      }
                      // Truncate the summary as extra lines become visible
                      // on Chrome
                      let summaryText = item["summary"];
                      if (summaryText.length > 60) {
                        summaryText = summaryText.substr(0, 87) + "...";
                      }
                      return $("<div>", { class: "bookings-item" })
                        .append(
                          $("<div>", {
                            class: "calendar-time",
                            html: startString,
                            style: "background:" + item["colour"],
                          })
                        )
                        .append(
                          $("<div>", {
                            class: "calendar-summary",
                            text: summaryText,
                          })
                        );
                    })
                  )
              );
          } else if (String(self.content["layout"]).startsWith("rooms-", 0)) {
            bookingsPreview
              .append(
                $("<div>", {
                  class: "content-header bookings-title ",
                  text: self.content["header"],
                })
              )
              .append(
                $("<div>", { class: "slide-bookings-column" })
                  .append(
                    $("<div>", {
                      class: "bookings-header",
                      text: "Meeting Room",
                    })
                  )
                  .append(
                    data.slice(0, 5).map(function (item) {
                      let start = new Date(item.start);
                      let startString =
                        start.getDate() + "<br>" + monthShort[start.getMonth()];
                      if (item["today"]) {
                        startString =
                          start.getHours() +
                          ":" +
                          ("0" + start.getMinutes()).slice(-2);
                      }
                      // Truncate the summary as extra lines become visible
                      // on Chrome
                      let summaryText = item["summary"];
                      if (summaryText.length > 60) {
                        summaryText = summaryText.substr(0, 87) + "...";
                      }
                      return $("<div>", { class: "bookings-item" })
                        .append(
                          $("<div>", {
                            class: "calendar-time",
                            html: startString,
                            style: "background:" + item["colour"],
                          })
                        )
                        .append(
                          $("<div>", {
                            class: "calendar-summary",
                            text: summaryText,
                          })
                        );
                    })
                  )
              )
              .append(
                $("<div>", { class: "slide-bookings-column" })
                  .append(
                    $("<div>", {
                      class: "bookings-header",
                      text: "Lecture Theatre",
                    })
                  )
                  .append(
                    data.slice(5, 10).map(function (item) {
                      let start = new Date(item.start);
                      let startString =
                        start.getDate() + "<br>" + monthShort[start.getMonth()];
                      if (item["today"]) {
                        startString =
                          start.getHours() +
                          ":" +
                          ("0" + start.getMinutes()).slice(-2);
                      }
                      // Truncate the summary as extra lines become visible
                      // on Chrome
                      let summaryText = item["summary"];
                      if (summaryText.length > 60) {
                        summaryText = summaryText.substr(0, 87) + "...";
                      }
                      return $("<div>", { class: "bookings-item" })
                        .append(
                          $("<div>", {
                            class: "calendar-time",
                            html: startString,
                            style: "background:" + item["colour"],
                          })
                        )
                        .append(
                          $("<div>", {
                            class: "calendar-summary",
                            text: summaryText,
                          })
                        );
                    })
                  )
              );
          } else {
            bookingsPreview
              .append(
                $("<div>", {
                  class: "slide-bookings-column slide-bookings-column-left",
                })
                  .append(
                    $("<div>", {
                      class: "content-header bookings-header",
                      text: self.content["header"],
                    })
                  )
                  .append(
                    $("<div>", {
                      class: "slide-bookings-current bookings-item",
                      text: current["summary"],
                    })
                      .append(
                        $("<div>", {
                          class: "slide-bookings-remaining",
                          text: "Started at 12:30 38 m remaining",
                        })
                      )
                      .append(
                        $("<div>", {
                          class: "slide-bookings-progress",
                          style:
                            "background:" +
                            (self.content["accent-2"] || accents["2"]),
                        }).append(
                          $("<div>", {
                            class: "slide-bookings-progress-complete",
                            style:
                              "background:" +
                              (self.content["accent-1"] || accents["1"]),
                          })
                        )
                      )
                  )
              )
              .append(
                $("<div>", { class: "slide-bookings-column" }).append(
                  data.slice(1, 7).map(function (item) {
                    let start = new Date(item.start);
                    let startString =
                      start.getDate() + "<br>" + monthShort[start.getMonth()];
                    if (item["today"]) {
                      startString =
                        start.getHours() +
                        ":" +
                        ("0" + start.getMinutes()).slice(-2);
                    }
                    // Truncate the summary as extra lines become visible
                    // on Chrome
                    let summaryText = item["summary"];
                    if (summaryText.length > 60) {
                      summaryText = summaryText.substr(0, 87) + "...";
                    }
                    return $("<div>", { class: "bookings-item" })
                      .append(
                        $("<div>", {
                          class: "calendar-time",
                          html: startString,
                          style: "background:" + item["colour"],
                        })
                      )
                      .append(
                        $("<div>", {
                          class: "calendar-summary",
                          text: summaryText,
                        })
                      );
                  })
                )
              );
          }
          slidePreview
            .find(".content-url")
            .before(bookingsPreview)
            .trigger("refresh"); // Set box/text colours after a re-draw
        });
      })
      .on("refresh", function () {
        // Background image fit to view
        let backgroundSize = self.content["image-fit"] || "cover";
        let backgroundFill = self.content["background-fill"] || "#ffffff";
        $(this)
          .css("background", backgroundFill)
          .find(".slide-preview-image")
          .css("background-size", backgroundSize);
        // update box/text colours on slide, doesn't require a new
        // data fetch, just css changes
        let opacity = 1.0;
        if (self.content["box-opacity"] !== undefined) {
          opacity = self.content["box-opacity"];
        }
        let textColour = self.content["text-colour"] || "#FFFFFF";
        let boxColour = self.content["box-colour"] || "#808080";
        let textShadow = hexToShadow(textColour);
        let boxShadow = hexToShadow(boxColour, opacity);
        $(this)
          .find(".bookings-header, .bookings-title")
          .css({
            color: self.content["text-colour"] || "#FFFFFF",
            textShadow: "0 1px 1px " + textShadow,
          });
        $(this)
          .find(".bookings-item")
          .css({
            color: self.content["text-colour"] || "#FFFFFF",
            backgroundColor: hexToRGBA(boxColour, opacity),
            textShadow: "0 1px 1px " + textShadow,
            boxShadow: "0 2px 2px " + boxShadow,
          });
      })
      .trigger("refresh-data");
  } else if (this.content["widget"] === "web_page") {
    // Nothing extra to add
    slidePreview.append();
  } else {
    // Text boxes as standard with no widget defined
    slidePreview
      .append(
        $("<div>", { class: "slide-text-container" })
          .append(
            $("<h2>", {
              id: "content-header-" + this.uid,
              class: "slide-text content-header",
              text: this.content["header"],
            })
          )
          .append(
            $("<p>", {
              id: "content-main-" + this.uid,
              class: "slide-text content-main",
              html: brJoin(this.content["main"]),
            })
          )
          .append(
            $("<p>", {
              id: "content-secondary-" + this.uid,
              class: "slide-text content-secondary",
              html: brJoin(this.content["secondary"]),
            })
          )
      )

      .on("refresh", function () {
        // Background image fit to view
        let backgroundSize = self.content["image-fit"] || "cover";
        let backgroundFill = self.content["background-fill"] || "#ffffff";
        $(this)
          .css("background", backgroundFill)
          .find(".slide-preview-image")
          .css("background-size", backgroundSize);

        // Colour of elements on the slide
        let opacity = 1.0;
        if (self.content["box-opacity"] !== undefined) {
          opacity = self.content["box-opacity"];
        }
        let textColour = self.content["text-colour"] || "#FFFFFF";
        let boxColour = self.content["box-colour"] || "#808080";
        let textShadow = hexToShadow(textColour);
        let boxShadow = hexToShadow(boxColour, opacity);
        // Layout of text
        let textContainerCSS = {
          "margin-left": "0%",
          "margin-right": "0%",
          width: "100%",
        };
        switch (self.content["box-position"]) {
          case "left":
            textContainerCSS["margin-right"] = "50%";
            textContainerCSS["width"] = "50%";
            break;
          case "right":
            textContainerCSS["margin-left"] = "50%";
            textContainerCSS["width"] = "50%";
            break;
        }
        $(this)
          .find(".slide-text-container")
          .css(textContainerCSS)
          .find(".slide-text")
          .css({
            color: self.content["text-colour"],
            backgroundColor: hexToRGBA(boxColour, opacity),
            textShadow: "0 1px 1px " + textShadow,
            boxShadow: "0 2px 2px " + boxShadow,
          });
      })
      .trigger("refresh")
      .on("text-resize", function () {
        // Check for overlap of text boxes
        let viewWindow = $(this);
        // Exit if we're not drawn yet
        if (viewWindow.height() === 0) return;
        let bottom = viewWindow[0].getBoundingClientRect().bottom;
        let fontSize = 0.8; // rem
        // Set minimum spacing based on 2% of the window height so it
        // looks the same with resizing
        let spacing = viewWindow.height() * 0.02;
        let contentMain = $(".content-main", viewWindow);
        let contentSecondary = $(".content-secondary", viewWindow);
        let textNodes = $("p", viewWindow);
        // Check if the middle boxes are too close, or fall of the end
        // of the page
        let overlap = function () {
          let mainBox = contentMain[0].getBoundingClientRect();
          let secondaryBox = contentSecondary[0].getBoundingClientRect();
          return (
            bottom - secondaryBox.bottom < spacing ||
            secondaryBox.top - mainBox.bottom < spacing
          );
        };
        // Scale boxes down until they fit, anything over 30 lines
        // looks stupid anyway so stop then
        for (let scaleFactor = 7; scaleFactor < 30; scaleFactor++) {
          textNodes.css("font-size", (7 * fontSize) / scaleFactor + "rem");
          // Always rescale at least once to allow scaling up and down
          if (!overlap()) break;
        }
      })
      .trigger("text-resize");
  }
  // Triggering "redraw" will cause the preview to overwrite itself
  // with new version based on the current content. Use for switching
  // widget where view changes completely
  slidePreview.on("redraw", function () {
    $(this).replaceWith(self.toSlidePreview());
  });
  // QR code
  slidePreview.append(
    $("<div>", {
      class: "content-url",
      text: this.content["url"],
    })
  );
  // Done
  return slidePreview;
};

Slide.prototype.toImagePane = function () {
  // A view of the slide and some background settings
  let self = this;
  // Triggering "reload" will redraw the whole panel useful
  // where a widget change needs several new options
  let imagePane = $("<fieldset>", {
    class: "slide-panel",
  })
    .append($("<legend>", { text: "Slide Image" }))
    .append(this.toSlidePreview())
    .on("reload", function () {
      $(this).replaceWith(self.toImagePane());
    });

  // Shortcut for web view which is only the image
  if (this.content["widget"] === "web_page") {
    return imagePane;
  }
  // Other views have interchangeable backgrounds
  return imagePane
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            class: "btn",
            for: "slide-upload-" + this.uid,
            text: "Upload image (or drag and drop)",
          })
        )
        .append(
          $("<input>", {
            class: "d-none file-upload-hidden",
            type: "file",
            name: "file",
            id: "slide-upload-" + this.uid,
          }).on("change", uploadSlideAction)
        )
    )
    .append(
      $("<div>", { class: "form-row" }).append(
        $("<button>", { type: "button", text: "Image Library" }).on(
          "click",
          function () {
            launchImageLibrary(this);
          }
        )
      )
    )
    .append(
      $("<div>", {
        id: "image-properties-" + this.uid,
        class: "image-properties",
      })
        .addClass(self.content.image ? null : "d-none")
        .append(
          $("<div>", { class: "form-row" })
            .append(
              $("<button>", {
                type: "button",
                text: "Remove image",
              }).on("click", function () {
                // Clear image contents and redraw pattern background
                delete self.content.image;
                delete self.content.thumbnail;
                delete self.content.asset;
                delete self.content["image-fit"];
                delete self.content["background-fill"];

                $(this).closest(".image-properties").addClass("d-none");
                $(this)
                  .closest(".slide-panel")
                  .find(".slide-preview-image")
                  .css(
                    "background-image",
                    "url('" +
                      patternImg(
                        self.content["pattern"],
                        self.content["colours"]
                      ) +
                      "')"
                  );
                $(this)
                  .closest(".slide-panel")
                  .find(".background-pattern")
                  .removeClass("d-none");
                $(this)
                  .closest("form")
                  .data("content", self.content)
                  .addClass("changed");
              })
            )
            .append(
              $("<button>", {
                class: "btn-active",
                text: "Save in library",
                role: "button",
              }).on("click", function () {
                let assetHash = asset_re.exec(self.content.asset)[1];
                let image = self.content.image;
                let thumbnail = self.content.thumbnail;
                $.ajax("/assets/library/" + assetHash, {
                  type: "PATCH",
                  data: JSON.stringify({
                    image: image,
                    thumbnail: thumbnail,
                  }),
                  contentType: "application/json",
                  success: function () {
                    launchImageLibrary(null, { asset: assetHash });
                  },
                });
              })
            )
        )
        .append(
          $("<div>", { class: "form-row" })
            .append(
              $("<label>", {
                for: "image-fit-select-" + this.uid,
                text: "Scale Image",
              })
            )
            .append(
              $("<select>", { id: "image-fit-select-" + this.uid })
                .append($("<option>", { value: "cover", text: "Fill screen" }))
                .append(
                  $("<option>", { value: "contain", text: "Fit to screen" })
                )
                .val(this.content["image-fit"] || "cover")
                .on("change", function () {
                  self.content["image-fit"] = this.value;
                  $(this)
                    .closest("form")
                    .find(".slide-preview")
                    .trigger("refresh");
                })
            )
        )
        .append(
          $("<div>", { class: "form-row" })
            .append(
              $("<label>", {
                for: "background-fill-" + this.uid,
                text: "Background fill: ",
              })
            )
            .append(
              $("<input>", {
                type: "color",
                id: "background-fill-" + this.uid,
                value: this.content["background-fill"] || "#ffffff",
              }).on("input", function () {
                self.content["background-fill"] = this.value;
                $(this)
                  .closest("form")
                  .data("content", self.content)
                  .addClass("changed")
                  .find(".slide-preview")
                  .trigger("refresh");
              })
            )
        )
    )
    .append(
      $("<div>", {
        id: "background-pattern-" + this.uid,
        class: "background-pattern",
      })
        .addClass(this.content.image ? "d-none" : null)
        .append(
          $("<div>", { class: "form-row" })
            .append(
              $("<label>", {
                for: "content-pattern-" + this.uid,
                text: "Background pattern: ",
              })
            )
            .append(
              $("<input>", {
                type: "number",
                min: 0,
                max: 13,
                size: 2,
                id: "content-pattern-" + this.uid,
                name: "pattern",
                value: this.content["pattern"],
              }).on("input", function (event) {
                self.content["pattern"] = this.value;
                $(event.target)
                  .closest("form")
                  .data("content", self.content)
                  .addClass("changed");
                $(event.target)
                  .closest(".slide-panel")
                  .find(".slide-preview-image")
                  .css(
                    "background-image",
                    "url('" +
                      patternImg(
                        self.content["pattern"],
                        self.content["colours"]
                      ) +
                      "')"
                  );
              })
            )
        )
        .append(
          $("<div>", { class: "form-row" })
            .append(
              $("<label>", {
                for: "content-colours-" + this.uid,
                text: "Colours: ",
              }).append(
                colours.map(function (element) {
                  return $("<span>", {
                    class: "bg-pattern-colour-btn",
                    text: element.code,
                    title: element.name,
                  })
                    .css("color", element.hex)
                    .on("click", function () {
                      let inp = $("#content-colours-" + self.uid);
                      inp.val(inp.val() + element.code).trigger("input");
                    });
                })
              )
            )
            .append(
              $("<input>", {
                type: "text",
                minlength: 1,
                maxlength: 10,
                size: 10,
                pattern: "[roygbivkw]+",
                value: this.content["colours"],
                id: "content-colours-" + this.uid,
                name: "colours",
              }).on("input", function (event) {
                self.content["colours"] = this.value;
                $(event.target)
                  .closest("form")
                  .data("content", self.content)
                  .addClass("changed");
                $(event.target)
                  .closest(".slide-panel")
                  .find(".slide-preview-image")
                  .css(
                    "background-image",
                    "url('" +
                      patternImg(
                        self.content["pattern"],
                        self.content["colours"]
                      ) +
                      "')"
                  );
              })
            )
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-blur-" + this.uid,
            text: "Blur image: ",
          })
        )
        .append(
          $("<input>", {
            type: "range",
            min: 0,
            max: 4,
            step: 0.2,
            id: "content-blur-" + this.uid,
            name: "blur",
            value: this.content["blur"] || 0,
          }).on("input", function () {
            self.content["blur"] = this.valueAsNumber;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".slide-preview-image")
              .css({
                filter: "blur(" + self.content["blur"] + "px)",
                transform: "scale(" + (1.0 + self.content["blur"] / 100) + ")",
              });
          })
        )
    )

    .on("drag dragstart dragover dragenter", function () {
      $(this).addClass("drag-over");
      return false;
    })
    .on("dragend dragleave drop", function () {
      $(this).removeClass("drag-over");
      return false;
    })
    .on("drop", uploadSlideAction);
};

/**
 * Content on the slide may be either the basic text layout
 * or a specific widget showing different information. This
 * function creates a pane and puts the contents of the
 * required widget inside with a switcher at the top.
 *
 * @returns {jQuery}
 */
Slide.prototype.toContentPane = function () {
  let self = this;
  let contentPane = $("<fieldset>", {
    class: "slide-content-fields",
  })
    .append($("<legend>", { text: "Slide Content" }))
    // The widget defines the content. When changing widget, just reload
    // the whole panel as the new widget customisations
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-widget-select-" + this.uid,
            text: "Content Type",
          })
        )
        .append(
          $("<select>", { id: "content-widget-select-" + this.uid })
            .append($("<option>", { value: "text", text: "Text" }))
            .append(
              $("<option>", { value: "calendar", text: "Events Calendar" })
            )
            .append($("<option>", { value: "bookings", text: "Room Bookings" }))
            .append($("<option>", { value: "web_page", text: "Web Page" }))
            .val(this.content["widget"] || "text")
            .on("change", function () {
              self.content["widget"] = this.value;
              $(this).closest("form").find(".slide-panel").trigger("reload");
              $(this).closest("fieldset").replaceWith(self.toContentPane());
            })
        )
    );
  // Choose the content to show
  if (this.content["widget"] === "calendar") {
    contentPane.append(this.toCalendarPane());
  } else if (this.content["widget"] === "bookings") {
    contentPane.append(this.toBookingsPane());
  } else if (this.content["widget"] === "web_page") {
    contentPane.append(this.toWebPagePane());
  } else {
    // Default to just the text view
    contentPane.append(this.toTextPane());
  }
  return contentPane;
};

/**
 * The text panel just has the contents of three boxes:
 * a header, a main text, and a secondary text
 * the second two are textarea boxes.
 *
 * Return the contents of the customisation panel,
 * to go after the widget selector.
 *
 * @returns {jQuery}
 */
Slide.prototype.toTextPane = function () {
  let self = this;
  return $("<div>", { class: "content-editing slide-text-fields" })
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-header-" + this.uid,
            text: "Header: ",
          })
        )
        .append(
          $("<input>", {
            id: "content-header-" + this.uid,
            value: this.content["header"],
          }).on("input", function () {
            self.content["header"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".content-header")
              .text(self.content["header"]);
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-main-" + this.uid,
            text: "Main text: ",
          })
        )
        .append(
          $("<textarea>", {
            rows: 3,
            id: "content-main-" + this.uid,
            text: this.content["main"],
          }).on("input", function () {
            self.content["main"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".content-main")
              .html(brJoin(self.content["main"]))
              .trigger("text-resize");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-secondary-" + this.uid,
            text: "Secondary text: ",
          })
        )
        .append(
          $("<textarea>", {
            rows: 3,
            id: "content-secondary-" + this.uid,
            text: this.content["secondary"],
          }).on("input", function () {
            self.content["secondary"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".content-secondary")
              .html(brJoin(self.content["secondary"]))
              .trigger("text-resize");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-url-" + this.uid,
            text: "Link for QR code: ",
            title: "A URL that will be converted to a QR code on the slide.",
          })
        )
        .append(
          $("<input>", {
            id: "content-url-" + this.uid,
            value: this.content["url"],
          }).on("input", function () {
            self.content["url"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".content-url")
              .text(self.content["url"]);
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-colour-" + this.uid,
            text: "Text colour: ",
          })
        )
        .append(
          $("<input>", {
            type: "color",
            id: "content-colour-" + this.uid,
            value: this.content["text-colour"] || "#ffffff",
          }).on("input", function () {
            self.content["text-colour"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-box-colour-" + this.uid,
            text: "Box colour: ",
          })
        )
        .append(
          $("<input>", {
            type: "color",
            id: "content-box-colour-" + this.uid,
            value: this.content["box-colour"] || "#808080",
          }).on("input", function () {
            self.content["box-colour"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-box-transparency-" + self.uid,
            text: "Box transparency: ",
          })
        )
        .append(
          $("<input>", {
            type: "range",
            min: 0,
            max: 1,
            step: 0.01,
            value: 1 - this.content["box-opacity"] || 0,
            id: "content-box-transparency-" + self.uid,
            name: "box-transparency",
          }).on("input", function () {
            self.content["box-opacity"] = 1 - this.valueAsNumber;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-box-position-" + self.uid,
            text: "Text position: ",
          })
        )
        .append(
          $("<select>", {
            id: "content-box-position-" + self.uid,
            name: "box-position",
          })
            .append($("<option>", { value: "left", text: "Left" }))
            .append($("<option>", { value: "middle", text: "Middle" }))
            .append($("<option>", { value: "right", text: "Right" }))
            .val(self.content["box-position"] || "middle")
            .on("change", function () {
              self.content["box-position"] = this.value;
              $(this)
                .closest("form")
                .data("content", self.content)
                .addClass("changed")
                .find(".slide-preview")
                .trigger("refresh")
                .trigger("text-resize");
            })
        )
    );
};

Slide.prototype.toCalendarPane = function () {
  let self = this;
  return $("<div>", { class: "content-editing slide-calendar-fields" })
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-header-" + this.uid,
            text: "Header: ",
          })
        )
        .append(
          $("<input>", {
            id: "content-header-" + this.uid,
            value: this.content["header"],
          }).on("input", function () {
            self.content["header"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".content-header")
              .text(self.content["header"]);
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-src-" + this.uid,
            text: "Data source (.ics): ",
            title:
              "Link to an ics location:\n" +
              "'Subscribe using ical/vcal' on talks.cam.ac.uk;\n" +
              "'Settings and sharing -> address in iCal format' in Google calendar;\n" +
              "'View all Outlook settings -> Shared calendars' in Outlook.",
          })
        )
        .append(
          $("<input>", {
            id: "content-src-" + this.uid,
            value: this.content["src"],
          }).on("input", function () {
            self.content["src"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh-data");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-url-" + this.uid,
            text: "Link for QR code: ",
            title: "A URL that will be converted to a QR code on the slide.",
          })
        )
        .append(
          $("<input>", {
            id: "content-url-" + this.uid,
            value: this.content["url"],
          }).on("input", function () {
            self.content["url"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".content-url")
              .text(self.content["url"]);
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-colour-" + this.uid,
            text: "Text colour: ",
          })
        )
        .append(
          $("<input>", {
            type: "color",
            id: "content-colour-" + this.uid,
            value: this.content["text-colour"] || "#ffffff",
          }).on("input", function () {
            self.content["text-colour"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-box-colour-" + this.uid,
            text: "Box colour: ",
          })
        )
        .append(
          $("<input>", {
            type: "color",
            id: "content-box-colour-" + this.uid,
            value: this.content["box-colour"] || "#808080",
          }).on("input", function () {
            self.content["box-colour"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-box-transparency-" + self.uid,
            text: "Box transparency: ",
          })
        )
        .append(
          $("<input>", {
            type: "range",
            min: 0,
            max: 1,
            step: 0.01,
            value: 1 - this.content["box-opacity"] || 0,
            id: "content-box-transparency-" + self.uid,
            name: "box-transparency",
          }).on("input", function () {
            self.content["box-opacity"] = 1 - this.valueAsNumber;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append($("<label>", { text: "Accents" }))
        .append(
          $("<input>", {
            type: "color",
            id: "content-accent-1-" + this.uid,
            value: this.content["accent-1"] || accents["1"],
          }).on("change", function () {
            self.content["accent-1"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh-data");
          })
        )
        .append(
          $("<input>", {
            type: "color",
            id: "content-accent-2-" + this.uid,
            value: this.content["accent-2"] || accents["2"],
          }).on("change", function () {
            self.content["accent-2"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh-data");
          })
        )
        .append(
          $("<input>", {
            type: "color",
            id: "content-accent-3-" + this.uid,
            value: this.content["accent-3"] || accents["3"],
          }).on("change", function () {
            self.content["accent-3"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh-data");
          })
        )
    );
};

Slide.prototype.toBookingsPane = function () {
  let self = this;
  let bookingType = this.content["booking-type"] || "mrbs";
  let layoutType = this.content["layout"] || "default";
  return $("<div>", { class: "content-editing slide-bookings-fields" })
    .attr("data-booking-type", bookingType)
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-booking-type-" + this.uid,
            text: "Booking type: ",
          })
        )
        .append(
          $("<select>", { id: "content-booking-type-" + this.uid })
            .append($("<option>", { value: "mrbs", text: "MRBS" }))
            .append($("<option>", { value: "booker", text: "Booker" }))
            .val(bookingType)
            .on("change", function () {
              self.content["booking-type"] = this.value;
              $(this)
                .closest(".slide-bookings-fields")
                .attr("data-booking-type", this.value)
                .closest("form")
                .addClass("changed")
                .find(".slide-preview")
                .trigger("refresh-data");
            })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-header-" + this.uid,
            text: "Header: ",
          })
        )
        .append(
          $("<input>", {
            id: "content-header-" + this.uid,
            value: this.content["header"],
          }).on("input", function () {
            self.content["header"] = this.value;
            $(this)
              .closest("form")
              .addClass("changed")
              .find(".content-header")
              .text(self.content["header"]);
          })
        )
    )
    .append(
      $("<div>", { class: "form-row not-booker" })
        .append(
          $("<label>", {
            for: "content-src-" + this.uid,
            text: "Source address: ",
            title:
              "Link to a bookings source web resource:\n" +
              "MRBS: https://your.mrbs.cam.ac.uk/;\n",
          })
        )
        .append(
          $("<input>", {
            id: "content-src-" + this.uid,
            value: this.content["src"],
          }).on("input", function () {
            self.content["src"] = this.value;
            $(this)
              .closest("form")
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh-data");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-room-" + this.uid,
            text: "Room identifier(s): ",
            title:
              "Major identifier for the requested room:\n" +
              "MRBS: A 'room' name search term, will include all matching rooms;\n" +
              "Booker: A room number, or list of room numbers, which can be\n" +
              "        found in the URLs of the rooms on the booker site;\n",
          })
        )
        .append(
          $("<input>", {
            id: "content-room-" + this.uid,
            value: this.content["room"],
          }).on("input", function () {
            self.content["room"] = this.value;
            $(this)
              .closest("form")
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh-data");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row not-booker" })
        .append(
          $("<label>", {
            for: "content-area-" + this.uid,
            text: "Secondary identifier: ",
            title:
              "Secondary identifier for the requested room:\n" +
              "MRBS: An 'area' search term, will show all rooms;\n",
          })
        )
        .append(
          $("<input>", {
            id: "content-area-" + this.uid,
            value: this.content["area"],
          }).on("input", function () {
            self.content["area"] = this.value;
            $(this)
              .closest("form")
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh-data");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-url-" + this.uid,
            text: "Link for QR code: ",
            title: "A URL that will be converted to a QR code on the slide.",
          })
        )
        .append(
          $("<input>", {
            id: "content-url-" + this.uid,
            value: this.content["url"],
          }).on("input", function () {
            self.content["url"] = this.value;
            $(this)
              .closest("form")
              .addClass("changed")
              .find(".content-url")
              .text(self.content["url"]);
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-layout-" + this.uid,
            text: "Layout: ",
          })
        )
        .append(
          $("<select>", { id: "content-layout-" + this.uid })
            .append(
              $("<option>", { value: "default", text: "Columns: Now + 7" })
            )
            .append(
              $("<option>", { value: "cols-6+6", text: "Columns: 6 + 6" })
            )
            .append(
              $("<option>", { value: "cols-7+7", text: "Columns: 7 + 7" })
            )
            .append(
              $("<option>", { value: "cols-9+9", text: "Columns: 9 + 9" })
            )
            .append(
              $("<option>", { value: "cols-8+8+8", text: "Columns: 8 + 8 + 8" })
            )
            .append($("<option>", { value: "rooms-6", text: "Rooms: 6 each" }))
            .append($("<option>", { value: "rooms-9", text: "Rooms: 9 each" }))
            .val(layoutType)
            .on("change", function () {
              self.content["layout"] = this.value;
              $(this)
                .closest("form")
                .addClass("changed")
                .find(".slide-preview")
                .trigger("refresh-data");
            })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-details-" + this.uid,
            text: "Show locations: ",
            title: "Add location information to event tiles",
          })
        )
        .append(
          $("<input>", {
            type: "checkbox",
            id: "content-details-" + this.uid,
            checked: this.content["details"],
          }).on("input", function () {
            self.content["details"] = this.checked;
            $(this)
              .closest("form")
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh-data");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-colour-" + this.uid,
            text: "Text colour: ",
          })
        )
        .append(
          $("<input>", {
            type: "color",
            id: "content-colour-" + this.uid,
            value: this.content["text-colour"] || "#ffffff",
          }).on("input", function () {
            self.content["text-colour"] = this.value;
            $(this)
              .closest("form")
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-box-colour-" + this.uid,
            text: "Box colour: ",
          })
        )
        .append(
          $("<input>", {
            type: "color",
            id: "content-box-colour-" + this.uid,
            value: this.content["box-colour"] || "#808080",
          }).on("input", function () {
            self.content["box-colour"] = this.value;
            $(this)
              .closest("form")
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-box-transparency-" + self.uid,
            text: "Box transparency: ",
          })
        )
        .append(
          $("<input>", {
            type: "range",
            min: 0,
            max: 1,
            step: 0.01,
            value: 1 - this.content["box-opacity"] || 0,
            id: "content-box-transparency-" + self.uid,
            name: "box-transparency",
          }).on("input", function () {
            self.content["box-opacity"] = 1 - this.valueAsNumber;
            $(this)
              .closest("form")
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append($("<label>", { text: "Accents" }))
        .append(
          $("<input>", {
            type: "color",
            id: "content-accent-1-" + this.uid,
            value: this.content["accent-1"] || accents["1"],
          }).on("change", function () {
            self.content["accent-1"] = this.value;
            $(this)
              .closest("form")
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh-data");
          })
        )
        .append(
          $("<input>", {
            type: "color",
            id: "content-accent-2-" + this.uid,
            value: this.content["accent-2"] || accents["2"],
          }).on("change", function () {
            self.content["accent-2"] = this.value;
            $(this)
              .closest("form")
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh-data");
          })
        )
        .append(
          $("<input>", {
            type: "color",
            id: "content-accent-3-" + this.uid,
            value: this.content["accent-3"] || accents["3"],
          }).on("change", function () {
            self.content["accent-3"] = this.value;
            $(this)
              .closest("form")
              .addClass("changed")
              .find(".slide-preview")
              .trigger("refresh-data");
          })
        )
    );
};

Slide.prototype.toWebPagePane = function () {
  let self = this;
  let initialMaxAge =
    this.content["max_age"] === undefined ? 10 : this.content["max_age"];
  return $("<div>", { class: "content-editing slide-web-page-fields" })
    .toggleClass("web-live", self.content["live"] || false)
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-src-" + this.uid,
            text: "Web Page URL: ",
            title:
              "Complete URL of page to display.\n" +
              "Some pages may not display correctly.\n" +
              "Check the rendered view using the preview.",
          })
        )
        .append(
          $("<input>", {
            id: "content-src-" + this.uid,
            value: this.content["src"],
          }).on("change", function () {
            self.content["src"] = this.value;
            let params = {
              url: self.content["src"],
              zoom: 0.25,
              max_age: 60,
            };
            let imageURL =
              "/image/snapshot/480x270?params=" + JSON.stringify(params);
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".slide-preview-image")
              .css({
                "background-image": "url('" + imageURL + "')",
                filter: "none",
                transform: "none",
              });
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-live-" + this.uid,
            text: "Live view: ",
            title:
              "If supported, show a live version of the page, not a snapshot.",
          })
        )
        .append(
          $("<input>", {
            type: "checkbox",
            id: "content-live-" + this.uid,
            checked: this.content["live"],
          }).on("input", function () {
            let maxAgeValue = self.content["max_age"];
            self.content["live"] = this.checked;
            let rangeAttr = this.checked
              ? { max: 360, min: 0, step: 10, value: maxAgeValue }
              : { max: 60, min: 1, step: 1, value: maxAgeValue };
            $(this)
              .closest(".slide-web-page-fields")
              .toggleClass("web-live", this.checked)
              .find(".max-age-range")
              .attr(rangeAttr)
              .closest("form")
              .data("content", self.content)
              .addClass("changed");
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-max-age-" + this.uid,
            text: "Refresh interval",
            title: "Store a cached copy of the page for faster loading.",
          })
        )
        .append(
          $("<input>", {
            type: "range",
            min: self.content["live"] ? 0 : 1,
            max: self.content["live"] ? 360 : 60,
            step: self.content["live"] ? 10 : 1,
            value: initialMaxAge,
            id: "content-max-age-" + self.uid,
            class: "max-age-range",
          }).on("input", function () {
            self.content["max_age"] = this.valueAsNumber;
            $(this)
              .siblings(".range-value")
              .text(this.valueAsNumber + " m");
          })
        )
        .append(
          $("<span>", {
            class: "range-value",
            text: initialMaxAge + " m",
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "content-zoom-" + this.uid,
            text: "Zoom page",
            title: "Zoom the view of the page to make it bigger or smaller.",
          })
        )
        .append(
          $("<input>", {
            type: "range",
            min: 0.5,
            max: 3.0,
            step: 0.1,
            value: this.content["zoom"] || 1.0,
            id: "content-zoom-" + self.uid,
          }).on("input", function () {
            self.content["zoom"] = this.valueAsNumber;
            $(this)
              .siblings(".range-value")
              .text(Math.round(this.valueAsNumber * 100) + "%");
          })
        )
        .append(
          $("<span>", {
            class: "range-value",
            text: Math.round((this.content["zoom"] || 1) * 100) + "%",
          })
        )
    )
    .append(
      $("<div>", { class: "form-row not-web-live" })
        .append(
          $("<label>", {
            for: "content-wait-" + this.uid,
            text: "Extra load time",
            title:
              "Allow extra time for loading the page" +
              " if it's not rendering properly.",
          })
        )
        .append(
          $("<input>", {
            type: "range",
            min: 0,
            max: 6,
            value:
              this.content["wait"] === undefined ? 1 : this.content["wait"],
            id: "content-wait-" + self.uid,
          }).on("input", function () {
            self.content["wait"] = this.valueAsNumber;
            $(this)
              .siblings(".range-value")
              .text(this.valueAsNumber + " s");
          })
        )
        .append(
          $("<span>", {
            class: "range-value",
            text:
              (this.content["wait"] === undefined ? 1 : this.content["wait"]) +
              " s",
          })
        )
    )
    .append(
      $("<div>", { class: "form-row not-web-live" })
        .append(
          $("<label>", {
            for: "content-url-" + this.uid,
            text: "Link for QR code: ",
            title: "A URL that will be converted to a QR code on the slide.",
          })
        )
        .append(
          $("<input>", {
            id: "content-url-" + this.uid,
            value: this.content["url"],
          }).on("input", function () {
            self.content["url"] = this.value;
            $(this)
              .closest("form")
              .data("content", self.content)
              .addClass("changed")
              .find(".content-url")
              .text(self.content["url"]);
          })
        )
    );
};

/**
 * Create a pane with editing controls for properties of
 * the slide. These correspond to all the non-content
 * parts of the data.
 * @returns {HTMLElement|jQuery} fieldset
 */
Slide.prototype.toParametersPane = function () {
  let self = this;
  let thisPreviewUrl = previewUrl(this.id, this.origin);
  let slideParameters = $("<fieldset>", {
    class: "slide-parameters",
  })
    .append($("<legend>", { text: "Slide Parameters" }))
    .append(
      $("<input>", {
        type: "checkbox",
        checked: self.active ? "checked" : null,
        visibility: "hidden",
        name: "active",
        disabled: "disabled",
      }).css("display", "none")
    )
    .append(
      $("<div>", { class: "form-row" })
        .append($("<label>", { for: "name-" + self.uid, text: "Name: " }))
        .append(
          $("<input>", {
            type: "text",
            value: this.name,
            id: "name-" + self.uid,
            name: "name",
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "description-" + self.uid,
            text: "Description: ",
          })
        )
        .append(
          $("<textarea>", {
            rows: 2,
            text: this.description,
            id: "description-" + self.uid,
            name: "description",
          })
        )
    )
    // start time picker
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "valid-from-date-" + self.uid,
            text: "Start date and time: ",
          })
        )
        .append(
          $("<input>", {
            type: "date",
            value: dateToInputDate(this.valid_from),
            id: "valid-from-date" + self.uid,
            name: "valid-from-date",
            "aria-label": "Valid from date",
          })
        )
        .append(
          $("<input>", {
            type: "time",
            value: dateToInputTime(this.valid_from),
            id: "valid-from-time-" + self.uid,
            name: "valid-from-time",
            "aria-label": "Valid from time",
          })
        )
    )
    // end time picker
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "valid-to-date-" + self.uid,
            text: "End date and time: ",
          })
        )
        .append(
          $("<input>", {
            type: "date",
            value: dateToInputDate(this.valid_to),
            id: "valid-to-date-" + self.uid,
            name: "valid-to-date",
            "aria-label": "Valid to date",
          })
        )
        .append(
          $("<input>", {
            type: "time",
            value: dateToInputTime(this.valid_to),
            id: "valid-to-time-" + self.uid,
            name: "valid-to-time",
            "aria-label": "Valid to time",
          })
        )
        .on("change", function () {
          // Some visual indication that the time has expired (or will soon)
          let row = $(this);
          let now = new Date();
          let thisDate = new Date(
            row.find("input[type=date]").val() +
              "T" +
              row.find("input[type=time]").val()
          );
          if (thisDate < now) {
            // It's in the past
            row.addClass("past").removeClass("soon");
          } else if (thisDate - now < 24 * 60 * 60 * 1000) {
            // It's within 24 hours
            row.addClass("soon").removeClass("past");
          } else {
            // Far enough in the future that it's fine
            row.removeClass("soon past");
          }
        })
        .trigger("change")
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "display-time-" + self.uid,
            text: "Display time: ",
          })
        )
        .append(
          $("<input>", {
            type: "range",
            min: 1,
            max: 120,
            value: this.display_time,
            id: "display-time-" + self.uid,
            name: "display-time",
          }).on("input", function () {
            $(this)
              .siblings(".range-value")
              .text(this.valueAsNumber + " s");
          })
        )
        .append(
          $("<span>", { class: "range-value", text: this.display_time + " s" })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            text: "Show clock: ",
            for: "show-clock-" + self.uid,
            title: "Select if clock is show for this slide.",
          })
        )
        .append(
          $("<input>", {
            type: "checkbox",
            id: "show-clock-" + self.uid,
            name: "show-clock",
            checked: self.show_clock,
          })
        )
    );
  if (self.created_by_id !== undefined) {
    slideParameters
      .append(
        $("<div>", { class: "form-row" })
          .append($("<label>", { text: "Created: " }))
          .append($("<span>", { text: this.created_by_id }))
          .append(
            $("<span>", {
              text: sinceDate(this.created),
              title: this.created,
            })
          )
      )
      .append(
        $("<div>", { class: "form-row" })
          .append($("<label>", { text: "Updated: " }))
          .append($("<span>", { text: this.updated_by_id }))
          .append(
            $("<span>", {
              text: sinceDate(this.updated),
              title: this.updated,
            })
          )
      );
  }
  // Buttons to change the ordering of the slides, only if it's already saved
  if (self.id !== undefined) {
    slideParameters.append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            text: "Re-order: ",
          })
        )
        .append(
          $("<button>", { type: "button", text: "Up" })
            .prepend(
              $("<i>", {
                class: "material-icons md-inline",
                text: "arrow_upward",
              })
            )
            .on("click", function () {
              let thisSlide = $(this).closest(".slide");
              let slideList = $(this).closest(".slide-list");
              let groupId = $(this).closest(".slide-group").data("groupId");
              $.ajax(basePrefix + "/slide_groups/" + groupId + "/order", {
                type: "PATCH",
                data: JSON.stringify({ op: "move", id: self.id, value: -1 }),
                contentType: "application/json",
                success: function (response) {
                  reOrderSlides(response.data, slideList);
                  scrollToMiddle(thisSlide);
                },
              });
            })
        )
        .append(
          $("<button>", { type: "button", text: "Down" })
            .prepend(
              $("<i>", {
                class: "material-icons md-inline",
                text: "arrow_downward",
              })
            )
            .on("click", function () {
              let thisSlide = $(this).closest(".slide");
              let slideList = $(this).closest(".slide-list");
              let groupId = $(this).closest(".slide-group").data("groupId");
              $.ajax(basePrefix + "/slide_groups/" + groupId + "/order", {
                type: "PATCH",
                data: JSON.stringify({ op: "move", id: self.id, value: 1 }),
                contentType: "application/json",
                success: function (response) {
                  reOrderSlides(response.data, slideList);
                  scrollToMiddle(thisSlide);
                },
              });
            })
        )
    );
  }
  // Save and delete are always shown, even if they behave slightly differently
  slideParameters.append(
    $("<div>", { class: "form-row" })
      .append(
        $("<button>", {
          type: "submit",
          name: "submit",
          class: "save-button",
          text: "Save",
        }).prepend(icon("save"))
      )
      .append(
        $("<button>", {
          type: "button",
          name: "delete",
          class: "delete-button",
          text: "Delete Slide",
        })
          .prepend(icon("delete"))
          .on("click", function () {
            let formElement = $(this).closest("form");
            if (self.id === undefined) {
              formElement.replaceWith(
                $("<form>", { class: "slide slide-row" })
                  .append(
                    $("<div>", { class: "form-row", text: "Deleted" }).prepend(
                      $("<button>", {
                        text: "close",
                        class: "material-icons md-inline mr-1",
                      })
                    )
                  )
                  .on("click", function () {
                    $(this).closest("form").remove();
                  })
              );
            } else if (confirm("Are you sure?")) {
              let formElement = $(this).closest("form");
              $.ajax(basePrefix + "/slides/" + self.id, {
                type: "DELETE",
                success: function (response) {
                  // Store the data from the deleted slide
                  let deletedSlide = $("<form>", {
                    class: "slide slide-row",
                  }).append(
                    $("<div>", {
                      class: "form-row",
                      text: "Undo Delete",
                    }).prepend(
                      $("<button>", {
                        text: "undo",
                        class: "material-icons md-inline mr-1",
                      }).on("click", function () {
                        // Reconstruct a slide from data as a "new" slide
                        response.data.id = undefined;
                        deletedSlide.replaceWith(
                          new Slide(response.data).toEdit().addClass("changed")
                        );
                      })
                    )
                  );
                  formElement.replaceWith(deletedSlide);
                },
              });
            }
          })
      )
  );
  if (self.id !== undefined) {
    // Interactions only available when slide is saved to the database
    // move, preview and send to scheduling rule
    slideParameters.append(
      $("<div>", { class: "form-row" })
        .append(
          $("<button>", {
            type: "button",
            text: "Move/Copy",
            title: "Move or copy the slide to another group",
          })
            .prepend(
              $("<i>", {
                class: "material-icons md-inline mr-1",
                text: "send",
              })
            )
            .on("click", function () {
              moveOrCopySlidePicker(this);
            })
        )
        .append(
          $("<a>", {
            class: "btn popup-link",
            href: thisPreviewUrl,
            text: "Preview",
            title: "Preview slide",
            target: "_blank",
          })
            .prepend(
              $("<i>", {
                class: "material-icons md-inline mr-1",
                text: "pageview",
              })
            )
            .on("click", function () {
              fullscreenPreview(thisPreviewUrl);
              return false;
            })
        )
        .append(
          $("<button>", {
            type: "button",
            text: "Show",
            title: "Add to a schedule",
          })
            .prepend(
              $("<i>", {
                class: "material-icons md-inline mr-1",
                text: "add_to_queue",
              })
            )
            .on("click", function () {
              addToRulePicker("slide", self.id, self.origin);
            })
        )
    );
  }
  return slideParameters;
};

/**
 * Create a form containing a view of the slide that can
 * be edited. Assumes that permissions have been checked
 * so will not have a disabled mode, use toView instead.
 *
 * @returns {*|jQuery} The <form> element containing the slide
 */
Slide.prototype.toEdit = function () {
  // Put the slide in an accessible scope for contained functions
  let self = this;
  // Build the view as a complete form element
  return (
    $("<form>", {
      class: "slide slide-row",
      id: "slide-" + self.uid,
      action: "#",
    })
      .data({ id: this.id, content: this.content || {} })
      .append(this.toImagePane())
      .append(this.toContentPane())
      .append(this.toParametersPane())
      // Update slide
      .on("submit", function () {
        // Existing slides should be PATCHED on their id,
        // new slides POSTed to the slide group
        let originalSlide = $(this);
        let serialisedForm = Slide.serializeForm(this);
        let slideId = $(this).data("id");
        let groupId = $(this).closest(".slide-group").data("group-id");
        let method, url;

        if (slideId === undefined) {
          method = "POST";
          url = basePrefix + "/slide_groups/" + groupId + "/slides";
        } else {
          method = "PATCH";
          url = basePrefix + "/slides/" + slideId;
        }
        $.ajax(url, {
          type: method,
          data: JSON.stringify(serialisedForm),
          contentType: "application/json",
          dataType: "json",
          success: function (response) {
            let newSlide = new Slide(response.data);
            originalSlide.replaceWith(newSlide.toEdit());
            $(".slide-preview", newSlide).trigger("text-resize");
          },
          error: function (err) {
            alert("Error changing slide\n" + err);
            console.log(err);
          },
        });
        return false;
      })
      .on("change", function () {
        $(this).addClass("changed");
      })
  );
};

/**
 * Create a static view of a slide which cannot be edited and shows
 * only limited information (such as info from a remote server
 * or where a user has restricted access)
 *
 * @return {*|jQuery} <form> element, but it does nothing
 */
Slide.prototype.toView = function () {
  // Always accessible object
  let self = this;
  // URL stays static
  let thisPreviewUrl = previewUrl(this.id, this.origin);
  // Using a form element lets it act like a slide for copy actions
  let slideView = $("<form>", {
    class: "slide-view",
    id: "slide-" + self.uid,
  }).data({ id: this.id, origin: this.origin, content: this.content });
  // Fieldset styles the cards the same as edit mode
  let slideContents = $("<fieldset>")
    .append(
      $("<div>", { class: "form-row" })
        .append($("<h4>", { text: this.name }))
        .append(
          $("<div>")
            .append(
              $("<a>", {
                class: "btn material-icons md-inline",
                href: thisPreviewUrl,
                text: "pageview",
                title: "Preview slide",
              }).on("click", function () {
                fullscreenPreview(thisPreviewUrl);
                return false;
              })
            )
            .append(
              $("<button>", {
                type: "button",
                class: "material-icons md-inline",
                text: "add_to_queue",
                title: "Add to a schedule",
              }).on("click", function () {
                addToRulePicker("slide", self.id, self.origin);
              })
            )
            .append(
              $("<button>", {
                type: "button",
                class: "material-icons md-inline",
                text: "send",
                title: "Copy the slide to a local slide group",
              }).on("click", function () {
                moveOrCopySlidePicker(this, "copy");
              })
            )
        )
    )
    .append(this.toSlidePreview())
    .append(
      $("<div>", { text: this.display_time + "s" }).prepend(
        $("<i>", { class: "material-icons md-inline", text: "timer" })
      )
    )
    .append(dateRange($("<div>"), this.valid_from, this.valid_to))
    .append($("<div>", { class: "long-description", text: this.description }));
  // Add the parameters as a hidden element for serialization copy actions
  return slideView
    .append(slideContents)
    .append(this.toParametersPane().addClass("d-none"));
};

/**
 * Data corresponding to a single display
 *
 * @constructor
 * @param {Object} [raw] - The raw JSON for a display object
 */
const Display = function (raw) {
  // Create a blank display if nothing is given
  if (!raw) raw = {};
  // This is the numeric id from the database (if it has one)
  this.id = raw.id;
  // New displays have no id defined (assigned later by database)
  // uid is used to give form elements unique id values
  this.uid = this.id || randomString(6);
  // ident is the IP address
  this.ident = raw.ident || "";
  this.name = raw.name || "";
  this.description = raw.description || "";
  // Read only from a serialized form
  this.groups = raw.groups || [];
  this.user_permissions = raw.user_permissions;
  this.user_group_permissions = raw.user_group_permissions;
  this.rules = raw.rules;
};

Display.serializeForm = function (formElement) {
  let form = $(formElement);
  let data = {};
  // Extract all named fields
  form.find("[name]").each(function () {
    if (this.type === "range") {
      // Range returns a string by default
      data[this.name] = this.valueAsNumber;
    } else if (this.type === "checkbox") {
      // Checkboxes are compiled into individual items
      data[this.name] = this.checked;
    } else if (this.type === "select") {
      data[this.name] = this.value;
    } else {
      data[this.name] = this.value;
    }
  });

  return {
    id: form.data("id"),
    ident: data.ident,
    name: data.name,
    description: data.description,
  };
};

/**
 * Create a list item from either a DisplayGroup in a Display.
 * The href will point to the current display within the given group
 * and will have a delete button to remove it from the display.
 *
 * @param {Object} group A display group object that has at least an
 *     id and name.
 * @param {Object} display The current display that is displaying the
 *     list. Should have an id.
 * @returns {*|jQuery}
 */
const displayDisplayGroupListItem = function (group, display) {
  return $("<li>", { class: "display-group-member" })
    .data("id", group.id)
    .append(
      $("<a>", {
        text: group.name,
        href: "#display_groups/" + group.id + "/displays/" + display.id,
        title: group.description,
      })
    )
    .append(
      userData.ifAdmin(
        iconButton("delete", "Remove from group").on("click", function () {
          let btn = $(this);
          let currentGroup = btn.closest(".display-group").data("id");
          $.ajax(basePrefix + "/displays/" + display.id + "/display_groups", {
            type: "PATCH",
            data: JSON.stringify({ op: "remove", id: group.id }),
            contentType: "application/json",
            dataType: "json",
          }).done(function () {
            if (group.id === currentGroup) {
              // Remove from this view as it's not in the group any more
              btn.closest("form").remove();
            } else {
              btn.closest("li").remove();
            }
          });
        })
      )
    );
};

/**
 * Create a list of DisplaysGroups that the display is a member of.
 * @param {Array} groups A list of DisplayGroups
 * @param {Object} display The display that owns the current list
 * @returns {*|jQuery} A <ul> of interactive elements
 */
const displayDisplayGroupList = function (groups, display) {
  return $("<ul>", { class: "member-list" }).append(
    groups.map(function (group) {
      return displayDisplayGroupListItem(group, display);
    })
  );
};

/**
 * A list of all the display groups that the display is a member of.
 *
 * @returns {*|jQuery} A <div> with a <ul> of the member items
 */
Display.prototype.toGroupList = function () {
  let self = this;
  let groupUI = $("<div>", { class: "members" });
  return groupUI.append(displayDisplayGroupList(this.groups, self)).append(
    userData.ifAdmin(
      $("<button>", { type: "button", text: "Add to group" })
        .prepend(icon("add"))
        .on("click", function () {
          let picker = $("<ul>").append($("<h3>", { text: "Select group" }));
          let currentDisplayGroups = groupUI
            .find(".display-group-member")
            .map(function () {
              return $(this).data("id");
            })
            .get();
          userData.getEdit("display_groups").done(function (response) {
            response.data.forEach(function (displayGroup) {
              // Don't show if already a member
              if (currentDisplayGroups.indexOf(displayGroup.id) > -1)
                return null;
              picker.append(
                $("<li>").append(
                  $("<button>", { text: displayGroup.name }).on(
                    "click",
                    function () {
                      let btn = this;
                      $.ajax(
                        basePrefix + "/displays/" + self.id + "/display_groups",
                        {
                          type: "PATCH",
                          data: JSON.stringify({
                            op: "add",
                            id: displayGroup.id,
                          }),
                          contentType: "application/json",
                          dataType: "json",
                        }
                      ).done(function (response) {
                        groupUI
                          .find(".member-list")
                          .replaceWith(
                            displayDisplayGroupList(response.data, self)
                          );
                        btn.closest("li").remove();
                      });
                    }
                  )
                )
              );
            });
          });
          modalElement(picker);
        })
    )
  );
};

/**
 * Editable view of a display for administrators to update the
 * information. Assumes the user is an admin and can make changes.
 *
 * @param {number} [groupId] If the display is being rendered within a
 *     specific group, supply it here.
 * @returns {*|jQuery} A <form> containing the display.
 */
Display.prototype.toEdit = function (groupId) {
  // Put the display in an accessible scope for contained functions
  let self = this;
  // Preview slides
  let thisPreviewUrl = "/client/" + this.ident;
  // Build the view as a complete form element
  return (
    $("<form>", {
      class: "display",
      id: "display-" + self.uid,
      action: "#",
    })
      .data({ id: this.id })
      .append(
        $("<fieldset>", {
          class: "display-parameters",
        })
          .append($("<legend>", { text: "Display Parameters" }))
          .append(
            $("<div>", { class: "slide-row" })
              .append(
                $("<div>")
                  .append(
                    $("<div>", { class: "form-row" })
                      .append(
                        $("<label>", {
                          for: "ident-" + self.uid,
                          text: "IP address: ",
                        })
                      )
                      .append(
                        $("<input>", {
                          type: "text",
                          value: this.ident,
                          id: "ident-" + self.uid,
                          name: "ident",
                          required: true,
                        })
                      )
                  )
                  .append(
                    $("<div>", { class: "form-row" })
                      .append(
                        $("<label>", {
                          for: "name-" + self.uid,
                          text: "Name: ",
                        })
                      )
                      .append(
                        $("<input>", {
                          type: "text",
                          value: this.name,
                          id: "name-" + self.uid,
                          name: "name",
                          required: true,
                        })
                      )
                  )
                  .append(
                    $("<div>", { class: "form-row" })
                      .append(
                        $("<label>", {
                          for: "description-" + self.uid,
                          text: "Description: ",
                        })
                      )
                      .append(
                        $("<textarea>", {
                          rows: 2,
                          text: this.description,
                          id: "description-" + self.uid,
                          name: "description",
                        })
                      )
                  )
                  .append(
                    $("<div>", { class: "form-row" })
                      .append(
                        $("<button>", {
                          type: "submit",
                          name: "submit",
                          class: "save-button",
                          text: "Save",
                        }).prepend(icon("save"))
                      )
                      .append(
                        $("<button>", {
                          type: "button",
                          class: "delete-button",
                          text: "Delete",
                          title: "Remove this display",
                        })
                          .prepend(icon("delete"))
                          .on("click", function () {
                            let btn = $(this);
                            if (self.id === undefined) {
                              btn.closest("form").remove();
                            } else if (confirm("Are you sure?")) {
                              $.ajax(basePrefix + "/displays/" + self.id, {
                                type: "DELETE",
                              }).done(btn.closest("form").remove());
                            }
                          })
                      )
                      .append(
                        self.id !== undefined
                          ? $("<a>", {
                              class: "btn",
                              href: thisPreviewUrl,
                              text: "Preview",
                            })
                              .prepend(icon("play"))
                              .on("click", function () {
                                fullscreenPreview(thisPreviewUrl);
                                return false;
                              })
                          : null
                      )
                  )
              )
              .append(
                $("<div>").append(
                  $("<div>", { class: "form-row" }).append(self.toGroupList())
                )
              )
          )
      )

      // Create or update display
      .on("submit", function () {
        // Existing displays should be PATCHED on their id,
        // new slides POSTed to the slide group
        let originalDisplay = $(this);
        let serialisedForm = Display.serializeForm(this);
        let method, url;
        if (self.id === undefined && groupId === undefined) {
          // We're a new display in the "All" list
          method = "POST";
          url = basePrefix + "/displays";
        } else if (self.id === undefined) {
          // A new display in a display group
          method = "POST";
          url = basePrefix + "/display_groups/" + groupId + "/displays";
        } else {
          // Modifying an existing display
          method = "PATCH";
          url = basePrefix + "/displays/" + self.id;
        }
        $.ajax(url, {
          type: method,
          data: JSON.stringify(serialisedForm),
          contentType: "application/json",
          dataType: "json",
          success: function (response) {
            let newDisplay = new Display(response.data);
            originalDisplay.replaceWith(newDisplay.toEdit());
          },
          error: function (err) {
            alert("Error changing display\n" + err);
            console.log(err);
          },
        });
        return false;
      })
      .on("change", function () {
        $(this).addClass("changed");
      })
  );
};

/**
 * Create a static view of a display which cannot be edited and shows
 * only limited information.
 *
 * @return {*|jQuery} <form> element, but it does nothing
 */
Display.prototype.toView = function () {
  // Put display in accessible scope for contained functions
  let self = this;
  // URL stays static
  let thisPreviewUrl = "/client/" + this.ident;
  // Use form element because that's what always happens
  let displayView = $("<form>", {
    class: "display-view",
    id: "display-" + self.uid,
  }).data({ id: this.id });
  let displayContents = $("<fieldset>")
    .append(
      $("<div>", { class: "form-row" })
        .append($("<h4>", { class: "mr-3", text: this.name }))
        .append(
          $("<div>").append(
            $("<a>", {
              class: "btn material-icons md-inline",
              href: thisPreviewUrl,
              text: "play_circle_outline",
              title: "Preview current slide rotation",
            }).on("click", function () {
              fullscreenPreview(thisPreviewUrl);
              return false;
            })
          )
        )
    )
    .append($("<div>", { class: "long-description", text: this.description }))
    .append(this.toGroupList());
  return displayView.append(displayContents);
};

/**
 * Data corresponding to a single display rule. A display rule
 * connects slides and displays and determines what is shown on
 * screens.
 *
 * @constructor
 * @param {Object} [raw] - The raw JSON for a display rule
 */
const Rule = function (raw) {
  // Create a blank rule if nothing is given
  if (!raw) raw = {};
  // This is the numeric id from the database (if it has one)
  this.id = raw.id;
  // New rules have id no defined (assigned later by database)
  // uid is used to give form elements unique id values
  this.uid = this.id || randomString(6);
  // the rest of the identifying information
  this.name = raw.name || "";
  this.description = raw.description || "";
  this.start_date = maybeDate(raw.start_date);
  this.end_date = maybeDate(raw.end_date);
  // Is the rule active
  this.active = raw.active !== false;
  // Active every day by default
  this.active_days = raw.active_days || [1, 2, 3, 4, 5, 6, 7];
  this.active_time_start = raw.active_time_start;
  this.active_time_end = raw.active_time_end;
  this.exclusive = raw.exclusive || false;
  this.allow_duplicates = raw.allow_duplicates;
  // Read only from a serialized form
  this.order = raw.order || [];
  this.displays = raw.displays || [];
  this.display_groups = raw.display_groups || [];
  this.slides = raw.slides || [];
  this.slide_groups = raw.slide_groups || [];
};

/**
 * Convert a rule definition in a form into a dictionary of values
 * that can be sent to the API.
 *
 * @param formElement
 * @returns {Object}
 */
Rule.serializeForm = function (formElement) {
  let form = $(formElement);
  let data = {};
  // Extract all named fields
  form.find("[name]").each(function () {
    if (this.type === "range") {
      // Range returns a string by default
      data[this.name] = this.valueAsNumber;
    } else if (this.name.indexOf("[]") === this.name.length - 2) {
      // checkbox array, collect items or have empty list
      let name = this.name.substring(0, this.name.length - 2);
      data[name] = data[name] || [];
      if (this.checked) {
        data[name].push(this.value);
      }
    } else if (this.type === "checkbox") {
      // Checkboxes are compiled into individual items
      data[this.name] = this.checked;
    } else if (this.type === "select") {
      data[this.name] = this.value;
    } else {
      data[this.name] = this.value;
    }
  });

  return {
    id: form.data("id"),
    name: data.name,
    description: data.description,
    start_date: maybeDateTime(data["start-date"], data["start-time"], "00:00"),
    end_date: maybeDateTime(data["end-date"], data["end-time"], "23:59"),
    active: data.active,
    active_days: data["active-days"],
    active_time_start: maybeTime(data["active-time-start"]),
    active_time_end: maybeTime(data["active-time-end"]),
    exclusive: data.exclusive,
    allow_duplicates: data["allow-duplicates"],
  };
};

/**
 * Users can only add a display if they have permissions for all current
 * outputs of a rule. Check if the current user has the required
 * permissions.
 *
 * @returns {boolean} - Whether the global user has permission
 */
Rule.prototype.canAddDisplay = function () {
  if (userData.admin) {
    return true;
  } else {
    return (
      this.displays.every(function (display) {
        return userData.hasPermission(display, "display");
      }) &&
      this.display_groups.every(function (group) {
        return userData.hasPermission(group, "display_group");
      })
    );
  }
};

/**
 * Some magic to convert a string representation of an external slide
 * or slide group into an object with a few more details.
 *
 * @param {string} oid - The id of the external object
 * @returns {Object} item - A structured representation of the remote item
 */
const externalMemberLookup = function (oid) {
  let item = oid_re.exec(oid);
  if (item) {
    return {
      oid: oid,
      origin: item[3],
      id: item[2],
      description: item[3],
    };
  } else {
    return null;
  }
};

/**
 * Check the document for any .external-member items and update
 * their information with the remote data reported from the server.
 *
 */
const lookupRemoteNames = function () {
  let remoteItems = $(".external-member");
  if (!remoteItems.length) {
    return;
  }
  $.getJSON(basePrefix + "/remote_slides").then(function (response) {
    let memberLookup = {};
    response.data.forEach(function (server) {
      server.slide_groups.forEach(function (group) {
        memberLookup[oid("slide_group", group.id, server.hostname)] = group;
        group.slides.forEach(function (slide) {
          memberLookup[oid("slide", slide.id, server.hostname)] = slide;
        });
      });
    });
    remoteItems.each(function () {
      let elem = $(this);
      let remoteInfo = memberLookup[elem.data("oid")];
      if (remoteInfo) {
        elem
          .find("a")
          .attr("title", remoteInfo.description)
          .find("span")
          .text(remoteInfo.name || "Slide " + remoteInfo.id);
      } else {
        elem.find("i").first().replaceWith(icon("external_off"));
      }
    });
  });
};

/**
 * Create a list item from either a Slide or a SlideGroup.
 * Class will be the type of item, name will be the name
 * or the slide.id for a slide with no name. The href will
 * point to the slide group, or the slide within the slide
 * group.
 *
 * @param {Object} item - A slide or slide group that has oid, and
 *     all other identifying information.
 * @param {number} idx - Position of the element in the member list.
 * @param {boolean} edit - Whether the user has edit rights for the list
 * @returns {*|jQuery}
 */
const ruleMemberListItem = function (item, idx, edit) {
  let listItem = $("<li>");
  // Remote items from /rules are only oid values and are updated later.
  // From /members the info is already there so can be used directly.
  let remote = externalMemberLookup(item.oid);
  if (remote) {
    if (item.oid.indexOf("SlideGroupExternal") === 0) {
      listItem
        .addClass("slide-group-member external-member")
        .data("oid", item.oid)
        .append(
          $("<a>", {
            href:
              "#remote_slides/" + remote.origin + /slide_groups/ + remote.id,
          })
            .append(icon("external"))
            .append(icon("slide_group"))
            .append(
              $("<span>", {
                text: item.name || "SlideGroup " + remote.id,
                title: item.description,
              })
            )
        );
    } else if (item.oid.indexOf("SlideExternal") === 0) {
      listItem
        .addClass("slide-member external-member")
        .data("oid", item.oid)
        .append(
          $("<a>", {
            href: "#remote_slides/" + remote.origin + /slides/ + remote.id,
          })
            .append(icon("external"))
            .append(icon("slide"))
            .append(
              $("<span>", {
                text: item.name || "Slide " + remote.id,
                title: item.description,
              })
            )
        );
    }
  } else if (item.oid.indexOf("SlideGroup") === 0) {
    listItem.addClass("slide-group-member").append(
      $("<a>", {
        href: "#slide_groups/" + item.id,
        text: item.name || "SlideGroup " + item.id,
        title: item.description,
      }).prepend(icon("slide_group"))
    );
  } else {
    listItem.addClass("slide-member").append(
      $("<a>", {
        href: "#slide_groups/" + item.group_id + "/slides/" + item.id,
        text: item.name || "Slide " + item.id,
        title: item.description,
      }).prepend(icon("slide"))
    );
  }
  if (edit) {
    listItem.append(
      $("<div>", { class: "actions-panel" })
        .append(
          iconButton("delete").on("click", function () {
            let btn = $(this);
            let ruleId = btn.closest("form").data("id");
            $.ajax(basePrefix + "/rules/" + ruleId + "/members", {
              type: "PATCH",
              data: JSON.stringify({ op: "remove", idx: idx }),
              contentType: "application/json",
              dataType: "json",
            }).done(function (response) {
              btn
                .closest(".member-list")
                .replaceWith(ruleMemberList(response.data, edit));
            });
          })
        )
        .append(
          iconButton("up", "Move up").on("click", function () {
            let btn = $(this);
            let ruleId = btn.closest("form").data("id");
            $.ajax(basePrefix + "/rules/" + ruleId + "/members", {
              type: "PATCH",
              data: JSON.stringify({ op: "move", idx: idx, value: -1 }),
              contentType: "application/json",
              dataType: "json",
            }).done(function (response) {
              btn
                .closest(".member-list")
                .replaceWith(ruleMemberList(response.data, edit));
            });
          })
        )
        .append(
          iconButton("down", "Move down").on("click", function () {
            let btn = $(this);
            let ruleId = btn.closest("form").data("id");
            $.ajax(basePrefix + "/rules/" + ruleId + "/members", {
              type: "PATCH",
              data: JSON.stringify({ op: "move", idx: idx, value: 1 }),
              contentType: "application/json",
              dataType: "json",
            }).done(function (response) {
              btn
                .closest(".member-list")
                .replaceWith(ruleMemberList(response.data, edit));
            });
          })
        )
    );
  }
  return listItem;
};

/**
 * Create a list of Slides and SlideGroups associated with a rule.
 * @param {Array} members A list of Slides and SlideGroups
 * @param {boolean} edit - Whether the user has edit rights for the list
 * @returns {*|jQuery} A <ul> of interactive elements
 */
const ruleMemberList = function (members, edit) {
  return $("<ul>", { class: "member-list" }).append(
    members.map(function (member, idx) {
      return ruleMemberListItem(member, idx, edit);
    })
  );
};

/**
 * A list of all the slides and slide groups that are included in the
 * rule. Management options included if edit is enabled.
 *
 * @param {boolean} edit - Whether the user has edit rights for the list
 * @returns {*|jQuery} - A <div> with a <ul> of the member items
 */
Rule.prototype.toMemberList = function (edit) {
  let self = this;
  // Can be a mixture of slides and groups
  let memberLookup = {};
  self.slides.concat(self.slide_groups).forEach(function (member) {
    memberLookup[member.oid] = member;
  });
  let orderedMembers = self.order.map(function (oid) {
    return memberLookup[oid] || { oid: oid };
  });
  // Member UI as an object accessible by events
  let memberUI = $("<div>", { class: "members" }).append(
    ruleMemberList(orderedMembers, edit)
  );
  if (edit) {
    memberUI.append(
      $("<button>", { type: "button", text: "Add slides" })
        .prepend(icon("add"))
        .on("click", function () {
          let pickerList = $("<ul>");
          let pickerUI = $("<div>")
            .append($("<h3>", { text: "Select group or individual slides" }))
            .append(pickerList);
          userData.getView("slide_groups").done(function (response) {
            response.data.forEach(function (slideGroup) {
              pickerList.append(
                $("<li>")
                  .append(
                    $("<button>", { text: slideGroup.name }).on(
                      "click",
                      function () {
                        $.ajax(basePrefix + "/rules/" + self.id + "/members", {
                          type: "PATCH",
                          data: JSON.stringify({
                            op: "add",
                            oid: slideGroup.oid,
                          }),
                          contentType: "application/json",
                          dataType: "json",
                        }).done(function (response) {
                          // Added as the last element
                          let newMembers = response.data;
                          memberUI
                            .find(".member-list")
                            .replaceWith(ruleMemberList(newMembers, true));
                        });
                      }
                    )
                  )
                  .append(
                    $("<button>", { type: "button", text: "<" }).on(
                      "click",
                      function () {
                        $(this)
                          .toggleClass("opened")
                          .siblings("ul")
                          .slideToggle();
                      }
                    )
                  )
                  .append(
                    $("<ul>", { style: "display: none" }).append(
                      slideGroup.slides.map(function (slide) {
                        return $("<li>").append(
                          $("<button>", {
                            class: "btn",
                            text: slide.name || "Slide " + slide.id,
                          }).on("click", function () {
                            $.ajax(
                              basePrefix + "/rules/" + self.id + "/members",
                              {
                                type: "PATCH",
                                data: JSON.stringify({
                                  op: "add",
                                  oid: slide.oid,
                                }),
                                contentType: "application/json",
                                dataType: "json",
                              }
                            ).done(function (response) {
                              let newMembers = response.data;
                              memberUI
                                .find(".member-list")
                                .replaceWith(ruleMemberList(newMembers, true));
                            });
                          })
                        );
                      })
                    )
                  )
              );
            });
          });
          modalElement(pickerUI);
        })
    );
  }
  return memberUI;
};

/**
 * Count the number of display or display group members in a
 * rule.
 *
 * @param {HTMLElement|jQuery} anchor - Some element within a rule <form>
 * @returns {number} total number of elements
 */
const countTargets = function (anchor) {
  let rule = $(anchor).closest("form");
  return (
    rule.find(".display-member").length +
    rule.find(".display-group-member").length
  );
};

/**
 * Create a list item from a display group in a rule with some
 * interaction buttons.
 *
 * @param {Object} group - A display group that has an id and other
 *     identifying information.
 * @param {boolean} edit - Whether the user has edit rights for the list
 * @returns {*|jQuery} - A <li> with the given item.
 */
const ruleDisplayGroupListItem = function (group, edit) {
  let displayGroupItem = $("<li>", { class: "display-group-member" })
    .data("id", group.id)
    .append(
      $("<a>", {
        href: "#display_groups/" + group.id,
        text: group.name,
        title: group.description,
      }).prepend(icon("display_group"))
    );
  if (edit && userData.hasPermission(group, "display_group")) {
    displayGroupItem.append(
      $("<button>", {
        type: "button",
        class: "material-icons md-inline",
        text: "delete_forever",
        title: "Remove from list",
      }).on("click", function () {
        let btn = $(this);
        let rule = btn.closest("form");
        let ruleId = rule.data("id");
        if (countTargets(btn) < 2) {
          if (confirm("Rule with no targets will be deleted.")) {
            $.ajax(basePrefix + "/rules/" + ruleId, { type: "DELETE" }).done(
              rule.remove()
            );
          }
        } else {
          $.ajax(basePrefix + "/rules/" + ruleId + "/display_groups", {
            type: "PATCH",
            data: JSON.stringify({ op: "remove", id: group.id }),
            contentType: "application/json",
            dataType: "json",
          }).done(function () {
            btn.closest("li").remove();
          });
        }
      })
    );
  }
  return displayGroupItem;
};

/**
 * Create a list of display groups associated with a rule.
 * @param {Array} groups - A list of display groups
 * @param {boolean} edit - Whether the user has edit rights for the list
 * @returns {*|jQuery} - A <ul> of interactive elements
 */
const ruleDisplayGroupList = function (groups, edit) {
  return $("<ul>", { class: "member-list display-group-list" }).append(
    groups.map(function (group) {
      return ruleDisplayGroupListItem(group, edit);
    })
  );
};

/**
 * Create a list item from a display in a rule with some
 * interaction buttons.
 *
 * @param {Object} display - A display that has an id and other
 *     identifying information.
 * @param {boolean} edit - Whether the user has edit rights for the list
 * @returns {*|jQuery} - A <li> with the given item.
 */
const ruleDisplayListItem = function (display, edit) {
  let displayItem = $("<li>", { class: "display-member" })
    .data("id", display.id)
    .append(
      $("<a>", {
        href: "#displays/" + display.id,
        text: display.name,
        title: display.description,
      }).prepend(icon("display"))
    );
  if (edit && userData.hasPermission(display, "display")) {
    displayItem.append(
      iconButton("delete", "Remove from list").on("click", function () {
        let btn = $(this);
        let rule = btn.closest("form");
        let ruleId = rule.data("id");
        if (countTargets(btn) < 2) {
          if (confirm("Rule with no targets will be deleted.")) {
            $.ajax(basePrefix + "/rules/" + ruleId, { type: "DELETE" }).done(
              rule.remove()
            );
          }
        } else {
          $.ajax(basePrefix + "/rules/" + ruleId + "/displays", {
            type: "PATCH",
            data: JSON.stringify({ op: "remove", id: display.id }),
            contentType: "application/json",
            dataType: "json",
          }).done(function () {
            btn.closest("li").remove();
          });
        }
      })
    );
  }
  return displayItem;
};

/**
 * Create a list of displays associated with a rule.
 * @param {Array} groups - A list of display groups
 * @param {boolean} edit - Whether the user has edit rights for the list
 * @returns {*|jQuery} - A <ul> of interactive elements
 */
const ruleDisplayList = function (groups, edit) {
  return $("<ul>", { class: "member-list display-list" }).append(
    groups.map(function (group) {
      return ruleDisplayListItem(group, edit);
    })
  );
};

/**
 * A list of all the displays and display groups that the rule will
 * output to. Includes management of them if requested.
 *
 * @param {boolean} edit - Whether the user has edit rights for the list
 * @returns {*|jQuery} - A <div> with a <ul> of the member items
 */
Rule.prototype.toDisplayList = function (edit) {
  let self = this;
  // Full display list UI has add display or group button
  let displayUI = $("<div>")
    .append(ruleDisplayGroupList(self.display_groups, edit))
    .append(ruleDisplayList(self.displays, edit));
  if (edit && this.canAddDisplay()) {
    displayUI.append(
      $("<button>", {
        type: "button",
        text: "Add screens",
        title: "Send slides to a screen or group",
      })
        .prepend(icon("add"))
        .on("click", function () {
          let displayGroupPicker = $("<ul>");
          let displayPicker = $("<div>");
          let pickerUI = $("<div>")
            .append($("<h3>", { text: "Select display group or screen" }))
            .append(displayGroupPicker)
            .append(displayPicker);
          let currentDisplayGroups = displayUI
            .find(".display-group-member")
            .map(function () {
              return $(this).data("id");
            })
            .get();
          let currentDisplays = displayUI
            .find(".display-member")
            .map(function () {
              return $(this).data("id");
            })
            .get();
          userData.getEdit("display_groups").done(function (response) {
            displayGroupPicker.append(
              response.data.map(function (displayGroup) {
                if (currentDisplayGroups.indexOf(displayGroup.id) > -1)
                  return null;
                return $("<li>")
                  .append(
                    $("<button>", {
                      text: displayGroup.name,
                      title: displayGroup.description,
                    }).on("click", function () {
                      $.ajax(
                        basePrefix + "/rules/" + self.id + "/display_groups",
                        {
                          type: "PATCH",
                          data: JSON.stringify({
                            op: "add",
                            id: displayGroup.id,
                          }),
                          contentType: "application/json",
                          dataType: "json",
                        }
                      ).done(function (response) {
                        displayUI
                          .find(".display-group-list")
                          .replaceWith(
                            ruleDisplayGroupList(response.data, edit)
                          );
                      });
                    })
                  )
                  .append(
                    $("<button>", { type: "button", text: "<" }).on(
                      "click",
                      function () {
                        $(this)
                          .toggleClass("opened")
                          .siblings("ul")
                          .slideToggle();
                      }
                    )
                  )
                  .append(
                    $("<ul>", { style: "display: none" }).append(
                      displayGroup.displays.map(function (display) {
                        if (currentDisplays.indexOf(display.id) > -1)
                          return null;
                        return $("<li>").append(
                          $("<button>", {
                            text: display.name,
                            title: display.description,
                          }).on("click", function () {
                            $.ajax(
                              basePrefix + "/rules/" + self.id + "/displays",
                              {
                                type: "PATCH",
                                data: JSON.stringify({
                                  op: "add",
                                  id: display.id,
                                }),
                                contentType: "application/json",
                                dataType: "json",
                              }
                            ).done(function (response) {
                              displayUI
                                .find(".display-list")
                                .replaceWith(
                                  ruleDisplayList(response.data, edit)
                                );
                            });
                          })
                        );
                      })
                    )
                  );
              })
            );
          });
          userData.getEdit("displays").done(function (response) {
            displayPicker.append(
              $("<h3>", { text: "Select individual display" }).append(
                $("<button>", { text: "<" }).on("click", function () {
                  $(this).parent().parent().find("ul").slideToggle();
                })
              )
            ); // collapse display list
            displayPicker.append(
              $("<ul>", { style: "display: none" }).append(
                response.data.map(function (display) {
                  if (currentDisplays.indexOf(display.id) > -1) return null;
                  return $("<li>").append(
                    $("<button>", {
                      text: display.name,
                      title: display.description,
                    }).on("click", function () {
                      $.ajax(basePrefix + "/rules/" + self.id + "/displays", {
                        type: "PATCH",
                        data: JSON.stringify({ op: "add", id: display.id }),
                        contentType: "application/json",
                        dataType: "json",
                      }).done(function (response) {
                        displayUI
                          .find(".display-list")
                          .replaceWith(ruleDisplayList(response.data, edit));
                      });
                    })
                  );
                })
              )
            );
          });
          modalElement(pickerUI);
        })
    );
  }
  return displayUI;
};

/**
 * Full view of a rule with all members and outputs
 * @returns {*|jQuery}
 */
Rule.prototype.toEdit = function () {
  // Put the rule in an accessible scope for contained functions
  let self = this;
  // delete button is only shown if user has permission (can add display)
  let btnPanel = $("<div>", { class: "form-row" });
  if (this.canAddDisplay()) {
    btnPanel.append(
      $("<button>", {
        type: "button",
        name: "delete",
        class: "delete-button",
        text: "Delete",
      })
        .prepend(icon("delete"))
        .on("click", function () {
          let btn = $(this);
          if (confirm("Are you sure?")) {
            $.ajax(basePrefix + "/rules/" + self.id, {
              type: "DELETE",
            }).done(btn.closest("form").remove());
          }
        })
    );
  } else {
    btnPanel.append($("<div>"));
  }
  // Save button always included in edit view
  btnPanel.append(
    $("<button>", {
      type: "submit",
      name: "submit",
      class: "save-button",
      text: "Save",
    }).prepend(icon("save"))
  );
  // Build the view as a complete form element
  return (
    $("<form>", {
      class: "rule slide-row",
      id: "rule-" + self.uid,
      action: "#",
    })
      .data({ id: this.id })
      .append(
        $("<fieldset>", {
          class: "rule-parameters",
        })
          .append($("<legend>", { text: "Scheduling rule" }))
          .append(
            $("<div>", { class: "slide-row" })
              .append(
                $("<div>", { class: "rule-members" })
                  .append($("<h4>", { text: "Contents" }))
                  .append(self.toMemberList(true))
              )
              .append(
                $("<div>", { class: "rule-parameters" })
                  .append(
                    $("<div>", { class: "form-row" })
                      .append(
                        $("<label>", {
                          for: "name-" + self.uid,
                          text: "Name: ",
                        })
                      )
                      .append(
                        $("<input>", {
                          type: "text",
                          value: this.name,
                          id: "name-" + self.uid,
                          name: "name",
                          required: true,
                        })
                      )
                  )
                  .append(
                    $("<div>", { class: "form-row" })
                      .append(
                        $("<label>", {
                          for: "description-" + self.uid,
                          text: "Description: ",
                        })
                      )
                      .append(
                        $("<textarea>", {
                          rows: 2,
                          text: this.description,
                          id: "description-" + self.uid,
                          name: "description",
                        })
                      )
                  )
                  // Rule can be disabled if required
                  .append(
                    $("<div>", { class: "form-row" })
                      .append(
                        $("<label>", {
                          text: "Active: ",
                          title: "Enable or disable this display rule.",
                        })
                      )
                      .append(
                        $("<input>", {
                          type: "checkbox",
                          name: "active",
                          checked: self.active,
                        })
                      )
                  )
                  // start time picker
                  .append(
                    $("<div>", { class: "form-row" })
                      .append(
                        $("<label>", {
                          for: "start-date-" + self.uid,
                          text: "Start date and time: ",
                        })
                      )
                      .append(
                        $("<input>", {
                          type: "date",
                          value: dateToInputDate(this.start_date),
                          id: "start-date" + self.uid,
                          name: "start-date",
                          "aria-label": "Start date",
                        })
                      )
                      .append(
                        $("<input>", {
                          type: "time",
                          value: dateToInputTime(this.start_date),
                          id: "start-time-" + self.uid,
                          name: "start-time",
                          "aria-label": "Start time",
                        })
                      )
                  )
                  // end time picker
                  .append(
                    $("<div>", { class: "form-row" })
                      .append(
                        $("<label>", {
                          for: "end-date-" + self.uid,
                          text: "End date and time: ",
                        })
                      )
                      .append(
                        $("<input>", {
                          type: "date",
                          value: dateToInputDate(this.end_date),
                          id: "end-date-" + self.uid,
                          name: "end-date",
                          "aria-label": "End date",
                        })
                      )
                      .append(
                        $("<input>", {
                          type: "time",
                          value: dateToInputTime(this.end_date),
                          id: "end-time-" + self.uid,
                          name: "end-time",
                          "aria-label": "End time",
                        })
                      )
                      .on("change", function () {
                        // Some visual indication that the time has expired (or will soon)
                        let row = $(this);
                        let now = new Date();
                        let thisDate = new Date(
                          row.find("input[type=date]").val() +
                            "T" +
                            row.find("input[type=time]").val()
                        );
                        if (thisDate < now) {
                          // It's in the past
                          row.addClass("past").removeClass("soon");
                        } else if (thisDate - now < 24 * 60 * 60 * 1000) {
                          // It's within 24 hours
                          row.addClass("soon").removeClass("past");
                        } else {
                          // Far enough in the future that it's fine
                          row.removeClass("soon past");
                        }
                      })
                      .trigger("change")
                  )
                  // active day chooser
                  .append(
                    $("<div>", { class: "form-row" })
                      .append($("<label>", { text: "Days: " }))
                      .append(
                        weekdays.map(function (weekday) {
                          return $("<label>", {
                            class: "checkbox",
                            text: weekday[1].substring(0, 1),
                            title: weekday[1],
                          }).append(
                            $("<input>", {
                              type: "checkbox",
                              name: "active-days[]",
                              value: weekday[0],
                              checked:
                                self.active_days.indexOf(weekday[0]) > -1,
                            })
                          );
                        })
                      )
                  )
                  // active time picker
                  .append(
                    $("<div>", { class: "form-row" })
                      .append(
                        $("<label>", {
                          for: "active-time-start-" + self.uid,
                          text: "Active time: ",
                        })
                      )
                      .append(
                        $("<input>", {
                          type: "time",
                          value: this.active_time_start,
                          id: "active-start-time-" + self.uid,
                          name: "active-time-start",
                        })
                      )
                      .append(
                        $("<label>", {
                          for: "active-time-end-" + self.uid,
                          text: "to",
                        })
                      )
                      .append(
                        $("<input>", {
                          type: "time",
                          value: this.active_time_end,
                          id: "active-time-end-" + self.uid,
                          name: "active-time-end",
                        })
                      )
                  )
                  // Exclusive rule overrides all others
                  .append(
                    $("<div>", { class: "form-row" })
                      .append(
                        $("<label>", {
                          text: "Exclusive: ",
                          title:
                            "Only show slides from this rule on the screens.",
                        })
                      )
                      .append(
                        $("<input>", {
                          type: "checkbox",
                          name: "exclusive",
                        }).prop("checked", self.exclusive)
                      )
                  )
                  // Submit button at end of form
                  .append(btnPanel)
              )
              .append(
                $("<div>", { class: "rule-displays" })
                  .append($("<h4>", { text: "Screens" }))
                  .append(self.toDisplayList(true))
              )
          )
      )
      // Update slide
      .on("submit", function () {
        // Rules will always exist and have an ID as they must be
        // linked to at least one display, so always PATCH
        let originalRule = $(this);
        let serialisedForm = Rule.serializeForm(this);
        let ruleId = $(this).data("id");
        // Send the update
        $.ajax(basePrefix + "/rules/" + ruleId, {
          type: "PATCH",
          data: JSON.stringify(serialisedForm),
          contentType: "application/json",
          dataType: "json",
          success: function (response) {
            let newRule = new Rule(response.data);
            originalRule.replaceWith(newRule.toEdit());
          },
          error: function (err) {
            alert("Error changing rule\n" + err);
            console.log(err);
          },
        });
        return false;
      })
      .on("change", function () {
        $(this).addClass("changed");
      })
  );
};

Rule.prototype.toView = function () {
  // Put the rule in an accessible scope for contained functions
  let self = this;
  // Build the view as a complete form element
  let title = $("<h4>", { text: this.name });
  return $("<form>", {
    class: "rule slide-row",
    id: "rule-" + self.uid,
  })
    .data({ id: this.id })
    .append(
      $("<fieldset>").append(
        $("<div>", { class: "slide-row" })
          .append(
            $("<div>", { class: "rule-members" })
              .append($("<h4>", { text: "Contents" }))
              .append(self.toMemberList(false))
          )
          .append(
            $("<div>", { class: "rule-parameters" })
              .append(title)
              .append(
                $("<div>", {
                  class: "long-description",
                  text: this.description,
                })
              )
              .append(dateRange($("<div>"), this.start_date, this.end_date))
              .append(activeDays($("<div>"), this.active_days))
              .append(
                timeRange(
                  $("<div>"),
                  this.active_time_start,
                  this.active_time_end
                )
              )
              .append(
                $("<div>")
                  .append(
                    $("<i>", {
                      class: "material-icons md-inline mr-1",
                      text: "tv_off",
                      title: "Inactive rule, not currently used",
                    }).toggleClass("d-none", this.active)
                  )
                  .append(
                    $("<i>", {
                      class: "material-icons md-inline mr-1",
                      text: "priority_high",
                      title: "Exclusive rule has priority over other schedules",
                    }).toggleClass("d-none", !this.exclusive)
                  )
              )
          )
          .append(
            $("<div>", { class: "rule-displays" })
              .append($("<h4>", { text: "Screens" }))
              .append(self.toDisplayList(false))
          )
      )
    );
};

/**
 * The data that can be returned from the api
 * @typedef {Object} apiData
 * @property {Array} slides
 * @property {Array} displays
 * @property {Array} order
 * @property {string} name
 * @property {string} crsid
 * @property {string} description
 * @property {number} group_id
 * @property {boolean} admin
 * @property {Array} slide_group_permissions
 * @property {Array} display_group_permissions
 * @property {Array} display_permissions
 */

/**
 * A response value
 * @typedef {Object} apiResponse
 * @property {apiData} apiData
 */

/**
 * Grab the current list of slides and render them
 * into the target div.
 *
 * @param {string|HTMLElement|jQuery} target The element into which the slide
 *     group should be inserted.
 * @param {number|string} groupId The id of the group in the database
 * @returns {Promise} The request to get the data
 */
const renderSlides = function (target, groupId) {
  //
  // SLIDE LIST
  //
  // Build and render slide list
  let targetElem = $(target);
  // Slide list container
  let slideGroup = $("<div>", {
    class: "slide-group",
    id: "slide-group-" + groupId,
  }).data("group-id", groupId);
  let slideList = $("<div>", {
    class: "slide-list",
    id: "slide-group-slides-" + groupId,
  });
  // no slide group given, request that they select one and exit
  if (groupId === undefined) {
    return targetElem
      .empty()
      .append(slideGroup.append($("<h3>", { text: "Select a slide group" })))
      .promise();
  }
  // Build the slide list from the API response
  return $.ajax(basePrefix + "/slide_groups/" + groupId, {
    dataType: "json",
    error: function (xhr) {
      let statusMessage = "An unknown error occurred";
      if (xhr.status === 404) {
        statusMessage = "Slide group not found.";
      } else if (xhr.status === 403) {
        statusMessage = "No permission to access slide group.";
      }
      targetElem
        .empty()
        .append(slideGroup.append($("<h3>", { text: statusMessage })));
    },
  }).done(function (response) {
    /** @param {apiResponse} response */
    let data = response.data;
    let slides = data.slides;
    let hasPermission = userData.hasPermission(data, "slide_group");
    // Slide ordering by "order" property.
    slides.sort(function (a, b) {
      //  Order is the "id" values in the desired order
      let idxA = data.order.indexOf(a.id);
      let idxB = data.order.indexOf(b.id);
      // b is not in the list so goes to end (after A)
      if (idxB < 0) {
        return -1;
      } else if (idxA < 0) {
        return 1;
      } else {
        return idxA - idxB;
      }
    });
    // Title
    slideGroup.append(
      $("<div>", { class: "slide-row" }).append(
        $("<div>")
          .append(
            $("<h2>", { text: data.name })
              .append(
                $("<i>", {
                  class: "material-icons md-inline ml-3",
                  text: visibilityIcons[data.visibility],
                  title: visibilityText[data.visibility],
                })
              )
              .append(
                hasPermission
                  ? $("<button>", {
                      class: "material-icons md-inline",
                      text: "edit",
                      title: "Edit slide group information",
                    }).on("click", function () {
                      editSlideGroupModal(data);
                    })
                  : null
              )
              .append(
                $("<button>", {
                  class: "material-icons md-inline",
                  text: "add_to_queue",
                  title: "Add to a scheduling rule",
                }).on("click", function () {
                  addToRulePicker(
                    "slide_group",
                    slideGroup.id,
                    slideGroup.origin
                  );
                })
              )
          )
          .append($("<h3>", { text: data.description }))
      )
    );
    if (hasPermission) {
      // Render the sorted slides
      slideList
        .append(
          slides.map(function (slide) {
            return new Slide(slide).toEdit();
          })
        )
        .append(
          // New slide button only shown in edit mode
          $("<div>", { class: "form-row" }).append(
            $("<button>", { text: "New Slide" })
              .prepend(icon("create"))
              .on("click", function () {
                new Slide()
                  .toEdit()
                  .addClass("changed")
                  .insertBefore(this.parentElement);
              })
          )
        );
    } else {
      slideList.addClass("slide-row").append(
        slides.map(function (slide) {
          return new Slide(slide).toView();
        })
      );
    }
    // Put everything together
    targetElem.empty().append(slideGroup.append(slideList));
    $(".slide-preview").trigger("text-resize");
  });
};

const moveOrCopySlidePicker = function (elem, action) {
  // elem is any element within the slide
  let slide = $(elem).closest("form");
  let pickerList = $("<ul>");
  let copyCheckbox = $("<input>", { type: "checkbox" });
  let pickerUI = $("<div>")
    .append($("<h2>", { text: "Move or copy slide to new group" }))
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            text: "Create a copy: ",
            title: "Check box to create a copy of the slide.",
          })
        )
        .append(copyCheckbox)
    )
    .append($("<h3>", { text: "Select target group" }))
    .append(pickerList);
  // No option to move if no write access to the source element
  if (action === "copy") {
    copyCheckbox.prop("checked", true).prop("disabled", true);
  }
  // Can add to any slide group with write access
  userData.getEdit("slide_groups").done(function (response) {
    pickerList.append(
      response.data.map(function (group) {
        return $("<li>").append(
          $("<button>", {
            text: group.name,
            title: group.description,
          }).on("click", function () {
            let req;
            if (copyCheckbox.prop("checked")) {
              // Make a new slide
              req = $.ajax(
                basePrefix + "/slide_groups/" + group.id + "/slides",
                {
                  type: "POST",
                  data: JSON.stringify(Slide.serializeForm(slide)),
                  contentType: "application/json",
                  dataType: "json",
                }
              );
            } else {
              // Issue move action by updating "group" of a slide
              req = $.ajax(basePrefix + "/slides/" + slide.data("id"), {
                type: "PATCH",
                data: JSON.stringify({ group: group.id }),
                contentType: "application/json",
                dataType: "json",
              });
            }
            // Redirect to the created slide
            req.done(function (response) {
              window.location.hash =
                "slide_groups/" + group.id + "/slides/" + response.data.id;
              pickerUI.trigger("dismiss");
            });
          })
        );
      })
    );
  });
  modalElement(pickerUI);
};

/**
 * Modal to add a slide or remote slide to a rule that is accessible
 * to the current user.
 *
 * @param {String} obj - Type of object being added e.g. "slide_group"
 * @param {Number|String} id - Identifier of the object
 * @param {String} [origin] - Remote server if object is not local
 */
const addToRulePicker = function (obj, id, origin) {
  let thisOid = oid(obj, id, origin);
  let pickerList = $("<ul>");
  let pickerUI = $("<div>")
    .append($("<h2>", { text: "Add to a schedule" }))
    .append(pickerList);
  userData.getEdit("rules").done(function (response) {
    pickerList.append(
      response.data.map(function (rule) {
        return $("<li>").append(
          $("<button>", { text: rule.name, title: rule.description }).on(
            "click",
            function () {
              let btn = $(this);
              $.ajax(basePrefix + "/rules/" + rule.id + "/members", {
                type: "PATCH",
                data: JSON.stringify({ op: "add", oid: thisOid }),
                contentType: "application/json",
                dataType: "json",
              }).done(function () {
                btn.addClass("material-icons").text("done");
                setTimeout(function () {
                  btn.removeClass("material-icons").text(rule.name);
                }, 2000);
              });
            }
          )
        );
      })
    );
  });
  modalElement(pickerUI);
};

/**
 * Get the list of remote slide groups from a neighbouring host
 * and display them all. Each section is a slide group with all slides.
 *
 * @param {string|HTMLElement|jQuery} target - The element into which the
 *    rendered slide groups should be inserted.
 * @param {number|string} hostname - The hostname of the remote server
 * @returns {Promise} The request to get the data
 */
const renderRemote = function (target, hostname) {
  //
  // SLIDE GROUP LIST
  //
  let targetElem = $(target);
  // Full view of the remote server's advertised slides
  let signageServer = $("<div>", {
    class: "remote-slides",
    id: "remote-slides-" + hostname,
  });
  let slideGroupList = $("<div>", {
    class: "slide-group-list",
    id: "remote-slide-groups-" + hostname,
  });
  signageServer.data("hostname", hostname);
  // no server is selected, request that they select one and exit
  if (hostname === undefined) {
    return targetElem
      .empty()
      .append(signageServer.append($("<h3>", { text: "Select a remote host" })))
      .promise();
  }
  // Get the info from the API and build the interface
  return $.ajax(basePrefix + "/remote_slides/" + hostname, {
    dataType: "json",
    error: function (xhr) {
      let statusMessage = "An unknown error occurred";
      if (xhr.status === 404) {
        statusMessage = "Neighbour not found.";
      } else if (xhr.status === 403) {
        statusMessage = "No permission to access remote server.";
      }
      targetElem
        .empty()
        .append(signageServer.append($("<h3>", { text: statusMessage })));
    },
  }).done(function (response) {
    /** @param {apiResponse} response */
    let data = response.data;
    // Title
    signageServer.append(
      $("<div>", { class: "slide-row" }).append(
        $("<div>")
          .append($("<h2>", { text: data.name }))
          .append($("<h3>", { text: data.description }))
      )
    );
    data.slide_groups.forEach(function (slideGroup) {
      // slide group header and add to view
      let slides = slideGroup.slides;
      // Header for this slide group
      let remoteSlideGroup = $("<div>", {
        class: "remote-slide-group",
        id: "slide_group-" + slideGroup.id,
      }).append(
        $("<div>", { class: "text-center" })
          .append(
            $("<h3>", { text: slideGroup.name }).append(
              $("<button>", {
                class: "material-icons md-inline ml-3",
                text: "add_to_queue",
                title: "Add to a scheduling rule",
              }).on("click", function () {
                addToRulePicker(
                  "slide_group",
                  slideGroup.id,
                  slideGroup.origin
                );
              })
            )
          )
          .append($("<h4>", { text: slideGroup.description }))
      );
      // Slide ordering by "order" property.
      slides.sort(function (a, b) {
        //  Order is the "id" values in the desired order
        let idxA = slideGroup.order.indexOf(a.id);
        let idxB = slideGroup.order.indexOf(b.id);
        // b is not in the list so goes to end (after A)
        if (idxB < 0) {
          return -1;
        } else if (idxA < 0) {
          return 1;
        } else {
          return idxA - idxB;
        }
      });
      // Render the sorted slides
      let slideList = $("<div>", { class: "slide-row" }).append(
        slides.map(function (slide) {
          return new Slide(slide).toView();
        })
      );
      slideGroupList.append(remoteSlideGroup.append(slideList));
    });
    // Put everything together
    targetElem.empty().append(signageServer.append(slideGroupList));
    $(".slide-preview").trigger("text-resize");
  });
};

/**
 * Grab the current list of display rules and render them
 * into the target div.
 *
 * @param {string|HTMLElement|jQuery} target The element into which the
 *     interface should be inserted.
 * @returns {Promise} The request to get the data
 */
const renderRules = function (target) {
  //
  // RULE LIST
  //
  // Build and render list of rules
  let targetElem = $(target);
  let ruleListEdit = $("<div>", {
    class: "rule-list",
    id: "rule-list-edit",
  });
  let ruleListView = $("<div>", {
    class: "rule-list",
    id: "rule-list-view",
  });
  // Put everything together, updated in the ajax call
  targetElem.empty().append(ruleListEdit).append(ruleListView);
  // Get a list of all displays
  return $.ajax(basePrefix + "/rules", {
    dataType: "json",
    success: function (response) {
      /** @param {apiResponse} response */
      let data = response.data;
      // Render the sorted slides
      // Full view for rules that can be edited by the user.
      // The rest are view only or hidden if not active
      data.forEach(function (rule) {
        let thisRule = new Rule(rule);
        if (userData.hasPermission(thisRule, "rule")) {
          ruleListEdit.append(thisRule.toEdit());
        } else if (thisRule.active) {
          ruleListView.append(thisRule.toView());
        }
      });
      // Create a new rule. Checks if user has permissions to add a rule
      // to any displays, otherwise don't offer the button.
      if (userData.hasRulePermission()) {
        ruleListEdit.append(
          $("<div>", { class: "form-row" }).append(
            $("<button>", { text: "New scheduling rule" })
              .prepend(icon("create"))
              .on("click", function () {
                let btnRow = $(this).parent();
                let displayGroupPicker = $("<div>");
                let displayPicker = $("<div>");
                let picker = $("<form>")
                  .append(
                    $("<h3>", { text: "Choose a name for the Scheduling Rule" })
                  )
                  .append(
                    $("<div>", { class: "form-row" })
                      .append(
                        $("<label>", {
                          for: "new-rule-name",
                          text: "Name: ",
                        })
                      )
                      .append(
                        $("<input>", {
                          type: "text",
                          id: "new-rule-name",
                          name: "name",
                          placeholder: "A memorable name",
                          required: true,
                        })
                      )
                  )
                  .append(
                    $("<h3>", {
                      text: "Select a target display group",
                    })
                  )
                  .append(displayGroupPicker)
                  .append(displayPicker)
                  .on("submit", function () {
                    $(this).trigger("dismiss");
                    return false;
                  });
                userData.getEdit("display_groups").done(function (response) {
                  displayGroupPicker.append(
                    $("<ul>").append(
                      response.data.map(function (displayGroup) {
                        return $("<li>")
                          .append(
                            $("<button>", {
                              type: "submit",
                              text: displayGroup.name,
                              title: displayGroup.description,
                            }).on("click", function () {
                              // Use onclick as submit can't easily get the
                              // button that was pressed
                              let data = {
                                name: $("#new-rule-name").val(),
                                display_groups: [displayGroup.id],
                              };
                              // Only send if form is valid
                              if (data.name) {
                                $.ajax(basePrefix + "/rules", {
                                  type: "POST",
                                  data: JSON.stringify(data),
                                  contentType: "application/json",
                                  dataType: "json",
                                }).done(function (response) {
                                  let newRule = new Rule(response.data);
                                  newRule
                                    .toEdit()
                                    .addClass("changed")
                                    .insertBefore(btnRow);
                                });
                              }
                            })
                          )
                          .append(
                            $("<button>", { type: "button", text: "<" }).on(
                              "click",
                              function () {
                                $(this)
                                  .toggleClass("opened")
                                  .siblings("ul")
                                  .slideToggle();
                              }
                            )
                          )
                          .append(
                            $("<ul>", { style: "display: none" }).append(
                              displayGroup.displays.map(function (display) {
                                return $("<li>").append(
                                  $("<button>", {
                                    type: "submit",
                                    text: display.name,
                                    title: display.description,
                                  }).on("click", function () {
                                    // Use onclick as submit can't easily get the
                                    // button that was pressed
                                    let data = {
                                      name: $("#new-rule-name").val(),
                                      displays: [display.id],
                                    };
                                    $.ajax(basePrefix + "/rules", {
                                      type: "POST",
                                      data: JSON.stringify(data),
                                      contentType: "application/json",
                                      dataType: "json",
                                    }).done(function (response) {
                                      let newRule = new Rule(response.data);
                                      newRule
                                        .toEdit()
                                        .addClass("changed")
                                        .insertBefore(btnRow);
                                    });
                                  })
                                );
                              })
                            )
                          );
                      })
                    )
                  );
                });
                userData.getEdit("displays").done(function (response) {
                  displayPicker
                    .append(
                      $("<h3>", {
                        text: "Or pick an individual display",
                      }).append(
                        $("<button>", { type: "button", text: "<" }).on(
                          "click",
                          function () {
                            $(this).closest("div").find("ul").slideToggle();
                          }
                        )
                      )
                    )
                    .append(
                      $("<ul>", { style: "display:none" }).append(
                        response.data.map(function (display) {
                          return $("<li>").append(
                            $("<button>", {
                              type: "submit",
                              text: display.name,
                              title: display.description,
                            }).on("click", function () {
                              // Use onclick as submit can't easily get the
                              // button that was pressed
                              let data = {
                                name: $("#new-rule-name").val(),
                                displays: [display.id],
                              };
                              // Stop click if not valid
                              if (data.name) {
                                $.ajax(basePrefix + "/rules", {
                                  type: "POST",
                                  data: JSON.stringify(data),
                                  contentType: "application/json",
                                  dataType: "json",
                                }).done(function (response) {
                                  let newRule = new Rule(response.data);
                                  newRule
                                    .toEdit()
                                    .addClass("changed")
                                    .insertBefore(btnRow);
                                });
                              }
                            })
                          );
                        })
                      )
                    );
                });
                modalElement(picker);
              })
          )
        );
      }
    },
    error: function () {
      let statusMessage = "An unknown error occurred";
      ruleListView.append($("<h3>", { text: statusMessage }));
    },
  }).done(lookupRemoteNames);
};

/**
 * Grab the current list of displays and render them
 * into the target div.
 *
 * @param {string|HTMLElement|jQuery} target The element into which the
 *     interface should be inserted.
 * @param {number|string} [groupId] The id of the group in the database.
 *     If not defined, show all displays.
 */
const renderDisplays = function (target, groupId) {
  //
  // DISPLAY LIST
  //
  // Build and render list of displays
  // (optionally show a specific display group)
  let url;
  let targetElem = $(target);
  // Attributes are numerical
  groupId = Number(groupId) || undefined;
  let displayGroup = $("<div>", {
    class: "display-group",
    id: "display-group-" + (groupId || "all"),
  }).data("id", groupId);
  let displayList = $("<div>", {
    class: "display-list",
    id: "display-list",
  });
  let displayHeader = $("<div>", { class: "slide-row" });

  // No group id is the full list of all displays
  if (groupId === undefined) {
    url = basePrefix + "/displays";
  } else {
    url = basePrefix + "/display_groups/" + groupId;
  }
  // Build the list from the API response
  return $.getJSON(url)
    .fail(function (xhr) {
      let statusMessage = "An unknown error occurred";
      if (xhr.status === 404) {
        statusMessage = "Display group not found.";
      } else if (xhr.status === 403) {
        statusMessage = "No permission to access display group.";
      }
      displayGroup.append($("<h3>", { text: statusMessage }));
    })
    .done(function (response) {
      /** @param {apiResponse} response */
      let data = response.data;
      let displays = data;
      if (groupId === undefined) {
        displayHeader.append(
          $("<div>").append($("<h2>", { text: "All displays" }))
        );
      } else {
        displays = data.displays;
        displayHeader.append(
          $("<div>")
            .append(
              $("<h2>", { text: data.name }).append(
                userData.ifAdmin(
                  $("<button>", {
                    class: "material-icons md-inline",
                    text: "edit",
                    title: "Edit display group information",
                  }).on("click", function () {
                    editDisplayGroupModal(data);
                  })
                )
              )
            )
            .append($("<h3>", { text: data.description }))
        );
      }
      // Admins shown an edit interface, everyone else is view only
      if (userData.admin) {
        // Displays in edit mode, and add new display button
        displayList
          .append(
            displays.map(function (display) {
              return new Display(display).toEdit(groupId);
            })
          )
          .append(
            $("<div>", { class: "form-row" }).append(
              $("<button>", { type: "button", text: "New display screen" })
                .prepend(icon("create"))
                .on("click", function () {
                  let newDisplay = new Display();
                  let displayView = newDisplay.toEdit(groupId);
                  displayView
                    .addClass("changed")
                    .insertBefore(this.parentElement);
                })
            )
          );
      } else {
        // View only slides
        displayList.addClass("slide-row").append(
          displays.map(function (display) {
            return new Display(display).toView();
          })
        );
      }
      displayGroup.append(displayHeader).append(displayList);
    })
    .then(function () {
      targetElem.empty().append(displayGroup);
    });
};

/**
 * Create a list item from a user group to be displayed in the table
 * of user properties. Will have a delete button to remove it.
 *
 * @param {Object} group - The group to render that has id and name.
 * @param {string} currentGroupId - Current group if in a group view.
 * @returns {*|jQuery}
 */
const userGroupsListItem = function (group, currentGroupId) {
  return $("<li>", { class: "user-group-member" })
    .data("id", group.id)
    .append(
      $("<a>", {
        href: "#user_groups/" + group.id,
        text: group.name,
        title: group.description,
      }).prepend(icon("group"))
    )
    .append(
      iconButton("delete", "Remove from group").on("click", function () {
        let btn = $(this);
        let crsid = btn.closest("tr").data("crsid");
        $.ajax(basePrefix + "/users/" + crsid + "/groups", {
          type: "PATCH",
          data: JSON.stringify({ op: "remove", id: group.id }),
          contentType: "application/json",
          dataType: "json",
        }).done(function () {
          if (group.id === currentGroupId) {
            btn.closest("tr").remove();
          } else {
            btn.closest("li").remove();
          }
        });
      })
    );
};

/**
 * Create a list of user groups that a user is a member of
 * @param {Array} groups - The list of groups to render
 * @param {Object} user - The user whose groups will be rendered
 * @param {string} [currentGroupId] - The group currently shown in the UI
 * @returns {*|jQuery} A <ul> of interactive elements
 */
const userGroupsList = function (groups, user, currentGroupId) {
  return $("<ul>", {
    id: "user-groups-" + user.crsid,
    class: "member-list inline-member-list",
  })
    .append(
      groups.sort(attrSort("name")).map(function (group) {
        return userGroupsListItem(group, currentGroupId);
      })
    )
    .append(
      iconButton("add", "Add to another group").on("click", function () {
        let picker = $("<ul>").append($("<h3>", { text: "Select group" }));
        let currentGroups = $(this)
          .closest("ul")
          .find(".user-group-member")
          .map(function () {
            return $(this).data("id");
          })
          .get();
        $.ajax(basePrefix + "/user_groups", { dataType: "json" }).done(
          function (response) {
            response.data.forEach(function (userGroup) {
              if (currentGroups.indexOf(userGroup.id) > -1) {
                return null;
              }
              picker.append(
                $("<li>").append(
                  $("<button>", {
                    text: userGroup.name,
                    description: userGroup.description,
                  }).on("click", function () {
                    $.ajax(basePrefix + "/users/" + user.crsid + "/groups", {
                      type: "PATCH",
                      data: JSON.stringify({ op: "add", id: userGroup.id }),
                      contentType: "application/json",
                      dataType: "json",
                    }).done(function (response) {
                      $("#user-groups-" + user.crsid).replaceWith(
                        userGroupsList(response.data, user, currentGroupId)
                      );
                    });
                  })
                )
              );
            });
          }
        );
        modalElement(picker);
      })
    );
};

/**
 * Create a list item from a user permission to be displayed in the table
 * of user properties. Will have a delete button to remove it.
 *
 * @param {Object} item - The permission to render
 * @param {string} type - The type of permission
 * @returns {null|*|jQuery}
 */
const userPermissionsListItem = function (item, type) {
  let listItem = $("<li>").data("id", item.id);
  if (type === "slide_group") {
    listItem.addClass("slide-group-member").append(
      $("<a>", {
        href: "#slide_groups/" + item.id,
        text: item.name,
        title: item.description,
      }).prepend(icon("slide_group"))
    );
  } else if (type === "display_group") {
    listItem.addClass("display-group-member").append(
      $("<a>", {
        href: "#display_groups/" + item.id,
        text: item.name,
        title: item.description,
      }).prepend(icon("display_group"))
    );
  } else if (type === "display") {
    listItem.addClass("display-member").append(
      $("<a>", {
        href: "#displays/" + item.id,
        text: item.name,
        title: item.description,
      }).prepend(icon("display"))
    );
  } else {
    return null;
  }
  return listItem.append(
    iconButton("delete", "Revoke permission").on("click", function () {
      let btn = $(this);
      let crsid = btn.closest("tr").data("crsid");
      $.ajax(basePrefix + "/users/" + crsid + "/permissions", {
        type: "PATCH",
        data: JSON.stringify({
          op: "remove",
          type: type,
          id: item.id,
        }),
        contentType: "application/json",
        dataType: "json",
      }).done(function () {
        btn.closest("li").remove();
      });
    })
  );
};

const userPermissionsList = function (items, type) {
  return $("<ul>", {
    class: "member-list inline-member-list",
  })
    .addClass("user-" + type + "-permissions")
    .append(
      items.sort(attrSort("name")).map(function (item) {
        return userPermissionsListItem(item, type);
      })
    );
};

/**
 * Create a list of individual permissions that the user has
 * @param {Object} user - The user whose permissions will be rendered
 * @returns {*|jQuery} A <ul> of interactive elements
 */
const userPermissionsRow = function (user) {
  return $("<div>", { id: "user-permissions-" + user.crsid })
    .append(userPermissionsList(user.slide_group_permissions, "slide_group"))
    .append(
      userPermissionsList(user.display_group_permissions, "display_group")
    )
    .append(userPermissionsList(user.display_permissions, "display"))
    .append(userAddPermissionBtn());
};

/**
 * Create a button which, when clicked, will pop up a modal that can
 * be used to add permission items to a user. Needs to be placed
 * somewhere in a tr element to get the current user crsid.
 *
 * @returns {*|jQuery}
 */
const userAddPermissionBtn = function () {
  return iconButton("add", "Grant new permissions").on("click", function () {
    let user = $(this).closest("tr");
    let crsid = user.data("crsid");
    let slideGroupPicker = $("<div>");
    let displayPicker = $("<div>");
    let displayGroupPicker = $("<div>");
    let currentSlideGroups = user
      .find(".slide-group-member")
      .map(getData("id"))
      .get();
    let currentDisplayGroups = user
      .find(".display-group-member")
      .map(getData("id"))
      .get();
    let currentDisplays = user.find(".display-member").map(getData("id")).get();
    userData.getEdit("slide_groups").done(function (response) {
      slideGroupPicker.append($("<h3>", { text: "Add a slide group" })).append(
        $("<ul>").append(
          response.data.map(function (slideGroup) {
            if (currentSlideGroups.indexOf(slideGroup.id) > -1) {
              return null;
            }
            return $("<li>").append(
              $("<button>", {
                text: slideGroup.name,
                title: slideGroup.description,
              }).on("click", function () {
                $.ajax(basePrefix + "/users/" + crsid + "/permissions", {
                  type: "PATCH",
                  data: JSON.stringify({
                    op: "add",
                    type: "slide_group",
                    id: slideGroup.id,
                  }),
                  contentType: "application/json",
                  dataType: "json",
                }).done(function (response) {
                  user
                    .find(".user-slide_group-permissions")
                    .replaceWith(
                      userPermissionsList(response.data, "slide_group")
                    );
                });
              })
            );
          })
        )
      );
    });
    userData.getEdit("display_groups").done(function (response) {
      displayGroupPicker
        .append($("<h3>", { text: "Add a display group" }))
        .append(
          $("<ul>").append(
            response.data.map(function (displayGroup) {
              if (currentDisplayGroups.indexOf(displayGroup.id) > -1) {
                return null;
              }
              return $("<li>").append(
                $("<button>", {
                  text: displayGroup.name,
                  title: displayGroup.description,
                }).on("click", function () {
                  $.ajax(basePrefix + "/users/" + crsid + "/permissions", {
                    type: "PATCH",
                    data: JSON.stringify({
                      op: "add",
                      type: "display_group",
                      id: displayGroup.id,
                    }),
                    contentType: "application/json",
                    dataType: "json",
                  }).done(function (response) {
                    user
                      .find(".user-display_group-permissions")
                      .replaceWith(
                        userPermissionsList(response.data, "display_group")
                      );
                  });
                })
              );
            })
          )
        );
    });
    userData.getEdit("displays").done(function (response) {
      displayPicker
        .append(
          $("<h3>", { text: "Add individual display" }).append(
            $("<button>", { type: "button", text: "<" }).on(
              "click",
              function () {
                $(this).closest("div").find("ul").slideToggle();
              }
            )
          )
        )
        .append(
          $("<ul>", { style: "display:none" }).append(
            response.data.map(function (display) {
              if (currentDisplays.indexOf(display.id) > -1) {
                return null;
              }
              return $("<li>").append(
                $("<button>", {
                  text: display.name,
                  title: display.description,
                }).on("click", function () {
                  $.ajax(basePrefix + "/users/" + crsid + "/permissions", {
                    type: "PATCH",
                    data: JSON.stringify({
                      op: "add",
                      type: "display",
                      id: display.id,
                    }),
                    contentType: "application/json",
                    dataType: "json",
                  }).done(function (response) {
                    user
                      .find(".user-display-permissions")
                      .replaceWith(
                        userPermissionsList(response.data, "display")
                      );
                  });
                })
              );
            })
          )
        );
    });
    // Launch picker
    modalElement(
      $("<div>")
        .append(slideGroupPicker)
        .append(displayGroupPicker)
        .append(displayPicker)
    );
  });
};

/**
 * A row in a table representing the properties of a single user.
 *
 * @param {Object} user - The user to render as a row.
 * @param {string} [currentGroupId] - Id of the current group, if only showing one.
 * @returns {*|jQuery} A <tr> of the rendered user.
 */
const userRow = function (user, currentGroupId) {
  return $("<tr>", { id: "user-" + user.crsid })
    .data("crsid", user.crsid)
    .append(
      $("<td>").append(
        iconButton("edit", "Edit user information").on("click", function () {
          editUserModal(user);
        })
      )
    )
    .append($("<td>", { text: user.crsid }))
    .append($("<td>", { text: user.name }))
    .append(
      $("<td>").append(
        $("<input>", { type: "checkbox", checked: user.admin }).on(
          "change",
          function () {
            let checkbox = this;
            $.ajax(basePrefix + "/users/" + user.crsid, {
              type: "PATCH",
              data: JSON.stringify({ admin: this.checked }),
              contentType: "application/json",
              dataType: "json",
              error: function () {
                checkbox.checked = !checkbox.checked;
              },
            });
          }
        )
      )
    )
    .append(
      $("<td>", { class: "member-list" }).append(
        userGroupsList(user.groups, user, currentGroupId)
      )
    )
    .append(
      $("<td>", { class: "member-list" }).append(userPermissionsRow(user))
    );
};

/**
 * Create a user or edit their basic details. Pops up a modal with fields
 * filled in if an existing user is passed.
 *
 * @param {Object} [user] - Pass an existing user to edit their details,
 *     otherwise a new user will be created.
 * @param {string} [groupId] - If given, create the user within the specified
 *     group.
 */
const editUserModal = function (user, groupId) {
  user = user || {};
  let userForm = $("<form>")
    .append($("<h3>", { class: "form-row", text: "User details" }))
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "user-crsid",
            text: "CRSid: ",
          })
        )
        .append(
          $("<input>", {
            type: "text",
            id: "user-crsid",
            name: "crsid",
            value: user.crsid,
            placeholder: "CRSid of the user",
            disabled: !!user.crsid,
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "new-user-name",
            text: "Name: ",
          })
        )
        .append(
          $("<input>", {
            type: "text",
            id: "new-user-name",
            name: "name",
            value: user.name,
            placeholder: "Name of the user",
          })
        )
    );
  if (user.crsid) {
    userForm
      .append(
        $("<div>", { class: "form-row" })
          .append(
            $("<button>", { type: "button", text: "Delete" }).on(
              "click",
              function () {
                let btn = $(this);
                if (confirm("Really delete?")) {
                  $.ajax(basePrefix + "/users/" + user.crsid, {
                    type: "DELETE",
                  }).done(function () {
                    $("[id='user-" + user.crsid + "']").remove();
                    btn.trigger("dismiss");
                  });
                }
              }
            )
          )
          .append(
            $("<button>", {
              type: "submit",
              text: "Update",
            })
          )
      )
      .on("submit", function () {
        let data = { name: $("[name=name]", this).val() };
        $.ajax(basePrefix + "/users/" + user.crsid, {
          type: "PATCH",
          data: JSON.stringify(data),
          contentType: "application/json",
        }).done(function () {
          updateOnHashChange();
          userForm.trigger("dismiss");
        });
        return false;
      });
  } else {
    userForm
      .append(
        $("<div>", { class: "form-row" })
          .append(
            $("<button>", { type: "button", text: "Cancel" }).on(
              "click",
              function () {
                $(this).trigger("dismiss");
              }
            )
          )
          .append(
            $("<button>", {
              type: "submit",
              text: "Create",
            })
          )
      )
      .on("submit", function () {
        // Can create within a group
        let groups = null;
        let hashPrefix = "users/";
        if (groupId) {
          groups = [groupId];
          hashPrefix = "user_groups/" + groupId + "/users/";
        }
        let data = {
          crsid: $("[name=crsid]", this).val(),
          name: $("[name=name]", this).val(),
          groups: groups,
        };
        $.ajax(basePrefix + "/users", {
          type: "POST",
          data: JSON.stringify(data),
          contentType: "application/json",
        }).then(function (response) {
          let newUser = response.data;
          window.location.hash = hashPrefix + newUser.crsid;
          userForm.trigger("dismiss");
        });
        return false;
      });
  }
  modalElement(userForm);
};

/**
 * Create an item within the list of items that a user group has
 * the permissions to manage. Item can be deleted.
 *
 * @param {Object} item - The item to render
 * @param {string} type - The type of the item
 * @returns {null|*|jQuery}
 */
const userGroupPermissionsListItem = function (item, type) {
  let listItem = $("<li>").data("id", item.id);
  if (type === "slide_group") {
    listItem.addClass("slide-group-member").append(
      $("<a>", {
        href: "#slide_groups/" + item.id,
        text: item.name,
        title: item.description,
      }).prepend(icon("slide_group"))
    );
  } else if (type === "display_group") {
    listItem.addClass("display-group-member").append(
      $("<a>", {
        href: "#display_groups/" + item.id,
        text: item.name,
        title: item.description,
      }).prepend(icon("display_group"))
    );
  } else if (type === "display") {
    listItem.addClass("display-member").append(
      $("<a>", {
        href: "#displays/" + item.id,
        text: item.name,
        title: item.description,
      }).prepend(icon("display"))
    );
  } else {
    return null;
  }
  return listItem.append(
    iconButton("delete", "Revoke permission").on("click", function () {
      let btn = $(this);
      let groupId = btn.closest(".user-list").data("id");
      $.ajax(basePrefix + "/user_groups/" + groupId + "/permissions", {
        type: "PATCH",
        data: JSON.stringify({
          op: "remove",
          type: type,
          id: item.id,
        }),
        contentType: "application/json",
        dataType: "json",
      }).done(function () {
        btn.closest("li").remove();
      });
    })
  );
};

/**
 * Create a list of all the members of a permissions list
 * @param {Array} items - The list of objects to be rendered
 * @param {string} type - Which type of object they are
 * @returns {*|jQuery} A <ul> of the items
 */
const userGroupPermissionsList = function (items, type) {
  return $("<ul>", {
    id: "permissions-list-" + type,
    class: "member-list inline-member-list",
  }).append(
    items.sort(attrSort("name")).map(function (item) {
      return userGroupPermissionsListItem(item, type);
    })
  );
};

/**
 * Create a button which, when clicked, will pop up a modal that can
 * be used to add permission items to a user group. Needs to be placed
 * somewhere in a .user-list element to get the current group.
 *
 * @returns {*|jQuery}
 */
const userGroupAddPermissionBtn = function () {
  return iconButton("add", "Grant new permissions").on("click", function () {
    let group = $(this).closest(".user-list");
    let groupId = group.data("id");
    let permissions = $(this).closest("#user-group-permissions");
    let slideGroupPicker = $("<div>");
    let displayPicker = $("<div>");
    let displayGroupPicker = $("<div>");
    let currentSlideGroups = permissions
      .find(".slide-group-member")
      .map(getData("id"))
      .get();
    let currentDisplayGroups = permissions
      .find(".display-group-member")
      .map(getData("id"))
      .get();
    let currentDisplays = permissions
      .find(".display-member")
      .map(getData("id"))
      .get();
    userData.getEdit("slide_groups").done(function (response) {
      slideGroupPicker.append($("<h3>", { text: "Add a slide group" })).append(
        $("<ul>").append(
          response.data.map(function (slideGroup) {
            if (currentSlideGroups.indexOf(slideGroup.id) > -1) {
              return null;
            }
            return $("<li>").append(
              $("<button>", {
                text: slideGroup.name,
                title: slideGroup.description,
              }).on("click", function () {
                $.ajax(
                  basePrefix + "/user_groups/" + groupId + "/permissions",
                  {
                    type: "PATCH",
                    data: JSON.stringify({
                      op: "add",
                      type: "slide_group",
                      id: slideGroup.id,
                    }),
                    contentType: "application/json",
                    dataType: "json",
                  }
                ).done(function (response) {
                  $("#permissions-list-slide_group").replaceWith(
                    userGroupPermissionsList(response.data, "slide_group")
                  );
                });
              })
            );
          })
        )
      );
    });
    userData.getEdit("display_groups").done(function (response) {
      displayGroupPicker
        .append($("<h3>", { text: "Add a display group" }))
        .append(
          $("<ul>").append(
            response.data.map(function (displayGroup) {
              if (currentDisplayGroups.indexOf(displayGroup.id) > -1) {
                return null;
              }
              return $("<li>").append(
                $("<button>", {
                  text: displayGroup.name,
                  title: displayGroup.description,
                }).on("click", function () {
                  $.ajax(
                    basePrefix + "/user_groups/" + groupId + "/permissions",
                    {
                      type: "PATCH",
                      data: JSON.stringify({
                        op: "add",
                        type: "display_group",
                        id: displayGroup.id,
                      }),
                      contentType: "application/json",
                      dataType: "json",
                    }
                  ).done(function (response) {
                    $("#permissions-list-display_group").replaceWith(
                      userGroupPermissionsList(response.data, "display_group")
                    );
                  });
                })
              );
            })
          )
        );
    });
    userData.getEdit("displays").done(function (response) {
      displayPicker
        .append(
          $("<h3>", { text: "Add individual display" }).append(
            $("<button>", { type: "button", text: "<" }).on(
              "click",
              function () {
                $(this).closest("div").find("ul").slideToggle();
              }
            )
          )
        )
        .append(
          $("<ul>", { style: "display:none" }).append(
            response.data.map(function (display) {
              if (currentDisplays.indexOf(display.id) > -1) {
                return null;
              }
              return $("<li>").append(
                $("<button>", {
                  text: display.name,
                  title: display.description,
                }).on("click", function () {
                  $.ajax(
                    basePrefix + "/user_groups/" + groupId + "/permissions",
                    {
                      type: "PATCH",
                      data: JSON.stringify({
                        op: "add",
                        type: "display",
                        id: display.id,
                      }),
                      contentType: "application/json",
                      dataType: "json",
                    }
                  ).done(function (response) {
                    $("#permissions-list-display").replaceWith(
                      userGroupPermissionsList(response.data, "display")
                    );
                  });
                })
              );
            })
          )
        );
    });
    // Launch picker
    modalElement(
      $("<div>")
        .append(slideGroupPicker)
        .append(displayGroupPicker)
        .append(displayPicker)
    );
  });
};

/**
 * If the user is not an admin, then just show their current permissions
 * in a clickable list.
 *
 * @param {string|HTMLElement} target - An element on the page to replace
 *     with the user info.
 * @returns {Promise}
 */
const renderMe = function (target) {
  // User info is populated from an async refresh of the user data
  let userInfo = $("<div>", { class: "user-list" });
  $(target).empty().append(userInfo);
  return userData.refresh().done(function () {
    let me = userData;
    // No user to render
    if (!me.crsid) return;
    userInfo
      .append(
        $("<div>", { class: "mb-3" })
          .append($("<h2>", { text: me.name || "Unknown User" }))
          .append($("<h3>", { text: me.crsid }))
      )
      .append(
        $("<div>")
          .append($("<h4>", { text: "User groups: " }))
          .append(
            $("<ul>", { class: "member-list" }).append(
              me.groups.map(function (group) {
                return $("<li>", { class: "user-group-member" }).append(
                  $("<a>", {
                    href: "#user_groups/" + group.id,
                    text: group.name,
                    title: group.description,
                  }).prepend(icon("group"))
                );
              })
            )
          )
      )
      .append(
        $("<div>")
          .append($("<h4>", { text: "Slide group permissions: " }))
          .append(
            $("<ul>", { class: "member-list" }).append(
              me.all_slide_group_permissions.map(function (group) {
                return $("<li>", { class: "slide-group-member" }).append(
                  $("<a>", {
                    href: "#slide_groups/" + group.id,
                    text: group.name,
                    title: group.description,
                  }).prepend(icon("slide_group"))
                );
              })
            )
          )
      )
      .append(
        $("<div>")
          .append($("<h4>", { text: "Display group permissions: " }))
          .append(
            $("<ul>", { class: "member-list" }).append(
              me.all_display_group_permissions.map(function (group) {
                return $("<li>", { class: "display-group-member" }).append(
                  $("<a>", {
                    href: "#display_groups/" + group.id,
                    text: group.name,
                    title: group.description,
                  }).prepend(icon("display_group"))
                );
              })
            )
          )
      )
      .append(
        $("<div>")
          .append($("<h4>", { text: "Display permissions: " }))
          .append(
            $("<ul>", { class: "member-list" }).append(
              me.all_display_permissions.map(function (display) {
                return $("<li>", { class: "display-member" }).append(
                  $("<a>", {
                    href: "#displays/" + display.id,
                    text: display.name,
                    title: display.description,
                  }).prepend(icon("display"))
                );
              })
            )
          )
      );
  });
};

/**
 * Draw the user management UI
 *
 * @param {string|HTMLElement|jQuery} target The element into which the
 *     interface should be inserted.
 * @param {string} [groupId] Display only a single group of users.
 */
const renderUsers = function (target, groupId) {
  //
  // USER LIST
  //
  // Admin only, show user info for non admin
  if (!userData.admin) return renderMe(target);
  // Build and render list of users
  let url;
  let targetElem = $(target);
  let userList = $("<div>", { class: "user-list" });
  if (groupId === undefined) {
    url = basePrefix + "/users";
  } else {
    url = basePrefix + "/user_groups/" + groupId;
  }

  return $.ajax(url, {
    dataType: "json",
    error: function (xhr) {
      let statusMessage = "An unknown error occurred";
      if (xhr.status === 404) {
        statusMessage = "User group not found.";
      } else if (xhr.status === 403) {
        statusMessage = "No permission to access user group.";
      }
      userList.append($("<h4>", { text: statusMessage }));
    },
  })
    .done(function (response) {
      /** @param {apiResponse} response */
      let data = response.data;
      let users = data;
      // User group information
      if (groupId !== undefined) {
        users = data.users;
        userList.data("id", groupId).append(
          $("<div>", { id: "user-group-info", class: "mb-3" })
            .append(
              $("<h2>", { id: "user-group-name", text: data.name }).append(
                iconButton("edit", "Edit user group information").on(
                  "click",
                  function () {
                    editUserGroupModal(data);
                  }
                )
              )
            )
            .append(
              $("<h3>", {
                id: "user-group-description",
                text: data.description,
              })
            )
            .append(
              $("<div>", { id: "user-group-permissions" })
                .append(
                  $("<h4>", {
                    text: "Group permissions",
                    class: "inline-member-list mr-3",
                  })
                )
                .append(
                  userGroupPermissionsList(
                    data.slide_group_permissions,
                    "slide_group"
                  )
                )
                .append(
                  userGroupPermissionsList(
                    data.display_group_permissions,
                    "display_group"
                  )
                )
                .append(
                  userGroupPermissionsList(data.display_permissions, "display")
                )
                .append(userGroupAddPermissionBtn())
            )
        );
      }
      // User list
      userList.append(
        $("<table>", { class: "table" })
          .append(
            $("<thead>").append(
              $("<tr>")
                .append($("<th>"))
                .append($("<th>", { text: "CRSid" }))
                .append($("<th>", { text: "Name" }))
                .append($("<th>", { text: "Admin" }))
                .append($("<th>", { text: "Groups" }))
                .append($("<th>", { text: "Permissions" }))
            )
          )
          .append(
            $("<tbody>").append(
              users.sort(attrSort("crsid")).map(function (item) {
                return userRow(item, groupId);
              })
            )
          )
      );
      // Add the new display button if the user can create a display;
      // Admins only can make displays
      if (userData.admin) {
        userList.append(
          $("<div>", { class: "form-row" }).append(
            $("<button>", { text: "New User" })
              .prepend(icon("create"))
              .on("click", function () {
                editUserModal({}, groupId);
              })
          )
        );
      }
    })
    .then(function () {
      targetElem.empty().append(userList);
    });
};

/**
 * A row in a table representing the properties of a federated server.
 *
 * @param {Object} neighbour - The server to render as a row.
 * @returns {*|jQuery} A <tr> of the rendered server.
 */
const neighbourRow = function (neighbour) {
  return $("<tr>", { id: "neighbour-" + neighbour.hostname })
    .data("hostname", neighbour.hostname)
    .append(
      $("<td>").append(
        $("<span>", {
          class: "material-icons",
          text: neighbour.online ? "cloud_done" : "cloud_off",
          title: "Online status",
        })
      )
    )
    .append($("<td>", { text: neighbour.name, title: neighbour.description }))
    .append(
      $("<td>").append(
        $("<a>", {
          href: "//" + neighbour.hostname,
          text: neighbour.hostname,
          target: "_blank",
        })
      )
    )
    .append(
      $("<td>").append(
        $("<input>", { type: "checkbox", checked: neighbour.trusted }).on(
          "change",
          function () {
            let checkbox = this;
            $.ajax(basePrefix + "/neighbours/" + neighbour.hostname, {
              type: "PATCH",
              data: JSON.stringify({ trusted: this.checked }),
              contentType: "application/json",
              dataType: "json",
              error: function () {
                checkbox.checked = !checkbox.checked;
              },
            });
          }
        )
      )
    )
    .append(
      $("<td>").append(
        $("<input>", {
          type: "checkbox",
          checked: neighbour.trust_neighbours,
        }).on("change", function () {
          let checkbox = this;
          $.ajax(basePrefix + "/neighbours/" + neighbour.hostname, {
            type: "PATCH",
            data: JSON.stringify({ trust_neighbours: this.checked }),
            contentType: "application/json",
            dataType: "json",
            error: function () {
              checkbox.checked = !checkbox.checked;
            },
          });
        })
      )
    )
    .append(
      $("<td>").append(
        $("<input>", { type: "checkbox", checked: neighbour.blacklisted }).on(
          "change",
          function () {
            let checkbox = this;
            $.ajax(basePrefix + "/neighbours/" + neighbour.hostname, {
              type: "PATCH",
              data: JSON.stringify({ blacklisted: this.checked }),
              contentType: "application/json",
              dataType: "json",
              error: function () {
                checkbox.checked = !checkbox.checked;
              },
            });
          }
        )
      )
    )
    .append(
      $("<button>", {
        type: "button",
        class: "material-icons",
        text: "delete_forever",
        title: "Remove record of remote server",
      }).on("click", function () {
        let btn = $(this);
        $.ajax(basePrefix + "/neighbours/" + neighbour.hostname, {
          type: "DELETE",
        }).done(function () {
          btn.closest("tr").remove();
        });
      })
    );
};

/**
 * Create a link to a neighbouring signage server. Pops up a modal
 * to enter the address. Other info is taken from the remote server
 * so it has no edit mode. Refreshes the page after a successful
 * creation.
 *
 */
const createNeighbourModal = function () {
  let neighbourForm = $("<form>")
    .append(
      $("<h3>", { class: "form-row", text: "Link to remote signage server" })
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "neighbour-hostname",
            text: "Hostname: ",
          })
        )
        .append(
          $("<input>", {
            type: "text",
            id: "neighbour-hostname",
            name: "hostname",
            placeholder: "Remote address of the server.",
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<button>", { type: "button", text: "Cancel" }).on(
            "click",
            function () {
              $(this).trigger("dismiss");
            }
          )
        )
        .append(
          $("<button>", {
            type: "submit",
            text: "Create",
          })
        )
    )
    .on("submit", function () {
      let data = {
        hostname: $("[name=hostname]", this).val(),
      };
      $.ajax(basePrefix + "/neighbours", {
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
      }).then(function (response) {
        let newNeighbour = response.data;
        window.location.hash = "#admin/_/neighbours/" + newNeighbour.hostname;
        neighbourForm.trigger("dismiss");
      });
      return false;
    });
  modalElement(neighbourForm);
};

/**
 * Create a button that can be clicked to bring up a modal for
 * editing the information about the local server.
 *
 * @param {Object} server - info about the hosting server
 * @returns {*|jQuery} <button> element that opens a modal
 */
const editServerInfoButton = function (server) {
  return $("<button>", { class: "material-icons", text: "edit" }).on(
    "click",
    function () {
      let editServerInfo = $("<form>")
        .append($("<h3>", { class: "form-row", text: "Server Info" }))
        .append(
          $("<div>", { class: "form-row" })
            .append(
              $("<label>", {
                text: "Name: ",
                for: "server-name",
              })
            )
            .append(
              $("<input>", {
                id: "server-name",
                name: "name",
                value: server.name,
                placeholder: "Name identifying this server",
              })
            )
        )
        .append(
          $("<div>", { class: "form-row" })
            .append(
              $("<label>", {
                text: "Description: ",
                for: "server-description",
              })
            )
            .append(
              $("<input>", {
                id: "server-description",
                name: "description",
                value: server.description,
                placeholder: "Longer description of this slide server",
              })
            )
        )
        .append(
          $("<div>", { class: "form-row" })
            .append($("<div>"))
            .append($("<button>", { type: "submit", text: "Update" }))
        )
        .on("submit", function () {
          let form = $(this);
          let data = {
            name: $("[name=name]", this).val(),
            description: $("[name=description]", this).val(),
          };
          $.ajax(basePrefix + "/neighbours/@server", {
            type: "PATCH",
            data: JSON.stringify(data),
            contentType: "application/json",
          }).done(function () {
            updateOnHashChange();
            // remove the modal
            form.trigger("dismiss");
          });
          return false;
        });
      modalElement(editServerInfo);
    }
  );
};

/**
 * Administrative interface!
 *
 * Page for most server oriented properties to be modified as other
 * other admin functions and privileges can be used in their respective
 * pages. Server info and federation links are managed here.
 *
 * @param {string|HTMLElement|jQuery} target - Where to render the interface
 * @returns {Promise}
 */
const renderAdmin = function (target) {
  //
  // ADMIN INTERFACE
  //
  // Admin only, error otherwise
  if (!userData.admin) {
    return $(target)
      .empty()
      .append($("<h4>", { text: "Not authorised" }))
      .promise();
  }
  // Build the admin functions
  let targetElem = $(target);
  let serverInfo = $("<div>", { class: "server-info" });
  let neighbourList = $("<div>", { class: "neighbour-list mt-3" });
  let adminUI = $("<div>").append(serverInfo).append(neighbourList);
  // Server info, what is sent in federation
  return $.when(
    $.ajax(basePrefix + "/neighbours/@server", { dataType: "json" }).done(
      function (response) {
        let info = response.data;
        serverInfo.append($("<h2>", { text: "Server information" })).append(
          $("<div>", { class: "form-row" })
            .append(
              $("<div>")
                .append($("<h3>", { text: info.name }))
                .append($("<h4>", { text: info.description }))
            )
            .append($("<div>").append(editServerInfoButton(info)))
        );
      }
    ),
    // Interface always returns a promise, the request for servers in this case
    $.ajax(basePrefix + "/neighbours", { dataType: "json" })
      .done(function (response) {
        let data = response.data;
        neighbourList
          .append($("<h2>", { text: "Neighbouring Signage Servers" }))
          .append(
            $("<table>", { class: "table" })
              .append(
                $("<thead>").append(
                  $("<tr>")
                    .append($("<th>"))
                    .append($("<th>", { text: "Name" }))
                    .append($("<th>", { text: "Address" }))
                    .append($("<th>", { text: "Show Slides" }))
                    .append($("<th>", { text: "Add Neighbours" }))
                    .append($("<th>", { text: "Blacklist" }))
                    .append($("<th>", { text: "Actions" }))
                )
              )
              .append(
                $("<tbody>").append(
                  data.sort(attrSort("hostname")).map(function (item) {
                    return neighbourRow(item);
                  })
                )
              )
          )
          .append(
            $("<div>", { class: "form-row" }).append(
              $("<button>", { text: "New neighbour" })
                .prepend(icon("create"))
                .on("click", function () {
                  createNeighbourModal({});
                })
            )
          );
      })
      .then(function () {
        targetElem.empty().append(adminUI);
      })
  );
};

/**
 * Embedded help
 *
 * Inject the Google doc of the user guide into the interface.
 *
 * @param {string|HTMLElement|jQuery} target - Where to render the interface
 * @returns {Promise}
 */
const renderHelp = function (target) {
  //
  // GOOGLE IFRAME full embed
  //
  // Use javascript to find the area available for the manual and fill it
  // with an iframe.
  let targetElem = $(target);
  let iframeCss = {
    border: "none",
    position: "fixed",
    height: "calc(100% - " + targetElem.offset().top + "px)",
    width: "100%",
    left: 0,
    zIndex: -1,
  };
  return targetElem
    .empty()
    .append(
      $("<iframe>", {
        src:
          "https://docs.google.com/document/d/e/2PACX-1vRuaum_TFfUUN8Zb2XNTbIilGt1doNnAnLZ0saNemMxCevLtL9NtnJ3Ow7SjEiLviBYXfb4QteWxPCN/pub?embedded=true",
      }).css(iframeCss)
    )
    .promise();
};

/**
 * Create or update a Slide Group.
 *
 * @param {Object} group - Data representing an existing group. If
 *     omitted a new group will be created.
 */
const editSlideGroupModal = function (group) {
  group = group || {};
  let groupForm = $("<form>")
    .append($("<h3>", { class: "form-row", text: "Slide group details" }))
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            text: "Group name: ",
            for: "slide-group-name",
          })
        )
        .append(
          $("<input>", {
            id: "slide-group-name",
            name: "name",
            value: group.name,
            placeholder: "A short name for the group",
            required: true,
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            text: "Group description: ",
            for: "slide-group-description",
          })
        )
        .append(
          $("<input>", {
            id: "slide-group-description",
            name: "description",
            value: group.description,
            placeholder: "More details about the group",
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            for: "slide-group-visibility",
            text: "Visibility",
          })
        )
        .append(
          $("<select>", { name: "visibility", id: "slide-group-visibility" })
            .append($("<option>", { value: "private", text: "Private" }))
            .append($("<option>", { value: "local", text: "Local" }))
            .append($("<option>", { value: "public", text: "Public" }))
            .append($("<option>", { value: "share", text: "Share" }))
            .val(group.visibility || "public")
        )
    );
  if (group.id) {
    groupForm
      .append(
        $("<div>", { class: "form-row" })
          .append(
            userData.ifAdmin(
              $("<button>", { type: "button", text: "Delete" }).on(
                "click",
                function () {
                  if (confirm("Are you sure?")) {
                    $.ajax(basePrefix + "/slide_groups/" + group.id, {
                      type: "DELETE",
                    }).done(function () {
                      window.location.hash = "slide_groups";
                      groupForm.trigger("dismiss");
                    });
                  }
                }
              ),
              $("<div>")
            )
          )
          .append($("<button>", { type: "submit", text: "Update" }))
      )
      .on("submit", function () {
        let form = $(this);
        let data = {
          name: $("[name=name]", this).val(),
          description: $("[name=description]", this).val(),
          visibility: $("[name=visibility] :selected", this).val(),
        };
        $.ajax(basePrefix + "/slide_groups/" + group.id, {
          type: "PATCH",
          data: JSON.stringify(data),
          contentType: "application/json",
        }).done(function () {
          updateOnHashChange();
          // remove the modal
          form.trigger("dismiss");
        });
        return false;
      });
  } else {
    groupForm
      .append(
        $("<div>", { class: "form-row" })
          .append($("<div>"))
          .append($("<button>", { type: "submit", text: "Create" }))
      )
      .on("submit", function () {
        let form = $(this);
        let data = {
          name: $("[name=name]", this).val(),
          description: $("[name=description]", this).val(),
          visibility: $("[name=visibility] :selected", this).val(),
        };
        $.ajax(basePrefix + "/slide_groups", {
          type: "POST",
          data: JSON.stringify(data),
          contentType: "application/json",
        }).done(function (response) {
          let newId = response.data.id;
          window.location.hash = "slide_groups/" + newId;
          // Remove the modal
          form.trigger("dismiss");
        });
        return false;
      });
  }
  modalElement(groupForm);
};

/**
 * Create a button that when clicked generates a modal to create a
 * new slide group and dismiss it when done.
 *
 * @returns {HTMLElement} button A button element
 */
const createNewSlideGroupBtn = function () {
  return $("<button>", {
    type: "button",
    text: "New slide group",
  })
    .prepend(icon("create"))
    .on("click", editSlideGroupModal);
};

/**
 * Create or update a Display Group.
 *
 * @param {Object} group - Data representing an existing group. If
 *     omitted a new group will be created.
 */
const editDisplayGroupModal = function (group) {
  group = group || {};
  let groupForm = $("<form>")
    .append($("<h3>", { class: "form-row", text: "Display group details" }))
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            text: "Group name: ",
            for: "display-group-name",
          })
        )
        .append(
          $("<input>", {
            id: "display-group-name",
            name: "name",
            value: group.name,
            placeholder: "Name to identify the group",
            required: true,
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            text: "Group description: ",
            for: "display-group-description",
          })
        )
        .append(
          $("<input>", {
            id: "display-group-description",
            name: "description",
            value: group.description,
            placeholder: "More details about the group",
          })
        )
    );
  if (group.id) {
    groupForm
      .append(
        $("<div>", { class: "form-row" })
          .append(
            userData.ifAdmin(
              $("<button>", { type: "button", text: "Delete" }).on(
                "click",
                function () {
                  if (confirm("Are you sure?")) {
                    $.ajax(basePrefix + "/display_groups/" + group.id, {
                      type: "DELETE",
                    }).done(function () {
                      window.location.hash = "displays";
                      groupForm.trigger("dismiss");
                    });
                  }
                }
              ),
              $("<div>")
            )
          )
          .append($("<button>", { type: "submit", text: "Update" }))
      )
      .on("submit", function () {
        let form = $(this);
        let data = {
          name: $("[name=name]", this).val(),
          description: $("[name=description]", this).val(),
        };
        $.ajax(basePrefix + "/display_groups/" + group.id, {
          type: "PATCH",
          data: JSON.stringify(data),
          contentType: "application/json",
        }).done(function () {
          updateOnHashChange();
          // remove the modal
          form.trigger("dismiss");
        });
        return false;
      });
  } else {
    groupForm
      .append(
        $("<div>", { class: "form-row" })
          .append($("<div>"))
          .append($("<button>", { type: "submit", text: "Create" }))
      )
      .on("submit", function () {
        let form = $(this);
        let data = {
          name: $("[name=name]", this).val(),
          description: $("[name=description]", this).val(),
        };
        $.ajax(basePrefix + "/display_groups", {
          type: "POST",
          data: JSON.stringify(data),
          contentType: "application/json",
        }).done(function (response) {
          let newId = response.data.id;
          window.location.hash = "display_groups/" + newId;
          // Remove the modal
          form.trigger("dismiss");
        });
        return false;
      });
  }
  modalElement(groupForm);
};

/**
 * Create a button that when clicked generates a modal to create a
 * new display group and dismiss it when done.
 *
 * @returns {HTMLElement} button A button element
 */
const createNewDisplayGroupBtn = function () {
  return $("<button>", {
    type: "button",
    text: "New group",
  })
    .prepend(icon("create"))
    .on("click", editDisplayGroupModal);
};

/**
 * Create or update a User Group.
 *
 * @param {Object} group - Data representing an existing group. If
 *     omitted a new group will be created.
 */
const editUserGroupModal = function (group) {
  group = group || {};
  let groupForm = $("<form>")
    .append($("<h3>", { class: "form-row", text: "User group details" }))
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            text: "Group name: ",
            for: "user-group-name",
          })
        )
        .append(
          $("<input>", {
            id: "user-group-name",
            name: "name",
            value: group.name,
            placeholder: "A short name for the group",
            required: true,
          })
        )
    )
    .append(
      $("<div>", { class: "form-row" })
        .append(
          $("<label>", {
            text: "Group description: ",
            for: "user-group-description",
          })
        )
        .append(
          $("<input>", {
            id: "user-group-description",
            name: "description",
            value: group.description,
            placeholder: "More details about the group",
          })
        )
    );
  if (group.id) {
    // modify existing
    groupForm
      .append(
        $("<div>", { class: "form-row" })
          .append(
            $("<button>", { type: "button", text: "Delete" }).on(
              "click",
              function () {
                if (confirm("Are you sure?")) {
                  $.ajax(basePrefix + "/user_groups/" + group.id, {
                    type: "DELETE",
                  }).done(function () {
                    window.location.hash = "user_groups";
                    groupForm.trigger("dismiss");
                  });
                }
              }
            )
          )
          .append($("<button>", { type: "submit", text: "Update" }))
      )
      .on("submit", function () {
        let form = $(this);
        let data = {
          name: $("[name=name]", this).val(),
          description: $("[name=description]", this).val(),
        };
        $.ajax(basePrefix + "/user_groups/" + group.id, {
          type: "PATCH",
          data: JSON.stringify(data),
          contentType: "application/json",
        }).done(function () {
          updateOnHashChange();
          // Remove the modal
          form.trigger("dismiss");
        });
        return false;
      });
  } else {
    // create new
    groupForm
      .append(
        $("<div>", { class: "form-row" })
          .append($("<div>"))
          .append($("<button>", { type: "submit", text: "Create" }))
      )
      .on("submit", function () {
        let form = $(this);
        let data = {
          name: $("[name=name]", this).val(),
          description: $("[name=description]", this).val(),
        };
        $.ajax(basePrefix + "/user_groups", {
          type: "POST",
          data: JSON.stringify(data),
          contentType: "application/json",
        }).done(function (response) {
          let newId = response.data.id;
          window.location.hash = "user_groups/" + newId;
          // Remove the modal
          form.trigger("dismiss");
        });
        return false;
      });
  }
  modalElement(groupForm);
};

/**
 * Create a button that when clicked generates a modal to create a
 * new user group and dismiss it when done.
 *
 * @returns {HTMLElement} button An anchor element
 */
const createNewUserGroupBtn = function () {
  return $("<button>", {
    type: "button",
    text: "New user group",
  })
    .prepend(icon("create"))
    .on("click", editUserGroupModal);
};

/**
 * Update the page nav to set the current page as "active".
 *
 * @param {string} page The currently viewed page
 */
const updatePageNav = function (page) {
  // match page name with nav item id
  let navId = {
    slide_groups: "nav-slides",
    remote_slides: "nav-remote",
    rules: "nav-rules",
    displays: "nav-displays",
    display_groups: "nav-displays",
    users: "nav-users",
    user_groups: "nav-users",
    admin: "nav-admin",
  }[page];
  // Update classes on the nav bar
  $("#top-nav")
    .find("a")
    .each(function () {
      if (this.id === navId) {
        this.classList.add("active");
      } else {
        this.classList.remove("active");
      }
    });
};

/**
 * Create a nav containing links to all the visible slide groups. The nav
 * is placed into the given element. Gets the list from an ajax request
 * so will appear asynchronously.
 *
 * @param {string|HTMLElement} element Where to put the nav.
 */
const renderSlideGroupNav = function (element) {
  let slideNav = $("<nav>", { class: "sub-nav" });
  let currentId = String(getUrlHashItem(1, "slide_groups"));
  let slideGroupList = $("<ul>", { class: "slide-nav" }).append(
    $("<li>", { class: "nav-title", text: "Slide Groups" })
  );
  userData
    .getView("slide_groups")
    .done(function (response) {
      response.data.forEach(function (slideGroup) {
        slideGroupList.append(
          $("<li>", { class: "nav-item" }).append(
            $("<a>", {
              href: "#slide_groups/" + slideGroup.id,
              text: slideGroup.name,
              title: slideGroup.description,
              class: String(slideGroup.id) === currentId ? "active" : null,
            })
              .prepend(
                $("<i>", {
                  class: "material-icons md-inline",
                  text: visibilityIcons[slideGroup.visibility],
                })
              )
              .addClass(
                userData.ifPermission(slideGroup, "slide_group", "can-edit")
              )
          )
        );
      });
      // Button that pops up a create new modal.
      slideGroupList.append(
        userData.ifAdmin(
          $("<li>", { class: "nav-item" }).append(createNewSlideGroupBtn())
        )
      );
    })
    .then(function () {
      $(element).empty().append(slideNav.append(slideGroupList));
    });
};

/**
 * Create a nav containing links to all the federated slide groups.
 * Gets the list from an ajax request so will appear asynchronously.
 *
 * @param {string|HTMLElement} element - Where to put the nav.
 */
const renderRemoteNav = function (element) {
  let serverNav = $("<nav>", { class: "sub-nav" });
  let currentServer = String(getUrlHashItem(1, "remote_slides"));
  let serverList = $("<ul>", { class: "server-nav" });
  $.getJSON(basePrefix + "/remote_slides")
    .done(function (response) {
      response.data.forEach(function (signageServer) {
        serverList.append(
          $("<li>", { class: "nav-item" }).append(
            $("<a>", {
              href: "#remote_slides/" + signageServer.hostname,
              text: signageServer.name || signageServer.hostname,
              title: signageServer.description,
              class: signageServer.hostname === currentServer ? "active" : null,
            })
          )
        );
      });
      serverList.append(
        $("<li>", { class: "nav-item" }).append(
          $("<a>", {
            class: "btn material-icons",
            text: "refresh",
            title: "Refresh remote data",
          }).on("click", function () {
            // Forced refresh has
            $.ajax("/api/remote_slides", {
              headers: { "Cache-Control": "max-age=0" },
            }).done(updateOnHashChange());
          })
        )
      );
    })
    .then(function () {
      $(element).empty().append(serverNav.append(serverList));
    });
};

/**
 * Rules have no groupings, clear the sub nav bar
 * @param element
 */
const renderRuleNav = function (element) {
  $(element)
    .empty()
    .append($("<nav>", { class: "sub-nav" }).append($("<ul>")));
};

/**
 * Create a nav containing links to all the visible slide groups. The nav
 * is placed into the given element. Gets the list from an ajax request
 * so will appear asynchronously.
 *
 * @param {string|HTMLElement} element Where to put the nav.
 */
const renderDisplayGroupNav = function (element) {
  let displayNav = $("<nav>", { class: "sub-nav" });
  let currentId = String(getUrlHashItem(1, "display_groups"));
  let displayGroupList = $("<ul>", { class: "display-nav" })
    .append($("<li>", { class: "nav-title", text: "Display Groups" }))
    .append(
      $("<li>", { class: "nav-item" }).append(
        $("<a>", {
          href: "#displays",
          text: "All",
          title: "All displays",
          class: currentId === "undefined" ? "active" : null,
        })
      )
    );
  $.ajax(basePrefix + "/display_groups", {
    dataType: "json",
  })
    .then(function (response) {
      response.data.forEach(function (displayGroup) {
        displayGroupList.append(
          $("<li>", { class: "nav-item" }).append(
            $("<a>", {
              href: "#display_groups/" + displayGroup.id,
              text: displayGroup.name,
              title: displayGroup.description,
              class: String(displayGroup.id) === currentId ? "active" : null,
            })
          )
        );
      });
      // Button that pops up a create new modal.
      displayGroupList.append(
        userData.ifAdmin(
          $("<li>", { class: "nav-item" }).append(createNewDisplayGroupBtn())
        )
      );
    })
    .then(function () {
      $(element).empty().append(displayNav.append(displayGroupList));
    });
};

/**
 * Create a nav containing links to all the user groupings. List is obtained
 * from an async request to user_groups.
 *
 * @param {string|HTMLElement} element - Where to put the nav.
 */
const renderUsersNav = function (element) {
  // Nav is only required for admin
  if (!userData.admin) {
    return $(element)
      .empty()
      .append($("<nav>", { class: "sub-nav" }).append($("<ul>")));
  }
  // Build the nav for admins
  let usersNav = $("<nav>", { class: "sub-nav" });
  let currentId = String(getUrlHashItem(1, "user_groups"));
  let userGroupsList = $("<ul>", { class: "user-nav" })
    .append($("<li>", { class: "nav-title", text: "User Groups" }))
    .append(
      $("<li>", { class: "nav-item" }).append(
        $("<a>", {
          href: "#users",
          text: "All",
          title: "All users",
          class: currentId === "undefined" ? "active" : null,
        })
      )
    );
  userData
    .getEdit("user_groups")
    .done(function (response) {
      response.data.forEach(function (userGroup) {
        userGroupsList.append(
          $("<li>", { class: "nav-item" }).append(
            $("<a>", {
              href: "#user_groups/" + userGroup.id,
              text: userGroup.name,
              title: userGroup.description,
              class: String(userGroup.id) === currentId ? "active" : null,
            })
          )
        );
      });
      // Button that pops up a create new modal.
      userGroupsList.append(
        $("<li>", { class: "nav-item" }).append(createNewUserGroupBtn())
      );
    })
    .then(function () {
      $(element).empty().append(usersNav.append(userGroupsList));
    });
};

/**
 * Admin interface nav is empty. Blank the element.
 *
 * @param {string|HTMLElement} element - Nav element to make empty
 */
const renderAdminNav = function (element) {
  return $(element)
    .empty()
    .append($("<nav>", { class: "sub-nav" }).append($("<ul>")));
};

/**
 * Help nav just has a lik to editing if it's a logged in user
 * @param element
 */
const renderHelpNav = function (element) {
  if (!userData.crsid) {
    return $(element).empty();
  }
  return $(element)
    .empty()
    .append(
      $("<a>", {
        class: "floating-nav",
        href:
          "https://docs.google.com/document/d/1v40O8ExhXWrsdlsgtQsZXgM0uhE6_p_j6W5S39_tG68/edit?usp=sharing",
        text: "Add comments",
        target: "_blank",
      }).prepend(icon("comment"))
    );
};

/**
 * Determine if the components refer to a component for which an
 * id can be constructed and scroll the view to that item.
 *
 * @param {string} itemType What type of object this is, e.g slides
 * @param {string|number} itemId The unique id of the object
 */
const focusItem = function (itemType, itemId) {
  if (!itemType) return;
  // Most are the singular noun with the item id, separated by a dash
  let elementId = itemType.substring(0, itemType.length - 1) + "-" + itemId;
  scrollToMiddle("#" + $.escapeSelector(elementId));
};

const updateOnHashChange = function () {
  // Build the slide list
  let page = getUrlHashItem(0);
  let itemId = getUrlHashItem(1);
  let subItem = getUrlHashItem(2);
  let subItemId = getUrlHashItem(3);
  // helper for post rendering, closure the required items
  let subFocus = function (itemType, itemId) {
    return function () {
      focusItem(itemType, itemId);
    };
  };
  // Highlight the current page in the top nav
  updatePageNav(page);
  if (page === "slide_groups") {
    renderSlideGroupNav("#page-nav");
    renderSlides("#main", itemId).then(subFocus(subItem, subItemId));
  } else if (page === "remote_slides") {
    renderRemoteNav("#page-nav");
    renderRemote("#main", itemId).then(subFocus(subItem, subItemId));
  } else if (page === "rules") {
    renderRuleNav("#page-nav");
    renderRules("#main").then(subFocus(page, itemId));
  } else if (page === "display_groups") {
    renderDisplayGroupNav("#page-nav");
    renderDisplays("#main", itemId).then(subFocus(subItem, subItemId));
  } else if (page === "displays") {
    renderDisplayGroupNav("#page-nav");
    renderDisplays("#main").then(subFocus(page, itemId));
  } else if (page === "user_groups") {
    renderUsersNav("#page-nav");
    renderUsers("#main", itemId).then(subFocus(subItem, subItemId));
  } else if (page === "users") {
    renderUsersNav("#page-nav");
    renderUsers("#main").then(subFocus(page, itemId));
  } else if (page === "admin") {
    renderAdminNav("#page-nav");
    renderAdmin("#main").then(subFocus(subItem, subItemId));
  } else if (page === "help") {
    renderHelpNav("#page-nav");
    renderHelp("#main").then();
  }
};

$(function () {
  // Register global variables before rendering the content
  userData
    .refresh()
    .then(function () {
      // Initial page setup
      if (userData.crsid) {
        $("#user-info")
          .attr("href", "#users")
          .find("span")
          .text(userData.crsid);
      }
      if (userData.admin) {
        $("body").addClass("admin");
      }
    })
    .then(function () {
      // Current page setup
      $(window).on("hashchange", updateOnHashChange).trigger("hashchange");
    });
});
