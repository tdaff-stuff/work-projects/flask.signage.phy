"""

Utility functions for *stuff*

"""
import socket
from functools import lru_cache
from ipaddress import ip_address
from typing import Optional

import psutil
from flask import Request, request


def whoami(req: Request = request) -> Optional[str]:
    """Determine the currently logged in Apache user from the request.

    Looks at the "REMOTE_USER" environment variable set by Apache for
    a logged in user. Failing that, it will look for the headers
    ``X-AAPrincipal`` added by Raven (if configured, ignores any hash)
    and ``X-Forwarded-User`` which can be added manually on a server.

    Parameters
    ----------
    req
        A request object that has headers instance attached. Typically this
        will be the global one that flask provides for a request.

    Returns
    -------
    remote_user
        The name of the user or None if no user can be determined.

    """
    if "REMOTE_USER" in req.environ:
        # Raven sets the REMOTE_USER in the request environment
        remote_user = req.environ.get("REMOTE_USER")
    elif "X-AAPrincipal" in req.headers:
        # raven default for AAHeaders
        # crypto hash, single space, value
        remote_user = req.headers.get("X-AAPrincipal").split(" ", 1)[-1]
    else:
        remote_user = None

    return remote_user


@lru_cache()
def is_local_addr(addr: str) -> bool:
    """
    Determine if an IP address corresponds to an interface on the local
    device.

    Use case is for local requests that need to come from a fully
    qualified hostname to reach the correct virtual web host so appear
    with the IP of an external interface, but still need to be
    recognised as coming from the local machine.

    Parameters
    ----------
    addr
        An ip address that can be parsed by the ipaddress library.

    Returns
    -------
    is_local
        Whether the address is considered as coming from the same machine.
    """
    ip_addr = ip_address(addr)

    for interfaces in psutil.net_if_addrs().values():
        for interface in interfaces:
            if interface.family in [socket.AF_INET, socket.AF_INET6]:
                if ip_addr == ip_address(interface.address.split("%")[0]):
                    return True

    return False
