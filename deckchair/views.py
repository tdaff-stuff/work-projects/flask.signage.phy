"""
Slide Deck Management

Simple access to slide list and contained assets through
API routes. Frontend must do all the rendering so html
and assets are send directly.

"""

from flask import Blueprint, abort
from flask import url_for, redirect, request, jsonify
from flask import send_file, send_from_directory
from werkzeug import Response

from deckchair.assets import AssetStore

# The `/edit/login` route must be Raven authenticated and all other
# routes must set the options to always identify a Raven authenticated user:
# `AAAlwaysDecode on` and `RequestHeader unset "X-AAPrincipal"`
edit = Blueprint("edit", __name__, url_prefix="/edit", static_folder="static")


@edit.route("/")
@edit.route("/<path:path>")
def index(path: str = "index.html") -> Response:
    """
    Send unmatched rules as files from static directory.

    Sends the raw file directly, with '/' treated as a special
    route to index.html

    Parameters
    ----------
    path
        Name of the file in `static` to return.
    """
    return send_from_directory(edit.static_folder, path)


@edit.route("/login")
def login() -> Response:
    return redirect(url_for("edit.index"), 302)


##
# Alias definitions to help people discover the edit interface
##
redirects = Blueprint("redirects", __name__)


@redirects.route("/user", strict_slashes=False)
def go_to_edit() -> Response:
    return redirect(url_for("edit.index"), 308)


##
#  Asset management API
##
assets = Blueprint("assets", __name__, url_prefix="/assets")


@assets.route("/", methods=["POST"], strict_slashes=False)
def assets_post() -> Response:
    """
    Store a new asset in the file store.

    Take the raw data from the request and store it as
    a file with versions converted to the other required
    formats. Once uploaded, redirect to the route path
    for the asset which returns the listing of known
    versions.
    """
    asset_hash = AssetStore().store(request.get_data())

    return redirect(url_for("assets.asset_dir", asset_hash=asset_hash), 303)


@assets.route("/<string:asset_hash>/")
def asset_dir(asset_hash: str) -> Response:
    """
    Return the directory listing for a given asset.

    Check the file store and return a list of the available
    versions of the asset as a dictionary in the `data`
    attribute of the response.

    Parameters
    ----------
    asset_hash
        The hash identifier for the asset (SHA1).
    """
    return jsonify({"data": AssetStore().dir(asset_hash)})


@assets.route("/<string:asset_hash>/<string:filename>")
def asset(asset_hash: str, filename: str) -> Response:
    """
    Retrieve the requested form of the asset.

    Directly send the specific version of the file requested.

    Parameters
    ----------
    asset_hash
        The hash identifier for the asset (SHA1).
    filename
        Name of the specific version of the asset requested.
    """
    origin = request.args.get("origin")
    try:
        return send_file(str(AssetStore().asset_location(asset_hash, filename, origin)))
    except FileNotFoundError:
        return abort(404)


# Persistent assets, for re-use and library functionality
@assets.route("/library", methods=["GET"], strict_slashes=False)
def assets_library_get() -> Response:
    """
    Retrieve the list of saved assets.
    """
    # Ensure we only get single values for dict keys; needed for Py3.5
    narrow = request.args.to_dict(flat=True)
    return jsonify({"data": AssetStore().library(**narrow)})


@assets.route("/library/<string:asset_hash>", methods=["PATCH"])
def assets_library_patch(asset_hash: str) -> Response:
    """
    Add or update an asset in the persistent library. If the resource
    does not already exist, it will be created, otherwise the payload
    is merged into the metadata.

    Response is the complete metadata for that asset.

    Parameters
    ----------
    asset_hash
        The hash identifying the asset (SHA1).
    """
    data = request.get_json(force=True)
    saved_asset = AssetStore().save(asset_hash, **data)
    return jsonify(saved_asset)


@assets.route("/library/<string:asset_hash>", methods=["DELETE"])
def assets_library_delete(asset_hash: str) -> Response:
    """
    Remove an asset from the library.

    Parameters
    ----------
    asset_hash
        The hash identifying the asset (SHA1).
    """
    AssetStore().forget(asset_hash)
    return jsonify({"message": "success"})
