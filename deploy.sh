#!/bin/sh
WHEEL="${1}"
DEST_HOST="${2}"
DEST_PATH="${3}"
DEST_HOSTNAME="${4}"
DEST_TMPDIR="$(mktemp -udt signage-deploy.XXXXXXXX)"
cat > remote-deploy.sh <<EOD
#!/bin/sh
set -ex
mkdir -p '${DEST_PATH}'
rm -f '${DEST_PATH}signage-env/bin/python3'
virtualenv '${DEST_PATH}signage-env' -p "\`which python3\`"
set +x
. '${DEST_PATH}/signage-env/bin/activate'
set -x
pip install --upgrade pip
pip install --upgrade '${DEST_TMPDIR}/${WHEEL}'
if [ "\`id -u\`" = 0 ]; then
    systemctl restart apache2
else
    cat > '${DEST_PATH%/}.ini' <<EOF
[uwsgi]
app_dir = %n
app_name = signage
env = SIGNAGE_SETTINGS=\$(readlink -f '${DEST_PATH}signage.cfg')
env = QT_QPA_PLATFORM=offscreen
EOF
    if [ ! -e '${DEST_PATH}/signage.cfg' ]; then
        python3 -c "import os;print('SECRET_KEY = \"%s\"' % os.urandom(32).hex())" > '${DEST_PATH}/signage.cfg'
        echo "DATA_PATH = \"\$(readlink -f '${DEST_PATH}')\"" >> '${DEST_PATH}/signage.cfg'
        echo "HOSTNAME = \"${DEST_HOSTNAME}\"" >> '${DEST_PATH}/signage.cfg'
    fi
fi
cd /
rm -rf '${DEST_TMPDIR}'
EOD
chmod 755 remote-deploy.sh
rsync "wheels/${WHEEL}" remote-deploy.sh "${DEST_HOST}:${DEST_TMPDIR}"
ssh "${DEST_HOST}" "${DEST_TMPDIR}/remote-deploy.sh"

