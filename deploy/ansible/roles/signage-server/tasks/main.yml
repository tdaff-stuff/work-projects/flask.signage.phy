---

- name: Install webserver packages
  apt:
    pkg:
      - aptitude
      - unattended-upgrades
      - sudo
      - fail2ban
      - rsync
      - python-virtualenv
      - apache2
      - libapache2-mod-wsgi-py3
      - build-essential
      - git
      - curl
      - python3
      - python3-dev
      - python3-cairo
      - python3-setuptools
      - file
      - libcairo2-dev
      - libqt5webengine5
      - poppler-utils
      - libreoffice
    state: latest
    update_cache: yes

# Python + server code setup
- name: Install Python packages
  pip:
    name: "{{ signage_src }}"
    virtualenv: "{{ signage_venv_dir }}"
    virtualenv_python: python3
    extra_args: --extra-index-url https://get.phy.cam.ac.uk/python/packages/
    state: latest
  notify: Reload Apache

- name: Create working directory
  file:
    path: "{{ signage_dir }}"
    state: directory
    owner: www-data
    group: www-data

- name: Install configuration
  template:
    src: signage.cfg.j2
    dest: "{{ signage_dir }}/signage.cfg"
  notify: Reload Apache

- name: Run migrations
  command:
    argv:
    - "{{ signage_venv_dir }}/bin/flask"
    - database
    - migrate
  args:
    chdir: "{{ signage_dir }}"
  environment:
    - FLASK_APP: signage.http.app
  become: yes
  become_user: www-data
  register: migration
  changed_when: migration.stdout != ""
  notify: Reload Apache

- name: Initialise admin users
  command:
    argv:
    - "{{ signage_venv_dir }}/bin/flask"
    - signage
    - create-admin
    - "{{ item }}"
  args:
    chdir: "{{ signage_dir }}"
  environment:
    - FLASK_APP: signage.http.app
  become: yes
  become_user: www-data
  with_items: "{{ admins }}"

# Web server setup
# Conf must be created before ucam_webauth is enabled otherwise it will not
# get loaded https://github.com/ansible/ansible/issues/54846
- name: Check Raven configuration
  stat:
    path: /etc/apache2/mods-available/ucam_webauth.conf
  register: ucam_webauth

- name: Install Raven configuration
  copy:
    content: "AACookieKey \"{{ lookup('password', '/dev/null chars=ascii_letters,digits') }}\""
    dest: /etc/apache2/mods-available/ucam_webauth.conf
  when: not ucam_webauth.stat.exists
  notify: Reload Apache

- name: Install Raven package
  apt:
    deb: https://get.phy.cam.ac.uk/software/mod_ucam_webauth/libapache2-mod-ucam-webauth_2.0.5apache24.debian-{{ ansible_distribution_major_version }}_amd64.deb
  when: ansible_distribution == "Debian"
  notify: Restart Apache

- name: Install Raven package
  apt:
    deb: https://get.phy.cam.ac.uk/software/mod_ucam_webauth/libapache2-mod-ucam-webauth_2.0.5apache24.ubuntu-{{ ansible_distribution_version }}_amd64.deb
  when: ansible_distribution == "Ubuntu"
  notify: Restart Apache

- name: Create Raven key directory
  file:
    path: /etc/apache2/conf/webauth_keys
    state: directory
    owner: root
    group: root
    mode: 0755

- name: Install Raven keys
  get_url:
    url: "{{ item.url }}"
    dest: /etc/apache2/conf/webauth_keys/
    checksum: "{{ item.checksum }}"
  with_items:
    - { url: "https://raven.cam.ac.uk/project/keys/pubkey2", checksum: "md5:084668f1b3806846168c591f1c210b76" }
    - { url: "https://raven.cam.ac.uk/project/keys/pubkey2.crt", checksum: "md5:9eadb8dc6b8e670e4990855a1411e7cd" }

- name: Create self-signed ssl certificate
  include_role:
    name: selfsigned
  when: enable_ssl == "selfsigned"

- name: Install certbot and generate letsencrypt ssl certificate
  include_role:
    name: certbot
  when: enable_ssl == "certbot"

- name: Install provided ssl certificates from files
  copy:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
  with_items:
    - { src: "{{ inventory_hostname }}.key", dest: "/etc/ssl/private/{{ inventory_hostname }}.key" }
    - { src: "{{ inventory_hostname }}.pem", dest: "/etc/ssl/certs/{{ inventory_hostname }}.pem" }
  when: enable_ssl == "files"

- name: Activate Apache modules
  apache2_module:
    name: "{{ item }}"
    state: present
    ignore_configcheck: yes
  with_items:
    - wsgi
    - ssl
    - headers
    - rewrite
    - ucam_webauth
  notify: Reload Apache

# If Raven `.conf` has not been enabled turn the module off and on
# again to enable it. See: https://github.com/ansible/ansible/issues/54846
- name: Check Raven configuration enabled
  stat:
    path: /etc/apache2/mods-enabled/ucam_webauth.conf
  register: ucam_webauth_enabled

- name: Cycle ucam_webauth off to enable config
  apache2_module:
    name: ucam_webauth
    state: absent
    ignore_configcheck: yes
  when: not ucam_webauth_enabled.stat.exists
  notify: Reload Apache

- name: Cycle ucam_webauth on to enable config
  apache2_module:
    name: ucam_webauth
    state: present
    ignore_configcheck: yes
  when: not ucam_webauth_enabled.stat.exists
  notify: Reload Apache

- name: Remove default site
  command: a2dissite 000-default
  args:
    removes: /etc/apache2/sites-enabled/000-default.conf
  notify: Reload Apache

- name: Install Apache site configuration file
  template:
    src: apache-site.conf.j2
    dest: /etc/apache2/sites-available/{{ inventory_hostname }}.conf
  notify: Reload Apache

- name: Activate Apache site
  command: a2ensite {{ inventory_hostname }}
  notify: Reload Apache
  args:
    creates: /etc/apache2/sites-enabled/{{ inventory_hostname }}.conf

- name: Activate cache clearing cron job
  cron:
    name: "clean cache for {{ inventory_hostname }}"
    minute: "32"
    hour: "4"
    job: "curl -kL http://{{ inventory_hostname }}/cache -X DELETE"

- name: Activate federation cron job
  cron:
    name: "update federated info for {{ inventory_hostname }}"
    minute: "*/10"
    job: "curl -kL http://{{ inventory_hostname }}/api/remote_slides -H 'Cache-Control: max-age=0' > /dev/null"

- name: Restart Apache if required already
  meta: flush_handlers
