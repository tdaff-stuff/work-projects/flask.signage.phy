#!/bin/sh
cd "`dirname "${0}"`"
set -e
if [ "${#}" -eq 0 ]; then
    virtualenv signage-env -p "`which python3`"
fi
. signage-env/bin/activate
if [ "${#}" -eq 0 ]; then
    pip install --upgrade pip
    pip install -e .
fi
export FLASK_APP=signage.http.app
export FLASK_ENV=development
export SIGNAGE_SETTINGS="$(readlink -f devel.cfg)"
exec flask run --reload
