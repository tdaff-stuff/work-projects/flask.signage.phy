$env:FLASK_APP="signage.http.app"
$env:SIGNAGE_SETTINGS = "$PSScriptRoot\signage.cfg"
Write-Host "FLASK_APP=$env:FLASK_APP"
Write-Host "SIGNAGE_SETTINGS=$env:SIGNAGE_SETTINGS"
if (Test-Path "$PSScriptRoot\venv\Scripts\Activate.ps1" -Type Leaf)
{
    &$PSScriptRoot\venv\Scripts\Activate.ps1
    Write-Host "VIRTUAL_ENV=$env:VIRTUAL_ENV"
}
elseif (Test-Path "$PSScriptRoot\.venv\Scripts\Activate.ps1" -Type Leaf )
{
    &$PSScriptRoot\.venv\Scripts\Activate.ps1
    Write-Host "VIRTUAL_ENV=$env:VIRTUAL_ENV"
}