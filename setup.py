"""
Digital Signage application

Flask based web application for managing and displaying
signage content to a range of clients with delegated management.
"""

import platform
from setuptools import setup, find_packages

if platform.system() == "Windows":
    python_magic = "python-magic-bin"
else:
    python_magic = "python-magic"

setup(
    name="flask.signage.phy",
    version="2.5.5",
    description="Server component of Physics Digital Signage",
    long_description=__doc__,
    scripts=["signage.wsgi"],
    packages=find_packages(),
    package_data={
        "signage": [
            "http/templates/*",
            "http/static/*",
            "http/static/*/*",
            "display/*/templates/*",
        ],
        "deckchair": ["static/*", "migrations/*", "migrations/versions/*"],
    },
    zip_safe=False,
    install_requires=[
        "Flask",  # web framework
        "Pillow",  # manipulating images
        "SQLAlchemy",  # database interaction
        "libsass",  # processing page stylesheets
        "pycairo",  # Creating patterns
        "colormath",  # blending colours
        "requests",  # remote we interactions
        "cachelib",  # simple cache for temporary data
        "qrcode",  # QR codes on displayed pages
        "PySide2==5.13.2",  # Generate web snapshots (5.15 segfaults Apache)
        "beautifulsoup4",  # live web pages
        "html5lib",  # live web pages
        # deckchair
        "alembic",  # database migrations
        "click",  # commandline for flask
        "icalevents",  # upcoming events
        "psutil",  # determine local IP addresses
        "python-dateutil",  # parsing dates
        python_magic,  # identifying uploaded file types
        "tinydb",  # asset database
        "tzlocal",  # fix ical with no timezone
    ],
)
