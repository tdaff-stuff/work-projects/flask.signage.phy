"""
Signage application wsgi script.

For plugging in to mod_wsgi with apache. A simple apache conf would
look like:

<VirtualHost *>
    ServerName server.name.com

    SetEnv SIGNAGE_SETTINGS "/path/to/signage.cfg"

    WSGIProcessGroup signage
    WSGIApplicationGroup %{GLOBAL}
    WSGIDaemonProcess signage user=www-data group=www-data \
        python-home=/path/to/virtualenv home=/path/to/data/dir
    WSGIScriptAlias / /path/to/signage.wsgi
</VirtualHost>
"""

import os


# set up mod_wsgi to use a virtual environment
# and install the app in there so you can just
# import it here
def application(request_environ, start_response):
    # Enable setting SIGNAGE_SETTINGS through Apache SetEnv.
    if "SIGNAGE_SETTINGS" in request_environ:
        os.environ["SIGNAGE_SETTINGS"] = request_environ["SIGNAGE_SETTINGS"]

    # QtWebEngineView needs a rendering buffer. It will try and use xcb
    # by default on Linux, however this will fail if there is no running
    # X server. Change the default to the minimal view that can run
    # without a screen. ENV can be overwritten from elsewhere.
    # Since it is not hardware accelerated, some pages may not display well
    os.environ["QT_DEBUG_PLUGINS"] = request_environ.get("QT_DEBUG_PLUGINS", "0")
    os.environ["QT_QPA_PLATFORM"] = request_environ.get("QT_QPA_PLATFORM", "minimal")

    # Must be imported after the environment setting
    # otherwise it doesn't see it
    from signage.http.app import app

    return app(request_environ, start_response)


# vim: ft=python
