"""
Configuration should be customised in the app settings.

Defaults are likely optimised for Physics - alter accordingly.
"""

from os import getcwd

from flask import safe_join

# Important data storage configuration
SLIDE_DB = "sqlite:///slides.sqlite3"
CACHE_DIR = safe_join(getcwd(), "cache")

# The SIGNAGE_BADGE is displayed at the top of every slide, to remind the
# visitor which particular system is being viewed. Recommended that this is an
# empty string in production, which also stops the HTML element from being
# shown
SIGNAGE_BADGE = ""

# Site specific parameters. Overwrite these in the
# configuration of the flask app.

# The web page snapshot tool will try and render any urls.
# Restrict what it will accept with a series of regex
WEB_PAGE_WHITELIST = [".*"]

# List of elements that can be clicked to "accept" TOS or GDPR popups
WEB_PAGE_AUTO_CLICK = [
    "#bbccookies-continue-button",  # bbc
    ".js-dismiss",  # stackexchange
    ".js-notice-close",  # stackexchange
    ".gdpr-cookie-consent-button",
    ".qc-cmp-button",
]

# Where clicking is not an option, just hide elements completely
WEB_PAGE_AUTO_HIDE = [
    "#cookiePrompt",  # bbc
    "#viewlet-cookiepolicy",  # cam.ac.uk
    ".nw-c-notification",  # bbc news
    ".critical-incident",  # Stagecoach header
    ".mfp-ready",  # Stagecoach overlay lightbox
]
