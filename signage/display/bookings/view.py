"""
Room display for room booking type content.
"""
import csv
import re
import urllib.parse
from collections import defaultdict
from datetime import datetime, timedelta
from typing import List, Dict, Optional, Union, Set

import requests
from cachelib import SimpleCache
from dateutil.parser import parse
from dateutil.tz import tz
from flask import render_template, g, Response, current_app, request, jsonify
from requests import HTTPError, Timeout

from deckchair.slides import SlideManager
from signage.display.common import parse_hex, preprocess
from signage.display.common import to_clock, format_duration, interpolate
from signage.http.util import DisplayBlueprint


bookings_blueprint = DisplayBlueprint("display.bookings", __name__)

MRBS_DATE = "%H:%M:%S - %A %d %B %Y"
PHY_FIX = re.compile(r"^(Part II Lecture.*|Pt II.*) \((09:15|11:45)(?:am|)\)$")
BOOKER = "https://booker-api.azurewebsites.net/{}"
USER_AGENT = "flask.signage.phy python-requests/{}".format(requests.__version__)


# Have a simple cache for web requests that resides in each
# process. Calendars should be slow moving data so not too
# bothered about cache mismatches as they will update within
# 15 minutes.
http_cache = SimpleCache(default_timeout=900)
# Secondary cache is used as a fallback if the resource is
# unavailable or unreachable for extended period
http_slow_cache = SimpleCache(default_timeout=28800)  # 8 hours


def make_aware(now: Optional[datetime] = None) -> datetime:
    """
    If the given datetime is not timezone aware, give it the
    local timezone. Use the current local time "now" if not given.

    Parameters
    ----------
    now
        datetime to check, defaults to `now`

    Returns
    -------
    aware
        A datetime with a timezone.
    """
    if now is None:
        now = datetime.now()

    if not now.tzinfo:
        now = now.replace(tzinfo=tz.tzlocal())

    return now


def ensure_url(url: str, endpoint: str = "report.php") -> str:
    """
    Ensure a URL points to a specific path or endpoint.

    Given either a hostname, or almost complete url, ensure that the
    scheme is some kind of http, and that the path ends in report.php.

    Parameters
    ----------
    url
        A network location, should work with anything from a hostname
        to a complete url including params (which are removed)
    endpoint
        Path to point the final URL at. Defaults to report.php for
        MRBS.
    """
    if not url.startswith("http"):
        url = "https://{}".format(url)
    return urllib.parse.urljoin(url, endpoint)


def phy_fix(event: Dict) -> None:
    """
    Apply specific fixes for MRBS entries in Physics.

    Currently alters entries that report quarter hour start or end
    times. Events are modified in place.

    Parameters
    ----------
    event
        The event dictionary after a the dates and text fields
        have been processed.
    """
    check = PHY_FIX.match(event["summary"])
    if check:
        event["summary"] = check.group(1)
        event["start"] += timedelta(minutes=15)
        event["end"] -= timedelta(minutes=15)


def get_mrbs(
    url: str, room: str, area: str, span: float = 60, now: Optional[datetime] = None
) -> List[Dict]:
    """
    Retrieve the CSV format report from MRBS and parse into events.

    Multi-day events are left to span their full length and not split up
    into multiple entries.

    Search matching does not need exact names, so either search term
    may be blank.

    Parameters
    ----------
    url
        The web address where the MRBS instance is hosted.
    room
        Name of the room in MRBS used as a lookup search term.
    area
        Name of the area the room belongs to, used as a lookup term.
    span
        Number of days to fetch events for.
    now
        Date to use as the basis for fetching events, will look a week before
        up to span days after. Defaults to the current time.

    Returns
    -------
    events
        A list of events as dictionary objects.
    """
    # Normalise the URL format
    url = ensure_url(url)
    now = make_aware(now)

    # Check if we have a cached copy
    cache_key = "get_mrbs:{}:{}:{}:{}:{}".format(url, room, area, span, now.date())
    current_events = http_cache.get(cache_key)

    if current_events is not None:
        return current_events

    try:
        date_from = now.date() - timedelta(days=7)
        date_to = now.date() + timedelta(days=span)
        params = {
            "from_day": date_from.day,
            "from_month": date_from.month,
            "from_year": date_from.year,
            "to_day": date_to.day,
            "to_month": date_to.month,
            "to_year": date_to.year,
            "areamatch": area,
            "roommatch": room,
            "namematch": "",
            "descrmatch": "",
            "creatormatch": "",
            "match_confirmed": "1",
            "match_approved": "1",
            "output": "0",
            "output_format": "1",  # csv
            "sortby": "r",
            "sumby": "d",
            "phase": "2",
            "datatable": "1",
        }  # type: Dict[str, Union[str, int]]
        remote_content = requests.get(url, params=params, timeout=3.05)
        remote_content.raise_for_status()
        text = remote_content.text.splitlines()
        current_events = []
        for row in csv.DictReader(text):
            start = make_aware(datetime.strptime(row["Start time"], MRBS_DATE))
            end = make_aware(datetime.strptime(row["End time"], MRBS_DATE))
            event = {
                "start": start,
                "end": end,
                "summary": row.get("Brief description") or "No Description",
                "description": row.get("Full Description", ""),
                "duration": end - start,
                "location": row["Room"],
            }
            # Update for quarter-past lectures
            phy_fix(event)
            current_events.append(event)

        http_cache.set(cache_key, current_events)
        # Keep slow cache in sync in case resource disappears
        http_slow_cache.set(cache_key, current_events)
    except (ValueError, HTTPError, KeyError):
        # Something went wrong, so try and get an older cached
        # version, otherwise it's just a empty list
        current_events = http_slow_cache.get(cache_key) or []

    return current_events


def get_booker_token(force: bool = False) -> str:
    """
    Authenticate against the booker API and return the token.

    Credentials need to be set in the configuration of the system.
    Once a token is obtained, it will be cached until it expires, or
    re-authentication is forced.

    Parameters
    ----------
    force
        Ignore any existing tokens and force re-validation

    Returns
    -------
    token
        A valid token.
    """

    cache_key = "get_booker_token"
    # Token can be supplied directly for testing.
    token = http_cache.get(cache_key) or current_app.config.get("BOOKER_TOKEN")

    if token is not None and not force:
        return token

    try:
        # POST the required details to Token
        data = {
            "grant_type": "password",
            "username": current_app.config["BOOKER_USERNAME"],
            "password": current_app.config["BOOKER_PASSWORD"],
        }
        headers = {"User-Agent": USER_AGENT}
        token_endpoint = BOOKER.format("Token")
        req = requests.post(token_endpoint, data=data, headers=headers, timeout=3.05)
        token_data = req.json()

        if "error_description" in token_data:
            raise ValueError(token_data["error_description"])

        # Trim the validity a little so it's flushed before it expires
        timeout = token_data["expires_in"] * 0.9
        token = token_data["access_token"]

        # Keep in the cache for the lifetime of the token.
        # Don't use slow cache as there's only a single valid token.
        http_cache.set(cache_key, token, timeout=timeout)
    except (ValueError, HTTPError, KeyError) as e:
        raise PermissionError("Booker Error: {}".format(e))

    return token


def seed_booker_rooms(rooms: List[int]) -> None:
    """
    Add rooms to the cached list of rooms that the Booker interface
    will retrieve, ahead of time, so that the first call will get
    all of them and pre-populate the caches for all required rooms
    when they are subsequently requested.

    Parameters
    ----------
    rooms
        The room numbers from Booker's system.
    """
    all_rooms: Set[int] = http_cache.get("get_booker:all_rooms") or set()
    if not all_rooms.issuperset(rooms):
        all_rooms.update(rooms)
        http_cache.set("get_booker:all_rooms", all_rooms, timeout=14400)


def get_booker_room(room: Union[int, str]) -> Dict:
    """
    Get the details of the requested room (e.g. Description). If
    the request fails, a placeholder is returned instead. The
    returned data structure is guaranteed to have some keys:
    * Description

    Parameters
    ----------
    room
        The numeric identifier of the room in the Booker system.

    Returns
    -------
    room_info
        The information about the room structured as a
        dictionary.
    """

    # Room requests can't be bundled like event requests, but are
    # also not rate limited in the same way so each room must be
    # requested individually.

    # Authorization data
    headers = {
        "User-Agent": USER_AGENT,
        "Authorization": "Bearer {}".format(get_booker_token()),
    }

    cache_key = "get_booker_room:{}".format(room)
    room_info = http_cache.get(cache_key)

    # Room is already known
    if room_info is not None:
        return room_info

    # Pull the room info from the API
    try:
        room_api = BOOKER.format("api/rooms/{}".format(room))
        req = requests.get(room_api, headers=headers, timeout=3.05)
        # Create an empty response for unknown room so it can
        # be cached. Other status errors can retry later.
        if req.status_code == 404:
            room_info = {}
        else:
            req.raise_for_status()
            room_info = req.json()
        # Ensure that the promised keys exist in the output.
        room_info["Description"] = room_info.get("Description", "Unknown Room")
    except (ValueError, HTTPError, KeyError):
        room_info = None

    # Cache room data if it's valid
    if room_info is not None:
        http_cache.set(cache_key, room_info)
        return room_info

    # Return a dummy room
    return {"Description": "Unknown Room"}


def get_booker(
    room: int, span: float = 30, now: Optional[datetime] = None
) -> List[Dict]:
    """
    Retrieve bookings from the University's Booker system.

    The ID of the room is required, which can be obtained from
    the URL when viewing the room in the web application.

    Parameters
    ----------
    room
        The room number in Booker's system.
    span
        Number of days to fetch events for.
    now
        Date to use as the basis for fetching events, will look a day before
        and up to span days after. Defaults to the current time.

    Returns
    -------
    events
        A list of events as dictionary objects.
    """
    now = make_aware(now)

    # Check if we have a cached copy
    cache_key = "get_booker:{}:{}:{}".format(room, span, now.date())
    all_events = http_cache.get(cache_key)

    if all_events is not None:
        return all_events

    # Maintain a list of all room IDs that have been requested so they can
    # combined into a single API call.
    # First room initialises the list, and will be populated alone. Any
    # new rooms requested during the following minute will be added to the
    # list, but will hit the Booker API limits, returning nothing. The next
    # time one of those rooms is requested after that minute, all rooms
    # will get populated.
    # The all_rooms list is allowed to expire after 7 hours since it was
    # last modified to help clear away rooms that are no longer needed. While
    # the list is re-populated, requests that hit the Booker API rate limit
    # will be served from the slow cache. Timeout should therefore be shorter
    # than the slow cache.
    all_rooms: Set[int] = http_cache.get("get_booker:all_rooms") or set()
    if room not in all_rooms:
        all_rooms.add(room)
        http_cache.set("get_booker:all_rooms", all_rooms, timeout=25200)

    try:
        # Suggested query from the API docs and inspecting the application
        data = {
            "StartDate": (now - timedelta(days=1)).isoformat(),
            "EndDate": (now + timedelta(days=span)).isoformat(),
            "RoomIds": list(all_rooms),
            "StatusList": ["Approved", "Pending"],
            "IncludeSubRoomBookings": True,
            "IncludeParentRoomBookings": True,
        }
        # Authorization data
        headers = {
            "User-Agent": USER_AGENT,
            "Authorization": "Bearer {}".format(get_booker_token()),
        }
        bookings_api = BOOKER.format("api/booker/booking/getBookingsForRooms")
        # As of Oct 2020 the API is taking over a second per room so the
        # timeout needs to be high enough to get a responses and fill the
        # cache. Ensure that Apache will not kill long requests.
        req = requests.post(bookings_api, json=data, headers=headers, timeout=60)
        # Error when it hits rate limit (or another problem).
        req.raise_for_status()
        listings = req.json()
        # Split all events by room
        all_events = defaultdict(list)
        for item in listings:
            event_room_id = item["RoomId"]
            start = parse(item["StartTime"]).astimezone()
            end = parse(item["EndTime"]).astimezone()
            event = {
                "start": start,
                "end": end,
                "summary": item.get("Title") or "Untitled Event",
                "description": item.get("Description", ""),
                "location": item["Room"]["Description"],
                "duration": end - start,
            }
            all_events[event_room_id].append(event)

        # Set each room individually in the cache
        for room_id in all_rooms:
            room_cache_key = "get_booker:{}:{}:{}".format(room_id, span, now.date())
            # Use a longer, 30-minute, for Booker to work with slow API
            http_cache.set(room_cache_key, all_events[room_id], timeout=1801)
            http_slow_cache.set(room_cache_key, all_events[room_id])

    except (ValueError, HTTPError, KeyError, Timeout) as e:
        print(repr(e))
        # Some kind of error encountered (includes rate limits)
        return http_slow_cache.get(cache_key) or []

    return all_events[room]


def get_bookings(content: Dict[str, str], now: Optional[datetime] = None) -> List[Dict]:
    """
    Determine which type of bookings are required and call the
    relevant downloading method.

    Parameters
    ----------
    content
        The full content dictionary, will look for the key "booking-type"
        and determine what needs to be done from there.
    now
        Date to use as the basis for fetching events. Passed to the fetching
        functions which usually fetch events around the given date.

    Returns
    -------
    events
        Raw event details with at least the keys "start", "end", "summary".
    """

    booking_type = content.get("booking-type", "mrbs")
    room = content.get("room", "")

    if booking_type == "booker":
        # for Booker, room might be a single room as a string or integer,
        # or a string with a list of room numbers which are picked out
        # as any digits.
        bookings = []
        room_ids = [int(room_id) for room_id in re.findall(r"\d+", str(room))]
        seed_booker_rooms(room_ids)

        # grab each room's bookings separately, but they will all be cached
        # after the first room
        for room_id in room_ids:
            bookings.extend(get_booker(room_id, now=now))

    else:
        # MRBS is the default/fallback
        src = content.get("src", "") or "http://mrbs.cta.phy.cam.ac.uk"
        area = content.get("area", "")
        bookings = get_mrbs(src, room, area, now=now)

    return bookings


def get_bookings_by_room(
    content: Dict[str, str], now: Optional[datetime] = None
) -> List[Dict[str, Union[Dict, List[Dict]]]]:
    """
    Obtain the bookings that have been requested and return them
    in separate lists by room. Rooms from booker will keep the order
    in which they are given, including empty rooms. MRBS will sort
    alphabetically and will not include empty rooms as it works
    using a search parameter.

    Parameters
    ----------
    content
        The full content dictionary, will look for the key "booking-type"
        and "room" to discover which data to retrieve.
    now
        Date to use as the basis for fetching events. Passed to the fetching
        functions which usually fetch events around the given date.

    Returns
    -------
    bookings_by_room
        A list containing dictionaries for each room. Each dictionary
        will have a `bookings` entry that contains the raw list of event
        details to be passed to the processing methods.
    """

    bookings_by_room = []

    booking_type = content.get("booking-type", "mrbs")
    room = content.get("room", "")

    if booking_type == "booker":
        # for Booker, room might be a single room as a string or integer,
        # or a string with a list of room numbers which are picked out
        # as any digits.
        room_ids = [int(room_id) for room_id in re.findall(r"\d+", str(room))]
        seed_booker_rooms(room_ids)

        # grab each room's bookings separately, but they will all be cached
        # after the first room. Room names are fetched at the same time.
        for room_id in room_ids:
            bookings_by_room.append(
                {
                    "room": get_booker_room(room_id),
                    "bookings": get_booker(room_id, now=now),
                }
            )

    else:
        # MRBS is the default/fallback
        src = content.get("src", "") or "http://mrbs.cta.phy.cam.ac.uk"
        area = content.get("area", "")
        bookings = get_mrbs(src, room, area, now=now)
        partitioned_bookings = defaultdict(list)
        for booking in bookings:
            partitioned_bookings[booking["location"]].append(booking)
        for location in sorted(partitioned_bookings):
            bookings_by_room.append(
                {
                    "room": {"Description": location},
                    "bookings": partitioned_bookings[location],
                }
            )

    return bookings_by_room


def process_bookings(
    events: List[Dict],
    max_events: int = 24,
    accent_1: Optional[str] = None,
    accent_2: Optional[str] = None,
    accent_3: Optional[str] = None,
    now: Optional[datetime] = None,
) -> List[Dict]:
    """
    Add some properties to a list of events based on the current time.
    Identifies if the event is in progress, or today, and adds a colour
    that varies with the time to the event start.

    Colouring scheme works best for events during the day. Things close to
    midnight will tend to flip colour on the day change.

    Added properties:

     - `today` - bool
     - `colour` - #rrggbb
     - `progress` - proportion of the booking that's complete

    Parameters
    ----------
    events
        List of events
    max_events
        Number of events that will be displayed on the screen. Fade
        the colours of the events based on the length of time until
        that event.
    accent_1
    accent_2
    accent_3
        Colours used for events as they get further in the future.
        Colours are interpolated between the values with accent_1
        for an event right now, accent_2 for tomorrow and accent_3
        for the furthest in the future.
    now
        Date to use for the process and display of the data. Defaults
        to the current time.

    Returns
    -------
    events
        List of events with properties added

    """
    now = make_aware(now)
    today = now.date()

    # Prune finished events (important if using cached events)
    events = [event for event in events if event["end"] > now]

    # Don't process if no events, skips IndexError
    if not events:
        return []

    # Ensure events are all sorted
    sorted_events = sorted(events, key=lambda x: x["start"])[:max_events]

    # Convert colours to numerical values that can be interpolated
    colour_1 = parse_hex(accent_1 or "#35ffbf")
    colour_2 = parse_hex(accent_2 or "#e85629")
    colour_3 = parse_hex(accent_3 or "#7484ed")

    # Work in seconds as it's easiest to get the total length of a
    # timedelta from `.total_seconds()`
    # Fade to the oldest displayed event, unless it is sooner than a few days
    oldest_event = sorted_events[-1]
    future_fade_span = max((oldest_event["start"] - now).days, 2) * 24 * 60 * 60

    for event in sorted_events:
        event["today"] = event["start"].date() == today
        # calculate the remaining time
        if event["start"] <= now:
            event["complete"] = 100 * (now - event["start"]) / event["duration"]
            event["remaining"] = format_duration(event["end"] - now) or "0m"
            event["colour"] = "rgb({:.0f},{:.0f},{:.0f})".format(*colour_1)
        else:
            event["colour"] = "rgb({:.0f},{:.0f},{:.0f})".format(
                *interpolate(
                    colour_2,
                    colour_3,
                    (event["start"] - now).total_seconds(),
                    future_fade_span,
                )
            )

    if sorted_events and sorted_events[0]["start"] > now:
        sorted_events[0]["free"] = (
            format_duration(sorted_events[0]["start"] - now, short=False) or "0 minutes"
        )

    return sorted_events


@bookings_blueprint.route("/events")
def events_json() -> Response:
    """
    A route to get to get representative data from a URL for previews.

    Takes the src argument and retrieves the list of events, presenting
    their attributes as a list of json items.

    Access as:
        /display/bookings/events?booking-type=mrbs&src=http://mrbs.url/
    """
    # most errors are skipped in processing functions that just
    # return an empty list
    accent_1 = request.args.get("accent-1")
    accent_2 = request.args.get("accent-2")
    accent_3 = request.args.get("accent-3")
    # Use vars() to get the attributes as json can't serialise Event
    return jsonify(
        process_bookings(
            get_bookings(request.args),
            accent_1=accent_1,
            accent_2=accent_2,
            accent_3=accent_3,
        )
    )


@bookings_blueprint.route_display("full", "/slide/<int:slide>")
@bookings_blueprint.route_display("half", "/slide/<int:slide>")
@bookings_blueprint.route_display("full", "/slide/<string:origin>/<int:slide>")
@bookings_blueprint.route_display("half", "/slide/<string:origin>/<int:slide>")
def show(slide: int, origin: Optional[str] = None) -> Union[str, Response]:
    slide_data = SlideManager(current_app.slide_db).slide(slide, origin=origin)
    # Slide does not exist
    if not slide_data:
        raise KeyError("Slide does not exist")

    # Everything displayed on the slide is in content
    # Use generic preprocess to make text-colour etc
    content = slide_data["content"].copy()
    preprocess(content, origin=origin)

    now = make_aware(g.now)

    # Different layouts of the events, each may require different
    # processing
    layout = content.get("layout", "default")

    # Get the data according to

    # Check if we need to split by room first
    if layout.startswith("rooms-"):
        if layout == "rooms-9":
            max_events = 9
            font_basis = "1.6vh"
        else:  # rooms-6
            max_events = 6
            font_basis = "2.2vh"

        # In room layout, one room per column
        columns = get_bookings_by_room(content, now=now)

        # Format data in each column so that it has a title
        # and the correct number of events to display
        for column in columns:
            column["events"] = process_bookings(
                column["bookings"],
                accent_1=content["accent-1"],
                accent_2=content["accent-2"],
                accent_3=content["accent-3"],
                max_events=max_events,
                now=now,
            )
            column["title"] = column["room"]["Description"]
    elif layout.startswith("cols-"):
        events = process_bookings(
            get_bookings(content, now=now),
            accent_1=content["accent-1"],
            accent_2=content["accent-2"],
            accent_3=content["accent-3"],
            now=now,
        )

        if layout == "cols-6+6":
            columns = [{"events": events[:6]}, {"events": events[6:12]}]
            font_basis = "2.2vh"
        elif layout == "cols-7+7":
            columns = [{"events": events[:7]}, {"events": events[7:14]}]
            font_basis = "2.0vh"
        elif layout == "cols-9+9":
            columns = [{"events": events[:9]}, {"events": events[9:18]}]
            font_basis = "1.6vh"
        else:  # "cols-8+8+8":
            columns = [
                {"events": events[:8]},
                {"events": events[8:16]},
                {"events": events[16:24]},
            ]
            font_basis = "1.7vh"
    else:  # layout == "default"
        # Default layout is like columns, but with events in
        # progress highlighted, using a different template
        events = process_bookings(
            get_bookings(content, now=now),
            accent_1=content["accent-1"],
            accent_2=content["accent-2"],
            accent_3=content["accent-3"],
            now=now,
        )
        # Only two running events will fit comfortably so other in-progress
        # events move to the right hand column
        left_events = [event for event in events if event["start"] <= now][:2]
        right_events = events[len(left_events) : len(left_events) + 7]
        return render_template(
            "bookings.html",
            left_events=left_events,
            right_events=right_events,
            content=content,
            to_clock=to_clock,
        )

    # All but the "default" layout use this template
    return render_template(
        "bookings-cols.html",
        columns=columns,
        font_basis=font_basis,
        content=content,
        to_clock=to_clock,
    )
