"""
Simple clock element in a <div>.
"""
from typing import Union

from flask import g, render_template_string
from werkzeug import Response

from signage.http.util import DisplayBlueprint

clock_blueprint = DisplayBlueprint("display.clock", __name__)

CLOCK_STATIC = """
<div class="clock kiosk-hidden" id="slide-clock">
    <span class="date">{{ now.strftime("%a %d %b") }}</span>
    <span class="time">{{ now.strftime("%H:%M") }}</span>
</div>
"""


@clock_blueprint.route_display("full")
@clock_blueprint.route_display("half")
@clock_blueprint.route_display("banner")
def index() -> Union[str, Response]:
    """A simple clock in a <div>"""
    return render_template_string(CLOCK_STATIC, now=g.now)
