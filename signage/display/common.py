"""
Utility functions used by several of the display classes

Includes, pre-processing of colours and contents of deckchair slides,
formatting dates and times, and other useful functions.
"""

from datetime import datetime, timedelta
from typing import Tuple, Optional, Sequence

CLOCK_PANEL = ("aside", [("display.empty.banner", {}), ("display.clock.banner", {})])


def parse_hex(colour: str) -> Tuple[int, int, int]:
    """Convert a #rrggbb hex string into a tuple of r, g, b"""
    return int(colour[1:3], 16), int(colour[3:5], 16), int(colour[5:7], 16)


def hex_to_rgba(colour: str, opacity: Optional[float] = None) -> str:
    """Convert a hex colour string #AABBCC to css rgba format."""
    if opacity is None:
        opacity = 1.0

    # hex to decimal
    r, g, b = parse_hex(colour)
    return "rgba({},{},{},{})".format(r, g, b, opacity)


def hex_to_shadow(colour: str, opacity: Optional[float] = None) -> str:
    """
    Find the appropriate drop shadow colour for the given colour.
    Will return either black or white depending on the darkness of
    the colour.
    """
    if opacity is None:
        opacity = 1.0

    r, g, b = int(colour[1:3], 16), int(colour[3:5], 16), int(colour[5:7], 16)
    linear = (0.2126 * r + 0.7152 * g + 0.0722 * b) / 255
    if linear <= 0.0031308:
        srgb = 12.92 * linear
    else:
        srgb = 1.055 * linear ** (1 / 2.4) - 0.055

    return "rgba({0},{0},{0},{1})".format(255 * round(1 - srgb), opacity)


def preprocess(content: dict, origin: Optional[str] = None) -> None:
    """
    Process colour data from the slide creation UI and convert to
    colours required by the slide templates.

    Adds opacity values to certain colours and ensures that required
    default values are present. Modifies the content in place.
    """
    # TODO: fully generalise content processing for all widgets
    # Boxes can have transparency (make shadow transparent too)
    box_opacity = content.get("box-opacity", 1.0)
    box_colour = content.get("box-colour", "#808080")
    content["box-opacity"] = box_opacity
    content["box-colour"] = hex_to_rgba(box_colour, box_opacity)
    content["box-shadow"] = hex_to_shadow(box_colour, box_opacity)
    # Text requires corresponding shadow only
    text_colour = content.get("text-colour", "#FFFFFF")
    content["text-colour"] = hex_to_rgba(text_colour)
    content["text-shadow"] = hex_to_shadow(text_colour)
    # Increase the blur to scale of the page
    content["blur"] = content.get("blur", 0) * 5
    # Accents are pure hex colours, not "rgb()"
    content["accent-1"] = content.get("accent-1", "#35ffbf")
    content["accent-2"] = content.get("accent-2", "#e85629")
    content["accent-3"] = content.get("accent-3", "#7484ed")
    # whether we fit the background image or not
    content["background-size"] = content.get("image-fit", "cover")
    content["background-fill"] = content.get("background-fill", "#ffffff")
    content["origin"] = origin


def to_clock(dt: datetime) -> str:
    """
    Get the unicode character for a clock 🕞.

    Rounds down to the most recently passed half hour.
    """
    # Clocks go up in hour increments from 128336 for 1 o'clock
    # and 128348 for 1:30.
    unicode_chr = 128335 + (dt.hour % 12 or 12) + (dt.minute >= 30) * 12
    return chr(unicode_chr)


def format_duration(
    dt: timedelta, short: bool = True, spacer: Optional[str] = None
) -> str:
    """
    Make a formatted version of a timedelta with the two most
    significant components. Either short form with d, h, m or
    long form with full words.

    Parameters
    ----------
    dt
        The duration to format as a timedelta object.
    short
        Whether to use the long or short form
    spacer
        Use a custom separator between components, defaults to a an empty
        string for short form or a space for long form.
    """

    # datetime keeps days and seconds
    hours, seconds = divmod(dt.seconds, 3600)
    minutes, seconds = divmod(seconds, 60)

    duration = []

    if dt.days > 0:
        if short:
            duration.append("{}d".format(dt.days))
            if hours > 0:
                duration.append("{}h".format(hours))
        else:
            if dt.days > 1:
                duration.append("{} days".format(dt.days))
            elif dt.days == 1:
                duration.append("1 day")
            if hours > 1:
                duration.append("{} hours".format(hours))
            elif hours == 1:
                duration.append("1 hour")
    else:
        if short:
            if hours > 0:
                duration.append("{}h".format(hours))
            if minutes > 0:
                duration.append("{}m".format(minutes))
        else:
            if hours > 1:
                duration.append("{} hours".format(hours))
            elif hours == 1:
                duration.append("1 hour")
            if minutes > 1:
                duration.append("{} minutes".format(minutes))
            elif minutes == 1:
                duration.append("1 minute")

    if spacer is None:
        spacer = [" ", ""][short]

    return spacer.join(duration)


def interpolate(
    initial: Sequence[float], final: Sequence[float], value: float, max_value: float,
) -> Sequence[float]:
    """
    Calculate the linear interpolation of each component of the lists.
    Values out of the range are truncated.

    Parameters
    ----------
    initial
        Where value == 0
    final
        Where value == max_value
    value
        Evaluate the interpolation for this value
    max_value
        Value for which interpolation reaches final

    Returns
    -------
    interpolated
        A tuple with same number of elements as the shortest input sequence.
    """
    fraction = max(min(value / max_value, 1), 0)  # Cap values
    return tuple(a + ((b - a) * fraction) for a, b in zip(initial, final))
