"""
Render a slide stored in the deckchair database.
"""
from typing import Optional

from flask import current_app, render_template, Response

from deckchair.slides import SlideManager
from signage.display.common import preprocess
from signage.http.util import DisplayBlueprint

deckchair_blueprint = DisplayBlueprint("display.deckchair", __name__)


@deckchair_blueprint.route_display("full", "/slide/<int:slide>")
@deckchair_blueprint.route_display("half", "/slide/<int:slide>")
@deckchair_blueprint.route_display("full", "/slide/<string:origin>/<int:slide>")
@deckchair_blueprint.route_display("half", "/slide/<string:origin>/<int:slide>")
def show(slide: int, origin: Optional[str] = None) -> Response:
    slide_data = SlideManager(current_app.slide_db).slide(slide, origin=origin)
    # Slide does not exist
    if not slide_data:
        raise KeyError("Slide does not exist")
    # Everything displayed on the slide is in content
    content = slide_data["content"].copy()
    preprocess(content, origin=origin)
    return render_template("deckchair.html", content=content)
