"""
Render an empty div and nothing else.
"""

from signage.http.util import DisplayBlueprint

empty_blueprint = DisplayBlueprint("display.empty", __name__)


@empty_blueprint.route_display("full")
@empty_blueprint.route_display("half")
@empty_blueprint.route_display("banner")
def index() -> str:
    return "<div></div>"
