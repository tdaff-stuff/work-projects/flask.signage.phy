"""
If there is nothing else to display then fall back to just showing the
url for the signage system.
"""

from urllib import parse

from flask import url_for, request, render_template_string, Response

from signage.http.util import DisplayBlueprint

fallback_blueprint = DisplayBlueprint("display.fallback", __name__)

FALLBACK_TEMPLATE = """
<div style="display:flex;justify-content:center;align-items:center;flex-direction:column">
    <a style="font-size:300%;color:black;text-decoration:none" href="{{ url }}">
        {{ netloc }}{{ path }}
    </a>
    <a style="font-size:300%;color:black;text-decoration:none" href="#">
        {{ request.remote_addr }}
    </a>
</div>
"""


@fallback_blueprint.route_display("full")
@fallback_blueprint.route_display("half")
@fallback_blueprint.route_display("banner")
def index() -> Response:
    """Display some information for a screen that's not set up."""
    url = parse.urlparse(url_for("view.index", _external=True, now=None))

    # Strip the path if it's empty
    path = url.path
    if url.path == "/":
        path = ""

    return render_template_string(
        FALLBACK_TEMPLATE, url=url.geturl(), netloc=url.netloc, path=path
    )
