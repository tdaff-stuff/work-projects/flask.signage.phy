"""
View for testing only.
"""

from flask import Response, render_template, render_template_string

from signage.http.util import DisplayBlueprint

test_blueprint = DisplayBlueprint("display.test", __name__)


@test_blueprint.route_display("full")
@test_blueprint.route_display("full", "/<string:arg>")
@test_blueprint.route_display("half")
@test_blueprint.route_display("half", "/<string:arg>")
def index(arg: str = "default") -> Response:
    return render_template("test.html", arg=arg)


@test_blueprint.route_display("banner")
@test_blueprint.route_display("banner", "/<string:arg>")
def banner(arg: str = "default") -> Response:
    return render_template_string("<aside><em>{{ arg }}</em></aside>", arg=arg)
