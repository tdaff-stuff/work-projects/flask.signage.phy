"""
Render the upcoming events from an iCalendar feed.

"""

from datetime import datetime, timedelta, timezone
from typing import Optional, List

import re
import requests
import tzlocal
from cachelib import SimpleCache
from dateutil import tz
from flask import current_app, render_template, jsonify, request, Response, g
from icalevents import icalevents
from icalevents.icalparser import Event
from requests.exceptions import RequestException

from deckchair.slides import SlideManager
from signage.display.common import parse_hex, preprocess
from signage.display.common import to_clock, format_duration, interpolate
from signage.http.util import DisplayBlueprint


upcoming_blueprint = DisplayBlueprint("display.upcoming", __name__)

# Have a simple cache for web requests that resides in each
# process. Calendars should be slow moving data so not too
# bothered about cache mismatches as they will update within
# 15 minutes.
http_cache = SimpleCache(default_timeout=900)
# Secondary cache is used as a fallback if the resource is
# unavailable or unreachable for extended period
http_slow_cache = SimpleCache(default_timeout=28800)  # 8 hours

# Regular expressions for identifying calendars with no
# defined timezones
RE_VTIMEZONE = re.compile("BEGIN:VTIMEZONE")  # No global timezone
RE_DTSTART = re.compile("^DTSTART(?!.*TZID)", re.MULTILINE)  # No event timezones


def calendar_timezone_fix(text: str) -> str:
    """
    Try to fix calendars without any timezones defined.

    If a calendar has no timezone for the whole file and does
    not add a timezone for individual events, then the specification
    says events should be interpreted as "floating times"
    (assumed as localtimes), but icalevents defaults to UTC. For
    calendars that need this fix, the name of the local timezone is
    injected into a VTIMEZONE block at the start of the file. The
    DST schedule does not need to be included as the icalendar parser
    uses internal timezone definitions.
    """
    if not RE_VTIMEZONE.search(text) and RE_DTSTART.search(text):
        # Use tzlocal as it can convert Windows to IANA zones
        local_timezone = tzlocal.get_localzone().zone
        lines = text.splitlines()
        # First line must be BEGIN:VCALENDAR, inject after that
        lines[1:1] = [
            "BEGIN:VTIMEZONE",
            "TZID:{}".format(local_timezone),
            "END:VTIMEZONE",
        ]
        return "\n".join(lines)
    else:
        return text


def get_events(url: str, span: float = 365) -> List[Event]:
    """
    Download an iCalendar document from the URL and parse into
    an event list with icalevents. Uses icalevents as it will
    apply event recurrences, but is lossy with some of the
    information in the events.

    Parameters
    ----------
    url
        The URL for an iCalendar (ics/webcal).
    span
        Number of days in the future to fetch events for.

    Returns
    -------
    events
        List of parsed Event objects.
    """

    # Default to fetching a year of events as the parsing by icalevents
    # only increases by about 3% over the one month case. Event parsing is
    # also a tenth of the time of the get request for the calendar and
    # once cached the cost is negligible (it's pickled).
    #
    # Google calendar: Get 263ms; parse 32ms; cache hit 0.4ms
    # talks.cam: Get 87ms; parse 6.8ms; cache hit 0.015ms
    #
    # Costs will go up, even with cache, but performance is about
    # 1 µs per event, so is only an issue if there are tens of thousands
    # in a year

    # if the url is not filled in yet, e.g. newly created slide
    # just give a blank list rather than errors
    if not url:
        return []

    # talks.cam and Apple like to use the webcal URI scheme,
    # but it's just http...
    if url.startswith("webcal://"):
        url = url.replace("webcal://", "http://", 1)

    # Check if we've recently parsed the events, and pull it
    # from the cache to prevent remote http for each request.
    cache_key = "get_events:{}:{}".format(url, span)
    current_events = http_cache.get(cache_key)

    if current_events is not None:
        return current_events

    try:
        remote_content = requests.get(url, timeout=3.05)
        # Fall back to cache during intermittent problems,
        # e.g. 5xx errors or API rate limits that return an error
        # status code but don't raise an exception in requests
        remote_content.raise_for_status()
        span = timedelta(days=span)
        calendar_text = calendar_timezone_fix(remote_content.text)
        current_events = icalevents.parse_events(calendar_text, default_span=span)

        http_cache.set(cache_key, current_events)
        # Keep slow cache in sync in case resource disappears
        http_slow_cache.set(cache_key, current_events)
    except (ValueError, RequestException):
        # Something went wrong, so try and get an older cached
        # version, otherwise it's just a empty list
        current_events = http_slow_cache.get(cache_key) or []

    return current_events


def process_events(
    events: List[Event],
    max_events: int = 9,
    accent_1: Optional[str] = None,
    accent_2: Optional[str] = None,
    accent_3: Optional[str] = None,
    now: Optional[datetime] = None,
) -> List[Event]:
    """
    Add some properties to a list of events based on the current time.
    Identifies if the event is "today" and adds a colour that varies
    with the time to the event start.

    Colouring scheme works best for events during the day. Things close to
    midnight will tend to flip colour on the day change.

    Added properties:

     - `today` - bool
     - `colour` - #rrggbb

    Parameters
    ----------
    events
        List of events
    max_events
        Number of events that will be displayed on the screen. Fade
        the colours of the events based on the length of time until
        that event.
    accent_1
    accent_2
    accent_3
        Colours used for events as they get further in the future.
        Colours are interpolated between the values with accent_1
        for an event right now, accent_2 for tomorrow and accent_3
        for the furthest in the future.
    now
        Date to use for the process and display of the data. Defaults
        to the current time.

    Returns
    -------
    events
        List of events with properties added

    """

    # times in event are timezone aware, so these must be too
    # for comparisons
    if now is None:
        now = datetime.now(timezone.utc)
    elif not now.tzinfo:
        # TODO: skip this step for Python>=3.6
        now = now.replace(tzinfo=tz.tzlocal())
    now = now.astimezone()
    today = now.date()

    # Prune finished events (important if using cached events)
    events = [event for event in events if event.end > now]

    # Don't process if no events, skips IndexError
    if not events:
        return []

    # Recurring events appear out of order, so re-sort
    sorted_events = sorted(events, key=lambda x: x.start)[:max_events]

    # Convert colours to numerical values that can be interpolated
    accent_1 = parse_hex(accent_1 or "#35ffbf")
    accent_2 = parse_hex(accent_2 or "#e85629")
    accent_3 = parse_hex(accent_3 or "#7484ed")

    # Work in seconds as it's easiest to get the total length of a
    # timedelta from `.total_seconds()`
    # Fade for 8 hours during the same day
    day_fade_span = 8 * 60 * 60
    # Fade to the oldest displayed event, unless it is sooner than a week
    oldest_event = sorted_events[-1]
    future_fade_span = max((oldest_event.start - now).days, 7) * 24 * 60 * 60

    for event in sorted_events:
        # icalevents converts None to a string, so change it to empty
        if event.description == "None":
            event.description = ""

        # Normalise dates to current local timezone
        event.start = event.start.astimezone()
        event.end = event.end.astimezone()

        # calculate the duration and put in short format
        event.duration = format_duration(event.end - event.start)

        if event.start.date() == today:
            event.today = True
            event.colour = "rgb({:.0f},{:.0f},{:.0f})".format(
                *interpolate(
                    accent_1,
                    accent_2,
                    (event.start - now).total_seconds(),
                    day_fade_span,
                )
            )
        else:
            event.today = False
            event.colour = "rgb({:.0f},{:.0f},{:.0f})".format(
                *interpolate(
                    accent_2,
                    accent_3,
                    (event.start - now).total_seconds(),
                    future_fade_span,
                )
            )

    return sorted_events


@upcoming_blueprint.route("/events")
def events_json() -> Response:
    """
    A route to get to get representative data from a URL for previews.

    Takes the src argument and retrieves the list of events, presenting
    their attributes as a list of json items.

    Access as:
        /display/upcoming/events?src=http://url.to/cal.ics
    """
    # most errors are skipped in processing functions that just
    # return an empty list
    src = request.args.get("src")
    accent_1 = request.args.get("accent-1")
    accent_2 = request.args.get("accent-2")
    accent_3 = request.args.get("accent-3")
    # Use vars() to get the attributes as json can't serialise Event
    return jsonify(
        [
            vars(x)
            for x in process_events(
                get_events(src),
                accent_1=accent_1,
                accent_2=accent_2,
                accent_3=accent_3,
            )
        ]
    )


@upcoming_blueprint.route_display("full", "/slide/<int:slide>")
@upcoming_blueprint.route_display("half", "/slide/<int:slide>")
@upcoming_blueprint.route_display("full", "/slide/<string:origin>/<int:slide>")
@upcoming_blueprint.route_display("half", "/slide/<string:origin>/<int:slide>")
def show(slide: int, origin: Optional[str] = None) -> Response:
    slide_data = SlideManager(current_app.slide_db).slide(slide, origin=origin)
    # Slide does not exist
    if not slide_data:
        raise KeyError("Slide does not exist")

    # Everything displayed on the slide is in content
    # Use generic preprocess to make text-colour etc
    content = slide_data["content"].copy()
    preprocess(content, origin=origin)

    src = content.get("src", "webcal://talks.cam.ac.uk/show/ics/5444")

    events = process_events(
        get_events(src),
        accent_1=content["accent-1"],
        accent_2=content["accent-2"],
        accent_3=content["accent-3"],
        now=g.now,
    )

    left_events, right_events = events[:3], events[3:9]

    return render_template(
        "upcoming.html",
        left_events=left_events,
        right_events=right_events,
        content=content,
        to_clock=to_clock,
    )
