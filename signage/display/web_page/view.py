"""
Render a view of a web page.

"""
from typing import Optional, Union

import requests
from cachelib import SimpleCache
from bs4 import BeautifulSoup
from flask import current_app, render_template
from PySide2.QtCore import QUrl
from werkzeug.wrappers import Response

from deckchair.slides import SlideManager
from signage.http.util import DisplayBlueprint

web_page_blueprint = DisplayBlueprint("display.web_page", __name__)

# Cache the post-processed contents of a web page so that
# web request and bs4 can be skipped for repeated requests.
http_cache = SimpleCache(default_timeout=900)
# Secondary cache is used as a fallback if the resource is
# unavailable or unreachable for extended period.
http_slow_cache = SimpleCache(default_timeout=28800)  # 8 hours


# CSS rules that will forcibly hide the named elements and scrollbars
HIDE = """
{to_hide} {{
    display: none !important;
}}
::-webkit-scrollbar {{
    display: none;
}}
* {{
    scrollbar-width: none;
}}
"""

INTERACT = """
window.addEventListener("load", () => {{
    document.querySelectorAll('{to_click}').forEach(elem => elem.click());
}})
"""


def get_embedded(url: str, max_age: float = 15) -> Optional[str]:
    """
    Get the contents of a web address and make it an embeddable string.
    Parses with BeautifulSoup using html5lib then injects a page <base>
    and some other minimal formatting code.

    Parameters
    ----------
    url
        Web address to retrieve. Will be interpreted fairly leniently
        so prefixes are not required.
    max_age
        Time, in minutes, to cache a response for.

    Returns
    -------
    page
        Modified contents of the page converted to a string. Response
        will be None if there were any problems retrieving the page.
    """

    # Same URL guessing function as static view
    url = QUrl.fromUserInput(url).toString()

    # Check if it's already cached and send that if it is.
    cache_key = "get_embedded:{}".format(url)
    page = http_cache.get(cache_key)

    if page is not None:
        return page

    try:
        # Pull the content, and fall back to cache if there are errors
        remote_content = requests.get(url, timeout=3.05)
        remote_content.raise_for_status()

        # Parse page with html5lib so that it's browser-like
        html = BeautifulSoup(remote_content.text, "html5lib")

        # Adding <base> at the start of a page without one makes sure that
        # relative URLs point to the correct place. Use the URL from requests
        # after following all redirects e.g. -> https
        if not html.head.base:
            base = html.new_tag("base", href=remote_content.url)
            html.head.insert(0, base)

        # Add the same page modifications as snapshots
        to_hide = ", ".join(current_app.config["WEB_PAGE_AUTO_HIDE"])
        to_click = ", ".join(current_app.config["WEB_PAGE_AUTO_CLICK"])

        # Hide some items with CSS
        hidden = html.new_tag("style", type="text/css")
        hidden.string = HIDE.format(to_hide=to_hide)
        html.head.append(hidden)

        # Interact with any buttons if they're found
        script = html.new_tag("script")
        script.string = INTERACT.format(to_click=to_click)
        html.html.append(script)

        # Render the modified page
        page = html.prettify()

        # Store the page since it's been rendered successfully
        http_cache.set(cache_key, page, timeout=60 * max_age)
        # Slow cache doesn't timeout
        http_slow_cache.set(cache_key, page)

    except IOError:
        page = http_slow_cache.get(cache_key)

    return page


@web_page_blueprint.route_display("full", "/slide/<int:slide>")
@web_page_blueprint.route_display("half", "/slide/<int:slide>")
@web_page_blueprint.route_display("full", "/slide/<string:origin>/<int:slide>")
@web_page_blueprint.route_display("half", "/slide/<string:origin>/<int:slide>")
def show(slide: int, origin: Optional[str] = None) -> Union[str, Response]:
    slide_data = SlideManager(current_app.slide_db).slide(slide, origin)
    # Slide does not exist
    if not slide_data:
        raise KeyError("Slide does not exist")

    content = slide_data["content"].copy()

    content["src"] = content.get("src", "https://www.cam.ac.uk/")
    content["max_age"] = float(content.get("max_age", 10))
    content["wait"] = float(content.get("wait", 1))
    content["zoom"] = float(content.get("zoom", 1.0))
    content["live"] = bool(content.get("live", False))

    # Finish here if it's a snapshot as generation happens in the image code
    if not content["live"]:
        return render_template("web_static.html", content=content)

    # Scrape the page contents and add it to the page
    page = get_embedded(url=content["src"], max_age=content["max_age"])
    return render_template("web_live.html", content=content, page=page)
