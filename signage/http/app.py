"""
signage application

Entry point for the Flask application. Builds the app, adding all the
integrated modules and processors exposing the `app` object.

"""
from typing import Any

from flask import Flask, safe_join
from importlib import import_module
from os import environ, getcwd

from deckchair.slides import SlideDatabase
from deckchair.api import api
from deckchair.cli import db_cli, signage_cli
from deckchair.views import edit, redirects, assets
from signage.http.util import ModJSONEncoder

app = Flask(__name__)

# Configuration for the app is initialised from the defaults.
# If a settings file is indicated in the Env it will be used and
# fail if not found. Otherwise configuration will be taken from
# "signage.cfg" in the working directory.
app.config.from_object("signage.config.defaults")
if "SIGNAGE_SETTINGS" in environ:
    app.config.from_envvar("SIGNAGE_SETTINGS")
else:
    app.config.from_pyfile(safe_join(getcwd(), "signage.cfg"), silent=True)

# Templates remove some of the whitespace so that Jinja indentation doesn't
# result in weird spacing in the rendered output.
app.jinja_env.lstrip_blocks = True
app.jinja_env.trim_blocks = True

# Custom encoder to cope with things like Time, Enum and timedelta
# when sending to jsonify
app.json_encoder = ModJSONEncoder


def register_http_blueprint(name: str, **kwargs: Any) -> None:
    """
    Import a blueprint from the http namespace and register it in the app.
    """
    module = import_module("signage.http.{}".format(name))
    blueprint = getattr(module, "{}_blueprint".format(name))
    app.register_blueprint(blueprint, **kwargs)


def register_display_blueprint(name: str, **kwargs: Any) -> None:
    """
    Import a blueprint from the display namespace and register it under
    the /display path.
    """
    module = import_module("signage.display.{}.view".format(name))
    blueprint = getattr(module, "{}_blueprint".format(name))
    prefix = "/display/{}".format(name)
    app.register_blueprint(blueprint, url_prefix=prefix, **kwargs)


# Standard viewing methods to build the application
register_http_blueprint("subrequest")
register_http_blueprint("styles")
register_http_blueprint("view")
register_http_blueprint("images")

# Individual display types in their own modules
register_display_blueprint("test")
register_display_blueprint("clock")
register_display_blueprint("empty")
register_display_blueprint("fallback")
register_display_blueprint("deckchair")
register_display_blueprint("upcoming")
register_display_blueprint("web_page")
register_display_blueprint("bookings")

# Admin interface from deckchair
app.slide_db = SlideDatabase(app)
app.register_blueprint(edit)
app.register_blueprint(redirects)
app.register_blueprint(assets)
app.register_blueprint(api)

# Adds migration commands for the deckchair database
# and cli commands for the slide management application.
app.cli.add_command(db_cli)
app.cli.add_command(signage_cli)
