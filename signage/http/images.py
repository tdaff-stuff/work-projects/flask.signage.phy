"""
Serve images and generate patterns.

Create resized versions of images and use the pattern generation
routines to create general background image patterns.
"""


import json
import os
import re
from hashlib import sha256
from os import rename
from os.path import splitext, dirname, exists, getmtime
from pathlib import Path
from tempfile import mkstemp
from time import time
from typing import Tuple, Any

from flask import Blueprint, current_app, make_response
from flask import request, redirect, url_for, jsonify
from flask import send_file, safe_join, abort
from PIL import Image
from PIL.Image import registered_extensions
from werkzeug import Response

from deckchair.assets import AssetStore
from signage.display.common import parse_hex
from signage.http.styles import colours
from signage.http.patterns import PatternImage
from signage.http.qr import make_qr_shield
from signage.http.snapshot import snapshot


images_blueprint = Blueprint("images", __name__)

# Extract the colours from those defined in the css as
# single character identifiers (rainbow names) with rgb
# tuples in 0.0 -> 1.0 range.
colours_rgb = {
    name[0]: tuple(x / 255 for x in parse_hex(colour_hex))
    for name, colour_hex in colours.items()
}

# Resized images and patterns will snap to the nearest dimension.
# Don't define specific combinations as aspect ratio may change.
dimensions = (
    216,
    384,
    480,
    576,
    640,
    720,
    960,
    1080,
    1280,
    1440,
    1920,
    2160,
    2560,
    3840,
)


def cache_filename(identifier: str, extension: str) -> str:
    """
    Convert an identifier for the filename (e.g. the URL path) to a
    filesystem location based on a hash of the identifier.
    """
    hashed = sha256(identifier.encode("utf-8")).hexdigest()
    cache_dir = current_app.config["CACHE_DIR"]
    return safe_join(cache_dir, hashed[:2], hashed[2:4], hashed + extension)


def file_age(filename: str) -> float:
    """
    Determine the age of a file in seconds.
    """
    return time() - getmtime(filename)


def snap_dimensions(width: int, height: int) -> Tuple[int, int]:
    """
    Find the dimensions with the closest match to the input. If nothing
    fits, just return the largest size.
    """
    dest_w, dest_h = dimensions[-1], dimensions[-1]

    for dimension in dimensions:
        if width <= dimension:
            dest_w = dimension
            break

    for dimension in dimensions:
        if height <= dimension:
            dest_h = dimension
            break

    return dest_w, dest_h


def fit_to_largest(image: Image, width: int, height: int) -> Image:
    """
    Transform the image into a thumbnail, keeping the aspect ratio,
    but resize to fit along the smallest dimension and overflow the
    other direction.

    Parameters
    ----------
    image
        A PIL Image instance
    width, height
        The dimensions to resize to

    Returns
    -------
    image
        The same image after it has been resized
    """

    # Scale up the dimension that does not fit and then
    # resize to those parameters
    image_aspect_ratio = image.width / image.height
    target_aspect = width / height

    if image_aspect_ratio < target_aspect:
        height = round(width / image_aspect_ratio)
    else:
        width = round(height * image_aspect_ratio)

    # Use thumbnail as it will only scale down, not up
    image.thumbnail((width, height), Image.LANCZOS)

    return image


def get_image(path: str, width: int, height: int) -> Image:
    """
    Retrieve the image at the given filesystem path.

    Resize to the required dimensions.
    """
    src_image = Image.open(path)
    return fit_to_largest(src_image, width=width, height=height)


def save_image_atomic(image: Image, path: str, **save_kwargs: Any) -> None:
    """
    Write file as an atomic operation to remove the chance of another
    process reading a half written file.
    """
    os.makedirs(dirname(path), exist_ok=True)
    # Create a temporary file that can be written to without
    # being accessed while writing
    fd, tmp_path = mkstemp(dir=dirname(path))
    with open(fd, "wb") as file_out:
        image.save(file_out, **save_kwargs)
        os.fsync(fd)
    # Move into place: atomic as long as we're on the same filesystem
    rename(tmp_path, path)


@images_blueprint.route("/pattern/__SIZE__/<int:idx>/<string:colourstr>.png")
@images_blueprint.route(
    "/pattern/<int:width>x<int:height>/<int:idx>/<string:colourstr>.png"
)
def generate_pattern(
    idx: int, colourstr: str, width: int = 1920, height: int = 1080
) -> Response:
    """
    Generate a background image using a procedural function.
    Images are redirected to the closest common resolution and cached since
    the generation can be slow for detailed patterns.

    Parameters
    ----------
    idx
        Pattern identifier. Several are implemented; lucky dip.
    colourstr
        List of colours to use in the image, choose from "roygbivwk".
        Varying the order will generate different permutations.
    width, height
        Size of the image, in pixels.
    """
    try:
        pattern_colours = [colours_rgb[k] for k in colourstr]
    except KeyError:
        return abort(404)
    if idx >= len(PatternImage.patterns):
        return abort(404)

    # If dimensions are not one of the predefined resolutions
    # redirect to one that is
    new_width, new_height = snap_dimensions(width, height)
    if (new_width, new_height) != (width, height):
        return redirect(
            url_for(
                ".generate_pattern",
                idx=idx,
                colourstr=colourstr,
                width=new_width,
                height=new_height,
            ),
            301,
        )
    # Send the file from the cache, otherwise regenerate it using
    # PyCairo by calling a pattern function on an empty image
    dest_filename = cache_filename(request.path, ".png")
    # If cached file exists, send that
    if exists(dest_filename):
        return send_file(dest_filename)

    pattern = PatternImage(width=width, height=height)
    pattern.draw_pattern(idx, pattern_colours)
    save_image_atomic(pattern, dest_filename)
    return send_file(dest_filename)


@images_blueprint.route("/image/__SIZE__/<path:filename>")
@images_blueprint.route("/image/<int:width>x<int:height>/<path:filename>")
def show_image(filename: str, width: int = 1920, height: int = 1080) -> Response:
    """
    Return an image and resize to fit within the given dimensions.

    Parameters
    ----------
    filename
        Name of file to retrieve from storage. May be an "asset" as part of
        a path.
    width, height
        Requested size of the image. Returned dimensions may not be the
        same as the image will retain it's aspect ratio, will not be scaled
        up and will fit the to the smallest relative dimension.

    """
    # Optional origin specification for remote slides
    origin = request.args.get("origin")

    # If dimensions are not one of the predefined resolutions
    # redirect to one that is
    new_width, new_height = snap_dimensions(width, height)
    if (new_width, new_height) != (width, height):
        return redirect(
            url_for(
                ".show_image",
                filename=filename,
                width=new_width,
                height=new_height,
                origin=origin,
            ),
            301,
        )

    # Check that it's supported by Pillow
    try:
        extension = splitext(filename)[1].lower()
        fmt = registered_extensions()[extension]
    except KeyError:
        return abort(404)

    dest_filename = cache_filename(request.path, extension)
    # If cached file exists, send it
    if exists(dest_filename):
        return send_file(dest_filename)

    # Attempt to cache and send the image
    try:
        if filename.startswith("assets/") or filename.startswith("/assets/"):
            # Find the file on the local filesystem, or download it
            image_path = AssetStore().file_location(filename, origin)
        else:
            # Find the file locally
            image_path = safe_join(current_app.static_folder, "images", filename)
        # Apply size and effects as required
        new_image = get_image(image_path, width, height)
        # Create the cache. Use atomic write so that parallel requests don't
        # retrieve partial data.
        save_image_atomic(
            new_image, dest_filename, format=fmt, quality=89, optimize=True
        )
        return send_file(dest_filename)
    except FileNotFoundError:
        # Trying to send something that's not in the filesystem
        return abort(404)


@images_blueprint.route("/image/qr")
def generate_qr() -> Response:
    """Return a QR code encoding the url parameter"""
    url = request.args.get("url")
    modifier = request.args.get("style")
    svg = make_qr_shield(url, modifier=modifier)
    response = make_response(svg)
    response.headers["Content-Type"] = "image/svg+xml"
    response.cache_control.max_age = 60 * 60 * 24 * 7
    return response


@images_blueprint.route("/favicon.ico")
def favicon() -> Response:
    """Send the favicon."""
    favicon_filename = safe_join(current_app.static_folder, "favicon.ico")
    return send_file(favicon_filename)


@images_blueprint.route("/image/snapshot/")
@images_blueprint.route("/image/snapshot/__SIZE__/")
@images_blueprint.route("/image/snapshot/<int:width>x<int:height>/")
def web_snapshot(width: int = 1920, height: int = 1080) -> Response:
    """Generate a screenshot of a web page, or return a cached version."""
    # The scss processing mangles args if there is more than one (escapes &)
    # put all parameters into a json encoded set of parameters
    params = request.args.get("params", default={}, type=json.loads)
    url = params.get("url", "")
    max_age = float(params.get("max_age", 10)) * 60  # minutes
    wait = float(params.get("wait", 1))
    zoom = float(params.get("zoom", 1.0))

    # upscale for anything above 1080p to be similar content area
    zoom *= max(width / 1920, height / 1080, 1.0)

    # Filter urls that don't match any whitelisted ones
    for regex in current_app.config["WEB_PAGE_WHITELIST"]:
        if re.match(regex, url):
            break
    else:
        return abort(404)

    # Interaction elements can be added to the config as needed
    auto_click = current_app.config["WEB_PAGE_AUTO_CLICK"]
    auto_hide = current_app.config["WEB_PAGE_AUTO_HIDE"]

    # Cache each version of the image, size and delay
    identifier = "{}-{}x{}-t{}-z{}".format(url, width, height, zoom, wait)
    filename = cache_filename(identifier, ".png")
    if not exists(filename) or file_age(filename) > max_age:
        snapshot(
            url,
            filename,
            width=width,
            height=height,
            zoom=zoom,
            wait=wait,
            auto_click=auto_click,
            auto_hide=auto_hide,
        )

    if exists(filename):
        # Ensure the image is not cached beyond the required time
        send = send_file(filename)
        send.headers["Cache-Control"] = "public, max-age={}".format(max_age)
        return send
    else:
        return abort(404)


@images_blueprint.route("/cache", methods=["DELETE"])
def clean_image_cache() -> Response:
    """
    Remove old files from the cache directory. Use the `age` parameter
    to filter to files that are a given number of days old and keep
    newer files. Default to keeping files younger than a day.

    Only available from the hosting machine that identifies as 127.0.0.1,
    e.g. through a cron job.
    """

    # Local ip check
    if request.remote_addr not in ["127.0.0.1", "::1"]:
        return abort(403)

    # Age is in days
    age = float(request.args.get("age", 1)) * 24 * 60 * 60
    mtime_cutoff = time() - age

    count = 0
    for item in Path(current_app.config["CACHE_DIR"]).rglob("*"):
        if item.is_file() and item.stat().st_mtime < mtime_cutoff:
            item.unlink()
            count += 1

    return jsonify({"message": "success", "count": count})
