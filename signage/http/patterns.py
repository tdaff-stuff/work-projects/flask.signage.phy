"""
Generation of simple patterns for background images.

"""

import math
import random
from functools import partial
from itertools import cycle
from typing import Sequence, List, Callable, Tuple, Optional
from typing import Any, Union, BinaryIO, Generator


from cairo import Context, ImageSurface, FORMAT_RGB24
from cairo import Pattern, SolidPattern, LineCap, LineJoin
from colormath.color_objects import sRGBColor, LabColor
from colormath.color_conversions import convert_color


def sub_sequence(seq: Sequence, num: int) -> Generator:
    """
    Chop a list into the `num` sub-lists with the members distributed
    between them evenly. Create a generator to yield them in turn.
    """
    per_bin, extra = divmod(len(seq), num)

    for sec_num in range(num):
        idx = sec_num * per_bin + min(sec_num, extra)
        idy = (sec_num + 1) * per_bin + min(sec_num + 1, extra)
        yield seq[idx:idy]


def gen_blob_points(
    min_length: int = 6,
    uniform: Callable = random.uniform,
    min_separation: float = 0.2,
) -> List[Tuple[float, float]]:
    """
    Generate a series of points that trace the outline of a "blob" shaped
    object. Will remove points that are too close, but ensure that there
    are at least `min_length` points.

    Parameters
    ----------
    min_length
        Number of points in the blob will be between min_length and
        1.5 * min_length
    uniform
        Function to generate random numbers between given values. Must
        take args for min and max then return a random number between
        those values
    min_separation
        Prune consecutive points that are closer than the given value

    Returns
    -------
    points
        A list of (x, y) points
    """
    blob_points = []  # type: List[Tuple[float, float]]
    retries = 100  # escape infinite loops if stupid values are passed

    # Ensure we have enough points.
    while len(blob_points) < min_length:
        # Generate a list of random (x, y) then order them anti-clockwise
        # around the origin
        random_points = sorted(
            [
                (uniform(-0.5, 0.5), uniform(-0.5, 0.5))
                for _ in range(int(min_length * 1.5))
            ],
            key=lambda x: math.atan2(x[0], x[1]),
        )

        # only add points that are not too close to the previous one
        blob_points = random_points[:1]
        for point in random_points[1:]:
            if norm(direction(point, blob_points[-1])) > min_separation:
                blob_points.append(point)

        retries -= 1
        if retries < 1:
            raise ValueError("Unable to generate a blob with given parameters")

    return blob_points


def draw_blob(
    ctx: Context,
    pat: Pattern,
    in_points: List[Tuple[float, float]],
    centre: Tuple[float, float],
    size: Tuple[float, float],
    curve: float = 0.7,
):
    """
    Draw a blob on the canvas and paint it with the pattern.

    The given coordinates are used to derive points on the blob at the
    midpoints between successive pairs and the control points for splines
    are derived from the directions between the points.
    """
    ctx.set_source(pat)

    working_points = in_points + in_points[0:2]  # loop around

    def transform(point: Sequence[float]):
        """Transform into required geometry space"""
        return point[0] * size[0] + centre[0], point[1] * size[1] + centre[1]

    # start point
    ctx.move_to(*transform(midpoint(working_points[0], working_points[1])))

    # Already at 0, so go through the rest and back to 0 at the other end
    for idx in range(1, len(working_points) - 1):
        # midpoints between successive blob points
        start_point = midpoint(working_points[idx - 1], working_points[idx])
        end_point = midpoint(working_points[idx], working_points[idx + 1])

        # Determine control points relative to the midpoints, and scale
        # them down a bit to give fewer sharp curves
        ctl_start = vadd(
            start_point, direction(start_point, working_points[idx], scale=curve),
        )
        ctl_end = vadd(
            end_point, direction(end_point, working_points[idx], scale=curve)
        )

        ctx.curve_to(*transform(ctl_start), *transform(ctl_end), *transform(end_point))

    ctx.fill()


# Geometry
def midpoint(point_a: Sequence[float], point_b: Sequence[float]) -> List[float]:
    """Geometric centre between two points"""
    return [(a + b) / 2 for a, b in zip(point_a, point_b)]


def direction(
    point_a: Sequence[float], point_b: Sequence[float], scale: float = 1
) -> List[float]:
    """Vector between two points"""
    return [(b - a) * scale for a, b in zip(point_a, point_b)]


def vadd(vec_a: Sequence[float], vec_b: Sequence[float]) -> List[float]:
    """Sum of two vectors"""
    return [a + b for a, b in zip(vec_a, vec_b)]


def norm(vec_a: Sequence[float]) -> float:
    """Magnitude of a vector"""
    return sum(a ** 2 for a in vec_a) ** 0.5


def average_colours(colours: Sequence[Sequence]) -> Tuple[float, float, float]:
    """
    Take the average values of all colours in LAB space,
    which should be a bit more perceptual than sRGB

    Parameters
    ----------
    colours
        A list of rgb colours in range 0.0 -> 1.0

    Returns
    -------
    average
        A tuple with the average value
    """
    # Default to flat white
    if not colours:
        return 1.0, 1.0, 1.0

    ave_l, ave_a, ave_b = 0.0, 0.0, 0.0
    for colour in colours:
        # input colours are in 0.0 to 1.0 range so can be used as arguments
        # for RGB; d65 illuminant is daylight-ish.
        as_lab = convert_color(sRGBColor(*colour), LabColor, target_illuminant="d65")
        ave_l += as_lab.lab_l / len(colours)
        ave_a += as_lab.lab_a / len(colours)
        ave_b += as_lab.lab_b / len(colours)

    final_lab = LabColor(ave_l, ave_a, ave_b)
    final_srgb = convert_color(final_lab, sRGBColor)

    # Expects rgb in 0.0 to 1.0 range, which is what tuple gives
    return final_srgb.get_value_tuple()


def flat_colour(ctx: Context, _w: int, _h: int, colours: Sequence[tuple]) -> None:
    """
    Create a single flat colour canvas. Background colour is the
    average of the input colours in Lab space. All colours are
    averaged to those specified multiple times become more prominent
    in the output.
    """
    ctx.save()
    # Expects rgb in 0.0 to 1.0 range, which is what average gives
    ctx.set_source_rgb(*average_colours(colours))
    # Fill the whole image with the colour
    ctx.paint()
    ctx.restore()


def abstract(
    ctx: Context,
    width: int,
    height: int,
    colours: Sequence[tuple],
    base_colour: Sequence = (1.0, 1.0, 1.0),
    sides: int = 4,
) -> None:
    """
    Create an angular abstract full page pattern. Consists of
    overlapping blocks of colour with a hole in the middle.

    Parameters
    ----------
    ctx
        Current pyCairo canvas to draw on. Modified in place.
    width
        Width of the canvas
    height
        Height of the canvas
    colours
        A list of rgb colour tuples in 0.0 -> 1.0 range. The list is
        chopped into `sides` parts and the colour averaged for each
        side.
    base_colour
        Background colour to use as the base of the image
    sides
        Number of blocks to include in the full rotation

    """
    ctx.save()
    # Ensure random numbers don't change with the resolution but
    # change with different combinations of colours
    uniform = random.Random(str(colours) + str(base_colour)).uniform
    # Tilt everything
    offset_angle = uniform(0, 2 * math.pi)
    opening = uniform(0, 1 / 3 * min(width, height))
    middle = (width * uniform(1 / 6, 5 / 6), height * uniform(1 / 6, 5 / 6))

    # When specifying more colours than sides, the colours will
    # get averaged for each side
    sections = sub_sequence(colours, sides)

    # Ensure the far edges of the blocks don't appear on the canvas
    # by making the rectangles huge
    size = 2 * max(width, height)

    # background is filled
    ctx.set_source_rgb(*base_colour)
    ctx.paint()

    # Set up the initial transformation for the complete drawing;
    # Centre of the image is moved from the origin and slanted as
    # required.
    ctx.translate(middle[0], middle[1])
    ctx.rotate(offset_angle)

    for shade in sections:
        if shade:
            alpha = uniform(0.2, 0.9)
        else:
            alpha = 0.0
        # Draw each block in the same position then add a
        # partial rotation for the next one
        # Apply a random transparency to each block for nice overlaps
        ctx.set_source_rgba(*average_colours(shade), alpha)
        ctx.rectangle(opening, -size / 2, size, size)
        ctx.rotate(2 * math.pi / sides)
        ctx.fill()

    # Done
    ctx.restore()


def fluid(
    ctx: Context,
    width: int,
    height: int,
    colours: List[Tuple[float, float, float]],
    base_colour: Sequence = (1.0, 1.0, 1.0),
) -> None:
    """
    Create an fluid abstract full page pattern. Consists of
    overlapping blobs of colour.

    Parameters
    ----------
    ctx
        Current pyCairo canvas to draw on. Modified in place.
    width
        Width of the canvas
    height
        Height of the canvas
    colours
        A list of rgb colour tuples in 0.0 -> 1.0 range. A blob is
        generated for each colour.
    base_colour
        Background colour to use as the base of the image

    """
    ctx.save()

    # Ensure random numbers don't change with the resolution but
    # change with different combinations of colours
    rng = random.Random(str(colours) + str(base_colour))
    uniform = rng.uniform
    shuffle = rng.shuffle

    # background is filled
    ctx.set_source_rgb(*base_colour)
    ctx.paint()

    # Make things bigger if fewer of them
    sparse_factor = max(4 - len(colours), 0) / 8
    # Visit all the corners of the image to cover the canvas as much as
    q1 = [-0.1, 0.6 + sparse_factor]
    q2 = [0.4 - sparse_factor, 1.1]
    quadrants = [[q1, q1], [q1, q2], [q2, q1], [q2, q2]]
    shuffle(quadrants)

    # A blob for each colour
    for colour, quad in zip(colours, cycle(quadrants)):
        # Position it somewhere
        (cxmin, cxmax), (cymin, cymax) = quad
        blob_centre = (
            uniform(cxmin * width, cxmax * width),
            uniform(cymin * height, cymax * height),
        )
        blob_size = (
            width * uniform(0.6 + sparse_factor, 1.6 + 3 * sparse_factor),
            height * uniform(0.6 + sparse_factor, 1.6 + 3 * sparse_factor),
        )
        # TODO: gradients?
        pat = SolidPattern(*colour, uniform(0.2, 0.8))
        # Draw a random blob, being sure to pass the same rng generator
        draw_blob(
            ctx,
            pat,
            in_points=gen_blob_points(uniform=uniform),
            centre=blob_centre,
            size=blob_size,
        )

    # Done
    ctx.restore()


def patt_a(
    ctx: Context, colours: List[tuple], n: int, m: int, stepx: int = 40, **_kwargs: Any
) -> None:
    """
    Filled shapes, rectangles and triangles.

    Parameters
    ----------
    ctx
        Current pyCairo canvas to draw on. Modified in place.
    colours
        A list of rgb colour tuples in 0.0 -> 1.0 range. One is chosen
        at random for this shape.
    n, m
        Shape modifiers.
    stepx
        Size of the shape.
    """
    z = stepx / 2
    ctx.save()
    ctx.rectangle(-z, -z, z * 2, z * 2)
    ctx.clip()
    ctx.rotate(math.pi * (math.floor(random.random() * n * 2) + m) / n)
    for j in range(2):
        if random.random() < 0.5:
            ctx.set_source_rgb(0, 0, 0)
        else:
            ctx.set_source_rgb(*random.choice(colours))
        ctx.rotate(math.pi * math.floor(random.random() * (n - 1) + 1) / n)
        ctx.move_to(z * 2, z * 2)
        ctx.line_to(z * 2, -z * 2)
        ctx.line_to(-z * 2, -z * 2)
        ctx.close_path()
        ctx.fill()
        if random.random() < 0.5:
            break
    ctx.restore()


def patt_b(
    ctx: Context,
    colours: List[tuple],
    stepx: int = 40,
    straight: bool = True,
    line_width: int = 4,
    **_kwargs: Any
) -> None:
    """
    Lines lines everywhere.

    Parameters
    ----------
    ctx
        Current pyCairo canvas to draw on. Modified in place.
    colours
        A list of rgb colour tuples in 0.0 -> 1.0 range. One is chosen
        at random for this component.
    stepx
        Size of the component to draw.
    straight
        Whether to draw straight or curved lines.
    line_width
        Thickness of the lines
    """
    z = stepx / 2
    ctx.save()
    ctx.set_line_width(line_width)
    ctx.rectangle(-z * 2, -z, z * 4, z * 2)
    ctx.clip()
    ctx.set_source_rgb(0, 0, 0)
    if straight:
        ctx.move_to(0, -z)
        ctx.line_to(-z, 0)
        ctx.line_to(0, z)
        ctx.line_to(z, 0)
        ctx.close_path()
    else:
        ctx.move_to(0, -z)
        ctx.curve_to(0, -z / 2, -z, -z / 2, -z, 0)
        ctx.curve_to(-z, z / 2, 0, z / 2, 0, z)
        ctx.move_to(0, -z)
        ctx.curve_to(0, -z / 2, z, -z / 2, z, 0)
        ctx.curve_to(z, z / 2, 0, z / 2, 0, z)
    ctx.stroke()
    ctx.set_source_rgb(*random.choice(colours))
    ctx.move_to(0, -z)
    xx = math.atan(random.gauss(0, 1)) / math.pi * z * 2
    if straight:
        ctx.line_to(xx, 0)
        ctx.line_to(0, z)
    else:
        ctx.curve_to(0, -z / 2, xx, -z / 2, xx, 0)
        ctx.curve_to(xx, z / 2, 0, z / 2, 0, z)
    ctx.stroke()
    ctx.restore()


def rect_grid(
    ctx: Context,
    width: int,
    height: int,
    stepx: int = 40,
    stepy: Optional[int] = None,
    stagger: int = 1,
    shift: bool = True,
    **_kwargs: Any
) -> Generator:
    """
    Iterator over subdivisions of the canvas. Iterate over the result
    to move the cursor in a grid over the surface in successive calls.

    Parameters
    ----------
    ctx
        Current pyCairo canvas to draw on. Modified in place.
    width
        Width of the canvas
    height
        Height of the canvas
    stepx, stepy
        Size of partitions in each direction
    stagger
        Gap between sections
    shift
        Add a random shift to the move.
    """
    if stepy is None:
        stepy = stepx
    stagger = math.floor(stagger * stepy / 2 + 0.5)
    ctx.save()
    if shift:
        ctx.translate(int(-random.random() * stepx), int(-random.random() * stepy))
    for y in range(math.ceil(height / stepy + 1.5 + stagger)):
        ctx.save()
        for x in range(math.ceil(width / stepx + 1.5)):
            yield (x, y)
            ctx.translate(stepx, (x % 2 * 2 - 1) * stagger)
        ctx.restore()
        ctx.translate(0, stepy)
    ctx.restore()


def repeat(
    ctx: Context,
    width: int,
    height: int,
    colours: List[tuple],
    grid: Callable,
    draw: Callable,
    **kwargs
):
    """
    Apply a drawing function at each point of a grid generating function

    Parameters
    ----------
    ctx
        Current pyCairo canvas to draw on. Modified in place.
    width
        Width of the canvas
    height
        Height of the canvas
    colours
        A list of rgb colour tuples in 0.0 -> 1.0 range. These are
        passed to the draw function.
    grid
        A generator that returns moves through the canvas and
        returns a set of (x, y) coordinates at each point.
    draw
        A function that will be called at each point on the grid.
    """
    for x, y in grid(ctx, width, height, **kwargs):
        draw(ctx, colours, x=x, y=y, **kwargs)


class PatternImage:
    """Helper to generate surfaces with patterns."""

    # Preset patterns are referenced by their index in this list.
    patterns = [
        flat_colour,
        partial(repeat, grid=rect_grid, draw=patt_a, n=4, m=0),
        partial(repeat, grid=rect_grid, draw=patt_a, n=2, m=0),
        partial(repeat, grid=rect_grid, draw=patt_a, n=2, m=0.5),
        partial(repeat, grid=rect_grid, draw=patt_a, n=4, m=0, stagger=0),
        partial(repeat, grid=rect_grid, draw=patt_a, n=2, m=0, stagger=0),
        partial(repeat, grid=rect_grid, draw=patt_b, straight=True),
        partial(repeat, grid=rect_grid, draw=patt_b, straight=False),
        partial(abstract, base_colour=(1.0, 1.0, 1.0), sides=3),
        partial(abstract, base_colour=(0.0, 0.0, 0.0), sides=3),
        partial(abstract, base_colour=(1.0, 1.0, 1.0), sides=4),
        partial(abstract, base_colour=(0.0, 0.0, 0.0), sides=4),
        partial(fluid, base_colour=(1.0, 1.0, 1.0)),
        partial(fluid, base_colour=(0.0, 0.0, 0.0)),
    ]  # type: List[Callable]

    def __init__(self, width: int, height: int) -> None:
        """Surface is created when an instance is created."""
        self.width = width
        self.height = height
        self.surface = ImageSurface(FORMAT_RGB24, width, height)

    def draw_pattern(self, idx: int, colours: List[tuple]) -> ImageSurface:
        """
        Draw the selected pattern onto the current surface.

        Parameters
        ----------
        idx
            Numerical identifier for the pattern. Various styles are
            available.
        colours
            A list of rgb colour tuples in 0.0 -> 1.0 range. These are
            used by the drawing function to generate the pattern depending
            on the function called.
        """
        ctx = Context(self.surface)
        ctx.set_line_cap(LineCap.ROUND)
        ctx.set_line_join(LineJoin.ROUND)
        ctx.set_source_rgb(1, 1, 1)
        ctx.paint()
        self.patterns[idx](ctx, self.width, self.height, colours)
        return self.surface

    def save(self, filename: Union[str, BinaryIO], **_kwargs) -> None:
        """
        Write the surface as a png.

        Helper method to allow the object to be saved like a PIL Image.
        """
        self.surface.write_to_png(filename)
