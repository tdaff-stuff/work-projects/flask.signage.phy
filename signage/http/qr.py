import xml.etree.ElementTree as ETree
from typing import Optional

import qrcode


def make_qr_shield(
    text: str,
    size: int = 60,
    border=1,
    modifier: Optional[str] = None,
    transparent: bool = False,
) -> str:
    """
    Create a QR code contained in an IT Services Shield, in SVG format.
    Both the unmodified and dark variations put a white/transparent
    border around the shield. Night variation is purely black code on
    a white shield.

    Parameters
    ----------
    text
        The string to encode in the QR code.
    size
        Relative percentage of the shield to take up with the QR code.
    border
        Number of extra rows of border to add around the code.
    modifier
        If "dark" or "night" then use different colour scheme.
    transparent
        If set then the QR code is a cutout instead of a solid colour.

    Returns
    -------
    svg
        String containing the svg file.
    """

    # Library generates the QR code
    qr = qrcode.QRCode()
    qr.add_data(text)
    qr.make()

    # Different variations
    if modifier == "dark":
        fill = "#503872"
        bg_fill = "white"
        invert = False
    elif modifier == "night":
        fill = "#fff"
        bg_fill = "black"
        invert = True
    else:
        fill = "#422e5d"
        bg_fill = "white"
        invert = False

    # SVG is resolution independent, so just use a 100x100
    doc = ETree.Element(
        "svg",
        width="100",
        height="100",
        xmlns="http://www.w3.org/2000/svg",
        viewBox="0 0 100 100",
    )

    # QR code is "cut out" from the shield as a mask.
    # Put elements within a group.
    mask = ETree.SubElement(ETree.SubElement(doc, "mask", id="qr-mask"), "g")

    # Make mask layer fully transparent
    ETree.SubElement(
        mask, "rect", x="0", y="0", width="100", height="100", fill="white"
    )

    content = ETree.SubElement(mask, "g", fill="black")

    # These are fixed wrt the shield placement
    origin_x, origin_y = 50 - size / 2, 39.3 - size / 2
    # Various positioning
    modules_count = qr.modules_count
    pixel = size / (modules_count + border * 2)

    # Generate a single path as "rects" tend to have lines visible
    # between them, even with identical coordinates
    path = []

    # append a path fragment for each pixel that
    for idx in range(modules_count):
        for idy in range(modules_count):
            if qr.modules[idx][idy] == invert:
                path.append(
                    "M {x0:.3f} {y0:.3f} L {x0:.3f} {y1:.3f} "
                    "L {x1:.3f} {y1:.3f} L {x1:.3f} {y0:.3f} z".format(
                        x0=origin_x + (idx + border) * pixel,
                        y0=origin_y + (idy + border) * pixel,
                        x1=origin_x + (idx + border + 1) * pixel,
                        y1=origin_y + (idy + border + 1) * pixel,
                    )
                )

    # border is a single path drawn as a doughnut with a diagonal break
    if border > 0 and not invert:
        path.append(
            "M {x0:.3f} {y0:.3f} L {x0:.3f} {y1:.3f} L {x1:.3f} {y1:.3f} "
            "L {x1:.3f} {y0:.3f} L {x0:.3f} {y0:.3f} L {x2:.3f} {y2:.3f} "
            "L {x3:.3f} {y2:.3f} L {x3:.3f} {y3:.3f} L {x2:.3f} {y3:.3f} "
            "L {x2:.3f} {y2:.3f} z".format(
                x0=origin_x,
                y0=origin_y,
                x1=origin_x + size,
                y1=origin_y + size,
                x2=origin_x + border * pixel,
                y2=origin_y + border * pixel,
                x3=origin_x + size - border * pixel,
                y3=origin_y + size - border * pixel,
            )
        )

    ETree.SubElement(content, "path", d=" ".join(path))

    # Webkit was rendering the background offset.
    # Try and put everything in a single group, and use path instead
    # of polygon.
    qr_shield = ETree.SubElement(doc, "g")

    # Create a solid background shield, rather than just using
    # the transparent fallthrough
    if not transparent:
        ETree.SubElement(
            qr_shield,
            "rect",
            x=str(origin_x),
            y=str(origin_y),
            width=str(size),
            height=str(size),
            fill=bg_fill,
        )

    # IT Services Shield, with mask applied
    ETree.SubElement(
        qr_shield,
        "path",
        d="M 7,0 93,0 93,78.6 50,100 7,78.6 z",
        fill=fill,
        mask="url(#qr-mask)",
    )

    # Done. Convert to a string so we dont have to deal with ETree elsewhere
    return ETree.tostring(doc, encoding="unicode")
