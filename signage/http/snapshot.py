"""
Load a webpage and save a snapshot as an image file.

Uses QtWebEngine so will be similar to the WebKit client,
but if a website does something funky there might be issues
(e.g. bbc homepage doesn't work!)
"""

import os
import sys
from io import BytesIO
from os import rename
from os.path import dirname
from multiprocessing import Process, active_children
from tempfile import mkstemp
from time import sleep
from typing import Optional, Sequence, Iterable, List

from PySide2 import QtCore
from PySide2.QtCore import Qt, QUrl, QPoint, QBuffer, QTimer
from PySide2.QtGui import QImage, QPainter
from PySide2.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings
from PySide2.QtWidgets import QApplication, QWidget


def save_qimage(image: QImage, filename: str, fmt: str = "png"):
    """
    Write a Qt image to a file as an atomic operation to remove the
    chance of another process reading a half written file.

    Parameters
    ----------
    image
        The QImage after the content has been rendered.
    filename
        Name of file to save to.
    fmt
        Image format to use to save the file, e.g. "png" or "jpg".
    """

    # For some reason, QImage wouldn't work with an absolute path.
    # Managed to work around by passing through a few buffers, but
    # bit of a long winded way of doing it!
    os.makedirs(dirname(filename), exist_ok=True)
    # Create a temporary file that can be written to without
    # being accessed while writing.
    fd, tmp_path = mkstemp(dir=dirname(filename))

    with open(fd, "wb") as file_out:
        # Save the image to a Qt buffer
        buffer = QBuffer()
        buffer.open(QBuffer.ReadWrite)
        image.save(buffer, fmt)
        # Write the Qt buffer contents to a buffer in Python space
        # Writing buffer directly to file gave segfaults
        bytes_io = BytesIO()
        bytes_io.write(buffer.data())
        bytes_io.seek(0)
        # Finally write the buffer to the open file
        file_out.write(bytes_io.read())
        # Ensure it's on the disk
        os.fsync(fd)
        buffer.close()

    # Move into place: atomic as long as we're on the same filesystem
    rename(tmp_path, filename)


class WebRender(QWebEngineView):
    def __init__(
        self,
        url,
        filename: str = "out.png",
        allow_local: bool = False,
        parent: Optional[QWidget] = None,
    ):
        super(WebRender, self).__init__(parent)
        # "loadFinished" is the best signal available to know when the
        # page is loaded. No way to tell when rendering is truly "finished"
        # in all cases when javascript is involved.
        self.loadFinished.connect(self._load_finished)
        self.url = url
        self.filename = filename
        self.allow_local = allow_local
        # Add a pause between page load and
        self.wait = 0.0
        self.zoom = 1.0
        # Interaction with elements
        self.auto_click = []  # type: Iterable[str]
        self.auto_hide = []  # type: Iterable[str]
        # No window as we're on a server
        self.setAttribute(Qt.WA_DontShowOnScreen, True)
        self.setAttribute(Qt.WA_DeleteOnClose, True)
        self.show()
        # Settings give some view of the page
        config = QWebEngineSettings.globalSettings()
        config.setAttribute(QWebEngineSettings.PluginsEnabled, True)
        config.setAttribute(QWebEngineSettings.ScreenCaptureEnabled, True)
        config.setAttribute(QWebEngineSettings.JavascriptCanOpenWindows, False)
        config.setAttribute(QWebEngineSettings.ShowScrollBars, False)

    def take_snapshot(
        self,
        width: int = 1920,
        height: int = 1080,
        zoom: float = 1.0,
        wait: float = 0,
        auto_click: Iterable[str] = (),
        auto_hide: Iterable[str] = (),
    ) -> None:
        """
        Load the attached URL and take a snapshot of the screen. Upon
        successfully loading the page, the `save()` method will be called
        automatically.

        Parameters
        ----------
        width, height
            Size of the viewing window to display.
        zoom
            Zoom the view of the page
        wait
            Delay in seconds to wait after the page reports that it has
            finished loading to take the snapshot
        auto_hide, auto_click
            Elements that will be clicked or hidden in the rendered page
        """
        # wait needs to be used in loadFinished so store as an attribute
        self.wait = wait
        # Zoom gets reset (bug in Chromium?) so must be set again once
        # finished, hence attach as an attribute
        self.zoom = zoom
        self.setZoomFactor(zoom)
        # Retrieved after load
        self.auto_click = auto_click
        self.auto_hide = auto_hide
        # Window same size as we need for snapshot
        self.resize(width, height)
        # Parse the URL, userInput allows fuzzy matching
        url = QUrl.fromUserInput(self.url)
        # Don't allow access to local files
        if url.isLocalFile() and not self.allow_local:
            raise ValueError
        # Take the snapshot
        self.load(url)

    @QtCore.Slot(bool)
    def _load_finished(self, finished: bool) -> bool:
        """
        Page has "finished" loading. Call postprocessing functions
        and save the page as an image.

        Parameters
        ----------
        finished
            Return status of the load. Will be true if the web page loaded

        Returns
        -------
        success
            Whether the image was saved successfully.
        """
        # Unable to load the page
        if not finished:
            print("Error rendering: {}".format(self.url))
            self.close()
            return False

        # Must set zoom here as it gets reset before this
        self.setZoomFactor(self.zoom)

        # Use QTimer so that we're waiting in the rendering thread
        # and calling methods after waiting in the thread

        # Clear cookie warnings and other popups etc.
        # Run once after the specified time
        QTimer.singleShot(int(self.wait * 1000), self.dismiss_modals)

        # Save slightly after the dismiss action
        QTimer.singleShot(int(self.wait * 1200), self.save)

        # Success
        return True

    def dismiss_modals(self):
        """
        Run javascript in the page to "click" buttons that are cookie
        and GDPR type warnings. Element id and class names are hard
        coded for now.

        This runs best with a slight delay after the page has loaded.
        """

        # Elements should be listed as selectors then we can bung them all
        # together in a query and click or hide as required.
        to_click = ", ".join(self.auto_click)
        to_hide = ", ".join(self.auto_hide)

        # Run JavaScript in the page
        self.page().runJavaScript(
            "document.querySelectorAll('{}')"
            ".forEach(elem => elem.click())".format(to_click)
        )
        self.page().runJavaScript(
            "document.querySelectorAll('{}')"
            ".forEach(elem => elem.style.display = 'none')".format(to_hide)
        )

    def save(self):
        """Render page to an image."""
        # render to a QImage
        size = self.contentsRect()
        img = QImage(size.width(), size.height(), QImage.Format_ARGB32)
        painter = QPainter(img)
        self.render(painter, QPoint(0, 0))
        painter.end()

        # Atomic save that does some buffer stuff
        save_qimage(img, self.filename, fmt="png")

        # Close allows `exec_` to return and shut down the thread.
        self.close()


def _make_snapshot(
    url: str,
    filename: str,
    width: int = 1920,
    height: int = 1080,
    zoom: float = 1.0,
    wait: float = 0,
    auto_click: Iterable[str] = (),
    auto_hide: Iterable[str] = (),
):
    """
    Generate a snapshot of the url and save it as filename.

    This should only be run as a single command as QApplication can't be
    called more than once and needs to be within the same thread that
    everything happens.
    """
    # This will result in errors if called more than once
    try:
        qt_app = QApplication(sys.argv)
        web_render = WebRender(url, filename)
        web_render.take_snapshot(
            width=width,
            height=height,
            zoom=zoom,
            wait=wait,
            auto_hide=auto_hide,
            auto_click=auto_click,
        )
        qt_app.exec_()
    except ValueError:
        pass
    finally:
        # Pointless since you can't make another app in the same interpreter :(
        del qt_app


def snapshot(
    url: str,
    filename: str,
    width: int = 1920,
    height: int = 1080,
    zoom: float = 1.0,
    wait: float = 0,
    auto_click: Iterable[str] = (),
    auto_hide: Iterable[str] = (),
):
    """
    Generate a snapshot of the url and save it as filename.

    Snapshot is carried out in a separate Process where QApplication can
    have the whole process to itself!
    """

    # Even in separate processes they kick up some errors if running
    # concurrently; wait until we're alone to start
    while any(child.name == "snapshot" for child in active_children()):
        sleep(0.01)

    bg_snapshot = Process(
        target=_make_snapshot,
        args=(url, filename, width, height, zoom, wait, auto_click, auto_hide),
        name="snapshot",
    )
    bg_snapshot.start()
    # 12 second timeout so it's not killed by Apache but will error out
    # instead
    bg_snapshot.join(12)

    # Hard kill any processes that are having issues. Dont' need them anyway
    if bg_snapshot.is_alive():
        bg_snapshot.terminate()
        if bg_snapshot.pid:
            os.kill(bg_snapshot.pid, 9)
        return False

    # All done
    return True
