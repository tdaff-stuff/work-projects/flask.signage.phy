"""
SASS preprocessor for css rules.

Compile the global css style and create a namespace in the templates
to process any scss rules set on it.

"""
from collections import OrderedDict
from typing import Any

import jinja2.utils
import sass
from flask import Blueprint, make_response, render_template, g, Response

styles_blueprint = Blueprint("styles", __name__)

# Predefined colour set used in CSS and creation of patterns
colours = {
    "red": "#e62b28ff",
    "orange": "#e7b200ff",
    "yellow": "#e4cd0eff",
    "green": "#30ab5dff",
    "blue": "#28b1e7ff",
    "indigo": "#1572b9ff",
    "violet": "#6b3b8fff",
    "white": "#ffffffff",
    "k": "#000000ff",
}


class ScssCompiler:

    # Pre-construct the colour definitions for scss anywhere
    colour_list = "$colours: {};".format(",".join(colours.values()))
    colours = "\n".join("${}: {};".format(key, value) for key, value in colours.items())

    @classmethod
    def compile(cls, string: str) -> str:
        """Compile an SCSS string into CSS, injecting colour definitions."""
        return sass.compile(string=cls.colour_list + cls.colours + string)


@styles_blueprint.route("/style.css")
def site_css() -> Response:
    """
    Compiled site css.

    Tell clients to cache for a week. Kiosks will clear their cache every
    reboot anyway.
    """
    css = ScssCompiler.compile(render_template("signage.scss"))
    response = make_response(css)
    response.headers["Content-Type"] = "text/css"
    response.cache_control.max_age = 60 * 60 * 24 * 7
    return response


class SCSSNamespace(jinja2.utils.Namespace):
    """
    Template component that compiles css snippets when they
    are set as attributes.
    """

    # Set on the class so there is a single instance and it
    # can be accessed through the custom accessor methods.
    compiler = ScssCompiler()

    @classmethod
    def __setitem__(cls, name: str, value: str) -> None:
        if "css" not in g:
            g.css = {}
        g.css[name] = cls.compiler.compile(value)


@styles_blueprint.app_context_processor
def template_scss():
    """Attach the SCSS compiling namespace to all templates."""
    return {"scss": SCSSNamespace()}
