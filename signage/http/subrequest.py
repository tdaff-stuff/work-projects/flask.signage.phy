from urllib import parse

from flask import Blueprint, request, current_app, url_for, Markup


subrequest_blueprint = Blueprint("subrequest", __name__)


def do_subrequest(target, **kwargs):
    new_url = parse.urlparse(url_for(target, **kwargs))
    new_environ = request.environ.copy()
    new_environ.update({"PATH_INFO": new_url.path, "QUERY_STRING": new_url.query})
    with current_app.request_context(new_environ):
        rv = current_app.preprocess_request()
        if rv is None:
            return Markup(current_app.dispatch_request())
        else:
            return rv


@subrequest_blueprint.app_context_processor
def add_subrequest():
    return dict(subrequest=do_subrequest)
