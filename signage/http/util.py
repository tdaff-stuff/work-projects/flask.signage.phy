"""
General http utilities.
"""

from datetime import time, datetime, timedelta
from enum import Enum
from math import ceil
from typing import Optional, Iterator, Sequence, Any, Dict, Callable

from flask import Blueprint, g, request, session, Response
from flask.json import JSONEncoder

from deckchair.slides import Slide


class ModJSONEncoder(JSONEncoder):
    """
    Extended signage JSON encoder.

    Additional types that can be encoded are:
    * `time` as HH:MM
    * `enum` as the item `name`
    * `timedelta` as `total_seconds`
    """

    def default(self, o: Any) -> Any:
        if isinstance(o, time):
            return o.strftime("%H:%M")
        elif isinstance(o, Enum):
            return o.name
        elif isinstance(o, timedelta):
            return o.total_seconds()
        else:
            return JSONEncoder.default(self, o)


class DisplayBlueprint(Blueprint):
    """
    Specialised Blueprint with added processing for display routes.

    Injects the current or required time into g and offers a way to
    add the "full", "half", and "banner" routes.
    """

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(
            *args, static_folder="static", template_folder="templates", **kwargs
        )

        @self.url_value_preprocessor
        def preprocess_global_time(endpoint: str, values: Dict[str, Any]) -> None:
            """Ensure the timestamp is available globally."""
            # `now` value is included in the route definitions
            # `time` argument is hard-coded into kiosk.phy client
            timestamp = values.pop("now", None) or request.args.get("time", type=int)
            if timestamp:
                g.now = datetime.fromtimestamp(timestamp)
            else:
                g.now = datetime.now()

        @self.url_defaults
        def add_time_to_defaults(endpoint: str, values: Dict[str, Any]) -> None:
            """Add the timestamp to routes by default."""
            if "now" in values or "now" not in g:
                return
            values["now"] = "{:.0f}".format(g.now.timestamp())

        @self.after_request
        def disable_html_cache(response: Response) -> Response:
            """Tell clients not to cache html responses."""
            if response.mimetype == "text/html":
                response.headers.add("Cache-Control", "no-store, must-revalidate")
            return response

    def route_display(self, endpoint: str, rule: str = "", **options: Any) -> Callable:
        """
        Modified route decorator.

        Adds an optional `now` component to the URL. Ensures that the
        endpoint is one of the prescribed display layouts.
        """
        if endpoint not in ["full", "half", "banner"]:
            raise ValueError("Unrecognised display route, {}".format(endpoint))

        def decorator(view_func: Callable) -> Callable:
            path = "/{}{}".format(endpoint, rule)
            path_t = "/<int:now>/{}{}".format(endpoint, rule)
            self.add_url_rule(path, endpoint, view_func, **options)
            self.add_url_rule(path_t, endpoint, view_func, **options)
            return view_func

        return decorator


##
#  Slide ordering and presentation
##
def use_standard_transition(display_time: float = 15) -> None:
    """Ensure that a transition is registered globally."""
    # When displaying slides from the past, don't transition immediately
    now = max(g.now.timestamp(), datetime.now().timestamp())
    # Show for at least one second
    show_until = ceil(now + max(display_time, 1))
    g.transitions = [{"transition": "fade 0.5", "show_until": show_until}]


class RotateSlides(Iterator):
    """Iterate through slides in the list using cookies or args."""

    def __init__(self, slides: Optional[Sequence[Slide]] = None):
        if slides is None:
            slides = []
        self.slides = list(slides)

    def __next__(self) -> Optional[dict]:
        """
        Get the next slide in the sequence.

        Next slide is determined from the session cookie or request
        args. None is returned if there are no slides to choose from.
        """
        # Slide order is fully managed in the deckchair database.
        # Empty list means no slides at this time.
        if not self.slides:
            return None

        # The slide that we are currently on, if defined by the
        # request or in a cookie. Otherwise start from the beginning
        if "slide_id" in request.args:
            initial_session = int(request.args["slide_id"])
        else:
            initial_session = session.get("rotate_slide", -1)

        # Next slide, with loop back to 0
        idx = (initial_session + 1) % len(self.slides)
        # Set in the session cookie for clients to chain
        session["rotate_slide"] = idx
        # Make any active slide into the view compatible version.
        return self.slides[idx].to_view()
