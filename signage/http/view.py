"""
Main slide view.


Index presents a slide in a rotating list. Returns the next slide
on each refresh.

View slide is a direct path to how a single slide would appear in
the normal rotation.
"""
from typing import Union

from flask import request, render_template, make_response, g, current_app, abort
from jinja2 import UndefinedError
from werkzeug import Response
from werkzeug.http import http_date
from werkzeug.routing import BuildError

from deckchair.slides import SlideManager, Slide
from signage.display.common import CLOCK_PANEL
from signage.http.util import DisplayBlueprint, RotateSlides
from signage.http.util import use_standard_transition

view_blueprint = DisplayBlueprint("view", __name__)

# Placeholder is clock and fallback slide showing server name IP address
FALLBACK_SLIDE = {
    "components": [CLOCK_PANEL, ("section", [("display.fallback.full", {})]),]
}


@view_blueprint.route("/time/<int:now>/<string:client>")
@view_blueprint.route("/time/<int:now>")
@view_blueprint.route("/client/<string:client>/<int:now>")
@view_blueprint.route("/client/<string:client>")
@view_blueprint.route("/")
def index(client=None):
    if client is None:
        client = request.remote_addr
    # Required for all templates
    g.css = {}

    current_slides = SlideManager(current_app.slide_db).slide_rotation(client, g.now)
    slide = next(RotateSlides(current_slides)) or FALLBACK_SLIDE

    # Set up transition between slides in global context so
    # slide renderer can access it.
    use_standard_transition(slide.get("display_time", 15))

    return render_template("display.html", components=slide["components"])


@view_blueprint.route("/view/<string:oid>")
def view_slide(oid: str) -> Union[str, Response]:
    """Direct view of slide contents from the oid identifier"""
    # empty transition required for each slide
    g.transitions = [{}]

    try:
        # Grab the slide contents from the database
        slide = SlideManager(current_app.slide_db).slide_or_group_by_oid(oid)

        # Only works for a Slide, so throw a wobbly for SlideGroups
        if not isinstance(slide, Slide):
            raise ValueError

        # slide content
        components = slide.to_view()["components"]
        # Gets added as a header
        meta = {"updated": http_date(slide.updated)}

        return render_template("display.html", components=components, meta=meta)

    except KeyError:
        # Slide does not exist
        return abort(404)

    except (AttributeError, ValueError):
        if current_app.debug:
            raise
        # Request invalid
        return abort(400)


@view_blueprint.route("/view/<string:route>/<string:component>/<string:size>")
def view_component(component: str, route: str = "display", size: str = "full"):
    """Direct route to display a specific slide."""

    try:
        # Only allow access to "display" type routes here
        if route not in ["display"]:
            raise KeyError("{} is not a valid view target".format(route))

        # Test to see if constructed endpoint exists in the app and accepts the
        # arguments
        endpoint = "{}.{}.{}".format(route, component, size)
        # Ensure we only get single values for dict keys; needed for Py3.5
        args = request.args.to_dict(flat=True)

        if not current_app.url_map.is_endpoint_expecting(endpoint, *args):
            raise KeyError("Invalid args {} for {}".format(args, endpoint))

        # Construction of the slide, the same as in the rotating view
        # with added clock and banners
        g.css = {}  # required
        g.transitions = [{}]  # required

        components = [("section", [(endpoint, args)])]

        return render_template("display.html", components=components)

    # In production, bad urls should just be 404, not 500
    except (KeyError, BuildError, UndefinedError):

        # Exceptions happen when we try to get to non existent endpoints
        # with bad arguments. Only catch these in production and give a
        # 404 for them (note that catching errors in debug mode gives
        # AssertionError unless PRESERVE_CONTEXT_ON_EXCEPTION is off)
        if current_app.debug:
            raise
        else:
            return abort(404)
